<?php

namespace App\Controllers;

use Myth\Auth\Models\UserModel;
use App\Models\Grupmodel;
use App\Models\Gruppermit;
use Arifrh\DynaModel\DB;

class M_coa_2 extends BaseController
{
    protected $grupmodel;
    protected $groppermi;
    private $menulink = 'm_menu';
    public function __construct()
    {
        $this->grupmodel = new Grupmodel();
        $this->gruppermit = new Gruppermit();
    }
    public function index()
    {

        $data['coa'] = $this->grupmodel->coa_2();

        // echo $db->getLastQuery();
        // dd($data['pengajuan']);

        $data['title'] = 'Master COA 2';
        $data['page'] = 'Master COA 2';
        $data['user'] = 'Daftar COA 2';

        return view('coa_2/coa_2', $data);
    }
    public function adddata()
    {
        $data['title'] = 'Tambah COA 2';
        $data['page'] = 'Tambah COA 2';
        $data['coa'] = $this->grupmodel->coa_1();



        return view('coa_2/adddata', $data);
    }

    public function action_add()
    {

        $judul = $this->request->getVar('nama_coa');
        $parent = $this->request->getVar('parent_id');
        $kode = $this->request->getVar('kode_coa_2');

        $data = [
            'nama_coa' => $judul,
            'm_coa_id' => $parent,
            'kode_coa_2' => $kode

        ];
        $simpan = $this->grupmodel->insert_coa_2($data);

        if ($simpan) {
            $msg = [
                'error' => 'sukses',
                'msg' => 'Input data berhasil '
            ];
        } else {
            $msg = [
                'error' => 'terjadi Error',
                'msg' => 'Silahkan coba lagi '
            ];
        }


        echo json_encode($msg);
    }
    public function edit($id)
    {
        $data['title'] = 'Ubah COA';
        $data['page'] = 'Ubah Data';



        $data['coa'] = $this->grupmodel->coa2($id);
        $data['coa1'] = $this->grupmodel->coa_1();

        $data['id'] = $id;

        return view('coa_2/edit', $data);
    }
    public function action_edit()
    {
        if ($this->request->isAJAX()) {


            $judul = $this->request->getVar('nama_coa');

            $id = $this->request->getVar('id');
            $parent = $this->request->getVar('parent_id');
            $kode = $this->request->getVar('kode_coa_2');

            $data = array(
                'nama_coa'  => $judul,  'm_coa_id' => $parent, 'kode_coa_2' => $kode

            );
            $update = $this->grupmodel->updatecoa2($data, $id);


            if ($update) {
                $msg = [
                    'error' => 'sukses',
                    'msg' => $this->request->getVar('nama_coa') . ' Dirubah'
                ];
            }


            echo json_encode($msg);
        }
    }
    public function delete()
    {


        $delete = $this->grupmodel->delete_coa_2($this->request->getVar('id'));

        if ($delete) {
            $msg = [
                'error' => 'sukses',
                'msg' => 'User '
            ];
        } else {
            $msg = [
                'error' => 'error',
                'msg' => 'Data Tidak Bisa Dihapus'
            ];
        }

        echo json_encode($msg);
    }
    public function status()
    {
        $id = $this->request->getVar('id');
        $posts = DB::table('m_coa_2');
        $this->request->getVar('val');
        if ($this->request->getVar('val') == 'inactive') {
            $status = 'inactive';
        } elseif ($this->request->getVar('val') == 'active') {
            $status = 'active';
        }
        $delete = $posts->updateBy(
            ['status' => "$status"],
            ['m_coa_2_id'   => $id],
        );

        if ($delete) {
            $msg = [
                'error' => 'sukses',
                'msg' => 'User '
            ];
        } else {
            $msg = [
                'error' => 'error',
                'msg' => 'Data Tidak Bisa Dihapus'
            ];
        }

        echo json_encode($msg);
    }
}

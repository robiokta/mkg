<?php

namespace App\Controllers;

use Myth\Auth\Models\UserModel;
use App\Models\Grupmodel;
use App\Models\Gruppermit;
use Arifrh\DynaModel\DB;

class M_coa_1 extends BaseController
{
    protected $grupmodel;
    protected $groppermi;
    private $menulink = 'm_menu';
    public function __construct()
    {
        $this->grupmodel = new Grupmodel();
        $this->gruppermit = new Gruppermit();
    }
    public function index()
    {
        $data['coa'] = $this->grupmodel->coa_1();

        // echo $db->getLastQuery();
        // dd($data['pengajuan']);
        $data['title'] = 'Master COA 1 ';
        $data['page'] = 'Master COA 1';
        $data['user'] = 'Daftar COA 1';

        return view('coa_1/coa_1', $data);
    }
    public function adddata()
    {
        $data['title'] = 'Pengajuan';
        $data['page'] = 'Tambah Pengajuan';

        return view('coa_1/adddata', $data);
    }
    public function action_add()
    {

        $judul = $this->request->getVar('nama_coa');
        $kode = $this->request->getVar('kode_coa');
    
        $data = [
            'nama_coa' => $judul,
            'kode_coa' => $kode,
        
        ];
        $simpan = $this->grupmodel->insert_coa_1($data);

        if ($simpan) {
            $msg = [
                'error' => 'sukses',
                'msg' => 'Input data berhasil '
            ];
        } else {
            $msg = [
                'error' => 'terjadi Error',
                'msg' => 'Silahkan coba lagi '
            ];
        }
        echo json_encode($msg);
    }
    public function edit($id)
    {  
        $data['title'] = 'Ubah COA';
        $data['page'] = 'Ubah Data';

        $data['coa'] = $this->grupmodel->coa($id);

        $data['id'] = $id;

        return view('coa_1/edit', $data);
    }
    public function action_edit()
    {
        if ($this->request->isAJAX()) {


            $judul = $this->request->getVar('nama_coa');
            $id = $this->request->getVar('id');
            $kode = $this->request->getVar('kode_coa');

            $data = array(
                'nama_coa'  => $judul,
                'kode_coa' => $kode,

            );
            $update = $this->grupmodel->updatecoa($data, $id);
            if ($update) {
                $msg = [
                    'error' => 'sukses',
                    'msg' => $this->request->getVar('nama_coa') . ' Dirubah'
                ];
            }

            echo json_encode($msg);
        }
    }
    public function delete()
    {
        $delete = $this->grupmodel->delete_coa($this->request->getVar('id'));

        if ($delete) {
            $msg = [
                'error' => 'sukses',
                'msg' => 'User '
            ];
        } else {
            $msg = [
                'error' => 'error',
                'msg' => 'Data Tidak Bisa Dihapus'
            ];
        }

        echo json_encode($msg);
    }

    public function status()
    {
        $id = $this->request->getVar('id');
        $posts = DB::table('m_coa');
        $this->request->getVar('val');
        if ($this->request->getVar('val') == 'inactive') {
            $status = 'inactive';
        } elseif ($this->request->getVar('val') == 'active') {
            $status = 'active';
        }
        $delete = $posts->updateBy(
            ['status' => "$status"],
            ['m_coa_id'   => $id],
        );

        if ($delete) {
            $msg = [
                'error' => 'sukses',
                'msg' => 'User '
            ];
        } else {
            $msg = [
                'error' => 'error',
                'msg' => 'Data Tidak Bisa Dihapus'
            ];
        }

        echo json_encode($msg);
    }
}

<?php

namespace App\Controllers;

use Myth\Auth\Models\UserModel;
use App\Models\Grupmodel;
use App\Models\Gruppermit;
use App\Models\Gruppermitl;
use Arifrh\DynaModel\DB;

class M_coa_4 extends BaseController
{
    protected $grupmodel;
    protected $groppermi;
    private $menulink = 'm_menu';
    public function __construct()
    {
        $this->grupmodel = new Grupmodel();
        $this->gruppermit = new Gruppermit();
    }

    public function index()
    {
        // then you can use it, to get all posts
        $data['coa'] = $this->grupmodel->query('select *,m_coa_4.nama_coa as namacoa, m_coa_4.status as statuss from m_coa_4 join m_coa_2 on m_coa_2.m_coa_2_id=m_coa_4.m_coa_2_id join m_coa on m_coa.m_coa_id=m_coa_4.m_coa_1_id join m_coa_3 on m_coa_3.m_coa_3_id=m_coa_4.m_coa_3_id order by kode_coa_3 asc' );
        // echo $db->getLastQuery();
        // dd($data['coa']);

        $data['title'] = 'Master COA 4';
        $data['page'] = 'Master COA 4';
        $data['user'] = 'Daftar COA 4';

        return view('coa_4/coa_4', $data);
    }

    public function adddata()
    {
        $data['title'] = 'Tambah COA 4';
        $data['page'] = 'Tambah COA 4';
        $data['coa'] = $this->grupmodel->coa_1();

        return view('coa_4/adddata', $data);
    }
    public function pilihcoa()
    {
        $coa1 = $this->request->getVar('coa_1');
        $coa2 = $this->request->getVar('coa_2');
        $coa3 = $this->request->getVar('coa_3');

        if (
            $coa1 != '' and $coa2 == ''

        ) {

            $data     = $this->grupmodel->coa22($coa1);
            foreach ($data as $key => $val) :
                $a = $val->m_coa_2_id;
                $b = $val->nama_coa;
                $c =$val->kode_coa_2;
                $arrisi[] = "<option value='$a' >$c - $b </option>";


            endforeach;
            @$isi = join(" ", $arrisi);
            @$response_array['coa_2_hasil'] = "<option value=''>COA 2</option>
										" . $isi;
            @$response_array['kd'] = 2;
        } elseif ($coa1 != '' and $coa2 != '' and $coa3 == '') {


            $coa4 = DB::table('m_coa_3');
            $data = $coa4->findBy(['m_coa_2_id' => $coa2, 'id_coa_1' => $coa1]);
            // print_r($data);
            // die();

            foreach ($data as $val) :
                $a = $val['m_coa_3_id'];
                $b = $val['nama_coa'];
                $c =$val ['kode_coa_3'];
                $arrisi[] = "<option value='$a' >$c - $b</option>";


            endforeach;
            @$isi = join(" ", $arrisi);
            @$response_array['coa_3_hasil'] = "<option value=''>COA 2</option>
										" . $isi;
            @$response_array['kd'] = 3;
        }

        echo json_encode($response_array);
    }

    public function action_add()
    {

        $nama_coa = $this->request->getVar('nama_coa');
        $coa1 = $this->request->getVar('id_coa_1');
        $coa2 = $this->request->getVar('id_coa_2');
        $coa3 = $this->request->getVar('id_coa_3');
        $kode = $this->request->getVar('kode_coa_4');

        $authors = DB::table('m_coa_4');

        $data = [
            'm_coa_1_id'   => $coa1,
            'm_coa_2_id'   => $coa2,
            'm_coa_3_id'   => $coa3,
            'kode_coa_4'  => $kode,
            'nama_coa' => $nama_coa,
        ];

        $simpan = $authors->insert($data);


        if ($simpan) {
            $msg = [
                'error' => 'sukses',
                'msg' => 'Input data berhasil '
            ];
        } else {
            $msg = [
                'error' => 'terjadi Error',
                'msg' => 'Silahkan coba lagi '
            ];
        }


        echo json_encode($msg);
    }
    public function edit($id)
    {
        $data['title'] = 'Ubah COA';
        $data['page'] = 'Ubah Data';

        $authors = DB::table('m_coa_4');

        $coa = $authors->find($id);
        //  dd($coa);

        $data['coa'] = $coa;

        $data['coa1'] = $this->grupmodel->coa_1();
        $postModel = \Arifrh\DynaModel\DB::table('m_coa_2');

        $data['coa2'] = $postModel->findAll();
        $coa3 = DB::table('m_coa_3');
        $data['coa3'] = $coa3->findAll();

        $data['id'] = $id;

        return view('coa_4/edit', $data);
    }
    public function action_edit()
    {
        // if ($this->request->isAJAX()) {


        $nama_coa = $this->request->getVar('nama_coa');
        $coa1 = $this->request->getVar('id_coa_1');
        $coa2 = $this->request->getVar('id_coa_2');
        $coa3= $this->request->getVar('id_coa_3');
        $kode = $this->request->getVar('kode_coa_4');
        $id = $this->request->getVar('id');

        $posts = DB::table('m_coa_4');

        $update = $posts->updateBy(
            ['nama_coa'   => $nama_coa, 'm_coa_1_id' => $coa1, 'm_coa_2_id' => $coa2,'m_coa_3_id' => $coa3, 'kode_coa_4' => $kode],
            ['m_coa_4_id'   => $id],
        );
        if ($update) {
            $msg = [
                'error' => 'sukses',
                'msg' => $this->request->getVar('nama_coa') . ' Dirubah'
            ];
        }


        echo json_encode($msg);
        // }
    }
    public function delete()
    {


        // $delete = $this->grupmodel->delete_coa_2();


        $posts = DB::table('m_coa_4');
        $delete = $posts->deleteBy([

            'm_coa_4_id'     => $this->request->getVar('id')
        ]);

        if ($delete) {
            $msg = [
                'error' => 'sukses',
                'msg' => 'User '
            ];
        } else {
            $msg = [
                'error' => 'error',
                'msg' => 'Data Tidak Bisa Dihapus'
            ];
        }

        echo json_encode($msg);
    }
    public function status()
    {
        $id = $this->request->getVar('id');
        $posts = DB::table('m_coa_4');
        $this->request->getVar('val');
        if ($this->request->getVar('val') == 'inactive') {
            $status = 'inactive';
        } elseif ($this->request->getVar('val') == 'active') {
            $status = 'active';
        }
        $delete = $posts->updateBy(
            ['status' => "$status"],
            ['m_coa_4_id'   => $id],
        );

        if ($delete) {
            $msg = [
                'error' => 'sukses',
                'msg' => 'User '
            ];
        } else {
            $msg = [
                'error' => 'error',
                'msg' => 'Data Tidak Bisa Dihapus'
            ];
        }

        echo json_encode($msg);
    }
}

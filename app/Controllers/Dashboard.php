<?php

namespace App\Controllers;

use App\Models\Grupmodel;

class Dashboard extends BaseController
{
    protected $grupmodel;
    public function __construct()
    {
        $this->grupmodel = new Grupmodel();
        helper('form');
    }
    public function index()
    {
        $data['title'] = 'Dashboard';
        if (in_groups('admin')) {
            $where = '';
            $builder = "";
        } elseif (in_groups('manager')) {
            $id_divisi=user()->id_divisi;
            $builder = " and id_divisi =$id_divisi";

            $where = " and status='PO01'";
            # code...
        } elseif (in_groups('menku')) {
            $where = " and status ='PO02'";
            $builder = "";
        } else {
            $where = '';
            $builder = "";
        }
        $hasil = $this->grupmodel->query("SELECT COUNT(*) as total from t_pengajuan where status != 'PO04' and status !='PO03' and status_hapus='ada' $where " . $builder);
        $data['jumlah'] = $hasil[0]->total;

        return view('dashboard/dashboard', $data);
    }
    public function data()
    {
        if ($this->request->isAJAX()) {
            # code...

            if (in_groups('admin')) {
                $where = '';
                $builder = "";
            } elseif (in_groups('manager')) {
                $db1      = \Config\Database::connect();
                $builder1 = $db1->table('users');
                $builder1->select('users.id as userid, username, email, description');
                $builder1->join('auth_groups_users', 'auth_groups_users.user_id=users.id');
                $builder1->join('auth_groups', 'auth_groups.id=auth_groups_users.group_id');
                $builder1->where('users.id', user_id());
                $query1 = $builder1->get();
                $user1 = $query1->getResult();

                foreach ($user1 as $key => $value1) {
                    $user_desc = $value1->description;
                }
                if ($user_desc == 'Manager Trans UMJ') {
                    $builder = "and user_manager= 'UMJT'";
                } elseif ($user_desc == 'Manager Engineering') {
                    $builder = "and user_manager= 'UMJE'";
                } elseif ($user_desc == 'Manager Offset') {
                    $builder = "and user_manager= 'UMJOP'";
                } elseif ($user_desc == 'Manager FGA') {
                    $builder = "and user_manager= 'GFA'";
                } elseif ($user_desc == 'Manager P3LM') {
                    $builder = "and user_manager= 'P3LM'";
                }

                $where = " and status='PO01'";
                # code...
            } elseif (in_groups('menku')) {
                $where = " and status ='PO02'";
                $builder = "";
            }
            $data['pengajuan'] = $this->grupmodel->query("select * from t_pengajuan where status != 'PO04' and status !='PO03' and status_hapus='ada' $where" . $builder);
            echo json_encode($data);
        }
    }

    //--- end of code 

}

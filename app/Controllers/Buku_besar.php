<?php

namespace App\Controllers;

use App\Models\Result_model;
use Irsyadulibad\DataTables\DataTables;
use App\Models\Grupmodel;
use Arifrh\DynaModel\DB;


class Buku_besar extends BaseController
{
	protected $model;
	protected $groppermi;
	protected $grupmodel;

	public function __construct()
	{
		$this->model = new Result_model();
		$this->grupmodel = new Grupmodel();
	}

	public function index()
	{
		$postModel = \Arifrh\DynaModel\DB::table('mak_4');
		$data['coa'] = $postModel->findAll();

		$data['title'] = 'Buku Besar';
		$data['page'] = 'Buku Besar';
		$data['user'] = 'Buku Besar';
		$this->grupmodel->query("CALL `resetmvdetailbukubesar`(@p0);");
		$authors = DB::table('m_divisi');
        $data['dev']= $authors->findAll();


		return view('buku_besar/buku_besar', $data);
	}

	function get_data1()
	{
		$level = $this->request->getVar('level');
		// $tahun = $this->request->getVar('tahun');
		$awal = date_format(date_create($this->request->getVar('tgl_awal')), "Y-m-d");
		$akhir = date_format(date_create($this->request->getVar('tgl_akhir')), "Y-m-d");
		$user = $this->request->getVar('user');
		$user == '' ? '' : $user = " and IdUnit= $user";

		// die();
		if ($level == 4) {

			$data2 = array();
			$query1 = $this->grupmodel->query("select * from coa ");
			// array_push($data, $query1[0]->namacoa);
			foreach ($query1 as $key => $value) {
				unset($data); // $foo is gone
				$data = array();
				$k = 1;
				$value->namacoa;

				$data['kode_coa'] = $value->kode_coa_1 . $value->kode_coa_2 . $value->kode_coa_3 . $value->kode_coa_4;
				$data['nama_coa'] =  $value->namacoa;


				// for ($i = 1; $i <= 12; $i++) {
				// $k = sprintf("%02d", $i);

				// $query = $this->grupmodel->query("Select  ifnull(sum(Nilai_debit),0) as Nilai_debit, ifnull(sum(Nilai_kredit),0) as Nilai_kredit, ifnull(sum(saldo),0) as saldo from mv_bukubesar where Tanggal>='$awal' and Tanggal <='$akhir' and kode_coa_1=$value->kode_coa_1 and kode_coa_2=$value->kode_coa_2 and kode_coa_3=$value->kode_coa_3 and kode_coa_4=$value->kode_coa_4 ");

				$query = $this->grupmodel->query("SELECT ifnull(sum(Nilai),0) as Nilai_debit FROM mv_detailbukubesar where kode_coa_1=$value->kode_coa_1 and kode_coa_2=$value->kode_coa_2 and kode_coa_3=$value->kode_coa_3 and kode_coa_4=$value->kode_coa_4 and Tanggal>='$awal' and Tanggal<='$akhir' $user and Type='Debit' ");

				$query2 = $this->grupmodel->query("SELECT ifnull(sum(Nilai),0) as Nilai_kredit FROM mv_detailbukubesar where kode_coa_1=$value->kode_coa_1 and kode_coa_2=$value->kode_coa_2 and kode_coa_3=$value->kode_coa_3 and kode_coa_4=$value->kode_coa_4 and Tanggal>='$awal' and Tanggal<='$akhir' $user and Type='kredit' ");

				$data['debit'] = $query[0]->Nilai_debit;
				$data['kredit'] =  $query2[0]->Nilai_kredit;
				$data['saldo'] =  $query[0]->Nilai_debit - $query2[0]->Nilai_kredit;;

				// }

				$data2[] = $data;
			}
			$datass['data'] = $data2;
			// $datass['draw']=3;
			echo json_encode($datass);
		} elseif ($level == 3) {

			$data2 = array();
			$query1 = $this->grupmodel->query("select * from coa3  ");
			// array_push($data, $query1[0]->namacoa);
			foreach ($query1 as $key => $value) {
				unset($data); // $foo is gone
				$data = array();
				$k = 1;
				$value->namacoa;

				$data['kode_coa'] = $value->kode_coa_1 . $value->kode_coa_2 . $value->kode_coa_3;
				$data['nama_coa'] =  $value->namacoa;


				$query = $this->grupmodel->query("SELECT ifnull(sum(Nilai),0) as Nilai_debit FROM mv_detailbukubesar where kode_coa_1=$value->kode_coa_1 and kode_coa_2=$value->kode_coa_2 and kode_coa_3=$value->kode_coa_3  and Tanggal>='$awal' and Tanggal<='$akhir' $user and Type='Debit' ");

				$query2 = $this->grupmodel->query("SELECT ifnull(sum(Nilai),0) as Nilai_kredit FROM mv_detailbukubesar where kode_coa_1=$value->kode_coa_1 and kode_coa_2=$value->kode_coa_2 and kode_coa_3=$value->kode_coa_3 and Tanggal>='$awal' and Tanggal<='$akhir' $user and Type='kredit' ");


				$data['debit'] = $query[0]->Nilai_debit;
				$data['kredit'] =  $query2[0]->Nilai_kredit;
				$data['saldo'] =  $query[0]->Nilai_debit - $query2[0]->Nilai_kredit;
				// }

				$data2[] = $data;
			}
			$datass['data'] = $data2;
			// $datass['draw']=3;
			echo json_encode($datass);
		} elseif ($level == 2) {

			$data2 = array();
			$query1 = $this->grupmodel->query("select * from coa2 ");
			// array_push($data, $query1[0]->namacoa);
			foreach ($query1 as $key => $value) {
				unset($data); // $foo is gone
				$data = array();
				$k = 1;
				$value->namacoa;

				$data['kode_coa'] = $value->kode_coa_1 . $value->kode_coa_2;
				$data['nama_coa'] =  $value->namacoa;


				// for ($i = 1; $i <= 12; $i++) {
				//     $k = sprintf("%02d", $i);


				$query = $this->grupmodel->query("SELECT ifnull(sum(Nilai),0) as Nilai_debit FROM mv_detailbukubesar where kode_coa_1=$value->kode_coa_1 and kode_coa_2=$value->kode_coa_2   and Tanggal>='$awal' and Tanggal<='$akhir' $user and Type='Debit' ");

				$query2 = $this->grupmodel->query("SELECT ifnull(sum(Nilai),0) as Nilai_kredit FROM mv_detailbukubesar where kode_coa_1=$value->kode_coa_1 and kode_coa_2=$value->kode_coa_2 and Tanggal>='$awal' and Tanggal<='$akhir' $user and Type='kredit' ");


				$data['debit'] = $query[0]->Nilai_debit;
				$data['kredit'] =  $query2[0]->Nilai_kredit;
				$data['saldo'] =  $query[0]->Nilai_debit - $query2[0]->Nilai_kredit;

				$data2[] = $data;
			}
			$datass['data'] = $data2;
			// $datass['draw']=3;
			echo json_encode($datass);
		} elseif ($level == 6) {

			$data2 = array();
			$query1 = $this->grupmodel->query("select * from m_coa ");
			// array_push($data, $query1[0]->namacoa);
			foreach ($query1 as $key => $value) {
				unset($data); // $foo is gone
				$data = array();
				$k = 1;
				// $value->namacoa; 

				$data['kode_coa'] = $value->kode_coa;
				$data['nama_coa'] =  $value->nama_coa;


				$query = $this->grupmodel->query("SELECT ifnull(sum(Nilai),0) as Nilai_debit FROM mv_detailbukubesar where kode_coa_1=$value->kode_coa  and Tanggal>='$awal' and Tanggal<='$akhir' $user and Type='Debit' ");

				$query2 = $this->grupmodel->query("SELECT ifnull(sum(Nilai),0) as Nilai_kredit FROM mv_detailbukubesar where kode_coa_1=$value->kode_coa and Tanggal>='$awal' and Tanggal<='$akhir' $user and Type='kredit' ");


				$data['debit'] = $query[0]->Nilai_debit;
				$data['kredit'] =  $query2[0]->Nilai_kredit;
				$data['saldo'] =  $query[0]->Nilai_debit - $query2[0]->Nilai_kredit;

				$data2[] = $data;
			}
			$datass['data'] = $data2;
			// $datass['draw']=3;
			echo json_encode($datass);
		} else {
			$datass['data'] = '';
			echo json_encode($datass);
		}
	}
	public function get_detail()
	{
		$level = $this->request->getVar('level');
		$user = $this->request->getVar('user');
		$user == '' ? '' : $user = " and description='" . $user . "'";
		$kodecoa = str_split($this->request->getVar('kode_coa'));
		// print_r($kodecoa);
		// $tahun = $this->request->getVar('tahun');
		$awal = date_format(date_create($this->request->getVar('tgl_awal')), "Y-m-d");
		$akhir = date_format(date_create($this->request->getVar('tgl_akhir')), "Y-m-d");
		$stop_date = date('Y-m-d', strtotime($awal . ' -1 day'));

		if ($level == 4) {
			$kodecoa1 = $kodecoa[0];
			$kodecoa2 = $kodecoa[1];
			$kodecoa3 = $kodecoa[2];
			$kodecoa4 = $kodecoa[3];
			$data['datass'] = $this->grupmodel->query("SELECT ifnull((select Nilai as nilai from mv_detailbukubesar where Tanggal<='$stop_date' and Type='Debit' and  kode_coa_1=$kodecoa1 and kode_coa_2=$kodecoa2 and kode_coa_3=$kodecoa3 and kode_coa_4=$kodecoa4 $user  ),0) as debit, ifnull((select Nilai as nilai from mv_detailbukubesar where Tanggal<='$stop_date' and Type='kredit' and  kode_coa_1=$kodecoa1 and kode_coa_2=$kodecoa2 and kode_coa_3=$kodecoa3 and kode_coa_4=$kodecoa4 $user ),0) as kredit ");


			$data['datas'] = $this->grupmodel->query("SELECT * FROM mv_detailbukubesar where kode_coa_1=$kodecoa1 and kode_coa_2=$kodecoa2 and kode_coa_3=$kodecoa3 and kode_coa_4=$kodecoa4 and Tanggal>='$awal' and Tanggal<='$akhir' $user");

			echo json_encode($data);
			# code...
		} else if ($level == 3) {
			$kodecoa1 = $kodecoa[0];
			$kodecoa2 = $kodecoa[1];
			$kodecoa3 = $kodecoa[2];

			$data['datass'] = $this->grupmodel->query("SELECT ifnull((select sum(Nilai) as nilai from mv_detailbukubesar where Tanggal<='$stop_date' and Type='Debit' and  kode_coa_1=$kodecoa1 and kode_coa_2=$kodecoa2 and kode_coa_3=$kodecoa3  $user ),0) as debit, ifnull((select sum(Nilai) as nilai from mv_detailbukubesar where Tanggal<='$stop_date' and Type='kredit' and  kode_coa_1=$kodecoa1 and kode_coa_2=$kodecoa2 and kode_coa_3=$kodecoa3  $user ),0) as kredit ");

			$data['datas'] = $this->grupmodel->query("SELECT * FROM mv_detailbukubesar where kode_coa_1=$kodecoa1 and kode_coa_2=$kodecoa2 and kode_coa_3=$kodecoa3  and Tanggal>='$awal' and Tanggal<='$akhir' $user");

			echo json_encode($data);
			# code...
		} else if ($level == 2) {
			$kodecoa1 = $kodecoa[0];
			$kodecoa2 = $kodecoa[1];

			$data['datass'] = $this->grupmodel->query("SELECT ifnull((select sum(Nilai) as nilai from mv_detailbukubesar where Tanggal<='$stop_date' and Type='Debit' and  kode_coa_1=$kodecoa1 and kode_coa_2=$kodecoa2  $user ),0) as debit, ifnull((select sum(Nilai) as nilai from mv_detailbukubesar where Tanggal<='$stop_date' and Type='kredit' and  kode_coa_1=$kodecoa1 and kode_coa_2=$kodecoa2 $user ),0) as kredit ");

			$data['datas'] = $this->grupmodel->query("SELECT * FROM mv_detailbukubesar where kode_coa_1=$kodecoa1 and kode_coa_2=$kodecoa2 and Tanggal>='$awal' and Tanggal<='$akhir' $user");

			echo json_encode($data);
			# code...
		} else if ($level == 6) {
			$kodecoa1 = $kodecoa[0];

			$data['datass'] = $this->grupmodel->query("SELECT ifnull((select sum(Nilai) as nilai from mv_detailbukubesar where Tanggal<='$stop_date' and Type='Debit' and  kode_coa_1=$kodecoa1  $user ),0) as debit, ifnull((select sum(Nilai) as nilai from mv_detailbukubesar where Tanggal<='$stop_date' and Type='kredit' and  kode_coa_1=$kodecoa1 $user ),0) as kredit ");

			$data['datas'] = $this->grupmodel->query("SELECT * FROM mv_detailbukubesar where kode_coa_1=$kodecoa1  and Tanggal>='$awal' and Tanggal<='$akhir' $user");

			echo json_encode($data);
			# code...
		}
	}
}

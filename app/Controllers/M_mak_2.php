<?php

namespace App\Controllers;

use Myth\Auth\Models\UserModel;
use App\Models\Grupmodel;
use App\Models\Gruppermit;
use App\Models\Gruppermitl;
use Arifrh\DynaModel\DB;

class M_mak_2 extends BaseController
{
    protected $grupmodel;
    protected $groppermi;
    private $menulink = 'm_menu';
    public function __construct()
    {
        $this->grupmodel = new Grupmodel();
        $this->gruppermit = new Gruppermit();
    }
    public function index()
    {
        $postModel = DB::table('mak_2');

        $data['coa'] = $this->grupmodel->query('select * from mak_2 join mak_1 on mak_2.mak_1_id=mak_1.mak_1_id');

        // echo $db->getLastQuery();
        // dd($data['pengajuan']);

        $data['title'] = 'Master MAK 2';
        $data['page'] = 'Master MAK 2';
        $data['user'] = 'Daftar MAK 2';

        return view('mak_2/mak_2', $data);
    }
    public function adddata()
    {
        $postModel = DB::table('mak_1');

        $data['coa'] = $postModel->findAll();
        $data['title'] = 'Tambah MAK 2';
        $data['page'] = 'Tambah MAK 2';

        return view('mak_2/adddata', $data);
    }

    public function action_add()
    {

        $judul = $this->request->getVar('nama_mak');
        $parent = $this->request->getVar('parent_id');
        $kode = $this->request->getVar('kode_mak_2');


        $authors = DB::table('mak_2');

        $data = [
            'nama_mak_2'   => $judul,
            'mak_1_id'   => $parent,
            'kode_mak_2'  => $kode,

        ];

        $simpan = $authors->insert($data);



        if ($simpan) {
            $msg = [
                'error' => 'sukses',
                'msg' => 'Input data berhasil '
            ];
        } else {
            $msg = [
                'error' => 'terjadi Error',
                'msg' => 'Silahkan coba lagi '
            ];
        }


        echo json_encode($msg);
    }
    public function edit($id)
    {
        $data['title'] = 'Ubah MAK 2';
        $data['page'] = 'Ubah Data';

        $authors = DB::table('mak_2');

        $coa = $authors->find($id);

        $data['coa'] = $coa;
        $data['coa1'] = $this->grupmodel->mak_1();

        $data['id'] = $id;

        return view('mak_2/edit', $data);
    }
    public function action_edit()
    {
        if ($this->request->isAJAX()) {


            $judul = $this->request->getVar('nama_mak');

            $id = $this->request->getVar('id');
            $parent = $this->request->getVar('parent_id');
            $kode = $this->request->getVar('kode_mak_2');

            $posts = DB::table('mak_2');

            $update = $posts->updateBy(
                ['nama_mak_2'  => $judul,  'mak_1_id' => $parent, 'kode_mak_2' => $kode],
                ['mak_2_id'   => $id],
            );

            if ($update) {
                $msg = [
                    'error' => 'sukses',
                    'msg' => $this->request->getVar('nama_coa') . ' Dirubah'
                ];
            }


            echo json_encode($msg);
        }
    }
    public function delete()
    {
        $this->request->getVar('id');
        $posts = DB::table('mak_2');
        $delete = $posts->deleteBy([
            'mak_2_id'     => $this->request->getVar('id')
        ]);

        if ($delete) {
            $msg = [
                'error' => 'sukses',
                'msg' => 'User '
            ];
        } else {
            $msg = [
                'error' => 'error',
                'msg' => 'Data Tidak Bisa Dihapus'
            ];
        }

        echo json_encode($msg);
    }
}

<?php

namespace App\Controllers;

use App\Models\Grupmodel;

class User extends BaseController
{
	protected $grupmodel;
	public function __construct()
	{
		$this->grupmodel = new Grupmodel();
		helper('form');
	}
	public function index()
	{
		$data['title'] = 'My Profile ';
		$data['page'] = 'My Profile';
		$data['user'] = 'My Profile';

		return view('profile/profile', $data);
	}
	public function edituser()
	{
		$data['title'] = 'My Profile ';
		$data['page'] = 'My Profile';
		$data['user'] = 'My Profile';
		return view('profile/edit', $data);
	}
	public function actionedit()
	{
		// if ($this->request->isAJAX()) {


		$credentials['password'] = $this->request->getVar('password');
		$id = $this->request->getVar('id');

		$password = $credentials['password'];
		if ($password !== '') {

			// Now hash password
			$config = config('Auth');
			if (
				(defined('PASSWORD_ARGON2I') && $config->hashAlgorithm == PASSWORD_ARGON2I)
				||
				(defined('PASSWORD_ARGON2ID') && $config->hashAlgorithm == PASSWORD_ARGON2ID)
			) {
				$hashOptions = [
					'memory_cost' => $config->hashMemoryCost,
					'time_cost'   => $config->hashTimeCost,
					'threads'     => $config->hashThreads
				];
			} else {
				$hashOptions = [
					'cost' => $config->hashCost
				];
			}
			$password_hash = password_hash(
				base64_encode(
					hash('sha384', $password, true)
				),
				$config->hashAlgorithm,
				$hashOptions
			);

			// pack the data
			$password_hash;

			$data = array(
				'password_hash'  => $password_hash,
				'fullname' => $this->request->getVar('nama'),
			);
			$update = $this->grupmodel->updateuser($data, $id);


			if ($update) {
				$msg = [
					'error' => 'sukses',
					'msg' => 'User ' . $this->request->getVar('nama') . ' Berhasil Diupdate'
				];
			}
		} else {
			$data = array(

				'fullname' => $this->request->getVar('nama'),
			);
			$update = $this->grupmodel->updateuser($data, $id);


			if ($update) {
				$msg = [
					'error' => 'sukses',
					'msg' => 'User ' . $this->request->getVar('nama') . ' Berhasil Diupdate'
				];
			}
		}


		echo json_encode($msg);
		// }
	}
	public static function deleteDir($dirPath) {
		if (! is_dir($dirPath)) {
			echo "salah";
		}
		if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
			$dirPath .= '/';
		}
		$files = glob($dirPath . '*', GLOB_MARK);
		foreach ($files as $file) {
			if (is_dir($file)) {
				self::deleteDir($file);
			} else {
				unlink($file);
			}
		}
		rmdir($dirPath);
	}
	public function upload()
	{

		$dir_r = ROOTPATH . 'public/img/profile/' . user()->id;
		$dir = $dir_r . '/';
		if (file_exists($dir) && is_dir($dir)) {
		} else {
			mkdir($dir_r, 0777);
			//echo 'aaaaaa';
		}
		$this->deleteDir($dir);
		if (file_exists($dir) && is_dir($dir)) {
		} else {
			mkdir($dir_r, 0777);
			//echo 'aaaaaa';
		}
echo ROOTPATH . $dir;
		

		$validated = $this->validate([
			'file_upload' => 'uploaded[file_upload]|mime_in[file_upload,image/jpg,image/jpeg,image/gif,image/png]|max_size[file_upload,4096]'
		]);
		$avatar = $this->request->getFile('file_upload');

		if ($validated == FALSE) {
			echo 'aaaa';
			// Kembali ke function index supaya membawa data uploads dan validasi
			// return $this->index();

		} else {

			$avatar = $this->request->getFile('file_upload');
			$avatar->move($dir);

			$data = [
				'user_img' => $avatar->getName()
			];

			$this->grupmodel->updateuser($data, user()->id);
			// return redirect()->to(base_url('upload'))->with('success', 'Upload successfully'); 
		}
	}

	//--------------------------------------------------------------------

}

<?php

namespace App\Controllers;

use Myth\Auth\Models\UserModel;
use App\Models\Grupmodel;
use App\Models\Gruppermit;
use Arifrh\DynaModel\DB;

class Laba_rugi extends BaseController
{
    protected $grupmodel;
    protected $groppermi;
    private $menulink = 'm_menu';
    public function __construct()
    {
        $this->grupmodel = new Grupmodel();
        $this->gruppermit = new Gruppermit();
    }
    public function index()
    {
        $data['coa'] = $this->grupmodel->query("select * from m_coa ");

        // echo $db->getLastQuery();
        // dd($data['pengajuan']);
        $data['title'] = 'Laba Rugi ';
        $data['page'] = 'Laba Rugi';
        $data['user'] = 'Laba Rugi';
       

        return view('laba_rugi/laba_rugi', $data);
    }
    public function get_rinci() 
    {
        $tahun = $this->request->getVar('tahun');
         $bulan = $this->request->getVar("bulan");
     
        if (!empty($tahun) & empty($bulan)) {
            $tahunn = "YEAR(t_jurnalumum.Tanggal)='$tahun'";
        } elseif (!empty($tahun) & !empty($bulan)) {
            $bulan = date('m', strtotime($this->request->getVar("bulan")));
             $tahunn = "YEAR(t_jurnalumum.Tanggal)='$tahun' and MONTH(t_jurnalumum.Tanggal)='$bulan'"; 
        }

        $data['pendapatan'] = $this->grupmodel->query("select *,  Sum(Nilai) AS nilai_akhir FROM t_jurnalumum JOIN coa on coa.m_coa_4_id=t_jurnalumum.IdKredit where coa.kode_coa_1=4 and $tahunn GROUP  BY coa.kode_coa_1, coa.kode_coa_2,coa.kode_coa_3,coa.kode_coa_4");
        $data['biaya'] = $this->grupmodel->query("select *,  Sum(Nilai) AS nilai_akhir FROM t_jurnalumum JOIN coa on coa.m_coa_4_id=t_jurnalumum.IdDebet where coa.kode_coa_1=6 and $tahunn GROUP  BY coa.kode_coa_1, coa.kode_coa_2,coa.kode_coa_3,coa.kode_coa_4");

        echo json_encode($data);
    }
}

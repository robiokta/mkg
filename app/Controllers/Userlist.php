<?php

namespace App\Controllers;

use Myth\Auth\Models\UserModel;
use App\Models\Grupmodel;
use App\Models\Gruppermit;
use App\Models\Gruppermitl;
use Arifrh\DynaModel\DB;
class Userlist extends BaseController
{
    protected $grupmodel;
    protected $groppermi;
    private $menulink = 'm_menu';
    public function __construct()
    {
        $this->grupmodel = new Grupmodel();
        $this->gruppermit = new Gruppermit();
    }
    public function index()
    {
        $db      = \Config\Database::connect();
        $builder = $db->table('users');
        $builder->select('users.id as userid, username, email, name, id_divisi');
        $builder->join('auth_groups_users', 'auth_groups_users.user_id=users.id');
        $builder->join('auth_groups', 'auth_groups.id=auth_groups_users.group_id');
        $query = $builder->get();

        $data['users'] = $query->getResult();

        // echo $db->getLastQuery();
        // dd($data['users']);

        $data['title'] = 'User List';
        $data['page'] = 'Admin';
        $data['user'] = 'Daftar Pengguna';

        return view('admin/userlist', $data);
    }
    public function adduser()
    {
        $data['title'] = 'User List';
        $data['page'] = 'Data User';

        $grup = $this->grupmodel->findAll();
        $data['grup'] = $grup;
        $authors = DB::table('m_divisi');
        $data['divisi']= $authors->findAll();

        return view('admin/adduser', $data);
    }
    public function user()
    {
        if ($this->request->isAJAX()) {

            $credentials['password'] = $this->request->getVar('password');
            $credentials['username'] = $this->request->getVar('username');
            $email = $this->request->getVar('email');
            $fullname = $this->request->getVar('fullname');
            //    die();
            // Only allowed 1 additional credential other than password
            $password = $credentials['password'];
            unset($credentials['password']);

            if (count($credentials) > 1) {
                return "Error - 01";
            }



            $user = model("UserModel");
            $all_emails = $user->findColumn("email");
            $all_usernames = $user->findColumn("username");

            if ((in_array($email, $all_emails)) || (in_array($credentials['username'], $all_usernames))) {
                $msg = [
                    'error' => 'error', 'msg' => 'User atau Email Sudah ada'
                ];
                echo json_encode($msg);
                die();
            }

            // Now hash password
            $config = config('Auth');
            if (
                (defined('PASSWORD_ARGON2I') && $config->hashAlgorithm == PASSWORD_ARGON2I)
                ||
                (defined('PASSWORD_ARGON2ID') && $config->hashAlgorithm == PASSWORD_ARGON2ID)
            ) {
                $hashOptions = [
                    'memory_cost' => $config->hashMemoryCost,
                    'time_cost'   => $config->hashTimeCost,
                    'threads'     => $config->hashThreads
                ];
            } else {
                $hashOptions = [
                    'cost' => $config->hashCost
                ];
            }
            $password_hash = password_hash(
                base64_encode(
                    hash('sha384', $password, true)
                ),
                $config->hashAlgorithm,
                $hashOptions
            );

            // pack the data
            $data = [
                'email'             => $email,
                'username'          => $credentials['username'],
                'password_hash'     => $password_hash,
                'fullname' => $fullname,
                'active'            => 1,
                "force_pass_reset"  => 0,
                'id_divisi'=>$this->request->getVar('divisi'),
            ];
            // save attempt
            $result = $user->insert($data, true);
            $user_id = $user->getInsertID();
            $resultid = $this->gruppermit->save([
                'group_id' => $this->request->getVar('gropus'),
                'user_id' => $user_id,
            ]);

            if ($result == false & $resultid == false | $result == false | $resultid == false) {
                // return $user->errors();
                $msg = [
                    'error' => ['msg' => $user->erros()]
                ];
            } else {
                $msg = [
                    'error' => 'sukses',
                    'msg' => 'User ' . $this->request->getVar('username') . ' Berhasil Ditambahkan'
                ];
            }
            echo json_encode($msg);
        } else {
            echo "anda tidak berhak mengakses halaman ini";
        }
    }
    public function edituser($id)
    {
        $data['title'] = 'User List';
        $data['page'] = 'Edit User';

        $grup = $this->grupmodel->findAll();

        $data['user'] = $this->grupmodel->getval($id);
        $data['grup'] = $grup;
        $authors = DB::table('m_divisi');
        $data['divisi']= $authors->findAll();

        return view('admin/edituser', $data);
    }
    public function action_edit()
    {
        if ($this->request->isAJAX()) {
            $credentials['password'] = $this->request->getVar('password');
            $id = $this->request->getVar('id');

            $password = $credentials['password'];

            // Now hash password
            $config = config('Auth');
            if (
                (defined('PASSWORD_ARGON2I') && $config->hashAlgorithm == PASSWORD_ARGON2I)
                ||
                (defined('PASSWORD_ARGON2ID') && $config->hashAlgorithm == PASSWORD_ARGON2ID)
            ) {
                $hashOptions = [
                    'memory_cost' => $config->hashMemoryCost,
                    'time_cost'   => $config->hashTimeCost,
                    'threads'     => $config->hashThreads
                ];
            } else {
                $hashOptions = [
                    'cost' => $config->hashCost
                ];
            }
            $password_hash = password_hash(
                base64_encode(
                    hash('sha384', $password, true)
                ),
                $config->hashAlgorithm,
                $hashOptions
            );

            // pack the data
            $password_hash;

            $data = array(
                'password_hash'  => $password_hash,
                'fullname' => $this->request->getVar('fullname'),
                'id_divisi'=>$this->request->getVar('divisi')
            );
            $update = $this->grupmodel->updateuser($data, $id);

            $grup = array(
                'group_id'  => $this->request->getVar('gropus'),
            );
            $updategroup = $this->grupmodel->updategroup($grup, $id);
            if ($update & $updategroup) {
                $msg = [
                    'error' => 'sukses',
                    'msg' => 'User ' . $this->request->getVar('username') . ' Berhasil Ditambahkan'
                ];
            }


            echo json_encode($msg);
        }
    }
    public function delete_user()
    {


        $delete = $this->grupmodel->deleteuser($this->request->getVar('id'));
        if ($delete) {
            $msg = [
                'error' => 'sukses',
                'msg' => 'User '
            ];
        } else {
            $msg = [
                'error' => 'error',
                'msg' => 'Data Tidak Bisa DIhapus'
            ];
        }

        echo json_encode($msg);
    }
}

<?php

namespace App\Controllers;

use Myth\Auth\Models\UserModel;
use App\Models\Grupmodel;
use App\Models\Gruppermit;
use App\Models\Gruppermitl;
use Arifrh\DynaModel\DB;

class Kas_keluar extends BaseController
{
    protected $grupmodel;
    protected $groppermi;
    private $menulink = 'm_menu';
    public function __construct()
    {
        $this->grupmodel = new Grupmodel();
        $this->gruppermit = new Gruppermit();
    }
    public function index()
    {
        $query = $this->grupmodel->query('select  IdJurnal, nama_divisi, Keterangan, Nilai,NoTransaksi,Tanggal, coa.namacoa as namacoa4,coa.kode_coa_1 as coa1,coa.kode_coa_2 as coa2, coa.kode_coa_3 as coa3, coa.kode_coa_4 as coa4 , a.namacoa as namacoa, a.kode_coa_1 as kd_coa_1,a.kode_coa_2 as kd_coa_2, a.kode_coa_3 as kd_coa_3 , a.kode_coa_4 as kd_coa_4 from t_jurnalumum join coa a on a.m_coa_4_id=t_jurnalumum.IdKredit join (select * from coa) as coa on coa.m_coa_4_id=t_jurnalumum.IdDebet join m_divisi on m_divisi.id_divisi=t_jurnalumum.IdUnit where (coa.kode_coa_1=5 or coa.kode_coa_2= 6 or(coa.kode_coa_1=7 and coa.kode_coa_1=2) or (coa.kode_coa_1=1 and coa.kode_coa_2=1 and coa.kode_coa_3=5) ) and (a.kode_coa_1=1 and a.kode_coa_2=1)');

        $data['datas'] = $query;

        $data['title'] = 'Kas Keluar ';
        $data['page'] = 'Kas Keluar';
        $data['user'] = 'Daftar Kas Keluar';

        return view('kas_keluar/kas_keluar', $data);
    }
    public function adddata()
    {
        $data['title'] = 'Kas Keluar';
        $data['page'] = 'Tambah Kas Keluar';

        $grup = $this->grupmodel->findAll();
        $userid = user_id();
        $data['grup'] = $grup;
        $string = "Select * from coa where kode_coa_1=1 and kode_coa_2= 1 ";
        // $string = "select * from mak_4";
        $data['coa'] = $this->grupmodel->query($string);

        $unit = 'SELECT * FROM `auth_groups` where name="unit"';

        $authors = DB::table('m_divisi');
        $data['unit']= $authors->findAll();

        // if (in_groups('admin')){
        $string1 = 'Select * from coa where kode_coa_1=5 or kode_coa_2= 6 or(kode_coa_1=7 and kode_coa_1=2) or (kode_coa_1=1 and kode_coa_2=1 and kode_coa_3=5) ';
        $data['coa1'] = $this->grupmodel->query($string1);

        $string2 = 'Select * from coa where kode_coa_1=1 and kode_coa_2=1 and kode_coa_3=1 or (kode_coa_1=1 and kode_coa_2=1 and kode_coa_3=2)';
        $data['coa2'] = $this->grupmodel->query($string2);

        return view('kas_keluar/adddata', $data);
    }
    public function action_add()
    {
        $unit = $this->request->getVar('unit'); 

        $tgl = $this->request->getVar('tgl');
        $idkas = $this->request->getVar('kas');
        $judul = $this->request->getVar('judul');
        $debet = $this->request->getVar('debet');
        $jmlh = $this->request->getVar('jumlah');
        $harga = $this->request->getVar('harga');
        $kredit = $this->request->getVar('kredit');


        $jumlah = count($debet);

        for ($i = 0; $i < $jumlah; $i++) {
            $author = DB::table('t_jurnalumum');

            $nilai = str_replace(".", "", $harga[$i]);
            $nilai = str_replace(",", ".", $nilai);
            //    echo $jumlah[$i];
            $nilai = $nilai * $jmlh[$i];
            $data = [
                'IdUnit'   => $unit,
                'Tanggal'   => date('Y-m-d', strtotime($tgl)),
                'NoTransaksi'  => $this->request->getVar('kuitansi'),
                'IdDebet'  => $debet,
                'IdKredit'  => $kredit[$i],
                'Nilai'  => $nilai,
                // 'Rincian' => $rincian,
                'IdUser' => user_id(),
                'jumlah' => $jmlh[$i],
                // 'idkascoa' => $idkas,

                // 'type_kas' => 'kas_masuk',
                // 'IdSup'=>$this->request->getVar('sup')
            ];
            $simpan = $author->insert($data);

            // $simpan = $authors->insert($data1);
        }

        if ($simpan) {
            $this->grupmodel->query("CALL `resetmvneraca_x`(@p0)");
            $this->grupmodel->query("CALL `resetmvneraca`(@p0)");
            $this->grupmodel->query("CALL `resetmvneraca_y`(@p0)");
            $msg = [
                'error' => 'sukses',
                'msg' => 'Input data berhasil '
            ];
        } else {
            $msg = [
                'error' => 'terjadi Error',
                'msg' => 'Silahkan coba lagi '
            ];
        }


        echo json_encode($msg);
    }
    public function nomer($unit)
    {
        $authors = DB::table('t_jurnalumum');
        $authors->findAll();

        $jumlah = $authors->countAllResults() + 1;
        $posts = DB::table('auth_groups');
        $publishPosts = $posts->findBy(['id' => $unit]);
        $publishPosts[0]['description'];

        return $notrans = date('d/m/Y') . "/" . $publishPosts[0]['description'] . '/' . $jumlah;
    }
    public function edit($id)
    {

        $data['title'] = 'Ubah Kas';
        $data['page'] = 'Ubah Data';

        $string = "Select * from coa where kode_coa_1=1 and kode_coa_2= 1 ";
        // $string = "select * from mak_4";
        $data['coa'] = $this->grupmodel->query($string);

        $unit = 'SELECT * FROM `auth_groups` where name="unit"';

        $authors = DB::table('m_divisi');
        $data['unit']= $authors->findAll();

        // if (in_groups('admin')){
        $string1 = 'Select * from coa where kode_coa_1=5 or kode_coa_2= 6 or(kode_coa_1=7 and kode_coa_1=2) or (kode_coa_1=1 and kode_coa_2=1 and kode_coa_3=5)';
        $data['coa1'] = $this->grupmodel->query($string1);

        $string2 = 'Select * from coa where kode_coa_1=1 and kode_coa_2=1 and kode_coa_3=1 or (kode_coa_1=1 and kode_coa_2=1 and kode_coa_3=2)';
        $data['coa2'] = $this->grupmodel->query($string2);
        $posts = DB::table('t_jurnalumum');
        $data['kas'] = $posts->find($id);

        $data['id'] = $id;


        return view('kas_keluar/editkas', $data);
    }
    public function action_edit()
    {
        // if ($this->request->isAJAX()) {

        $unit = $this->request->getVar('unit');

        $tgl = $this->request->getVar('tgl');
        $idkas = $this->request->getVar('kas');
        $judul = $this->request->getVar('judul');
        $debet = $this->request->getVar('debet');
        $jmlh = $this->request->getVar('jumlah');
        $harga = $this->request->getVar('harga');
        $kredit = $this->request->getVar('kredit');
        $id = $this->request->getVar('id');
        $nilai = str_replace(".", "", $harga);
        $nilai = str_replace(",", ".", $nilai);
        //    echo $jumlah[$i];
        $nilai = $nilai * $jmlh;
        $posts = DB::table('t_jurnalumum');
        $update = $posts->updateBy(
            [
                'IdUnit'   => $unit,
                'Tanggal'   => date('Y-m-d', strtotime($tgl)),
                'NoTransaksi'  => $this->request->getVar('kuitansi'),
                'IdDebet'  => $debet,
                'IdKredit'  => $kredit,
                'Nilai'  => $nilai,
                // 'Rincian' => $rincian,
                // 'IdUser' => user_id(),
                'jumlah' => $jmlh,
                // 'idkascoa' => $idkas,
                // 'IdSup'=>$this->request->getVar('sup')
            ],
            ['IdJurnal'   => $id],
        );


        if ($update) {
            $this->grupmodel->query("CALL `resetmvneraca_x`(@p0)");
            $this->grupmodel->query("CALL `resetmvneraca`(@p0)");
            $this->grupmodel->query("CALL `resetmvneraca_y`(@p0)");

            $msg = [
                'error' => 'sukses',
                'msg' => 'Data Berhasil Dirubah Dirubah'
            ];
        }


        echo json_encode($msg);
        // }
    }
   
    
  
    // }
    
    // public function coba()
    // {
    //     for ($i = 0; $i < 4000; $i++) {


    //         $this->grupmodel->query("INSERT INTO `t_realisasi` (`id_realisasi`, `id_rinci`, `nilai_realisasi`, `tgl_realisasi`, `no_trans`, `tgl_aksi`, `detail`, `supplier`) VALUES (NULL, '1', '1000', '2021-01-18', 'qsadd', current_timestamp(), 'asdasdasd', '3')");
    //     }
    // }
}

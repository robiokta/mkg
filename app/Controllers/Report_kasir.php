<?php

namespace App\Controllers;

use App\Models\Result_model;
use Irsyadulibad\DataTables\DataTables;
use App\Models\Grupmodel;

class Report_kasir extends BaseController
{
	protected $model;
	protected $groppermi;
	protected $grupmodel;

	public function __construct()
	{
		$this->model = new Result_model();
		$this->grupmodel = new Grupmodel();
	}

	public function index()
	{
		$postModel = \Arifrh\DynaModel\DB::table('mak_4');
		$data['coa'] = $postModel->findAll();

		$data['title'] = 'Report Kasir';
		$data['page'] = 'Log Aktivitas Kasir';
		$data['user'] = 'Log Aktivitas';


		return view('report/report', $data);
	}

	// public function datalog(){
	// 	$table = [['t_realisasi'],['t_rinci']];

	// 	//$table3 = 'tabel3_join_disini';
	// 	$relation= [[],['t_realisasi.id_rinci=t_rinci.id_rinci']];

	// 	//$relation2 = 'tabel.id=tabel3.id';


	// 	//set column field database for datatable orderable
	// 	$column_order = array('tgl_realisasi', 'nomer_trans', 'total_harga');
	// 	//set column field database for datatable searchable just firstname , lastname , address are searchable
	// 	$column_search = array('tgl_realisasi', 'total_harga');
	// 	$order = array('id_realisasi' => 'ASC'); // default order
	// 	$condition = '';

	// 	$list = $this->model->get_datatables('2', $table, $relation, '', '', '', $condition, $column_order, '', $order, $column_search);
	// 	// print_r($list);
	// 	$data = array();
	// 	@$no = $_POST['start'];
	// 	foreach ($list as $dt) {
	// 		$no++;
	// 		$row = array();
	// 		// $row[] = $no.".";
	// 		$row[] = $dt->id_realisasi;
	// 		$row[] = $dt->nilai_realisasi;
	// 		// $row[] = $dt->no_hp;
	// 		// $row[] = $dt->foto;

	// 		//add html for action
	// 		// $row[] = '<center>
	// 		// 		  <a href="javascript:void(0);" onclick="edit(\''.$dt->id_karyawan.'\')" title="Edit">
	// 		// 			  Edit
	// 		// 		  </a>

	// 		// 		  <a href="javascript:void(0);" onclick="return confirm(\'Yakin akan menghapus?\')?hapus(\''.$dt->id_karyawan.'\'):false;" title="Delete">
	// 		// 			  Del
	// 		// 		  </a>
	// 		// 		  </center>';

	// 		$data[] = $row;
	// 	}

	// 	$output = array(
	// 				"draw" => $_POST['draw'],
	// 				"recordsTotal" => $this->model->count_all($table),
	// 				"recordsFiltered" => $this->model->count_filtered('1', $table, '', '', '', '', $condition, $column_order, '', $order, $column_search),
	// 				"data" => $data,
	// 			  );
	// 	//output to json format
	// 	echo json_encode($output);
	// }
	public function report()
	{
		return DataTables::use('t_realisasi')
			->select('no_pengajuan,detail,supplier,  nilai_realisasi,no_trans,tgl_realisasi,tgl_aksi')
			->join('t_rinci', 't_realisasi.id_rinci=t_rinci.id_rinci')
			->join('t_pengajuan', 't_rinci.id_pengajuan=t_pengajuan.id')
			->where(['t_pengajuan.status_hapus'=>'ada'])
			// ->hideColumns(['password'])

			->make(true);
	}
	public function filter_report()
	{
		$awal = $this->request->getVar('tgl_awal');
		$akhir = $this->request->getVar('tgl_akhir');
		if (!empty($awal) and !empty($akhir)) {
			return DataTables::use('t_realisasi')
				->select('no_pengajuan, detail,no_trans,supplier,nama_supplier,no_tlp,  nilai_realisasi,no_trans,tgl_realisasi,tgl_aksi')
				->join('t_rinci', 't_realisasi.id_rinci=t_rinci.id_rinci')
				->join('t_pengajuan', 't_rinci.id_pengajuan=t_pengajuan.id')
				->join('m_supplier', 'm_supplier.id_supplier=t_realisasi.supplier')
				->where(['tgl_realisasi >=' => date('Y-m-d', strtotime("$awal")), 'tgl_realisasi <=' => date('Y-m-d', strtotime("$akhir")),'t_pengajuan.status_hapus'=>'ada'])
				// ->hideColumns(['password'])

				->make(true);
		} elseif (!empty($awal) and empty($akhir)) {
			return DataTables::use('t_realisasi')
				->select('no_pengajuan,no_trans, detail,supplier,nama_supplier,no_tlp,  nilai_realisasi,no_trans,tgl_realisasi,tgl_aksi')
				->join('t_rinci', 't_realisasi.id_rinci=t_rinci.id_rinci')
				->join('t_pengajuan', 't_rinci.id_pengajuan=t_pengajuan.id')
				->join('m_supplier', 'm_supplier.id_supplier=t_realisasi.supplier')
				->where(['tgl_realisasi >=' => date('Y-m-d', strtotime("$awal")),'t_pengajuan.status_hapus'=>'ada'])
				// ->hideColumns(['password'])

				->make(true);
		} elseif (empty($awal) and !empty($akhir)) {
			return DataTables::use('t_realisasi')
				->select('no_pengajuan,no_trans, detail,supplier,nama_supplier,no_tlp,  nilai_realisasi,no_trans,tgl_realisasi,tgl_aksi')
				->join('t_rinci', 't_realisasi.id_rinci=t_rinci.id_rinci')
				->join('t_pengajuan', 't_rinci.id_pengajuan=t_pengajuan.id')
				->join('m_supplier', 'm_supplier.id_supplier=t_realisasi.supplier')
				->where(['tgl_realisasi <=' => date('Y-m-d', strtotime("$akhir")),'t_pengajuan.status_hapus'=>'ada'])
				// ->hideColumns(['password'])

				->make(true);
		} elseif (empty($awal) and empty($akhir)) {
			return DataTables::use('t_realisasi')
				->select('no_pengajuan,no_trans,detail,supplier,nama_supplier,no_tlp,  nilai_realisasi,no_trans,tgl_realisasi,tgl_aksi')
				->join('t_rinci', 't_realisasi.id_rinci=t_rinci.id_rinci')
				->join('t_pengajuan', 't_rinci.id_pengajuan=t_pengajuan.id')
				->join('m_supplier', 'm_supplier.id_supplier=t_realisasi.supplier')
				->where(['t_pengajuan.status_hapus'=>'ada'])
				// ->hideColumns(['password'])

				->make(true);
		}
	}
}

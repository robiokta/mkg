<?php

namespace App\Controllers;

use Myth\Auth\Models\UserModel;
use App\Models\Grupmodel;
use App\Models\Gruppermit;
use App\Models\Gruppermitl;
use Arifrh\DynaModel\DB;

class Pengajuan extends BaseController
{
    protected $grupmodel;
    protected $groppermi;
    private $menulink = 'm_menu';
    public function __construct()
    {
        $this->grupmodel = new Grupmodel();
        $this->gruppermit = new Gruppermit();
    }
    public function index()
    {

        // dd(user()->description);
        // die();
        $db      = \Config\Database::connect();
        $builder = $db->table('t_pengajuan');
        $builder->select('no_pengajuan,judul,pengajuan,tgl_pengajuan,id, status_name, user_id, status');
        $builder->join('t_status', 't_status.status_code=t_pengajuan.status');
        $builder->where('status_hapus', 'ada');
        if (in_groups('admin') | in_groups('manager') | in_groups('menku')) {
            if (in_groups('manager')) {

          
                    $builder->where('id_divisi', user()->id_divisi);
                   

            } else if (in_groups('menku')) {
                $builder->where('status', 'PO02');
            }
            # code...
        } else {
            $builder->where('id_divisi', user()->id_divisi);
        }
        $query   = $builder->get();


        $data['pengajuan'] = $query->getResult();

        // echo $db->getLastQuery();
        // dd($data['pengajuan']);

        $data['title'] = 'Pengajuan ';
        $data['page'] = 'Pengajuan';
        $data['user'] = 'Daftar Pengajuan';

        return view('pengajuan/pengajuan', $data);
    }
    public function adddata()
    {
        $data['title'] = 'Pengajuan';
        $data['page'] = 'Tambah Pengajuan';

        $grup = $this->grupmodel->findAll();
        $userid = user_id();
        $data['grup'] = $grup;
        if (in_groups('admin')) {
            $string = 'select *,nama_mak_4 as namamak from mak_4';
        } else {
            $string = "SELECT description,mak_4_id,nama_mak_4 as namamak FROM `mak_1` join auth_groups on mak_1.grup_user=auth_groups.description join auth_groups_users on auth_groups_users.group_id=auth_groups.id join mak_4 on mak_4.mak_1_id=mak_1.mak_1_id where auth_groups_users.user_id=$userid";
        }
        // $string = "select * from mak_4";
        $data['coa'] = $this->grupmodel->query($string);

        // if (in_groups('admin')){
        $string1 = "SELECT  namacoa, m_coa_4_id FROM `coa`";
        // }
        // else {
        //     $string1 = "SELECT description,m_coa_4_id,m_coa_4.nama_coa as namacoa FROM `m_coa` join auth_groups on m_coa.grup_user=auth_groups.description join auth_groups_users on auth_groups_users.group_id=auth_groups.id join m_coa_4 on m_coa_4.m_coa_1_id=m_coa_id where auth_groups_users.user_id=$userid";
        // }
        $data['coa1'] = $this->grupmodel->query($string1);

        return view('pengajuan/adddata', $data);
    }
    public function action_add()
    {
        $rincian = $this->request->getVar('rincian');
        $judul = $this->request->getVar('judul');
        $mak = $this->request->getVar('mak_4');

        $coa4 = $this->request->getVar('coa_4');

        $jumlah = count($coa4);

        // $dana = str_replace(".", "", $this->request->getVar('dana'));
        // echo date("d-m-y");


        $lastid = $this->grupmodel->lastid();
        if (empty($lastid)) {

            $id_peng = 0 + 1;
        } else {
            foreach ($lastid as $key => $val) {
                $id_peng = $val->id + 1;
            }
        }

        $db      = \Config\Database::connect();
        $builder = $db->table('users');
        $builder->select('users.id as userid, username, email, description');
        $builder->join('auth_groups_users', 'auth_groups_users.user_id=users.id');
        $builder->join('auth_groups', 'auth_groups.id=auth_groups_users.group_id');
        $builder->where('users.id', user_id());
        $query = $builder->get();
        $user = $query->getResult();

        foreach ($user as $key => $value) {
            $user_desc = $value->description;
        }

        $data = $query->getResult();
        $data = [
            'judul' => $judul,
            'id_mak_4' => $mak,
            'pengajuan' => $rincian,
            'tgl_pengajuan' => date("Y-m-d"),
            'no_pengajuan' => $id_peng . "/SPM" . '/' . $user_desc  . '/' . date('d/m/Y'),
            'status' => 'PO01',
            'user_manager' => "$user_desc",
            'id_divisi' => user()->id_divisi,
            'user_id' => user_id()
        ];
        $simpan = $this->grupmodel->insert_pengajuan($data);
        $id_pengajuan = $db->insertID();


        $authors = DB::table('t_rinci');
        for ($i = 0; $i < $jumlah; $i++) {

            $harga = str_replace(".", "", $this->request->getVar('harga'))[$i];
            $harga=str_replace(",", ".", $harga);
            $total = $harga * $this->request->getVar('jumlah')[$i];
            $data1 = [
                'id_pengajuan' => $id_pengajuan,
                'coa_4_id' => $coa4[$i],
                'jumlah' => $this->request->getVar('jumlah')[$i],
                'harga' => $harga,
                'total_harga' => $total,
                // 'status' => 'PO01',
                // 'user_id' => user_id()
            ];
            $simpan = $authors->insert($data1);
        }

        if ($simpan) {
            $msg = [
                'error' => 'sukses',
                'msg' => 'Input data berhasil '
            ];
        } else {
            $msg = [
                'error' => 'terjadi Error',
                'msg' => 'Silahkan coba lagi '
            ];
        }


        echo json_encode($msg);
    }
    public function edit($id)
    {
        $data['title'] = 'Ubah Pengajuan';
        $data['page'] = 'Ubah Data';

        $grup = $this->grupmodel->pengajuan($id);

        $authors = DB::table('t_pengajuan');
        $userid = user_id();
        if (in_groups('admin')) {
            $string = 'select  *,mak_4.nama_mak_4 as namamak from mak_4';
        } else {
            $string = "SELECT description,mak_4_id,nama_mak_4 as namamak FROM `mak_1` join auth_groups on mak_1.grup_user=auth_groups.description join auth_groups_users on auth_groups_users.group_id=auth_groups.id join mak_4 on mak_4.mak_1_id=mak_1.mak_1_id where auth_groups_users.user_id=$userid";
        }
        $data['coa'] = $this->grupmodel->query($string);
        $string1 = "select *, namacoa as nama_coa from coa";
        $data['coa1'] = $this->grupmodel->query($string1);
        $data['pengajuan'] = $authors->find($id);

        $authors = DB::table('t_rinci');


        $data['rinci'] = $authors->findBy(['id_pengajuan' => $id]);

        $data['grup'] = $grup;
        $data['id'] = $id;

        return view('pengajuan/editpengajuan', $data);
    }
    public function action_edit()
    {
        // if ($this->request->isAJAX()) {

        $rincian = $this->request->getVar('rincian');
        $judul = $this->request->getVar('judul');
        $mak = $this->request->getVar('mak_4');
        $coa4 = $this->request->getVar('coa_4');
        $id = $this->request->getVar('id');

        $jumlah = count($coa4);
        $posts = DB::table('t_rinci');
        $delete = $posts->deleteBy([

            'id_pengajuan'     => $id
        ]);

        // $dana = str_replace(".", "", $this->request->getVar('dana'));
        $data = array(
            'judul'  => $judul,
            'pengajuan' => $rincian,
            'id_mak_4' => $mak,
            'status' => 'PO01'
        );
        $update = $this->grupmodel->updatepengajuan($data, $id);
        $authors = DB::table('t_rinci');
        for ($i = 0; $i < $jumlah; $i++) {

            $harga = str_replace(".", "", $this->request->getVar('harga'))[$i];
            $harga=str_replace(",", ".", $harga);
            $total = $harga * $this->request->getVar('jumlah')[$i];
            $data1 = [
                'id_pengajuan' => $id,
                'coa_4_id' => $coa4[$i],
                'jumlah' => $this->request->getVar('jumlah')[$i],
                'harga' => $harga,
                'total_harga' => $total,
                // 'status' => 'PO01',
                // 'user_id' => user_id()
            ];
            $simpan = $authors->insert($data1);
        }


        if ($simpan) {

            $msg = [
                'error' => 'sukses',
                'msg' => 'Pengajuan ' . $this->request->getVar('judul') . ' Dirubah'
            ];
        }


        echo json_encode($msg);
        // }
    }
    public function delete_pengajuan()
    {
        $data = array(

            'status_hapus' => 'hapus'
        );

        $delete = $this->grupmodel->delete_pengajuan($data, $this->request->getVar('id'));

        if ($delete) {
            $msg = [
                'error' => 'sukses',
                'msg' => 'User '
            ];
        } else {
            $msg = [
                'error' => 'error',
                'msg' => 'Data Tidak Bisa Dihapus'
            ];
        }

        echo json_encode($msg);
    }
    public function persetujuan($id)
    {
        $db      = \Config\Database::connect();
        $builder = $db->table('t_pengajuan');
        $builder->select('no_pengajuan,judul,pengajuan,fullname, tgl_pengajuan,t_pengajuan.id idpengajuan, status_name, t_pengajuan.user_id, description, t_pengajuan.id idpengajuan, t_pengajuan.status statuspengajuan');
        $builder->join('t_status', 't_status.status_code=t_pengajuan.status');
        $builder->join('users', 'users.id=t_pengajuan.user_id');
        $builder->join('auth_groups_users', 'auth_groups_users.user_id=users.id');
        $builder->join('auth_groups', 'auth_groups.id=auth_groups_users.group_id');
        $builder->where('t_pengajuan.id', $id);
        

        $db1      = \Config\Database::connect();
        $builder1 = $db1->table('users');
        $builder1->select('users.id as userid, username, email, description');
        $builder1->join('auth_groups_users', 'auth_groups_users.user_id=users.id');
        $builder1->join('auth_groups', 'auth_groups.id=auth_groups_users.group_id');
        $builder1->where('users.id', user_id());
        $query1 = $builder1->get();
        $user1 = $query1->getResult();

        foreach ($user1 as $key => $value1) {
            $user_desc = $value1->description;
        }
        if ($user_desc == 'Manager Trans UMJ') {
            $builder->where('user_manager', 'UMJT');
            
        } elseif ($user_desc == 'Manager Engineering') {
            $builder->where('user_manager', 'UMJE');
           
        } elseif ($user_desc == 'Manager Offset') {
            $builder->where('user_manager', 'UMJOP');
          
        } elseif ($user_desc == 'Manager FGA') {
            $builder->where('user_manager', 'GFA');
          
        }
        $query   = $builder->get();
        $data['page'] = 'Persetujuan';


        $authors = DB::table('t_rinci');
        $data['rinci'] = $authors->findBy(['id_pengajuan' => $id]);
        $string = "select * from mak_4";
        $data['coa'] = $this->grupmodel->query($string);
        $string1 = "select *, namacoa as nama_coa from coa";
        $data['coa1'] = $this->grupmodel->query($string1);
        $data['pengajuan1'] = $authors->find($id);

        $data['pengajuan'] = $query->getResult();
        $data['id'] = $id;
        return view('pengajuan/persetujuan', $data);
    }
    public function ubah()

    {
        if ($this->request->isAJAX()) {

            $rincian = $this->request->getVar('rincian');
            $judul = $this->request->getVar('judul');
            $mak = $this->request->getVar('mak_4');
            $coa4 = $this->request->getVar('coa_4');
            $id = $this->request->getVar('id');

            $jumlah = count($coa4);
            $posts = DB::table('t_rinci');
            $delete = $posts->deleteBy([

                'id_pengajuan'     => $id
            ]);

            $authors = DB::table('t_rinci');
            for ($i = 0; $i < $jumlah; $i++) {

                $harga = str_replace(".", "", $this->request->getVar('harga'))[$i];
                $harga=str_replace(",", ".", $harga);
                $total = $harga * $this->request->getVar('jumlah')[$i];
                $data1 = [
                    'id_pengajuan' => $id,
                    'coa_4_id' => $coa4[$i],
                    'jumlah' => $this->request->getVar('jumlah')[$i],
                    'harga' => $harga,
                    'total_harga' => $total,
                    // 'status' => 'PO01',
                    // 'user_id' => user_id()
                ];
                $simpan = $authors->insert($data1);
            }


            if ($simpan) {
                $msg = [
                    'error' => 'sukses',
                    'msg' => 'Pengajuan ' . $this->request->getVar('judul') . ' Dirubah'
                ];
            }


            echo json_encode($msg);
        }
    }

    public function acc()
    {
        // if ($this->request->isAJAX()) {


        $id = $this->request->getVar('id');
        $status = $this->request->getVar('status');


        if ($status == 'PO02') {
            $status = 'PO03';

            $data = [
                'id_pengajuan' => $id,

            ];
            $simpan = $this->grupmodel->insert_kasir($data);
        } else if ($status == 'PO01') {
            $status = 'PO02';
        }



        $data = array(

            'status' => $status
        );
        $update = $this->grupmodel->acc($data, $id);
        if ($update) {
            $msg = [
                'error' => 'sukses',
                'msg' => 'Pengajuan Berhasil di Acc '
            ];
        }


        echo json_encode($msg);
    }
    // }
    public function decline()
    {
        if ($this->request->isAJAX()) {


            $id = $this->request->getVar('id');


            $data = array(

                'status' => 'PO04'
            );
            $update = $this->grupmodel->acc($data, $id);
            if ($update) {
                $msg = [
                    'error' => 'sukses',
                    'msg' => 'Pengajuan Ditolak'
                ];
            }


            echo json_encode($msg);
        }
    }
    // public function coba()
    // {
    //     for ($i = 0; $i < 4000; $i++) {


    //         $this->grupmodel->query("INSERT INTO `t_realisasi` (`id_realisasi`, `id_rinci`, `nilai_realisasi`, `tgl_realisasi`, `no_trans`, `tgl_aksi`, `detail`, `supplier`) VALUES (NULL, '1', '1000', '2021-01-18', 'qsadd', current_timestamp(), 'asdasdasd', '3')");
    //     }
    // }
}

<?php

namespace App\Controllers;

use Myth\Auth\Models\UserModel;
use App\Models\Grupmodel;
use App\Models\Gruppermit;
use App\Models\Gruppermitl;
use Arifrh\DynaModel\DB;
use DateTime;
use DatePeriod;
use DateInterval;

class Neraca_lajur extends BaseController
{
    protected $grupmodel;
    protected $groppermi;
    private $menulink = 'm_menu';
    public function __construct()
    {
        $this->grupmodel = new Grupmodel();
        $this->gruppermit = new Gruppermit();
    }
    public function index()
    {
        $data['title'] = 'Neraca Lajur Bulanan';
        $data['page'] = 'Neraca Lajur Bulanan';
        $data['user'] = 'Neraca Lajur Bulanan';
        $this->grupmodel->query("CALL `resetmvneraca_x`(@p0)");
        $this->grupmodel->query("CALL `resetmvneraca`(@p0)");
        $this->grupmodel->query("CALL `resetmvneraca_y`(@p0)");
        return view('neraca_lajur/neraca_lajur', $data);
    }
    public function adddata()
    {
        $data['title'] = 'Jurnal Umum';
        $data['page'] = 'Jurnal Umum';

        $grup = $this->grupmodel->findAll();
        $userid = user_id();
        $data['grup'] = $grup;

        $string = 'SELECT * FROM `auth_groups` where name="unit"';

        $data['coa'] = $this->grupmodel->query($string);
        $string1 = 'SELECT nama_coa as namacoa, m_coa_4_id FROM `m_coa_4`';

        $data['coa1'] = $this->grupmodel->query($string1);

        $string2 = 'SELECT DISTINCT type FROM `m_supplier`';

        $data['sup'] = $this->grupmodel->query($string2);

        return view('j_umum/adddata', $data);
    }



    function get_data1()
    {
        $level = $this->request->getVar('level');
        $tahun = $this->request->getVar('tahun');

        // die();
        if ($level == 4) {
            $tahunn = $tahun - 1;

            $data2 = array();
            $query1 = $this->grupmodel->query("select * from coa ");
            // array_push($data, $query1[0]->namacoa);
            foreach ($query1 as $key => $value) {
                $saldo_akhir = 0;
                unset($data); // $foo is gone
                $data = array();
                $k = 1;
                $value->namacoa;

                $queryy = $this->grupmodel->query("Select  ifnull(sum(Nilai_debit),0) as Nilai_debit, ifnull(sum(Nilai_kredit),0) as Nilai_kredit, ifnull(sum(saldo),0) as saldo from mv_neraca_lajur_y where  kode_coa_1=$value->kode_coa_1 and kode_coa_2=$value->kode_coa_2 and kode_coa_3=$value->kode_coa_3 and kode_coa_4=$value->kode_coa_4 and Tanggal='$tahunn' ");
                if (!empty($queryy)) {
                    foreach ($queryy as $key => $va) {
                        $saldo = $va->saldo;
                    }
                } else {
                    $saldo = 0;
                }

                $data[] = $value->kode_coa_1 . $value->kode_coa_2 . $value->kode_coa_3 . $value->kode_coa_4;
                $data[] =  $value->namacoa;
                $data[] =  $saldo;

                $start = new DateTime($tahun . '-01-01');
                $interval = new DateInterval('P1M');
                $end = new DateTime(date("Y-m-d"));
                $period = new DatePeriod($start, $interval, $end);


                foreach ($period as $dt) {
                    $tgl = $dt->format('Y-m');


                    $query = $this->grupmodel->query("Select  ifnull(sum(Nilai_debit),0) as Nilai_debit, ifnull(sum(Nilai_kredit),0) as Nilai_kredit, ifnull(sum(saldo),0) as saldo from mv_neraca_lajur_x where Tanggal='$tgl' and kode_coa_1=$value->kode_coa_1 and kode_coa_2=$value->kode_coa_2 and kode_coa_3=$value->kode_coa_3 and kode_coa_4=$value->kode_coa_4 ");

                    // $query = $this->grupmodel->query("Select * from mv_neraca_lajur where Tanggal='$tahun-$k' and id=$value->m_coa_4_id ");

                    if (!empty($query) & $query !== '') {
                        # code...  
                        foreach ($query as $key => $val) {
                            $saldo_akhir += $val->saldo;

                            $data[] = $val->Nilai_debit;
                            $data[] =  $val->Nilai_kredit;
                            $data[] =  $saldo_akhir;
                        }
                    } elseif (empty($query)) {
                        $data[] = 0;
                        $data[] =  0;
                        $data[] =  0;
                    }
                }
                $data2[] = $data;
            }
            $datass['data'] = $data2;
            // $datass['draw']=3;
            echo json_encode($datass);
        } elseif ($level == 3) {

            $data2 = array();
            $query1 = $this->grupmodel->query("select * from coa3  ");
            // array_push($data, $query1[0]->namacoa);
            foreach ($query1 as $key => $value) {
                $tahunn = $tahun - 1;

                $saldo_akhir = 0;

                unset($data); // $foo is gone
                $data = array();
                $k = 1;
                $value->namacoa;
                $queryy = $this->grupmodel->query("Select  ifnull(sum(Nilai_debit),0) as Nilai_debit, ifnull(sum(Nilai_kredit),0) as Nilai_kredit, ifnull(sum(saldo),0) as saldo from mv_neraca_lajur_y where  kode_coa_1=$value->kode_coa_1 and kode_coa_2=$value->kode_coa_2 and kode_coa_3=$value->kode_coa_3  and Tanggal='$tahunn' ");
                if (!empty($queryy)) {
                    foreach ($queryy as $key => $va) {
                        $saldo = $va->saldo;
                    }
                } else {
                    $saldo = 0;
                }

                $data[] = $value->kode_coa_1 . $value->kode_coa_2 . $value->kode_coa_3;
                $data[] =  $value->namacoa;
                $data[] =  $saldo;

                $start = new DateTime($tahun . '-01-01');
                $interval = new DateInterval('P1M');
                $end = new DateTime(date("Y-m-d"));
                $period = new DatePeriod($start, $interval, $end);

                foreach ($period as $dt) {
                    $tgl = $dt->format('Y-m');


                    $query = $this->grupmodel->query("Select  ifnull(sum(Nilai_debit),0) as Nilai_debit, ifnull(sum(Nilai_kredit),0) as Nilai_kredit, ifnull(sum(saldo),0) as saldo from mv_neraca_lajur_x where Tanggal='$tgl' and kode_coa_1=$value->kode_coa_1 and kode_coa_2=$value->kode_coa_2 and kode_coa_3=$value->kode_coa_3 ");

                    if (!empty($query) & $query !== '') {
                        # code...
                        // foreach ($data as &$item) {
                        foreach ($query as $key => $val) {
                            $saldo_akhir += $val->saldo;

                            $data[] = $val->Nilai_debit;
                            $data[] =  $val->Nilai_kredit;
                            $data[] =  $saldo_akhir;
                        }
                        // }
                    } elseif (empty($query) | $query == "null") {

                        // echo "aaa";
                        $data[] = 0;
                        $data[] =  0;
                        $data[] =  0;
                    }
                }

                $data2[] = $data;
            }
            $datass['data'] = $data2;
            // $datass['draw']=3;
            echo json_encode($datass);
        } elseif ($level == 2) {

            $data2 = array();
            $query1 = $this->grupmodel->query("select * from coa2 ");
            // array_push($data, $query1[0]->namacoa);
            foreach ($query1 as $key => $value) {
                $saldo_akhir = 0;
                $tahunn = $tahun - 1;

                unset($data); // $foo is gone
                $data = array();
                $k = 1;
                $value->namacoa;

                $queryy = $this->grupmodel->query("Select  ifnull(sum(Nilai_debit),0) as Nilai_debit, ifnull(sum(Nilai_kredit),0) as Nilai_kredit, ifnull(sum(saldo),0) as saldo from mv_neraca_lajur_y where  kode_coa_1=$value->kode_coa_1 and kode_coa_2=$value->kode_coa_2  and Tanggal='$tahunn' ");
                if (!empty($queryy)) {
                    foreach ($queryy as $key => $va) {
                        $saldo = $va->saldo;
                    }
                } else {
                    $saldo = 0;
                }

                $data[] = $value->kode_coa_1 . $value->kode_coa_2;
                $data[] =  $value->namacoa;
                $data[] =  $saldo;

                $start = new DateTime($tahun . '-01-01');
                $interval = new DateInterval('P1M');
                $end = new DateTime(date("Y-m-d"));
                $period = new DatePeriod($start, $interval, $end);


                foreach ($period as $dt) {
                    $tgl = $dt->format('Y-m');


                    $query = $this->grupmodel->query("Select  ifnull(sum(Nilai_debit),0) as Nilai_debit, ifnull(sum(Nilai_kredit),0) as Nilai_kredit, ifnull(sum(saldo),0) as saldo from mv_neraca_lajur_x where Tanggal='$tgl' and kode_coa_1=$value->kode_coa_1 and kode_coa_2=$value->kode_coa_2 ");

                    if (!empty($query) & $query !== '') {
                        # code...
                        // foreach ($data as &$item) {
                        foreach ($query as $key => $val) {
                            $saldo_akhir += $val->saldo;

                            $data[] = $val->Nilai_debit;
                            $data[] =  $val->Nilai_kredit;
                            $data[] =  $saldo_akhir;
                        }
                        // }
                    } elseif (empty($query) | $query == "null") {

                        // echo "aaa";
                        $data[] = 0;
                        $data[] =  0;
                        $data[] =  0;
                    }
                }

                $data2[] = $data;
            }
            $datass['data'] = $data2;
            // $datass['draw']=3;
            echo json_encode($datass);
        } elseif ($level == 6) {

            $data2 = array();
            $query1 = $this->grupmodel->query("select * from m_coa ");
            // array_push($data, $query1[0]->namacoa);
            foreach ($query1 as $key => $value) {
                $saldo_akhir = 0;
                $tahunn = $tahun - 1;

                unset($data); // $foo is gone
                $data = array();
                $k = 1;
                // $value->namacoa; 
                $queryy = $this->grupmodel->query("Select  ifnull(sum(Nilai_debit),0) as Nilai_debit, ifnull(sum(Nilai_kredit),0) as Nilai_kredit, ifnull(sum(saldo),0) as saldo from mv_neraca_lajur_y where  kode_coa_1=$value->kode_coa  and Tanggal='$tahunn' ");
                if (!empty($queryy)) {
                    foreach ($queryy as $key => $va) {
                        $saldo = $va->saldo;
                    }
                } else {
                    $saldo = 0;
                }

                $data[] = $value->kode_coa;
                $data[] =  $value->nama_coa;
                $data[] =  $saldo;
                $start = new DateTime($tahun . '-01-01');
                $interval = new DateInterval('P1M');
                $end = new DateTime(date("Y-m-d"));
                $period = new DatePeriod($start, $interval, $end);


                foreach ($period as $dt) {
                    $tgl = $dt->format('Y-m');

                    $query = $this->grupmodel->query("Select  ifnull(sum(Nilai_debit),0) as Nilai_debit, ifnull(sum(Nilai_kredit),0) as Nilai_kredit, ifnull(sum(saldo),0) as saldo from mv_neraca_lajur_x where Tanggal='$tgl' and kode_coa_1=$value->kode_coa ");

                    if (!empty($query) & $query !== '') {
                        # code...
                        // foreach ($data as &$item) {
                        foreach ($query as $key => $val) {
                            $saldo_akhir += $val->saldo;

                            $data[] = $val->Nilai_debit;
                            $data[] =  $val->Nilai_kredit;
                            $data[] =  $saldo_akhir;
                        }
                        // }
                    } elseif (empty($query) | $query == "null") {

                        // echo "aaa";
                        $data[] = 0;
                        $data[] =  0;
                        $data[] =  0;
                    }
                }

                $data2[] = $data;
            }
            $datass['data'] = $data2;
            // $datass['draw']=3;
            echo json_encode($datass);
        } else {
            $datass['data'] = '';
            echo json_encode($datass);
        }
    }


    public function get_data3()

    {
        $queri = '';
        for ($i = 1; $i <= 12; $i++) {
            $queri .= "
       (
       SELECT
           SUM(Nilai)
       FROM
           t_jurnalumum
       WHERE
           IdDebet = coa.m_coa_4_id and year(Tanggal)=2021 and month(Tanggal)=$i
   ) AS Nilai_debet$i,
   (
       SELECT
           SUM(Nilai)
       FROM
           t_jurnalumum
       WHERE
           IdKredit = coa.m_coa_4_id
   and year(Tanggal)=2021 and month(Tanggal)=$i) AS Nilai_kredit$i,
    (
       SELECT
           SUM(Nilai)
       FROM
           t_jurnalumum
       WHERE
           IdDebet = coa.m_coa_4_id and year(Tanggal)=2021 and month(Tanggal)=$i
   ) -
   (
       SELECT
           SUM(Nilai)
       FROM
           t_jurnalumum
       WHERE
           IdKredit = coa.m_coa_4_id and year(Tanggal)=2021 and month(Tanggal)=$i
   ) as total$i ,";
        }
        echo "SELECT $queri namacoa
        
        FROM
            coa";
        die();
        // echo $queri;
        $query = $this->grupmodel->query("SELECT $queri namacoa
        
    FROM
        coa");
        echo "<pre>";
        print_r($query);
        echo "/<pre>";
    }
    public function get_data2()
    {
        for ($i = 0; $i < 100000; $i++) {
            # code...

            $query = $this->grupmodel->query("INSERT INTO `t_jurnalumum` (`IdJurnal`, `idkascoa`, `IdUnit`, `NoTransaksi`, `Tanggal`, `NoRekening`, `Rekening`, `Uraian`, `IdDebet`, `IdKredit`, `Nilai`, `Keterangan`, `IdUser`, `TglSave`, `NU`, `MTransaksi`, `NA`, `IdSup`, `jumlah`) VALUES
        ('', 0, 3, '20/04/2021/UMJE/1', '2021-04-21', '', '', '', 4, 6, 2000000, '', 34, '2021-04-21 03:32:37', 'N', '', 'N', 3, 1),
        ('', 0, 3, '20/04/2021/UMJE/2', '2021-04-22', '', '', '', 3, 6, 1000000, '', 34, '2021-04-21 03:33:04', 'N', '', 'N', 3, 1),
        ('', 0, 2, '20/04/2021/UMJT/3', '2021-04-21', '', '', '', 3, 7, 200000, '', 34, '2021-04-21 03:33:40', 'N', '', 'N', 3, 1),
        ('', 0, 2, '20/04/2021/UMJT/3', '2021-04-21', '', '', '', 4, 3, 200000, '', 34, '2021-04-21 03:33:40', 'N', '', 'N', 3, 1);
        ");
        }
    }
}

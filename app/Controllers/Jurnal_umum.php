<?php

namespace App\Controllers;

use Myth\Auth\Models\UserModel;
use App\Models\Grupmodel;
use App\Models\Gruppermit;
use App\Models\Gruppermitl;
use Arifrh\DynaModel\DB;

class Jurnal_umum extends BaseController
{
    protected $grupmodel;
    protected $groppermi;
    private $menulink = 'm_menu';
    public function __construct()
    {
        $this->grupmodel = new Grupmodel();
        $this->gruppermit = new Gruppermit();
    }
    public function index()
    {
        $query = $this->grupmodel->query('select fullname, nama_supplier,alamat,no_tlp, Keterangan, Nilai,NoTransaksi, IdJurnal,Tanggal, coa.namacoa as namacoa4,coa.kode_coa_1 as coa1,coa.kode_coa_2 as coa2, coa.kode_coa_3 as coa3, coa.kode_coa_4 as coa4 ,a.namacoa as namacoa, a.kode_coa_1 as kd_coa_1,a.kode_coa_2 as kd_coa_2, a.kode_coa_3 as kd_coa_3 , a.kode_coa_4 as kd_coa_4 from t_jurnalumum join coa a on a.m_coa_4_id=t_jurnalumum.IdKredit join (select * from coa) as coa on coa.m_coa_4_id=t_jurnalumum.IdDebet left join m_supplier on t_jurnalumum.IdSup=m_supplier.id_supplier join users on users.id=t_jurnalumum.IdUser');

        $data['datas'] = $query;
        $coa3 = DB::table('coa');
        $data['coa'] = $coa3->findAll();
        $data['title'] = 'Jurnal Umum ';
        $data['page'] = 'Jurnal Umum';
        $data['user'] = 'Jurnal Umum';

        return view('j_umum/j_umum', $data);
    }
    public function adddata()
    {
        $data['title'] = 'Jurnal Umum';
        $data['page'] = 'Jurnal Umum';

        $grup = $this->grupmodel->findAll();
        $userid = user_id();
        $data['grup'] = $grup;

        $authors = DB::table('m_divisi');
        $data['coa']= $authors->findAll();
        $string1 = 'SELECT nama_coa as namacoa, m_coa_4_id FROM `m_coa_4`';

        $data['coa1'] = $this->grupmodel->query($string1);

        $string2 = 'SELECT DISTINCT type FROM `m_supplier`';

        $data['sup'] = $this->grupmodel->query($string2);

        return view('j_umum/adddata', $data);
    }
    public function action_add()
    {
        if ($this->request->isAJAX()) {
            $unit = $this->request->getVar('unit');
            // $notrans = $this->request->getVar('notrans');
            $tgltrans = $this->request->getVar('tgltrans');
            $debet = $this->request->getVar('debet');
            $kredit = $this->request->getVar('kredit');
            $nilai = $this->request->getVar('dana');
            $no_trans = $this->request->getVar('kuitansi');
            $rincian = $this->request->getVar('rincian');

            $authors = DB::table('t_jurnalumum');
            $authors->findAll();

            $jumlah = $authors->countAllResults() + 1;
            $posts = DB::table('auth_groups');
            // $publishPosts = $posts->findBy(['id' => $unit]);
            // $publishPosts[0]['description'];

            // $notrans = date('d/m/Y') . "/" . $publishPosts[0]['description'] . '/' . $jumlah;

            $authors = DB::table('t_jurnalumum');
            $nilai= str_replace(".", "", $nilai);

            $data = [
                'IdUnit'   => $unit,
                'Tanggal'   => date('Y-m-d', strtotime($tgltrans)),
                'NoTransaksi'  => $no_trans,
                'IdDebet'  => $debet,
                'IdKredit'  => $kredit,
                'Nilai'  => str_replace(",", ".", $nilai),
                'Rincian' => $rincian,
                'IdUser' => user_id(),
                'IdSup' => $this->request->getVar('sup')

            ];
            $simpan = $authors->insert($data);

            if ($simpan) {
                $this->grupmodel->query("CALL `resetmvneraca_x`(@p0)");
                $this->grupmodel->query("CALL `resetmvneraca`(@p0)");
                $this->grupmodel->query("CALL `resetmvneraca_y`(@p0)");
                
                $msg = [
                    'error' => 'sukses',
                    'msg' => 'Input data berhasil '
                ];
            } else {
                $msg = [
                    'error' => 'terjadi Error',
                    'msg' => 'Silahkan coba lagi '
                ];
            }

            echo json_encode($msg);
        }
    }
    public function supplier()
    {
        $sup = $this->request->getVar('sup');


        $data     = $this->grupmodel->query("SELECT * FROM m_supplier where type='$sup'");
        foreach ($data as $key => $val) :
            $a = $val->id_supplier;
            $b = $val->alamat;
            $arrisi[] = "<option value='$a' >$val->nama_supplier - $b</option>";


        endforeach;
        @$isi = join(" ", $arrisi);
        @$response_array['coa_2_hasil'] = "<option value=''>Supplier</option>
										" . $isi;
        @$response_array['kd'] = 2;


        echo json_encode($response_array);
    }
    public function report_saldo()
    {
        // if ($this->request->isAJAX()) {
            $idcoa = $this->request->getVar('coa_id');
            $tgl_awal = $this->request->getVar('tgl_awal');
            $tgl_akhir = $this->request->getVar('tgl_akhir');
            if (!empty($tgl_awal) & !empty($tgl_akhir)) {
                $tgl_awal = date('Y-m-d', strtotime($tgl_awal));
                $tgl_akhir = date('Y-m-d', strtotime($tgl_akhir));

                $tgl = "and t_jurnalumum.Tanggal >= '$tgl_awal' and t_jurnalumum.Tanggal <= '$tgl_akhir'";
            } elseif (!empty($tgl_awal) & empty($tgl_akhir)) {
                $tgl_awal = date('Y-m-d', strtotime($tgl_awal));

                $tgl = "and t_jurnalumum.Tanggal >= '$tgl_awal' ";
            } elseif (empty($tgl_awal) & !empty($tgl_akhir)) {
                $tgl_akhir = date('Y-m-d', strtotime($tgl_akhir));
                $tgl = "and t_jurnalumum.Tanggal <= '$tgl_akhir'";
            } elseif (empty($tgl_awal) & empty($tgl_akhir)) {
                $tgl = '';
            }
            // echo "SELECT (select namacoa from coa where m_coa_4_id=$idcoa) as namacoa,
            // (select kode_coa_1 from coa where m_coa_4_id=$idcoa) as kode_coa_1,
            // (select kode_coa_2 from coa where m_coa_4_id=$idcoa) as kode_coa_2,
            // (select kode_coa_3 from coa where m_coa_4_id=$idcoa) as kode_coa_3,
            // (select kode_coa_4 from coa where m_coa_4_id=$idcoa) as kode_coa_4,
            //  IFNULL(sum(t_jurnalumum.Nilai),0) nilai_kredit, IFNULL(( select sum(Nilai) from t_jurnalumum WHERE IdDebet =$idcoa $tgl ),0) as nilai_debet from t_jurnalumum left join coa a on a.m_coa_4_id=t_jurnalumum.IdKredit where t_jurnalumum.IdKredit = $idcoa $tgl";
            // die();
            if (empty($idcoa)) {
                if (!empty($tgl_awal) & !empty($tgl_akhir)) {
                    $tgl_awal = date('Y-m-d', strtotime($tgl_awal));
                    $tgl_akhir = date('Y-m-d', strtotime($tgl_akhir));

                    $tgl = " t_jurnalumum.Tanggal >= '$tgl_awal' and t_jurnalumum.Tanggal <= '$tgl_akhir'";
                } elseif (!empty($tgl_awal) & empty($tgl_akhir)) {
                    $tgl_awal = date('Y-m-d', strtotime($tgl_awal));

                    $tgl = " t_jurnalumum.Tanggal >= '$tgl_awal' ";
                } elseif (empty($tgl_awal) & !empty($tgl_akhir)) {
                    $tgl_akhir = date('Y-m-d', strtotime($tgl_akhir));
                    $tgl = " t_jurnalumum.Tanggal <= '$tgl_akhir'";
                } elseif (empty($tgl_awal) & empty($tgl_akhir)) {
                    $tgl = '';
                }
                $query = $this->grupmodel->query("SELECT a.namacoa as namacoa,a.kode_coa_1 as kode_coa_1,
                a.kode_coa_2 as kode_coa_2,
                a.kode_coa_3 as kode_coa_3,
                a.kode_coa_1 as kode_coa_4,
                 t_jurnalumum.IdJurnal, IFNULL(sum(t_jurnalumum.Nilai),0) nilai_kredit,IFNULL(sum(b.Nilai),0) nilai_debet from t_jurnalumum left join coa a on a.m_coa_4_id=t_jurnalumum.IdKredit left join t_jurnalumum b on a.m_coa_4_id=b.IdDebet where $tgl GROUP by t_jurnalumum.IdKredit ");
            } else {
                $query = $this->grupmodel->query("SELECT (select namacoa from coa where m_coa_4_id=$idcoa) as namacoa,
            (select kode_coa_1 from coa where m_coa_4_id=$idcoa) as kode_coa_1,
            (select kode_coa_2 from coa where m_coa_4_id=$idcoa) as kode_coa_2,
            (select kode_coa_3 from coa where m_coa_4_id=$idcoa) as kode_coa_3,
            (select kode_coa_4 from coa where m_coa_4_id=$idcoa) as kode_coa_4,
             IFNULL(sum(t_jurnalumum.Nilai),0) nilai_kredit, IFNULL(( select sum(Nilai) from t_jurnalumum WHERE IdDebet =$idcoa $tgl ),0) as nilai_debet from t_jurnalumum left join coa a on a.m_coa_4_id=t_jurnalumum.IdKredit where t_jurnalumum.IdKredit = $idcoa $tgl ");
            }
            $data['data'] = $query;
            echo json_encode($data);
        // }
    }
    // public function edit($id)
    // {
    //     $data['title'] = 'Ubah Pengajuan';
    //     $data['page'] = 'Ubah Data';

    //     $grup = $this->grupmodel->pengajuan($id);

    //     $authors = DB::table('t_pengajuan');
    //     $userid = user_id();
    //     if (in_groups('admin')) {
    //         $string = 'select  *,mak_4.nama_mak_4 as namamak from mak_4';
    //     } else {
    //         $string = "SELECT description,mak_4_id,nama_mak_4 as namamak FROM `mak_1` join auth_groups on mak_1.grup_user=auth_groups.description join auth_groups_users on auth_groups_users.group_id=auth_groups.id join mak_4 on mak_4.mak_1_id=mak_1.mak_1_id where auth_groups_users.user_id=$userid";
    //     }
    //     $data['coa'] = $this->grupmodel->query($string);
    //     $string1 = "select * from m_coa_4";
    //     $data['coa1'] = $this->grupmodel->query($string1);
    //     $data['pengajuan'] = $authors->find($id);

    //     $authors = DB::table('t_rinci');


    //     $data['rinci'] = $authors->findBy(['id_pengajuan' => $id]);

    //     $data['grup'] = $grup;
    //     $data['id'] = $id;

    //     return view('pengajuan/editpengajuan', $data);
    // }
    // public function action_edit()
    // {
    //     // if ($this->request->isAJAX()) {

    //     $rincian = $this->request->getVar('rincian');
    //     $judul = $this->request->getVar('judul');
    //     $mak = $this->request->getVar('mak_4');
    //     $coa4 = $this->request->getVar('coa_4');
    //     $id = $this->request->getVar('id');

    //     $jumlah = count($coa4);
    //     $posts = DB::table('t_rinci');
    //     $delete = $posts->deleteBy([

    //         'id_pengajuan'     => $id
    //     ]);

    //     // $dana = str_replace(".", "", $this->request->getVar('dana'));
    //     $data = array(
    //         'judul'  => $judul,
    //         'pengajuan' => $rincian,
    //         'id_mak_4' => $mak,
    //         'status' => 'PO01'
    //     );
    //     $update = $this->grupmodel->updatepengajuan($data, $id);
    //     $authors = DB::table('t_rinci');
    //     for ($i = 0; $i < $jumlah; $i++) {

    //         $harga = str_replace(".", "", $this->request->getVar('harga'))[$i];
    //         $total = $harga * $this->request->getVar('jumlah')[$i];
    //         $data1 = [
    //             'id_pengajuan' => $id,
    //             'coa_4_id' => $coa4[$i],
    //             'jumlah' => $this->request->getVar('jumlah')[$i],
    //             'harga' => $harga,
    //             'total_harga' => $total,
    //             // 'status' => 'PO01',
    //             // 'user_id' => user_id()
    //         ];
    //         $simpan = $authors->insert($data1);
    //     }


    //     if ($simpan) {

    //         $msg = [
    //             'error' => 'sukses',
    //             'msg' => 'Pengajuan ' . $this->request->getVar('judul') . ' Dirubah'
    //         ];
    //     }


    //     echo json_encode($msg);
    //     // }
    // }

}

<?php

namespace App\Controllers;

use Myth\Auth\Models\UserModel;
use App\Models\Grupmodel;
use App\Models\Gruppermit;
use App\Models\Gruppermitl;
use Arifrh\DynaModel\DB;

class M_coa_3 extends BaseController
{
    protected $grupmodel;
    protected $groppermi;
    private $menulink = 'm_menu';
    public function __construct()
    {
        $this->grupmodel = new Grupmodel();
        $this->gruppermit = new Gruppermit();
    }

    public function index()
    {

        $data['coa'] = $this->grupmodel->query('select *,m_coa_3.nama_coa as namacoa, m_coa_3.status as statuss from m_coa_3 join m_coa_2 on m_coa_2.m_coa_2_id=m_coa_3.m_coa_2_id join m_coa on m_coa.m_coa_id=m_coa_3.id_coa_1 order by kode_coa_3 asc' );


        // echo $db->getLastQuery();
        // dd($data['coa']);

        $data['title'] = 'Master COA 3';
        $data['page'] = 'Master COA 3';
        $data['user'] = 'Daftar COA 3';

        return view('coa_3/coa_3', $data);
    }

    public function adddata()
    {
        $data['title'] = 'Tambah COA 3';
        $data['page'] = 'Tambah COA 3';
        $data['coa'] = $this->grupmodel->coa_1();

        return view('coa_3/adddata', $data);
    }
    public function pilihcoa()
    {
        $coa1 = $this->request->getVar('coa_1');
        $coa2 = $this->request->getVar('coa_2');

        if (
            $coa1 != '' and $coa2 == ''

        ) {

            $data     = $this->grupmodel->coa22($coa1);
            foreach ($data as $key => $val) :
                $a = $val->m_coa_2_id;
                $b = $val->nama_coa;
                $arrisi[] = "<option value='$a' >$val->kode_coa_2 - $b</option>";


            endforeach;
            @$isi = join(" ", $arrisi);
            @$response_array['coa_2_hasil'] = "<option value=''>Kode Rek 2</option>
										" . $isi;
            @$response_array['kd'] = 2;
        }

        echo json_encode($response_array);
    }

    public function action_add()
    {

        $nama_coa = $this->request->getVar('nama_coa');
        $coa1 = $this->request->getVar('id_coa_1');
        $coa2 = $this->request->getVar('id_coa_2');
        $kode = $this->request->getVar('kode_coa_3');

        $authors = DB::table('m_coa_3');

        $data = [
            'id_coa_1'   => $coa1,
            'm_coa_2_id'   => $coa2,
            'kode_coa_3'  => $kode,
            'nama_coa' => $nama_coa,
        ];

        $simpan = $authors->insert($data);


        if ($simpan) {
            $msg = [
                'error' => 'sukses',
                'msg' => 'Input data berhasil '
            ];
        } else {
            $msg = [
                'error' => 'terjadi Error',
                'msg' => 'Silahkan coba lagi '
            ];
        }


        echo json_encode($msg);
    }
    public function edit($id)
    {
        $data['title'] = 'Ubah COA';
        $data['page'] = 'Ubah Data';

        $authors = DB::table('m_coa_3');

        $coa = $authors->find($id);
        // dd($test);

        $data['coa'] = $coa;

        $data['coa1'] = $this->grupmodel->coa_1();
        $postModel = \Arifrh\DynaModel\DB::table('m_coa_2');

        $data['coa2'] = $postModel->findAll();

        $data['id'] = $id;

        return view('coa_3/edit', $data);
    }
    public function action_edit()
    {
        // if ($this->request->isAJAX()) {


        $nama_coa = $this->request->getVar('nama_coa');
        $coa1 = $this->request->getVar('id_coa_1');
        $coa2 = $this->request->getVar('id_coa_2');
        $kode = $this->request->getVar('kode_coa_3');
        $id = $this->request->getVar('id');

        $posts = DB::table('m_coa_3');

        $update = $posts->updateBy(
            ['nama_coa'   => $nama_coa, 'id_coa_1' => $coa1, 'm_coa_2_id' => $coa2, 'kode_coa_3' => $kode],
            ['m_coa_3_id'   => $id],
        );
        if ($update) {
            $msg = [
                'error' => 'sukses',
                'msg' => $this->request->getVar('nama_coa') . ' Dirubah'
            ];
        }


        echo json_encode($msg);
        // }
    }
    public function delete()
    {


        // $delete = $this->grupmodel->delete_coa_2();


        $posts = DB::table('m_coa_3');
        $delete = $posts->deleteBy([

            'm_coa_3_id'     => $this->request->getVar('id')
        ]);

        if ($delete) {
            $msg = [
                'error' => 'sukses',
                'msg' => 'User '
            ];
        } else {
            $msg = [
                'error' => 'error',
                'msg' => 'Data Tidak Bisa Dihapus'
            ];
        }

        echo json_encode($msg);
    }
    public function status()
    {
        $id = $this->request->getVar('id');
        $posts = DB::table('m_coa_3');
        $this->request->getVar('val');
        if ($this->request->getVar('val') == 'inactive') {
            $status = 'inactive';
        } elseif ($this->request->getVar('val') == 'active') {
            $status = 'active';
        }
        $delete = $posts->updateBy(
            ['status' => "$status"],
            ['m_coa_3_id'   => $id],
        );

        if ($delete) {
            $msg = [
                'error' => 'sukses',
                'msg' => 'User '
            ];
        } else {
            $msg = [
                'error' => 'error',
                'msg' => 'Data Tidak Bisa Dihapus'
            ];
        }

        echo json_encode($msg);
    }
}

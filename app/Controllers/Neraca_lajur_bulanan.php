<?php

namespace App\Controllers;

use Myth\Auth\Models\UserModel;
use App\Models\Grupmodel;
use App\Models\Gruppermit;
use App\Models\Gruppermitl;
use Arifrh\DynaModel\DB;

class Neraca_lajur_bulanan extends BaseController
{
    protected $grupmodel;
    protected $groppermi;
    private $menulink = 'm_menu';
    public function __construct()
    {
        $this->grupmodel = new Grupmodel();
        $this->gruppermit = new Gruppermit();
    }
    public function index()
    {
        $this->grupmodel->query("CALL `resetmvneraca_x`(@p0)");
        $this->grupmodel->query("CALL `resetmvneraca`(@p0)");
        $this->grupmodel->query("CALL `resetmvneraca_y`(@p0)");
        $data['title'] = 'Neraca Lajur Bulanan';
        $data['page'] = 'Neraca Lajur Bulanan';
        $data['user'] = 'Neraca Lajur Bulanan';

        return view('neraca_lajur_bulanan/neraca_lajur_bulanan', $data);
    }
    public function adddata()
    {
        $data['title'] = 'Jurnal Umum';
        $data['page'] = 'Jurnal Umum';

        $grup = $this->grupmodel->findAll();
        $userid = user_id();
        $data['grup'] = $grup;

        $string = 'SELECT * FROM `auth_groups` where name="unit"';

        $data['coa'] = $this->grupmodel->query($string);
        $string1 = 'SELECT nama_coa as namacoa, m_coa_4_id FROM `m_coa_4`';

        $data['coa1'] = $this->grupmodel->query($string1);

        $string2 = 'SELECT DISTINCT type FROM `m_supplier`';

        $data['sup'] = $this->grupmodel->query($string2);

        return view('j_umum/adddata', $data);
    }
    public function action_add()
    {
        if ($this->request->isAJAX()) {
            $unit = $this->request->getVar('unit');
            // $notrans = $this->request->getVar('notrans');
            $tgltrans = $this->request->getVar('tgltrans');
            $debet = $this->request->getVar('debet');
            $kredit = $this->request->getVar('kredit');
            $nilai = $this->request->getVar('dana');
            $rincian = $this->request->getVar('rincian');

            $authors = DB::table('t_jurnalumum');
            $authors->findAll();

            $jumlah = $authors->countAllResults() + 1;
            $posts = DB::table('auth_groups');
            $publishPosts = $posts->findBy(['id' => $unit]);
            $publishPosts[0]['description'];

            $notrans = date('d/m/Y') . "/" . $publishPosts[0]['description'] . '/' . $jumlah;

            $authors = DB::table('t_jurnalumum');

            $data = [
                'IdUnit'   => $unit,
                'Tanggal'   => date('Y-m-d', strtotime($tgltrans)),
                'NoTransaksi'  => $notrans,
                'IdDebet'  => $debet,
                'IdKredit'  => $kredit,
                'Nilai'  => str_replace(".", "", $nilai),
                'Rincian' => $rincian,
                'IdUser' => user_id(),
                'IdSup' => $this->request->getVar('sup')

            ];
            $simpan = $authors->insert($data);

            if ($simpan) {
                
                $msg = [
                    'error' => 'sukses',
                    'msg' => 'Input data berhasil '
                ];
            } else {
                $msg = [
                    'error' => 'terjadi Error',
                    'msg' => 'Silahkan coba lagi '
                ];
            }

            echo json_encode($msg);
        }
    }
    public function supplier()
    {
        $sup = $this->request->getVar('sup');


        $data     = $this->grupmodel->query("SELECT * FROM m_supplier where type='$sup'");
        foreach ($data as $key => $val) :
            $a = $val->id_supplier;
            $b = $val->alamat;
            $arrisi[] = "<option value='$a' >$val->nama_supplier - $b</option>";


        endforeach;
        @$isi = join(" ", $arrisi);
        @$response_array['coa_2_hasil'] = "<option value=''>Supplier</option>
										" . $isi;
        @$response_array['kd'] = 2;


        echo json_encode($response_array);
    }
    public function report_saldo()
    {
        if ($this->request->isAJAX()) {
            $idcoa = $this->request->getVar('coa_id');
            $tgl_awal = $this->request->getVar('tgl_awal');
            $tgl_akhir = $this->request->getVar('tgl_akhir');

            if (empty($tgl_awal) & empty($tgl_akhir) & empty($idcoa)) {
                $data['data'] = '';
                echo json_encode($data);
                die();
            }


            if (!empty($tgl_awal) & !empty($tgl_akhir)) {
                $tgl_awal = date('Y-m-d', strtotime($tgl_awal));
                $tgl_akhir = date('Y-m-d', strtotime($tgl_akhir));

                $tgl = "and t_jurnalumum.Tanggal >= '$tgl_awal' and t_jurnalumum.Tanggal <= '$tgl_akhir'";
            } elseif (!empty($tgl_awal) & empty($tgl_akhir)) {
                $tgl_awal = date('Y-m-d', strtotime($tgl_awal));

                $tgl = "and t_jurnalumum.Tanggal >= '$tgl_awal' ";
            } elseif (empty($tgl_awal) & !empty($tgl_akhir)) {
                $tgl_akhir = date('Y-m-d', strtotime($tgl_akhir));
                $tgl = "and t_jurnalumum.Tanggal <= '$tgl_akhir'";
            } elseif (empty($tgl_awal) & empty($tgl_akhir)) {
                $tgl = '';
            }
            // echo "SELECT (select namacoa from coa where m_coa_4_id=$idcoa) as namacoa,
            // (select kode_coa_1 from coa where m_coa_4_id=$idcoa) as kode_coa_1,
            // (select kode_coa_2 from coa where m_coa_4_id=$idcoa) as kode_coa_2,
            // (select kode_coa_3 from coa where m_coa_4_id=$idcoa) as kode_coa_3,
            // (select kode_coa_4 from coa where m_coa_4_id=$idcoa) as kode_coa_4,
            //  IFNULL(sum(t_jurnalumum.Nilai),0) nilai_kredit, IFNULL(( select sum(Nilai) from t_jurnalumum WHERE IdDebet =$idcoa $tgl ),0) as nilai_debet from t_jurnalumum left join coa a on a.m_coa_4_id=t_jurnalumum.IdKredit where t_jurnalumum.IdKredit = $idcoa $tgl";
            // die();
            if (empty($idcoa)) {
                if (!empty($tgl_awal) & !empty($tgl_akhir)) {
                    $tgl_awal = date('Y-m-d', strtotime($tgl_awal));
                    $tgl_akhir = date('Y-m-d', strtotime($tgl_akhir));

                    $tgl = " t_jurnalumum.Tanggal >= '$tgl_awal' and t_jurnalumum.Tanggal <= '$tgl_akhir'";
                } elseif (!empty($tgl_awal) & empty($tgl_akhir)) {
                    $tgl_awal = date('Y-m-d', strtotime($tgl_awal));

                    $tgl = " t_jurnalumum.Tanggal >= '$tgl_awal' ";
                } elseif (empty($tgl_awal) & !empty($tgl_akhir)) {
                    $tgl_akhir = date('Y-m-d', strtotime($tgl_akhir));
                    $tgl = " t_jurnalumum.Tanggal <= '$tgl_akhir'";
                } elseif (empty($tgl_awal) & empty($tgl_akhir)) {
                    $tgl = '';
                }
                $query = $this->grupmodel->query("SELECT a.namacoa as namacoa,a.kode_coa_1 as kode_coa_1,
                a.kode_coa_2 as kode_coa_2,
                a.kode_coa_3 as kode_coa_3,
                a.kode_coa_1 as kode_coa_4,
                 t_jurnalumum.IdJurnal, IFNULL(sum(t_jurnalumum.Nilai),0) nilai_kredit,IFNULL(sum(b.Nilai),0) nilai_debet from t_jurnalumum left join coa a on a.m_coa_4_id=t_jurnalumum.IdKredit left join t_jurnalumum b on a.m_coa_4_id=b.IdDebet where $tgl GROUP by t_jurnalumum.IdKredit ");
                $data['data'] = $query;
                echo json_encode($data);
            } else {
                $query = $this->grupmodel->query("SELECT (select namacoa from coa where m_coa_4_id=$idcoa) as namacoa,
            (select kode_coa_1 from coa where m_coa_4_id=$idcoa) as kode_coa_1,
            (select kode_coa_2 from coa where m_coa_4_id=$idcoa) as kode_coa_2,
            (select kode_coa_3 from coa where m_coa_4_id=$idcoa) as kode_coa_3,
            (select kode_coa_4 from coa where m_coa_4_id=$idcoa) as kode_coa_4,
             IFNULL(sum(t_jurnalumum.Nilai),0) nilai_kredit, IFNULL(( select sum(Nilai) from t_jurnalumum WHERE IdDebet =$idcoa $tgl ),0) as nilai_debet from t_jurnalumum left join coa a on a.m_coa_4_id=t_jurnalumum.IdKredit where t_jurnalumum.IdKredit = $idcoa $tgl ");
            }
            $data['data'] = $query;
            echo json_encode($data);
        }
    }
    function get_data1()
    {
        $level = $this->request->getVar('level');
        $tahun = $this->request->getVar('tahun');
        // die();
        if ($level == 4) {

            $data2 = array();
            $query1 = $this->grupmodel->query("select * from coa ");
            // array_push($data, $query1[0]->namacoa);
            foreach ($query1 as $key => $value) {
                unset($data); // $foo is gone
                $data = array();
                $k = 1;
                $value->namacoa;
                // array_push(
                //     $data,
                //     $value->kode_coa_1 . $value->kode_coa_2 . $value->kode_coa_3 . $value->kode_coa_4,
                //     $value->namacoa,
                // );
                $data[] = $value->kode_coa_1 . $value->kode_coa_2 . $value->kode_coa_3 . $value->kode_coa_4;
                $data[] =  $value->namacoa;


                for ($i = 1; $i <= 12; $i++) {
                    $k = sprintf("%02d", $i);

                    // $query = $this->grupmodel->query("SELECT  ifnull((Nilai_debet),0) as Nilai_debet, ifnull((Nilai_kredit),0) as Nilai_kredit, ifnull((Nilai_debet),0)-ifnull((Nilai_kredit),0) as saldo FROM (select Tanggal, m_coa_4_id, sum(Nilai) as Nilai_debet,IdDebet,IdKredit FROM mv_jurnal_lajur GROUP BY IdDebet) as test1 left join (select Tanggal, sum(Nilai) as Nilai_Kredit,IdDebet,IdKredit FROM mv_jurnal_lajur GROUP BY IdKredit) as testb on test1.IdDebet=testb.IdKredit where year(test1.Tanggal)=2021 and month(test1.Tanggal)=$i and test1.m_coa_4_id=$value->m_coa_4_id");

                    $query = $this->grupmodel->query("Select  ifnull(sum(Nilai_debit),0) as Nilai_debit, ifnull(sum(Nilai_kredit),0) as Nilai_kredit, ifnull(sum(saldo),0) as saldo from mv_neraca_lajur_x where Tanggal='$tahun-$k' and kode_coa_1=$value->kode_coa_1 and kode_coa_2=$value->kode_coa_2 and kode_coa_3=$value->kode_coa_3 and kode_coa_4=$value->kode_coa_4 ");

                    // $query = $this->grupmodel->query("Select * from mv_neraca_lajur where Tanggal='$tahun-$k' and id=$value->m_coa_4_id ");

                    if (!empty($query) & $query !== '') {
                        # code...
                        // foreach ($data as &$item) {
                        foreach ($query as $key => $val) {

                            $data[] = $val->Nilai_debit;
                            $data[] =  $val->Nilai_kredit;
                            $data[] =  $val->saldo;
                        }
                        // }
                    } elseif (empty($query)) {


                        $data[] = 0;
                        $data[] =  0;
                        $data[] =  0;
                    }
                }

                $data2[] = $data;
            }
            $datass['data'] = $data2;
            // $datass['draw']=3;
            echo json_encode($datass);
        } elseif ($level == 3) {

            $data2 = array();
            $query1 = $this->grupmodel->query("select * from coa3  ");
            // array_push($data, $query1[0]->namacoa);
            foreach ($query1 as $key => $value) {
                unset($data); // $foo is gone
                $data = array();
                $k = 1;
                $value->namacoa;

                $data[] = $value->kode_coa_1 . $value->kode_coa_2 . $value->kode_coa_3;
                $data[] =  $value->namacoa;


                for ($i = 1; $i <= 12; $i++) {
                    $k = sprintf("%02d", $i);


                    $query = $this->grupmodel->query("Select  ifnull(sum(Nilai_debit),0) as Nilai_debit, ifnull(sum(Nilai_kredit),0) as Nilai_kredit, ifnull(sum(saldo),0) as saldo from mv_neraca_lajur_x where Tanggal='$tahun-$k' and kode_coa_1=$value->kode_coa_1 and kode_coa_2=$value->kode_coa_2 and kode_coa_3=$value->kode_coa_3 ");

                    if (!empty($query) & $query !== '') {
                        # code...
                        // foreach ($data as &$item) {
                        foreach ($query as $key => $val) {

                            $data[] = $val->Nilai_debit;
                            $data[] =  $val->Nilai_kredit;
                            $data[] =  $val->saldo;
                        }
                        // }
                    } elseif (empty($query) | $query == "null") {

                        // echo "aaa";
                        $data[] = 0;
                        $data[] =  0;
                        $data[] =  0;
                    }
                }

                $data2[] = $data;
            }
            $datass['data'] = $data2;
            // $datass['draw']=3;
            echo json_encode($datass);
        } elseif ($level == 2) {

            $data2 = array();
            $query1 = $this->grupmodel->query("select * from coa2 ");
            // array_push($data, $query1[0]->namacoa);
            foreach ($query1 as $key => $value) {
                unset($data); // $foo is gone
                $data = array();
                $k = 1;
                $value->namacoa;

                $data[] = $value->kode_coa_1 . $value->kode_coa_2;
                $data[] =  $value->namacoa;


                for ($i = 1; $i <= 12; $i++) {
                    $k = sprintf("%02d", $i);


                    $query = $this->grupmodel->query("Select  ifnull(sum(Nilai_debit),0) as Nilai_debit, ifnull(sum(Nilai_kredit),0) as Nilai_kredit, ifnull(sum(saldo),0) as saldo from mv_neraca_lajur_x where Tanggal='$tahun-$k' and kode_coa_1=$value->kode_coa_1 and kode_coa_2=$value->kode_coa_2 ");

                    if (!empty($query) & $query !== '') {
                        # code...
                        // foreach ($data as &$item) {
                        foreach ($query as $key => $val) {

                            $data[] = $val->Nilai_debit;
                            $data[] =  $val->Nilai_kredit;
                            $data[] =  $val->saldo;
                        }
                        // }
                    } elseif (empty($query) | $query == "null") {

                        // echo "aaa";
                        $data[] = 0;
                        $data[] =  0;
                        $data[] =  0;
                    }
                }

                $data2[] = $data;
            }
            $datass['data'] = $data2;
            // $datass['draw']=3;
            echo json_encode($datass);
        } elseif ($level == 6) {

            $data2 = array();
            $query1 = $this->grupmodel->query("select * from m_coa ");
            // array_push($data, $query1[0]->namacoa);
            foreach ($query1 as $key => $value) {
                unset($data); // $foo is gone
                $data = array();
                $k = 1;
                // $value->namacoa; 

                $data[] = $value->kode_coa;
                $data[] =  $value->nama_coa;


                for ($i = 1; $i <= 12; $i++) {
                    $k = sprintf("%02d", $i);


                    $query = $this->grupmodel->query("Select  ifnull(sum(Nilai_debit),0) as Nilai_debit, ifnull(sum(Nilai_kredit),0) as Nilai_kredit, ifnull(sum(saldo),0) as saldo from mv_neraca_lajur_x where Tanggal='$tahun-$k' and kode_coa_1=$value->kode_coa ");

                    if (!empty($query) & $query !== '') {
                        # code...
                        // foreach ($data as &$item) {
                        foreach ($query as $key => $val) {

                            $data[] = $val->Nilai_debit;
                            $data[] =  $val->Nilai_kredit;
                            $data[] =  $val->saldo;
                        }
                        // }
                    } elseif (empty($query) | $query == "null") {

                        // echo "aaa";
                        $data[] = 0;
                        $data[] =  0;
                        $data[] =  0;
                    }
                }

                $data2[] = $data;
            }
            $datass['data'] = $data2;
            // $datass['draw']=3;
            echo json_encode($datass);
        } else {
            $datass['data'] = '';
            echo json_encode($datass);
        }

    }
    
    
    public function get_data3()

    {
        $queri = '';
        for ($i = 1; $i <= 12; $i++) {
            $queri .= "
       (
       SELECT
           SUM(Nilai)
       FROM
           t_jurnalumum
       WHERE
           IdDebet = coa.m_coa_4_id and year(Tanggal)=2021 and month(Tanggal)=$i
   ) AS Nilai_debet$i,
   (
       SELECT
           SUM(Nilai)
       FROM
           t_jurnalumum
       WHERE
           IdKredit = coa.m_coa_4_id
   and year(Tanggal)=2021 and month(Tanggal)=$i) AS Nilai_kredit$i,
    (
       SELECT
           SUM(Nilai)
       FROM
           t_jurnalumum
       WHERE
           IdDebet = coa.m_coa_4_id and year(Tanggal)=2021 and month(Tanggal)=$i
   ) -
   (
       SELECT
           SUM(Nilai)
       FROM
           t_jurnalumum
       WHERE
           IdKredit = coa.m_coa_4_id and year(Tanggal)=2021 and month(Tanggal)=$i
   ) as total$i ,";
        }
        echo "SELECT $queri namacoa
        
        FROM
            coa";
        die();
        // echo $queri;
        $query = $this->grupmodel->query("SELECT $queri namacoa
        
    FROM
        coa");
        echo "<pre>";
        print_r($query);
        echo "/<pre>";
    }
    public function get_data2()
    {
        for ($i = 0; $i < 200000; $i++) {
            # code...

            $query = $this->grupmodel->query("INSERT INTO `t_jurnalumum` (`IdJurnal`, `idkascoa`, `IdUnit`, `NoTransaksi`, `Tanggal`, `no_trans`, `Rekening`, `Uraian`, `IdDebet`, `IdKredit`, `Nilai`, `Keterangan`, `IdUser`, `TglSave`, `NU`, `MTransaksi`, `NA`, `IdSup`, `jumlah`) VALUES (NULL, '3', '10', '15/SPM/Administrator/27/05/2021', '2021-05-12', '', '', '', '45', '3', '300000', '', '34', '2021-05-28 10:39:00', 'N', '', 'N', '8', '1');
        ");
        }
    }
}

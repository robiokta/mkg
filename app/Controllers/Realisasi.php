<?php

namespace App\Controllers;

use App\Models\Result_model;
use Irsyadulibad\DataTables\DataTables;
use App\Models\Grupmodel;

class Realisasi extends BaseController
{
	protected $model;
	protected $groppermi;
	protected $grupmodel;

	public function __construct()
	{
		$this->model = new Result_model();
		$this->grupmodel = new Grupmodel();
	}

	public function index()
	{
		$postModel = \Arifrh\DynaModel\DB::table('mak_4');
		$data['coa'] = $postModel->findAll();

		$data['title'] = 'Report Kasir';
		$data['page'] = 'Log Aktivitas Kasir';
		$data['user'] = 'Log Aktivitas';


		return view('realisasi/report', $data);
	}

	
	// public function report()
	// {
	// 	$awal = $this->request->getVar('tgl_awal');
	// 	$akhir = $this->request->getVar('tgl_akhir');
	// 	return DataTables::use('t_realisasi')
	// 		->select('no_pengajuan, nilai_realisasi,no_trans,tgl_realisasi,tgl_aksi')
	// 		->join('t_rinci', 't_realisasi.id_rinci=t_rinci.id_rinci')
	// 		->join('t_pengajuan', 't_rinci.id_pengajuan=t_pengajuan.id')
	// 		->where(['tgl_aksi >=' => date('Y-m-d', strtotime("$awal")), 'tgl_aksi <=' => date('Y-m-d', strtotime("$akhir"))])
	// 		// ->where(['role' => 'admin'])
	// 		// ->hideColumns(['password'])

	// 		->make(true);
	// }
	// public function filter_report()
	// {
	// 	$awal = $this->request->getVar('tgl_awal');
	// 	$akhir = $this->request->getVar('tgl_akhir');
	// 	return DataTables::use('t_realisasi')
	// 		->select('no_pengajuan, nilai_realisasi,no_trans,tgl_realisasi,tgl_aksi')
	// 		->join('t_rinci', 't_realisasi.id_rinci=t_rinci.id_rinci')
	// 		->join('t_pengajuan', 't_rinci.id_pengajuan=t_pengajuan.id')
	// 		->where(['tgl_aksi >=' => date('Y-m-d', strtotime("$awal")), 'tgl_aksi <=' => date('Y-m-d', strtotime("$akhir"))])
	// 		// ->hideColumns(['password'])

	// 		->make(true);
	// }
	public function get_rinci()
    {
        $id = $this->request->getVar("id");

        $data['rek_coa'] = $this->grupmodel->query("select sum(t_realisasi.nilai_realisasi) as realisasi, t_rinci.id_rinci as rinci, m_coa_4.nama_coa as nama_coa_4, kode_coa,kode_coa_2,kode_coa_3, kode_coa_4, jumlah, harga,total_harga from t_rinci join t_pengajuan on t_pengajuan.id=t_rinci.id_pengajuan join m_coa_4 on t_rinci.coa_4_id=m_coa_4.m_coa_4_id join m_coa_3 on m_coa_3.m_coa_3_id=m_coa_4.m_coa_3_id join m_coa_2 on m_coa_2.m_coa_2_id=m_coa_4.m_coa_2_id JOIN m_coa on m_coa.m_coa_id=m_coa_4.m_coa_1_id left join t_realisasi on t_rinci.id_rinci =t_realisasi.id_rinci where id_pengajuan=$id GROUP By rinci");
        $data['rek_mak'] = $this->grupmodel->query("select nama_mak_4, kode_mak_4, kode_mak_3, kode_mak_2, kode_mak_1 from t_pengajuan JOIN mak_4 on t_pengajuan.id_mak_4=mak_4.mak_4_id join mak_3 on mak_3.mak_3_id=mak_4.mak_3_id join mak_2 on mak_4.mak_2_id=mak_2.mak_2_id join mak_1 on mak_4.mak_1_id=mak_1.mak_1_id where id=$id");
        // $authors = DB::table('t_rinci');


        // $data = $authors->findBy(['id_pengajuan' => 42]);
        echo json_encode($data);
	}
	public function get_rincian()
    {
        $id = $this->request->getVar("id");

        $data['rek_coa'] = $this->grupmodel->query("select * from t_realisasi join m_supplier on m_supplier.id_supplier = t_realisasi.supplier join t_rinci on t_rinci.id_rinci=t_realisasi.id_rinci JOIN t_pengajuan on t_pengajuan.id=t_rinci.id_pengajuan where t_realisasi.id_rinci=$id");
       

        // $data = $authors->findBy(['id_pengajuan' => 42]);
        echo json_encode($data);
	}
	public function kasir()
    {
		if (in_groups('unit')) {
			
		
			$a='t_pengajuan.id,(SELECT SUM(total_harga) FROM t_rinci as tr WHERE tr.id_pengajuan=t_pengajuan.id) price,(SELECT SUM(nilai_realisasi) FROM t_realisasi as tre JOIN t_rinci as tri ON tri.id_rinci=tre.id_rinci WHERE tri.id_pengajuan= t_pengajuan.id ) spent ,id_kasir,no_pengajuan,judul,pengajuan,tgl_pengajuan,t_pengajuan.id as id_pengajuan, status_name,t_pengajuan.user_id,nama_divisi, t_pengajuan.status statuss';
			return DataTables::use("t_pengajuan")
				->select("$a")
				->join("t_status", "t_status.status_code=t_pengajuan.status")
				->join("t_kasir", 't_kasir.id_pengajuan=t_pengajuan.id')
				->join("users", 'users.id=t_pengajuan.user_id')
				->join("m_divisi", "m_divisi.id_divisi=users.id_divisi")
				// ->join("auth_groups", "auth_groups.id=auth_groups_users.group_id") 
				->join("t_rinci", "t_pengajuan.id = t_rinci.id_pengajuan","left")
				->join("t_realisasi", "t_realisasi.id_rinci = t_rinci.id_rinci","left")
            
            ->where(['t_pengajuan.id_divisi' => user()->id_divisi,'t_pengajuan.status_hapus'=>'ada']) 
			->make(true,'t_pengajuan.id');

			

		} else {
			return DataTables::use("t_pengajuan")
            ->select("t_pengajuan.id,(SELECT SUM(total_harga) FROM t_rinci as tr WHERE tr.id_pengajuan=t_pengajuan.id) price,(SELECT SUM(nilai_realisasi) FROM t_realisasi as tre JOIN t_rinci as tri ON tri.id_rinci=tre.id_rinci WHERE tri.id_pengajuan= t_pengajuan.id ) spent ,id_kasir,no_pengajuan,judul,pengajuan,tgl_pengajuan,t_pengajuan.id as id_pengajuan, status_name,t_pengajuan.user_id,nama_divisi, t_pengajuan.status statuss")
            ->join("t_status", "t_status.status_code=t_pengajuan.status")
            ->join("t_kasir", 't_kasir.id_pengajuan=t_pengajuan.id')
            ->join("users", 'users.id=t_pengajuan.user_id')
			->join("m_divisi", "m_divisi.id_divisi=users.id_divisi")
            ->join("t_rinci", "t_pengajuan.id = t_rinci.id_pengajuan","left")
            ->join("t_realisasi", "t_realisasi.id_rinci = t_rinci.id_rinci","left")
           
            ->where(['t_pengajuan.status_hapus'=>'ada'])
            // ->hideColumns(['password'])
            // left join t_rinci ON t_pengajuan.id = t_rinci.id_pengajuan left join t_realisasi on t_realisasi.id_rinci = t_rinci.id_rinci group by t_pengajuan.id

            ->make(true,'t_pengajuan.id');
		}
    }
}

// select d.price, s.spent,id_kasir,no_pengajuan,judul,pengajuan,tgl_pengajuan,t_pengajuan.id as id_pengajuan, status_name,t_pengajuan.user_id, t_pengajuan.status statuss from t_pengajuan join t_status on t_status.status_code=t_pengajuan.status join t_kasir on t_kasir.id_pengajuan=t_pengajuan.id JOIN ( SELECT id_pengajuan id,id_rinci, SUM(total_harga) price FROM t_rinci GROUP BY id ) d on d.id=t_pengajuan.id left JOIN ( SELECT b.id_pengajuan as pengajuann, t_realisasi.id_rinci as id, SUM(nilai_realisasi) spent FROM t_realisasi join t_rinci b on t_realisasi.id_rinci=b.id_rinci GROUP BY pengajuann ) s on s.id=d.id_rinci
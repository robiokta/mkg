<?php

namespace App\Controllers;


use App\Models\Grupmodel;
use App\Models\Gruppermit;
use Arifrh\DynaModel\DB;

class Master_supplier extends BaseController
{
    protected $grupmodel;
    protected $groppermi;
    private $menulink = 'm_menu';
    public function __construct()
    {
        $this->grupmodel = new Grupmodel();
        $this->gruppermit = new Gruppermit();
    }
    public function index()
    {
        $postModel = \Arifrh\DynaModel\DB::table('m_supplier');

        // dd($test);

        // then you can use it, to get all posts
        $data['supplier'] = $postModel->findAll();
        // echo $db->getLastQuery();
        // dd($data['users']);

        $data['title'] = 'Supplier';
        $data['page'] = 'Supplier';
        $data['user'] = 'Daftar Supplier';

        return view('supplier/supplist', $data);
    }
    public function addsupplier()
    {
        $data['title'] = 'Add Supplier';
        $data['page'] = 'Add Supplier';

        return view('supplier/addsupplier', $data);
    }
    public function supplier()
    {
        if ($this->request->isAJAX()) {

            $nama = $this->request->getVar('namasup');
            $no_tlp = $this->request->getVar('no_tlp');
            $alamat = $this->request->getVar('alamat');
            $tipe = $this->request->getVar('tipe');


            $authors = DB::table('m_supplier');

            $data = [
                'nama_supplier'   => $nama,
                'no_tlp'   => $no_tlp,
                'alamat'  => $alamat,
                'type' => $tipe

            ];

            $simpan = $authors->insert($data);
            if ($simpan) {
                // return $user->errors();
                $msg = [
                    'error' => 'sukses',
                    'msg' => 'Supplier ' . $this->request->getVar('namasup') . ' Berhasil Ditambahkan'
                ];
            } else {
            }
            echo json_encode($msg);
        } else {
            echo "anda tidak berhak mengakses halaman ini";
        }
    }
    public function editsup($id)
    {
        $data['title'] = 'Edit Supplier';
        $data['page'] = 'Edit Supplier';

        $authors = DB::table('m_supplier');

        $authors->find($id);
        $data['id'] = $id;

        $data['sup'] =  $authors->find($id);


        return view('supplier/editsup', $data);
    }
    public function action_edit()
    {
        if ($this->request->isAJAX()) {
            $posts = DB::table('m_supplier');
            $nama = $this->request->getVar('namasup');
            $no_tlp = $this->request->getVar('no_tlp');
            $alamat = $this->request->getVar('alamat');
            $id = $this->request->getVar('id');
            $tipe = $this->request->getVar('tipe');

            $update = $posts->updateBy(
                ['nama_supplier'   => $nama, 'no_tlp' => $no_tlp, 'alamat' => $alamat, 'type' => $tipe],
                ['id_supplier'   => $id],
            );

            if ($update) {
                $msg = [
                    'error' => 'sukses',
                    'msg' => 'Supplier ' . $this->request->getVar('namasup') . ' Berhasil Diubah'
                ];
            }


            echo json_encode($msg);
        }
    }
    public function delete_user()
    {
        $posts = DB::table('m_supplier');
        $delete = $posts->deleteBy([

            'id_supplier'     => $this->request->getVar('id')
        ]);
        if ($delete) {
            $msg = [
                'error' => 'sukses',
                'msg' => 'User '
            ];
        } else {
            $msg = [
                'error' => 'error',
                'msg' => 'Data Tidak Bisa DIhapus'
            ];
        }

        echo json_encode($msg);
    }
}

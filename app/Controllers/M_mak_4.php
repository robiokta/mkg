<?php

namespace App\Controllers;

use Myth\Auth\Models\UserModel;
use App\Models\Grupmodel;
use App\Models\Gruppermit;
use App\Models\Gruppermitl;
use Arifrh\DynaModel\DB;

class M_mak_4 extends BaseController
{
    protected $grupmodel;
    protected $groppermi;
    private $menulink = 'm_menu';
    public function __construct()
    {
        $this->grupmodel = new Grupmodel();
        $this->gruppermit = new Gruppermit();
    }

    public function index()
    {


        $postModel = \Arifrh\DynaModel\DB::table('mak_4');

        // dd($test);

        // then you can use it, to get all posts
        $data['coa'] = $this->grupmodel->query('select * from mak_4 join mak_1 on mak_4.mak_1_id=mak_1.mak_1_id join mak_2 on mak_2.mak_2_id=mak_4.mak_2_id join mak_3 on mak_3.mak_3_id = mak_4.mak_3_id' );


        // echo $db->getLastQuery();
        // dd($data['coa']);

        $data['title'] = 'Master MAK 4';
        $data['page'] = 'Master MAK 4';
        $data['user'] = 'Daftar MAK 4';

        return view('mak_4/mak_4', $data);
    }

    public function adddata()
    {
        $data['title'] = 'Tambah MAK 4';
        $data['page'] = 'Tambah MAK 4';
        $data['coa'] = $this->grupmodel->mak_1();

        return view('mak_4/adddata', $data);
    }

    public function pilihmak()
    {
        $coa1 = $this->request->getVar('coa_1');
        $coa2 = $this->request->getVar('coa_2');
        $coa3 = $this->request->getVar('coa_3');


        if (
            $coa1 != '' and $coa2 == ''

        ) {
            $authors = DB::table('mak_2');

            $data = $authors->findBy(['mak_1_id' => $coa1]);
            // $data     = $this->grupmodel->mak($coa1);
            foreach ($data as $val) :
                $a = $val['mak_2_id'];
                $b = $val['nama_mak_2'];
                $arrisi[] = "<option value='$a' >" . $val['kode_mak_2'] . " - $b</option>";


            endforeach;
            @$isi = join(" ", $arrisi);
            @$response_array['coa_2_hasil'] = "<option value=''>Kode Rek 2</option>
										" . $isi;
            @$response_array['kd'] = 2;
        } elseif ($coa1 != '' and $coa2 != '' and $coa3 == '') {


            $coa4 = DB::table('mak_3');
            $data = $coa4->findBy(['mak_2_id' => $coa2, 'mak_1_id' => $coa1]);
            // print_r($data);
            // die();

            foreach ($data as $val) :
                $a = $val['mak_3_id'];
                $b = $val['nama_mak_3'];
                $c = $val['kode_mak_3'];
                $arrisi[] = "<option value='$a' >$c - $b</option>";


            endforeach;
            @$isi = join(" ", $arrisi);
            @$response_array['coa_3_hasil'] = "<option value=''>COA 2</option>
										" . $isi;
            @$response_array['kd'] = 3;
        }


        echo json_encode($response_array);
    }
    public function action_add()
    {

        $nama_coa = $this->request->getVar('nama_mak');
        $coa1 = $this->request->getVar('mak_1_id');
        $coa2 = $this->request->getVar('mak_2_id');
        $coa3 = $this->request->getVar('mak_3_id');
        $kode = $this->request->getVar('kode_mak_4');

        $authors = DB::table('mak_4');

        $data = [
            'mak_1_id'   => $coa1,
            'mak_2_id'   => $coa2,
            'mak_3_id'   => $coa3,
            'kode_mak_4'  => $kode,
            'nama_mak_4' => $nama_coa,
        ];

        $simpan = $authors->insert($data);


        if ($simpan) {
            $msg = [
                'error' => 'sukses',
                'msg' => 'Input data berhasil '
            ];
        } else {
            $msg = [
                'error' => 'terjadi Error',
                'msg' => 'Silahkan coba lagi '
            ];
        }


        echo json_encode($msg);
    }
    public function edit($id)
    {
        $data['title'] = 'Ubah MAK 4';
        $data['page'] = 'Ubah Data';

        $authors = DB::table('mak_4');

        $coa = $authors->find($id);

        $data['coa'] = $coa;

        $data['coa1'] = $this->grupmodel->mak_1();

        $postModel = \Arifrh\DynaModel\DB::table('mak_2');

        $data['coa2'] = $postModel->findAll();
        $coa3 = DB::table('mak_3');
        $data['coa3'] = $coa3->findAll();

        $data['id'] = $id;

        return view('mak_4/edit', $data);
    }
    public function action_edit()
    {
        // if ($this->request->isAJAX()) {


        $nama_coa = $this->request->getVar('nama_mak');
        $coa1 = $this->request->getVar('mak_1_id');
        $coa2 = $this->request->getVar('mak_2_id');
        $coa3 = $this->request->getVar('mak_3_id');
        $kode = $this->request->getVar('kode_mak_4');
        $id = $this->request->getVar('id');

        $posts = DB::table('mak_4');

        $update = $posts->updateBy(
            ['nama_mak_4'   => $nama_coa, 'mak_1_id' => $coa1, 'mak_2_id' => $coa2, 'mak_3_id' => $coa3, 'kode_mak_4' => $kode],
            ['mak_4_id'   => $id],
        );
        if ($update == true) {
            $msg = [
                'error' => 'sukses',
                'msg' => $this->request->getVar('nama_mak') . ' Dirubah'
            ];
        }


        echo json_encode($msg);
        // }
    }
    public function delete()
    {


        // $delete = $this->grupmodel->delete_coa_2();


        $posts = DB::table('m_coa_4');
        $delete = $posts->deleteBy([

            'm_coa_4_id'     => $this->request->getVar('id')
        ]);

        if ($delete) {
            $msg = [
                'error' => 'sukses',
                'msg' => 'User '
            ];
        } else {
            $msg = [
                'error' => 'error',
                'msg' => 'Data Tidak Bisa Dihapus'
            ];
        }

        echo json_encode($msg);
    }
}

<?php

namespace App\Controllers;

use Myth\Auth\Models\UserModel;
use App\Models\Grupmodel;
use App\Models\Gruppermit;
use App\Models\Gruppermitl;
use Arifrh\DynaModel\DB;

class M_mak_3 extends BaseController
{
    protected $grupmodel;
    protected $groppermi;
    private $menulink = 'm_menu';
    public function __construct()
    {
        $this->grupmodel = new Grupmodel();
        $this->gruppermit = new Gruppermit();
    }

    public function index()
    {

        $data['coa'] = $this->grupmodel->query('select * from mak_3 join mak_1 on mak_3.mak_1_id=mak_1.mak_1_id join mak_2 on mak_2.mak_2_id=mak_3.mak_2_id' );

        // echo $db->getLastQuery();
        // dd($data['coa']);

        $data['title'] = 'Master MAK 3';
        $data['page'] = 'Master MAK 3';
        $data['user'] = 'Daftar MAK 3';

        return view('mak_3/mak_3', $data);
    }

    public function adddata()
    {
        $data['title'] = 'Tambah MAK 3';
        $data['page'] = 'Tambah MAK 3';
        $data['coa'] = $this->grupmodel->mak_1();

        return view('mak_3/adddata', $data);
    }
    public function pilihmak()
    {
        $coa1 = $this->request->getVar('coa_1');
        $coa2 = $this->request->getVar('coa_2');

        if (
            $coa1 != '' and $coa2 == ''

        ) {
            $authors = DB::table('mak_2');

            $data = $authors->findBy(['mak_1_id' => $coa1]);
            // $data     = $this->grupmodel->mak($coa1);
            foreach ($data as $val) :
                $a = $val['mak_2_id'];
                $b = $val['nama_mak_2'];
                $arrisi[] = "<option value='$a' >" . $val['kode_mak_2'] . " - $b</option>";


            endforeach;
            @$isi = join(" ", $arrisi);
            @$response_array['coa_2_hasil'] = "<option value=''>Kode Rek 2</option>
										" . $isi;
            @$response_array['kd'] = 2;
        }

        echo json_encode($response_array);
    }

    public function action_add()
    {

        $nama_coa = $this->request->getVar('nama_mak');
        $coa1 = $this->request->getVar('mak_1_id');
        $coa2 = $this->request->getVar('mak_2_id');
        $kode = $this->request->getVar('kode_mak_3');

        $authors = DB::table('mak_3');

        $data = [
            'mak_1_id'   => $coa1,
            'mak_2_id'   => $coa2,
            'kode_mak_3'  => $kode,
            'nama_mak_3' => $nama_coa,
        ];

        $simpan = $authors->insert($data);


        if ($simpan) {
            $msg = [
                'error' => 'sukses',
                'msg' => 'Input data berhasil '
            ];
        } else {
            $msg = [
                'error' => 'terjadi Error',
                'msg' => 'Silahkan coba lagi '
            ];
        }


        echo json_encode($msg);
    }
    public function edit($id)
    {
        $data['title'] = 'Ubah MAK';
        $data['page'] = 'Ubah Data';

        $authors = DB::table('mak_3');

        $coa = $authors->find($id);
        // dd($test);

        $data['coa'] = $coa;

        $data['coa1'] = $this->grupmodel->mak_1();
        $postModel = \Arifrh\DynaModel\DB::table('mak_2');

        $data['coa2'] = $postModel->findAll();

        $data['id'] = $id;

        return view('mak_3/edit', $data);
    }
    public function action_edit()
    {
        // if ($this->request->isAJAX()) {


        $nama_coa = $this->request->getVar('nama_mak');
        $coa1 = $this->request->getVar('mak_1_id');
        $coa2 = $this->request->getVar('mak_id_2');
        $kode = $this->request->getVar('kode_mak_3');
        $id = $this->request->getVar('id');

        $posts = DB::table('mak_3');

        $update = $posts->updateBy(
            ['nama_mak_3'   => $nama_coa, 'mak_1_id' => $coa1, 'mak_2_id' => $coa2, 'kode_mak_3' => $kode],
            ['mak_3_id'   => $id],
        );
        if ($update) {
            $msg = [
                'error' => 'sukses',
                'msg' => $this->request->getVar('nama_mak') . ' Diubah'
            ];
        }


        echo json_encode($msg);
        // }
    }
    public function delete()
    {
        $posts = DB::table('mak_3');
        $delete = $posts->deleteBy([

            'mak_3_id'     => $this->request->getVar('id')
        ]);

        if ($delete == true) {
            $msg = [
                'error' => 'sukses',
                'msg' => 'Berhasil Dihapus '
            ];
        } else {
            $msg = [
                'error' => 'error',
                'msg' => 'Data Tidak Bisa Dihapus'
            ];
        }

        echo json_encode($msg);
    }
}

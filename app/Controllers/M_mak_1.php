<?php

namespace App\Controllers;

use Myth\Auth\Models\UserModel;
use App\Models\Grupmodel;
use App\Models\Gruppermit;
use App\Models\Gruppermitl;
use Arifrh\DynaModel\DB;

class M_mak_1 extends BaseController
{
    protected $grupmodel;
    protected $groppermi;
    private $menulink = 'm_menu';
    public function __construct()
    {
        $this->grupmodel = new Grupmodel();
        $this->gruppermit = new Gruppermit();
    }
    public function index()
    {


        $data['coa'] = $this->grupmodel->mak_1();

        // echo $db->getLastQuery();
        // dd($data['pengajuan']);

        $data['title'] = 'Master MAK 1 ';
        $data['page'] = 'Master MAK 1';
        $data['user'] = 'Daftar MAK 1';

        return view('mak_1/mak_1', $data);
    }
    public function adddata()
    {
        $data['title'] = 'Insert MAK';
        $data['page'] = 'Tambah Master MAK';

        $grup = $this->grupmodel->findAll();
        $data['grup'] = $grup;

        return view('mak_1/adddata', $data);
    }
    public function action_add()
    {
        if ($this->request->isAJAX()) {

            $judul = $this->request->getVar('nama_mak');
            $kode = $this->request->getVar('kode_mak');
            $akses = $this->request->getVar('akses');

            $data = [
                'nama_mak' => $judul,
                'kode_mak_1' => $kode,
                'grup_user' => $akses

            ];
            $simpan = $this->grupmodel->insert_mak_1($data);

            if ($simpan) {
                $msg = [
                    'error' => 'sukses',
                    'msg' => 'Input data berhasil '
                ];
            } else {
                $msg = [
                    'error' => 'terjadi Error',
                    'msg' => 'Silahkan coba lagi '
                ];
            }


            echo json_encode($msg);
        }
    }
    public function edit($id)
    {
        $data['title'] = 'Ubah MAK';
        $data['page'] = 'Ubah Data';

        $grup = $this->grupmodel->findAll();
        $data['grup'] = $grup;

        $data['mak'] = $this->grupmodel->mak($id);

        $data['id'] = $id;

        return view('mak_1/edit', $data);
    }
    public function action_edit()
    {
        if ($this->request->isAJAX()) {


            $judul = $this->request->getVar('nama_mak');

            $id = $this->request->getVar('id');
            $kode = $this->request->getVar('kode_mak');
            $akses = $this->request->getVar('akses');


            $data = array(
                'nama_mak'  => $judul,
                'kode_mak_1' => $kode,
                'grup_user' => $akses

            );
            $update = $this->grupmodel->updatemak($data, $id);




            if ($update) {
                $msg = [
                    'error' => 'sukses',
                    'msg' => $this->request->getVar('nama_coa') . ' Dirubah'
                ];
            }


            echo json_encode($msg);
        }
    }
    public function delete()
    {
        $posts = DB::table('mak_1');
        $this->request->getVar('id');
        $delete = $posts->deleteBy([

            'mak_1_id'     => $this->request->getVar('id')
        ]);

        if ($delete) {
            $msg = [
                'error' => 'sukses',
                'msg' => 'User '
            ];
        } else {
            $msg = [
                'error' => 'error',
                'msg' => 'Data Tidak Bisa Dihapus'
            ];
        }

        echo json_encode($msg);
    }
}

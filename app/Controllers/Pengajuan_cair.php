<?php

namespace App\Controllers;

use Myth\Auth\Models\UserModel;
use App\Models\Grupmodel;
use App\Models\Gruppermit;
use Irsyadulibad\DataTables\DataTables;
use Arifrh\DynaModel\DB;

class Pengajuan_cair extends BaseController
{
    protected $grupmodel;
    protected $groppermi;
    private $menulink = 'm_menu';
    public function __construct()
    {
        $this->grupmodel = new Grupmodel();
        $this->gruppermit = new Gruppermit();
    }
    public function index()
    {
        $data['title'] = 'Pengajuan ';
        $data['page'] = 'Pengajuan';
        $data['user'] = 'Daftar Pengajuan';

        return view('kasir/cair', $data);
    }
    public function print($id)
    {
        $data['title'] = 'Ubah Pengajuan';
        $data['page'] = 'Ubah Data';

        $dataupdate = array(
            'status'  => 'KO01',

        );
        $update = $this->grupmodel->updatekasir($dataupdate, $id);
        $dataklik = [
            'tgl_klik' => date("Y")

        ];
        $simpan = $this->grupmodel->insert_sttk($dataklik);
        $data['sttk'] = $this->grupmodel->nosttk();

        // dd($update);
        $data['print'] = $this->grupmodel->kasir($id);

        foreach ($data['print'] as $key => $value) {
            $id_pengajuan = $value->id_pengajuan;
        }
        $data['id'] = $id;
        $data['coa'] = $this->grupmodel->query("SELECT kode_coa,kode_coa_2,kode_coa_3, kode_coa_4, jumlah, harga,total_harga from m_coa_4 JOIN m_coa_3 on m_coa_3.m_coa_3_id=m_coa_4.m_coa_3_id JOIN m_coa_2 on m_coa_2.m_coa_2_id=m_coa_4.m_coa_2_id JOIN m_coa on m_coa.m_coa_id=m_coa_4.m_coa_1_id LEFT JOIN t_rinci on t_rinci.coa_4_id=m_coa_4.m_coa_4_id where id_pengajuan=$id_pengajuan and  `m_coa_4`.`status` = 'active' and  `m_coa_3`.`status` = 'active' and  `m_coa_2`.`status` = 'active' and  `m_coa`.`status` = 'active' ");
        // print_r($data['coa']);
        // die();

        return view('kasir/print', $data);
    }

    public function pencairan()
    {

        $db      = \Config\Database::connect();
        $builder = $db->table('t_pengajuan');
        $builder->select('id_kasir,no_pengajuan,judul,pengajuan,tgl_pengajuan,id, status_name, user_id, t_pengajuan.status statuss');
        $builder->join('t_status', 't_status.status_code=t_pengajuan.status');
        $builder->join('t_kasir', 't_kasir.id_pengajuan=t_pengajuan.id');

        $builder->where('t_pengajuan.status', 'PO03');
        $builder->where('t_kasir.status is null');
        $builder->where('t_kasir.status is null');
        $builder->where('t_pengajuan.status_hapus','ada');

        $query   = $builder->get();



        $data['pengajuan'] = $query->getResult();

        // $db->getLastQuery();
        // dd($data['pengajuan']);

        $data['title'] = 'Pengajuan ';
        $data['page'] = 'Pengajuan';
        $data['user'] = 'Daftar Pengajuan';

        return view('kasir/kasir', $data);
    }
    public function kasir()
    {
        
        return DataTables::use("t_pengajuan")
            ->select("t_pengajuan.id,(SELECT SUM(total_harga) FROM t_rinci as tr WHERE tr.id_pengajuan=t_pengajuan.id) price,(SELECT SUM(nilai_realisasi) FROM t_realisasi as tre JOIN t_rinci as tri ON tri.id_rinci=tre.id_rinci WHERE tri.id_pengajuan= t_pengajuan.id ) spent ,id_kasir,no_pengajuan,judul,pengajuan,tgl_pengajuan,t_pengajuan.id as id_pengajuan, status_name,t_pengajuan.user_id,nama_divisi, t_pengajuan.status statuss")
            ->join("t_status", "t_status.status_code=t_pengajuan.status")
            ->join("t_kasir", 't_kasir.id_pengajuan=t_pengajuan.id')
            ->join("users", 'users.id=t_pengajuan.user_id')
            ->join("m_divisi", "m_divisi.id_divisi=users.id_divisi")
            // ->join("auth_groups", "auth_groups.id=auth_groups_users.group_id") 
            ->join("t_rinci", "t_pengajuan.id = t_rinci.id_pengajuan","left")
            ->join("t_realisasi", "t_realisasi.id_rinci = t_rinci.id_rinci","left")
           
            ->where(['status_hapus' => 'ada'])
            // ->hideColumns(['password'])
            // left join t_rinci ON t_pengajuan.id = t_rinci.id_pengajuan left join t_realisasi on t_realisasi.id_rinci = t_rinci.id_rinci group by t_pengajuan.id

            ->make(true,'t_pengajuan.id');
            
    }
    public function get_rinci()
    {
        $id = $this->request->getVar("id");

        $data['rek_coa'] = $this->grupmodel->query("select t_pengajuan.id as idpengajuan, sum(t_realisasi.nilai_realisasi) as realisasi, t_rinci.id_rinci as rinci, m_coa_4.nama_coa as nama_coa_4, kode_coa,kode_coa_2,kode_coa_3, kode_coa_4, jumlah, harga,total_harga from t_rinci join t_pengajuan on t_pengajuan.id=t_rinci.id_pengajuan join m_coa_4 on t_rinci.coa_4_id=m_coa_4.m_coa_4_id join m_coa_3 on m_coa_3.m_coa_3_id=m_coa_4.m_coa_3_id join m_coa_2 on m_coa_2.m_coa_2_id=m_coa_4.m_coa_2_id JOIN m_coa on m_coa.m_coa_id=m_coa_4.m_coa_1_id left join t_realisasi on t_rinci.id_rinci =t_realisasi.id_rinci where id_pengajuan=$id GROUP By rinci");
        $data['rek_mak'] = $this->grupmodel->query("select nama_mak_4, kode_mak_4, kode_mak_3, kode_mak_2, kode_mak_1 from t_pengajuan JOIN mak_4 on t_pengajuan.id_mak_4=mak_4.mak_4_id join mak_3 on mak_3.mak_3_id=mak_4.mak_3_id join mak_2 on mak_4.mak_2_id=mak_2.mak_2_id join mak_1 on mak_4.mak_1_id=mak_1.mak_1_id where id=$id");
        // $authors = DB::table('t_rinci');
        // $data = $authors->findBy(['id_pengajuan' => 42]);
        echo json_encode($data);
    }
    public function realisasi($id,$id_pengajuan)
    {
        $data['title'] = 'Realisasi';
        $data['page'] = 'Realisasi Anggaran';

        $data['id'] = $id;
        $postModel = \Arifrh\DynaModel\DB::table('m_supplier');
        
        $unit = 'SELECT * FROM `auth_groups` where name="unit"';

        $authors = DB::table('m_divisi');
        $data['unit']= $authors->findAll();

        $data['supplier'] = $postModel->findAll();
        $string1 = 'Select * from coa where kode_coa_1=1 and kode_coa_2= 1 ';
        $data['coa1'] = $this->grupmodel->query($string1);
        $posts = DB::table('t_pengajuan');
        $data['kas'] = $posts->find($id_pengajuan);
        $posts = DB::table('t_rinci');
        $data['rinci'] = $posts->find($id);
        return view('kasir/realisasi', $data);
    }
    public function action_add()
    {
        if ($this->request->isAJAX()) {
            $id_rinci = $this->request->getVar('id_rinci');
            $nilai = str_replace(".", "", $this->request->getVar('nilai'));
            $nilai=str_replace(",", ".", $nilai);
            $tgl = $this->request->getVar('tgl_trans');
            $kuitansi = $this->request->getVar('kuitansi');
            $sup = $this->request->getVar('supplier');
            $detail = $this->request->getVar('detail');

            $unit = $this->request->getVar('unit');
            // $tgl = $this->request->getVar('tgl');
            $idkas = $this->request->getVar('kas');
            $judul = $this->request->getVar('judul');
            $debet = $this->request->getVar('debit');
            $jmlh = $this->request->getVar('jumlah');
            $harga = $this->request->getVar('harga');
            $kredit = $this->request->getVar('kredit');


            $author = DB::table('t_jurnalumum');
            $data = [
                'IdUnit'   => $unit,
                'Tanggal'   => date('Y-m-d', strtotime($tgl)),
                'NoTransaksi'  => $kuitansi,
                'IdDebet'  => $debet,
                'IdKredit'  => $kredit,
                'Nilai'  => $nilai,
                // 'Rincian' => $rincian,
                'IdUser' => user_id(),
                // 'jumlah' => $jmlh[$i],
                'idkascoa' => $kredit,
                

                // 'type_kas' => 'kas_masuk',
                'IdSup'=>$sup
            ];
            $simpan1 = $author->insert($data);


            $authors = DB::table('t_realisasi');

            $data = [
                'id_rinci'   => $id_rinci,
                'nilai_realisasi'   => $nilai,
                'tgl_realisasi'  => date('Y-m-d', strtotime($tgl)),
                'no_trans' => $kuitansi,
                'detail' => $detail,
                'supplier' => $sup,
            ];

            $simpan = $authors->insert($data);


            if ($simpan) {
                $this->grupmodel->query("CALL `resetmvneraca_x`(@p0)");
                $this->grupmodel->query("CALL `resetmvneraca_y`(@p0)");
                $this->grupmodel->query("CALL `resetmvneraca`(@p0)");
                $msg = [
                    'error' => 'sukses',
                    'msg' => 'Input data berhasil '
                ];
            } else {
                $msg = [
                    'error' => 'terjadi Error',
                    'msg' => 'Silahkan coba lagi '
                ];
            }


            echo json_encode($msg);
        }
    }
}
// select d.id_rinci, s.id, d.price as price, s.spent as spent,id_kasir,no_pengajuan,judul,pengajuan,tgl_pengajuan,t_pengajuan.id as id_pengajuan, status_name,t_pengajuan.user_id,description, t_pengajuan.status statuss from t_pengajuan join
//             t_status on t_status.status_code=t_pengajuan.status
//             join t_kasir on t_kasir.id_pengajuan=t_pengajuan.id
//             join users on users.id=t_pengajuan.user_id
//             join auth_groups_users on auth_groups_users.user_id=users.id 
//             join auth_groups on auth_groups.id=auth_groups_users.group_id JOIN ( SELECT id_pengajuan id,id_rinci, SUM(total_harga) price FROM t_rinci  group by id) d on d.id=t_pengajuan.id left JOIN ( SELECT b.id_pengajuan as pengajuann, t_realisasi.id_rinci as id, SUM(nilai_realisasi) spent FROM t_realisasi join t_rinci b on t_realisasi.id_rinci=b.id_rinci group by id ) s on s.id=d.id_rinci
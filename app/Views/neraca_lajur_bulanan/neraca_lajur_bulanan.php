<?= $this->extend('templates/index'); ?>
<?= $this->section('konten'); ?>
<!-- <link href="/select2-bootstrap-theme/select2-bootstrap.min.css" type="text/css" rel="stylesheet" /> -->
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script src="//cdn.rawgit.com/ashl1/datatables-rowsgroup/v1.0.0/dataTables.rowsGroup.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <!-- <h1 class="h3 mb-4 text-gray-800"><?= $page ?></h1> -->

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"><?= $user ?></h6>
        </div>
        <div class="card-body">
            <div autocomplete="off" class="form-inline">

                <div class="form-group sm-2">
                    <label for="debet" class="sr-only">Pilih coa</label>
                    <select required class="custom-select my-1 mr-sm-2 debet select2" name="debet" id="tahun">
                        <option></option>
                        <?php
                        $year = date('Y');
                        $add = $year - 2020;
                        $min = 2018 + $add;
                        $max = $min + 10;
                        for ($i = $min; $i <= $max; $i++) {
                            echo '<option value=' . $i . '>' . $i . '</option>';
                        } ?>
                        <option value="2019">Level 3</option>
                    </select>

                </div>
                &nbsp;

                <div class="form-group mx-sm-5 sm-2">
                    <label for="level" class="sr-only">Level</label>
                    <select required class="custom-select my-1 mr-sm-5 debet select2" name="level" id="level">
                        <option></option>
                        <option value="6">Level 1</option>
                        <option value="2">Level 2</option>
                        <option value="3">Level 3</option>
                        <option value="4">Level 4</option>
                        <!-- <option value="5">Level 5</option> -->

                    </select>

                </div>
                <button type="button" class="btn btn-primary btn-sm btncari">Cari</button>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered table-hovered table-sm" id="table">
                    <thead>
                        <tr class="bg-blue text-center">
                            <!-- <th rowspan="2">No</th> -->
                            <th rowspan="2">Kode</th>
                            <th rowspan="2">Nama Rekening</th>
                            <th colspan="3">Januari</th>
                            <th colspan="3">Februari</th>
                            <th colspan="3">Maret</th>
                            <th colspan="3">April</th>
                            <th colspan="3">Mei</th>
                            <th colspan="3">Juni</th>
                            <th colspan="3">Juli</th>
                            <th colspan="3">Agustus</th>
                            <th colspan="3">September</th>
                            <th colspan="3">Oktober</th>
                            <th colspan="3">November</th>
                            <th colspan="3">Desember</th>
                        </tr>
                        <tr class="bg-blue text-center">
                            <th>Debet</th>
                            <th>Kredit</th>
                            <th>Saldo</th>
                            <th>Debet</th>
                            <th>Kredit</th>
                            <th>Saldo</th>
                            <th>Debet</th>
                            <th>Kredit</th>
                            <th>Saldo</th>
                            <th>Debet</th>
                            <th>Kredit</th>
                            <th>Saldo</th>
                            <th>Debet</th>
                            <th>Kredit</th>
                            <th>Saldo</th>
                            <th>Debet</th>
                            <th>Kredit</th>
                            <th>Saldo</th>
                            <th>Debet</th>
                            <th>Kredit</th>
                            <th>Saldo</th>
                            <th>Debet</th>
                            <th>Kredit</th>
                            <th>Saldo</th>
                            <th>Debet</th>
                            <th>Kredit</th>
                            <th>Saldo</th>
                            <th>Debet</th>
                            <th>Kredit</th>
                            <th>Saldo</th>
                            <th>Debet</th>
                            <th>Kredit</th>
                            <th>Saldo</th>
                            <th>Debet</th>
                            <th>Kredit</th>
                            <th>Saldo</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                    <tfoot>
                        <tr>
                            <th colspan="2" style="text-align:right">Total:</th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
        <!-- Large modal -->


    </div>
    <script>
        // $('.debet').select2({
        //     placeholder: "Pilih Debet",
        //     allowClear: true,
        //     theme: 'bootstrap4',
        $('.select2bs4').select2({
            theme: 'bootstrap4'
        });

        // function datatable() {
        $('#datatable').DataTable().destroy()
        var table = $('#table').DataTable({
            // "responsive": true,

            // "order": [],
            "paging": false,
            "order": [[ 0, "desc" ]],
            "info": false,
            "ordering": false,
            "processing": true,
            "serverSide": true,

            "pageLength": 15,
            "lengthMenu": [15, 25, 50, 100, 200, 500, 1000, 10000],
            "ajax": {
                "url": "<?= base_url() ?>/neraca_lajur_bulanan/get_data1",
                "type": "POST",
                "data": function(d) {
                    d.level = $('#level').val();
                    d.tahun = $('#tahun').val();
                }

            },

            "language": {

                "sLengthMenu": "_MENU_ &nbsp;&nbsp;Data Per Halaman &nbsp;&nbsp;",
            },
            "columns": [{
                "className": "text-center",
            }, {
                "className": "text-left dt-body-nowrap",
            }, {
                "className": "text-right",
                render: $.fn.dataTable.render.number(",", ".", 2)
            }, {
                "className": "text-right",
                render: $.fn.dataTable.render.number(",", ".", 2)
            }, {
                "className": "text-right bg-realisasi",
                render: $.fn.dataTable.render.number(",", ".", 2)
            }, {
                "className": "text-right",
                render: $.fn.dataTable.render.number(",", ".", 2)
            }, {
                "className": "text-right",
                render: $.fn.dataTable.render.number(",", ".", 2)
            }, {
                "className": "text-right bg-realisasi",
                render: $.fn.dataTable.render.number(",", ".", 2)
            }, {
                "className": "text-right",
                render: $.fn.dataTable.render.number(",", ".", 2)
            }, {
                "className": "text-right",
                render: $.fn.dataTable.render.number(",", ".", 2)
            }, {
                "className": "text-right bg-realisasi",
                render: $.fn.dataTable.render.number(",", ".", 2)
            }, {
                "className": "text-right",
                render: $.fn.dataTable.render.number(",", ".", 2)
            }, {
                "className": "text-right",
                render: $.fn.dataTable.render.number(",", ".", 2)
            }, {
                "className": "text-right bg-realisasi",
                render: $.fn.dataTable.render.number(",", ".", 2)
            }, {
                "className": "text-right",
                render: $.fn.dataTable.render.number(",", ".", 2)
            }, {
                "className": "text-right",
                render: $.fn.dataTable.render.number(",", ".", 2)
            }, {
                "className": "text-right bg-realisasi",
                render: $.fn.dataTable.render.number(",", ".", 2)
            }, {
                "className": "text-right",
                render: $.fn.dataTable.render.number(",", ".", 2)
            }, {
                "className": "text-right",
                render: $.fn.dataTable.render.number(",", ".", 2)
            }, {
                "className": "text-right bg-realisasi",
                render: $.fn.dataTable.render.number(",", ".", 2)
            }, {
                "className": "text-right",
                render: $.fn.dataTable.render.number(",", ".", 2)
            }, {
                "className": "text-right",
                render: $.fn.dataTable.render.number(",", ".", 2)
            }, {
                "className": "text-right bg-realisasi",
                render: $.fn.dataTable.render.number(",", ".", 2)
            }, {
                "className": "text-right",
                render: $.fn.dataTable.render.number(",", ".", 2)
            }, {
                "className": "text-right",
                render: $.fn.dataTable.render.number(",", ".", 2)
            }, {
                "className": "text-right bg-realisasi",
                render: $.fn.dataTable.render.number(",", ".", 2)
            }, {
                "className": "text-right",
                render: $.fn.dataTable.render.number(",", ".", 2)
            }, {
                "className": "text-right",
                render: $.fn.dataTable.render.number(",", ".", 2)
            }, {
                "className": "text-right bg-realisasi",
                render: $.fn.dataTable.render.number(",", ".", 2)
            }, {
                "className": "text-right",
                render: $.fn.dataTable.render.number(",", ".", 2)
            }, {
                "className": "text-right",
                render: $.fn.dataTable.render.number(",", ".", 2)
            }, {
                "className": "text-right bg-realisasi",
                render: $.fn.dataTable.render.number(",", ".", 2)
            }, {
                "className": "text-right",
                render: $.fn.dataTable.render.number(",", ".", 2)
            }, {
                "className": "text-right",
                render: $.fn.dataTable.render.number(",", ".", 2)
            }, {
                "className": "text-right bg-realisasi",
                render: $.fn.dataTable.render.number(",", ".", 2)
            }, {
                "className": "text-right",
                render: $.fn.dataTable.render.number(",", ".", 2)
            }, {
                "className": "text-right",
                render: $.fn.dataTable.render.number(",", ".", 2)
            }, {
                "className": "text-right bg-realisasi",
                render: $.fn.dataTable.render.number(",", ".", 2)
            }, ],
            // "fnRowCallback" : function(nRow, aData, iDisplayIndex){
            //     $("td:first", nRow).html(iDisplayIndex +1);
            //    return nRow;
            // },

            "footerCallback": function(row, data, start, end, display) {
                var api = this.api(),
                    data;
                // Remove the formatting to get integer data for summation
                var intVal = function(i) {
                    return typeof i === 'string' ?
                        // i.replace(/[\$,.]/g, '') * 1 :
                        //i.replace(/(<font[^>]+>|<li>|<\/font>|[\$,.])/g, '') * 1 :
                        i.replace(/[\$,]/g, '') * 1 :

                        // i.replace(/(<[^>]+>|<[^>]>|<\/[^>]>)/g, '') * 1 :
                        typeof i === 'number' ?
                        i : 0;
                };

                // Total over all pagestotal1 = api.column(4).data().reduce(function(a, b) { return intVal(a) + intVal(b);}, 0);
                total0 = api.column(2).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total1 = api.column(3).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total2 = api.column(4).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total3 = api.column(5).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total4 = api.column(6).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total5 = api.column(7).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total6 = api.column(8).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total7 = api.column(9).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total8 = api.column(10).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total9 = api.column(11).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total10 = api.column(12).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total11 = api.column(13).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total12 = api.column(14).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total13 = api.column(15).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total14 = api.column(16).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total15 = api.column(17).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total16 = api.column(18).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total17 = api.column(19).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total18 = api.column(20).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total19 = api.column(21).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total20 = api.column(22).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total21 = api.column(23).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total22 = api.column(24).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total23 = api.column(25).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total24 = api.column(26).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total25 = api.column(27).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total26 = api.column(28).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total27 = api.column(29).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total28 = api.column(30).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total29 = api.column(31).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total30 = api.column(32).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total31 = api.column(33).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total32 = api.column(34).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total33 = api.column(35).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total34 = api.column(36).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total35 = api.column(37).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);


                // Update footer
                $(api.column(2).footer()).html(formatter.format(total0));
                $(api.column(3).footer()).html(formatter.format(total1));
                $(api.column(4).footer()).html(formatter.format(total2));
                $(api.column(5).footer()).html(formatter.format(total3));
                $(api.column(6).footer()).html(formatter.format(total4));
                $(api.column(7).footer()).html(formatter.format(total5));
                $(api.column(8).footer()).html(formatter.format(total6));
                $(api.column(9).footer()).html(formatter.format(total7));
                $(api.column(10).footer()).html(formatter.format(total8));
                $(api.column(11).footer()).html(formatter.format(total9));
                $(api.column(12).footer()).html(formatter.format(total10));
                $(api.column(13).footer()).html(formatter.format(total11));
                $(api.column(14).footer()).html(formatter.format(total12));
                $(api.column(15).footer()).html(formatter.format(total13));
                $(api.column(16).footer()).html(formatter.format(total14));
                $(api.column(17).footer()).html(formatter.format(total15));
                $(api.column(18).footer()).html(formatter.format(total16));
                $(api.column(19).footer()).html(formatter.format(total17));
                $(api.column(20).footer()).html(formatter.format(total18));
                $(api.column(21).footer()).html(formatter.format(total19));
                $(api.column(22).footer()).html(formatter.format(total20));
                $(api.column(23).footer()).html(formatter.format(total21));
                $(api.column(24).footer()).html(formatter.format(total22));
                $(api.column(25).footer()).html(formatter.format(total23));
                $(api.column(26).footer()).html(formatter.format(total24));
                $(api.column(27).footer()).html(formatter.format(total25));
                $(api.column(28).footer()).html(formatter.format(total26));
                $(api.column(29).footer()).html(formatter.format(total27));
                $(api.column(30).footer()).html(formatter.format(total28));
                $(api.column(31).footer()).html(formatter.format(total29));
                $(api.column(32).footer()).html(formatter.format(total30));
                $(api.column(33).footer()).html(formatter.format(total31));
                $(api.column(34).footer()).html(formatter.format(total32));
                $(api.column(35).footer()).html(formatter.format(total33));
                $(api.column(36).footer()).html(formatter.format(total34));
                $(api.column(37).footer()).html(formatter.format(total35));
            },
            "rowCallback": function(row, data, index) {
                if ((data[2] == 0 | data[2] == "-") & (data[3] == 0 | data[3] == "-") & (data[5] == 0 | data[5] == "-") & (data[6] == 0 | data[6] == "-") & (data[8] == 0 | data[8] == "-") & (data[9] == 0 | data[9] == "-") & (data[11] == 0 | data[11] == "-") & (data[12] == 0 | data[12] == "-") & (data[14] == 0 | data[14] == "-") & (data[15] == 0 | data[15] == "-") & (data[17] == 0 | data[17] == "-") & (data[18] == 0 | data[18] == "-") & (data[20] == 0 | data[20] == "-") & (data[21] == 0 | data[21] == "-") & (data[23] == 0 | data[23] == "-") & (data[24] == 0 | data[24] == "-") & (data[26] == 0 | data[26] == "-") & (data[27] == 0 | data[27] == "-") & (data[29] == 0 | data[29] == "-") & (data[30] == 0 | data[30] == "-") & (data[32] == 0 | data[32] == "-") & (data[33] == 0 | data[33] == "-") & (data[35] == 0 | data[35] == "-") & (data[36] == 0 | data[36] == "-")) {
                    $('td', row).remove();

                }
            },
            "columnDefs": [{
                // "targets": [0, 3],
                "orderable": false
            }],
            "order": [
                [1, "desc"]
            ]
        });
        // }
        // table.on('draw.dt', function() {
        //     var info = table.page.info();
        //     table.column(0, {
        //         search: 'applied',
        //         order: 'applied',
        //         page: 'applied'
        //     }).nodes().each(function(cell, i) {
        //         cell.innerHTML = i + 1 + info.start;
        //     });
        // });

        $('.btncari').click(function() {
            // alert('lll');
            // console.log($('#tahun').val())

            // table.draw();
            // datatable();
            table.ajax.reload();
        })

        $(".datepicker").datepicker();

        var formatter = new Intl.NumberFormat('id-ID', {
  style: 'currency',
  currency: 'IDR',

  
});

        $('#tahun').select2({
            placeholder: "Pilih Tahun",
            allowClear: true,
            theme: 'bootstrap4',
        });

        $('#level').select2({
            placeholder: "Pilih Level",
            allowClear: true,
            theme: 'bootstrap4',
        });
    </script>
    <!-- /.container-fluid -->
    <?= $this->endSection(); ?>
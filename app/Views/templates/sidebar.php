<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?=base_url()?>">
        <div class="sidebar-brand-icon">
           <img src="<?=base_url()?>/img/logopt.png" width="60" height="60">
        </div>
        <div class="sidebar-brand-text mx-3"style="text-align: left;">Mentari Karya Gemilang</div>
    </a>



    <?php if (in_groups('admin')|| in_groups('kasir')||in_groups('menku')) {
        echo category_menu();
        echo category_menu2();
    } else {
        

        echo category_menu2();
    }

    ?>





    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
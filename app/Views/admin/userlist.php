<?= $this->extend('templates/index'); ?>
<?= $this->section('konten'); ?>

<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $page ?></h1>

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"><?= $user ?></h6>
        </div>
        <div class="card-body">
            <a class="btn btn-success float-right" href="<?= base_url('userlist/adduser') ?>" role="button"><i class="fas fa-user-plus"></i></a>

            <table class="table table-striped table-hover dt-responsive display nowrap" cellspacing="0" id="datatable">
                <thead>
                    <tr>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($users as $user) : ?>
                        <tr>
                            <td><?= $user->username ?></td>
                            <td><?= $user->email ?></td>
                            <td><?= $user->name ?></td>
                            <td>
                                <a href="<?= base_url('userlist/edituser/' . $user->userid) ?>" class="btn btn-primary active">
                                    <i class="fas fa-edit"></i>
                                </a>
                                <a class="btn btn-danger active delete" id="<?= $user->userid ?>">
                                    <i class="far fa-trash-alt"></i>
                                </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>

                </tbody>
            </table>
        </div>
    </div>




</div>
<script>
    $(document).ready(function() {
        $('#datatable').DataTable({
            autoWidth: false,
            responsive: true
        });

    });

    $(document).on('click', '.delete', function() {
        var $button = $(this);
        var table = $('#datatable').DataTable();
        Swal.fire({
            title: 'Apakah Anda Yakin?',
            text: "Data Tidak Bisa Dikembalikan Lagi",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya!'
        }).then((result) => {

            if (result.isConfirmed) {
                $.ajax({
                    type: "post",
                    url: "<?= base_url('userlist/delete_user') ?>",
                    data: {
                        id: $(this).attr('id')
                    },
                    dataType: "json",
                    success: function(response) {
                        if (response.error === 'error') {
                            Swal.fire({
                                icon: 'error',
                                title: 'gagal',
                                text: response.msg,

                            })
                        }
                        if (response.error === 'sukses') {
                            table.row( $button.parents('tr') ).remove().draw();
                            
                            
                            Swal.fire(
                                'Berhasil!',
                                'Data Berhasil Dihapus.',
                                'success'
                            )
                        }

                    }
                    // error: function (xhr, ajaxOptions, thrownError) {

                })



            }
        })
    });
</script>
<!-- /.container-fluid -->
<?= $this->endSection(); ?>
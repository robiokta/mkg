<?= $this->extend('templates/formindex'); ?>
<?= $this->section('konten'); ?>

<!-- Begin Page Content -->
<div class="container">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">Tambah User</h1>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"><?= @$user ?></h6>
        </div>
        <div class="card-body">
            <form action="<?= base_url() ?>/userlist/user" class="adduser" method="post">
                <?= csrf_field(); ?>
                <div class="form-row">
                    <div class="form-group col-lg-6">
                        <label for="inputEmail4">Email</label>
                        <input name="email" type="email" autofocus class="form-control" id="inputEmail4" placeholder="Email">

                    </div>
                    <div class="form-group col-lg-6">
                        <label for="username">User Name</label>
                        <input name="username" type="text" class="form-control" id="username" placeholder="Username" required>
                    </div>
                    <div class="form-group col-lg-6">
                        <label for="fulname">Nama Lengkap</label>
                        <input type="text" class="form-control" id="fullname" placeholder="Nama Lengkap" name="fullname" required>
                    </div>
                    <div class="form-group col-lg-6">
                        <label for="select">Divisi</label>
                        <select required class="custom-select my-1 mr-sm-2" name="divisi" id="divisi">
                            <option>Pilih Divisi</option>
                            <?php foreach ($divisi as $div) : ?>
                                <option value="<?= $div['id_divisi'] ?>"><?= $div['nama_divisi'] ?></option>

                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group col-lg-6">
                        <label for="inputPassword4">Password</label>
                        <input required type="password" class="form-control" name="password" id="inputPassword4" placeholder="Password">
                    </div>
                    <div class="form-group col-lg-6">
                        <label for="select">Hak Akses</label>
                        <select required class="custom-select my-1 mr-sm-2" name="gropus" id="select">
                            <option>Pilih Hak akses</option>
                            <?php foreach ($grup as $groups) : ?>
                                <option value="<?= $groups['id'] ?>"><?= $groups['description'] ?></option>

                            <?php endforeach; ?>
                        </select>
                    </div>
                    <br>

                    <div class="col text-center">
                        <button type="submit" class="btn btn-outline-success btnsubmit">Kirim</button>
                        <a href="<?= base_url('userlist') ?>" class="btn btn-outline-danger btnsubmit">Kembali</a>
                    </div>

                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('.adduser').submit(function(e) {
            e.preventDefault();
            $.ajax({
                type: "post",
                url: $(this).attr('action'),
                data: $(this).serialize(),
                dataType: "json",
                beforeSend: function() {
                    $('.btnsubmit').attr('disable', 'disabled')

                    $('.btnsubmit').html('<i class="fa fa-spin fa-spinner"</i>')
                },
                complete: function() {
                    $('.btnsubmit').removeAttr('disable')

                    $('.btnsubmit').html('simpan')
                },
                success: function(response) {
                    if (response.error === 'error') {
                        alertify.set('notifier', 'position', 'top-right');
                        var msg = alertify.error(response.msg, 0);
                        $('body').one('click', function() {
                            msg.dismiss();
                        });
                    }
                    if (response.error === 'sukses') {
                        alertify.set('notifier', 'position', 'top-right');
                        var msg = alertify.success(response.msg, 0);
                        setTimeout(function() {
                            window.location.href = "<?= base_url() ?>/userlist";
                        }, 1000)
                      


                    }
                },
                // error: function (xhr, ajaxOptions, thrownError) {
                //         alert(xhr.status+"\n"+xhr.responseText+"\n"+thrownError);

                //     }
            })
        })
    })
</script>
<!-- /.container-fluid -->
<?= $this->endSection(); ?>
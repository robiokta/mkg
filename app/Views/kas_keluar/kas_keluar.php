<?= $this->extend('templates/index'); ?>
<?= $this->section('konten'); ?>
<script src="//cdn.rawgit.com/ashl1/datatables-rowsgroup/v1.0.0/dataTables.rowsGroup.js"></script>

<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $page ?></h1>

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"><?= $user ?></h6>
        </div>
        <div class="card-body">
            <?php if (in_groups('admin') | in_groups('menku')| in_groups('kasir')) {

            ?>
                <a class="btn btn-success float-right" href="<?= base_url('kas_keluar/adddata') ?>" role="button"><i class="fas fa-plus"></i></a>
            <?php } ?>
            <div class="table-responsive">
                <table class="table datatable table-hover" cellspacing="0" id="datatable">
                    <thead>
                        <tr>
                            <th>Tanggal</th>
                            <th>Rekening Debit</th>
                            <th>Debit</th>
                            <th>Rekening Kredit</th>
                            <th>Kredit</th>
                            <!-- <th>Ket</th> -->
                            <th>User</th>
                            <?php if (in_groups('admin') | in_groups('menku')| in_groups('kasir')) {

                                echo " <th>Aksi</th>";
                            }
                            ?>
                            <!-- <th>Supplier</th> -->
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($datas as $val) : ?>
                            <tr>
                                <td><?= $val->Tanggal ?> &nbsp;<sup><?="<br />". $val->NoTransaksi ?></sup> </td>
                                <td><?= $val->coa1 . $val->coa2 . $val->coa3 . $val->coa4 . " " . $val->namacoa4 ?></td>
                                <td><?= $val->Nilai  ?> </td>
                                <td><?= $val->kd_coa_1 . $val->kd_coa_2 . $val->kd_coa_3 . $val->kd_coa_4 . " " . $val->namacoa ?></td>
                                <td><?= $val->Nilai ?></td>
                                <td><?= $val->nama_divisi ?></td>
                                <?php if (in_groups('admin') | in_groups('menku')| in_groups('kasir')) { ?>

                                    <td>
                                        <a href="<?= base_url('kas_keluar/edit/' . $val->IdJurnal) ?>" class="btn btn-sm btn-primary active" data-toggle="tooltip" data-placement="top" title="Edit">
                                            <i class="fas fa-edit"></i>
                                        </a>
                                        <a class="btn btn-danger btn-sm active delete" data-toggle="tooltip" data-placement="top" title="Hapus" id="<?= $val->IdJurnal ?>">
                                            <i class="far fa-trash-alt"></i>
                                        </a>
                                    </td>
                                <?php } ?>
                            </tr>

                        <?php endforeach; ?>

                    </tbody>
                </table>
            </div>
        </div>

    </div>

</div>
<script>
    $(document).ready(function() {

        $('#datatable').DataTable({
            autoWidth: false,
            "lengthMenu": [
                [10, 25, 50, -1],
                [10, 25, 50, "All"]
            ],
            'columnDefs': [{
                "targets": 0, // your case first column
                "className": "text-center",
                "width": "7%"

            }],
            columns: [{
                    name: 'first',
                    title: 'Tanggal',
                },
                {
                    name: 'second',
                    title: 'Rekening Debit',
                },
                {
                    name: 'user',
                    render: $.fn.dataTable.render.number(".", ".", 2, 'Rp. ')

                },
                {
                    title: 'Rekening Kredit',

                },
                {
                    title: 'Kredit',
                    render: $.fn.dataTable.render.number(".", ".", 2, 'Rp. ')
                },
                {
                    title: 'Keterangan',
                },
                <?php if (in_groups('admin') | in_groups('menku')| in_groups('kasir')) {
                ?> {
                        title: 'Aksi',
                    },
                <?php } ?>

            ],
            // rowsGroup: [
            //     'first:name',
            //     // 'supplier:name'
            //     'user:name'

            // ],

        });

    });


    $(document).on('click', '.delete', function() {
        var $button = $(this);
        var table = $('#datatable').DataTable();
        Swal.fire({
            title: 'Apakah Anda Yakin?',
            text: "Data Tidak Bisa Dikembalikan Lagi",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya!'
        }).then((result) => {

            if (result.isConfirmed) {
                $.ajax({
                    type: "post",
                    url: "<?= base_url('kas_keluar/delete') ?>",
                    data: {
                        id: $(this).attr('id')
                    },
                    dataType: "json",
                    success: function(response) {
                        if (response.error === 'error') {
                            Swal.fire({
                                icon: 'error',
                                title: 'gagal',
                                text: response.msg,

                            })
                        }
                        if (response.error === 'sukses') {
                            table.row($button.parents('tr')).remove().draw();
                            Swal.fire(
                                'Berhasil!',
                                'Data Berhasil Dihapus.',
                                'success'
                            )
                        }

                    }
                    // error: function (xhr, ajaxOptions, thrownError) {

                })
            }
        })
    });
</script>
<!-- /.container-fluid -->
<?= $this->endSection(); ?>
<?= $this->extend('templates/formindex'); ?>
<?= $this->section('konten'); ?>

<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!-- Begin Page Content -->
<div class="container">
    <!-- Page Heading -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"><?= $page ?></h6>
        </div>
        <div class="card-body">
            <form action="<?= base_url() ?>/kas_keluar/action_edit" class="add" enctype="multipart/form-data" method="post">
            <input type="hidden" name="id" value="<?=$id?>">
                <?= csrf_field(); ?>
                <div class="form-row">
                <div class="form-group col-lg-4">
                        <label for="kuitansi">No. Bukti Transaksi/No. Kuitansi</label>
                        <input required name="kuitansi" value="<?=$kas['NoTransaksi']?>" type="text" autofocus class="form-control" id="kuitansi" placeholder="No. Bukti Transaksi/ Kuitansi">

                    </div>

                    <!-- <div class="form-group col-lg-4">
                        <label for="mak">Pilih Kas</label>
                        <select required class="custom-select my-1 mr-sm-2 select2" name="kas" id="mak">
                            <option></option>
                            <?php foreach ($coa as $key => $val) : ?>
                                <option <?= ($kas['idkascoa'] == $val->m_coa_4_id) ? " selected" : "" ?> value="<?= $val->m_coa_4_id ?>"><?= $val->namacoa ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div> -->
                    <div class="form-group col-lg-4">
                        <label for="unit">Unit</label>
                        <select required class="custom-select my-1 mr-sm-2 select2" name="unit" id="unit">
                            <option></option>
                            <?php foreach ($unit as $val) : ?>
                                <option <?= ($kas['IdUnit'] == $val['id_divisi']) ? " selected" : "" ?> value="<?= $val['id_divisi'] ?>"><?= $val['nama_divisi'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group col-lg-6">
                        <label for="judul">Tanggal</label>
                        <input name="tgl" type="text" value="<?=date_format( new DateTime($kas['Tanggal']),"m/d/Y");?>" class="form-control datepicker" id="tgl" autocomplete="off" placeholder="Tanggal">

                    </div>
                    <div class="form-group col-lg-12" id="input_fields_wrap">
                        <div class="form-row">
                            <div class="form-group col-lg-6">
                                <label for="debet">Debet</label>
                                <select required class="custom-select my-1 mr-sm-2 coa debet select2" name="debet" id="debet">
                                    <option></option>
                                    <?php foreach ($coa1 as $key => $val) : ?>
                                        <option <?=($kas['IdDebet']==$val->m_coa_4_id)?" Selected":""?> value="<?= $val->m_coa_4_id ?>"><?= $val->namacoa ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <br>
                            </div>
                            <div class="form-group col-lg-6">
                                <label for="kredit">Kredit</label>
                                <select required class="custom-select my-1 mr-sm-2 coa kredit select2" name="kredit" id="kredit">
                                    <option></option>
                                    <?php foreach ($coa2 as $key => $val) : ?>
                                        <option <?=($kas['IdKredit']==$val->m_coa_4_id)?" Selected":""?> value="<?= $val->m_coa_4_id ?>"><?= $val->namacoa ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <br>
                            </div>
                        </div>
                        <div class="form-row penjumlahan">
                            <div class="form-group col-lg-6">
                                <label for="jumlah">Jumlah</label>
                                <input name="jumlah" value="<?=$kas['jumlah']?>" type="number" autofocus class="form-control jumlah" id="Jumlah" placeholder="jumlah">
                            </div>
                            <div class="form-group col-lg-6">
                                <label for="harga">Harga Satuan</label>
                                <input name="harga"  value="<?=number_format($kas['Nilai']/$kas['jumlah'], 2, ',', '.')?>"type="text" onkeyup="rupiah(this)" class="form-control dana" id="harga" placeholder="Harga">
                            </div>

                        </div>

                        <!-- <button type="button" id="btn" class="btn btn-outline-success input_fields_wrap"><i class="fas fa-plus"></i></button>
                        <button type="button" id="hapus_tombol" class="btn btn-outline-danger input_fields_wrap"><i class="fas fa-minus"></i></button> -->
                    </div>

                    <div class="form-group col-lg-12">
                        <label for="dana">Total</label>
                        <input name="dana" type="text" class="form-control" id="dana" onchange="rupiah(this)" disabled placeholder="Total Anggaran." required>
                    </div>

                    <br>

                    <div class="col text-center">
                        <button type="submit" class="btn btn-outline-success btnsubmit">Kirim</button>
                        <a href="<?= base_url('kas_keluar') ?>" class="btn btn-outline-danger btnsubmit">Kembali</a>
                    </div>

                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $(".datepicker").datepicker({

        });
        $('#mak').select2({
            placeholder: "Pilih Kas",
            allowClear: true,
            theme: 'bootstrap4',
        });
        $('#unit').select2({
            placeholder: "Pilih Unit",
            allowClear: true,
            theme: 'bootstrap4',
        });
        $('.debet').select2({
            placeholder: "Pilih Debet",
            allowClear: true,
            theme: 'bootstrap4',
        });

        $('.kredit').select2({
            placeholder: "Pilih Kredit",
            allowClear: true,
            theme: 'bootstrap4',
        });
        $('.add').submit(function(e) {
            e.preventDefault();
            var datas = $('.add').serialize();
            $.ajax({
                type: "post",
                url: $(this).attr('action'),
                data: datas,
                dataType: "json",
                beforeSend: function() {
                    $('.btnsubmit').attr('disable', 'disabled')

                    $('.btnsubmit').html('<i class="fa fa-spin fa-spinner"</i>')
                },
                complete: function() {
                    $('.btnsubmit').removeAttr('disable')

                    $('.btnsubmit').html('simpan')
                },
                success: function(response) {
                    if (response.error === 'error') {
                        alertify.set('notifier', 'position', 'top-right');
                        var msg = alertify.error(response.msg, 0);
                        $('body').one('click', function() {
                            msg.dismiss();
                        });
                    }
                    if (response.error === 'sukses') {
                        alertify.set('notifier', 'position', 'top-right');
                        var msg = alertify.success(response.msg, 0);
                        setTimeout(function() {
                            window.location.href = "<?= base_url() ?>/kas_keluar";
                        }, 1000)

                    }
                },

            })
        })
    })
    $(document).on("change blur", ".dana", function() {
        var dana = 0
        var satuan = 0
        var hasil = 0

        $("div.penjumlahan").each(function() {
            // get the values from this div:
            var val1 = $('.dana', this).val().split('.').join("")
            val1=val1.split(',').join(".")
            var val2 = $('.jumlah', this).val();
            var total = (val1 * 1) * (val2 * 1)
            hasil += total

        });
        hasil = format(hasil)
        $('#dana').val(hasil);

    });
    $(document).on("change blur", ".jumlah", function() {
        var dana = 0
        var satuan = 0
        var hasil = 0

        $("div.penjumlahan").each(function() {
            // get the values from this div:
            var val1 = $('.dana', this).val().split('.').join("")
            val1=val1.split(',').join(".")

            var val2 = $('.jumlah', this).val();

            var total = (val1 * 1) * (val2 * 1)
            hasil += total

        });
        hasil = format(hasil)
        $('#dana').val(hasil);

    });
    function rupiah(e) {
        // i am spammy!
        e.value = formatRupiah(e.value, 'Rp. ');
    }

    function format(n, sep, decimals) {
        sep = sep || "."; // Default to period as decimal separator
        decimals = decimals || 2; // Default to 2 decimals

        return n.toLocaleString().split(sep)[0] +
            sep +
            n.toFixed(decimals).split(sep)[1];
    }

    /* Fungsi formatRupiah */
    function formatRupiah(angka, prefix) {
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
            split = number_string.split(','),
            sisa = split[0].length % 3,
            rupiah = split[0].substr(0, sisa),
            ribuan = split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
    }
</script>
<!-- /.container-fluid -->
<?= $this->endSection(); ?>
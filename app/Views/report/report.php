<?= $this->extend('templates/index'); ?>
<?= $this->section('konten'); ?>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $page ?></h1>

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"><?= $user ?></h6>
        </div>
        <div class="card-body">
            <form autocomplete="off" class="form-inline">
                <div class="form-group mb-2">
                    <label for="tgl_awal" class="sr-only">Tanggal Awal</label>
                    <input type="text" name="tgl_awal" class="form-control datepicker" placeholder="Tanggal Awal" id="tgl_awal">
                </div>
                <div class="form-group mx-sm-3 mb-2">
                    <label for="tgl_akhir" class="sr-only">Tanggal Akhir</label>
                    <input type="text" class="form-control datepicker" id="tgl_akhir" placeholder="Tanggal Akhir">
                </div>
                <button type="button" class="btn btn-primary mb-2 btncari">Cari</button>
            </form>
            <div class="table-responsive">
                <table class="table table-striped table-hover" style="display:none" cellspacing="0" id="datatable">
                    <thead>
                        <tr>
                            <th></th>
                            <th>No Pengajuan</th>
                            <th>No Bukti</th>
                            <th>Tanggal</th>
                            <th>Supplier</th>
                            <th>Nilai Realisasi</th>
                            
                            <!-- <th>Detail</th> -->
                            

                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th colspan="5" style="text-align:right">Total:</th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

</div>
<script>
    $(".datepicker").datepicker();

    function getNumberWithCommas(number) {
        return  formatter = new Intl.NumberFormat('id-ID', {
  style: 'currency',
  currency: 'IDR',

  
}).format(number);
    }
    $(document).ready(function() {
        $('.btncari').on('click', function() {
            $('#datatable').show()
            $('#datatable').DataTable().destroy()
            // $('#datatable').empty()
            var table = $('#datatable').DataTable({
                processing: true,
                serverSide: true,

                ajax: {
                    url: '<?= base_url() ?>/report_kasir/filter_report',
                    "data": {
                        "tgl_awal": $('#tgl_awal').val(),
                        "tgl_akhir": $('#tgl_akhir').val()
                    }
                },
                "language": {
                    lengthMenu: "Tampilkan _MENU_ Data Per Halaman",
                    zeroRecords: "Data Tidak Ditemukan",
                    info: "Menampilkan _PAGE_ dari _PAGES_",
                    infoEmpty: "Data Tidak ditemukan",
                    infoFiltered: "(Penyaringan _MAX_ total data)",
                    "search": "Cari",
                    "paginate": {
                        "next": "Berikutnya",
                        "previous": "Sebelumnya"
                    }
                },
                "footerCallback": function(row, data, start, end, display) {
                    var api = this.api(),
                        data;

                    // Remove the formatting to get integer data for summation
                    var intVal = function(i) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '') * 1 :
                            typeof i === 'number' ?
                            i : 0;
                    };

                    // Total over all pages
                    total = api
                        .column(5)
                        .data()
                        .reduce(function(a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                    // Total over this page
                    pageTotal = api
                        .column(5, {
                            page: 'current'
                        })
                        .data()
                        .reduce(function(a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                    // Update footer
                    $(api.column(5).footer()).html(
                        $.fn.dataTable.render.number('.', '.', '2', 'Rp.').display(pageTotal)
                    );

                },
                columns: [{
                        "searchable": false,
                        "orderable": false,
                        "targets": 0,
                        "orderable": false,
                        "data": null,
                        "defaultContent": ''
                    },
                    {

                        data: 'no_pengajuan',
                        name: 'no_pengajuan'
                    }, {
                        data: 'no_trans',
                        name: 'no_trans'
                    },

                    {
                        data: 'tgl_realisasi',
                        name: 'tgl_realisasi'
                    },
                   
                    {
                        data: 'nama_supplier',
                        name: 'nama_supplier'
                    },
                    {
                        data: 'nilai_realisasi',
                        name: 'nilai_realisasi',
                        width: '20%',
                        render: $.fn.dataTable.render.number(".", ".", 2, 'Rp. ')


                    },

                ],

                "order": [
                    [1, 'asc']
                ],
                createdRow: function(row, datas, index) {
                    if (datas.extn === '') {
                        var td = $(row).find("td:first");
                        td.removeClass('details-control');
                    }
                },
                rowCallback: function(row, datas, index) {

                    //console.log('rowCallback');
                }
            });

            table.on('order.dt search.dt draw.dt', function() {
                table.column(0, {
                    search: 'applied',
                    order: 'applied',
                    draw: 'applied'
                }).nodes().each(function(cell, i) {
                    cell.innerHTML = i + 1;
                });
            }).draw();

        });

    });
</script>
<!-- /.container-fluid -->
<?= $this->endSection(); ?>
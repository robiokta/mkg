<?= $this->extend('templates/index'); ?>
<?= $this->section('konten'); ?>
<!-- <link href="/select2-bootstrap-theme/select2-bootstrap.min.css" type="text/css" rel="stylesheet" /> -->
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script src="//cdn.rawgit.com/ashl1/datatables-rowsgroup/v1.0.0/dataTables.rowsGroup.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css">

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>


<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <!-- <h1 class="h3 mb-4 text-gray-800"><?= $page ?></h1> -->

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"><?= $user ?></h6>
        </div>
        <div class="card-body">
            <?php if (in_groups('admin') | in_groups('menku')) {

            ?>
                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target=".bd-example-modal-xl">Report Saldo</button>

                <a class="btn btn-success float-right btn-sm" href="<?= base_url('jurnal_umum/adddata') ?>" role="button"><i class="fas fa-plus"></i></a>
            <?php } ?>
            <div class="table-responsive">
                <table class="table datatable table-hover" cellspacing="0" id="datatable">
                    <thead>
                        <tr>
                            <th>Tanggal</th>
                            <th>Kode Rekening</th>

                            <th>Debit</th>
                            <th>Kredit</th>
                            <th>Ket</th>
                            <th>Supplier</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($datas as $val) : ?>
                            <tr>
                                <td class="rowss"><?= $val->Tanggal ?> &nbsp;<sup><?= "<br/>" . $val->NoTransaksi ?></sup> </td>
                                <td><?= $val->coa1 . $val->coa2 . $val->coa3 . $val->coa4 . " " . $val->namacoa4 ?></td>

                                <td><?= $val->Nilai ?> </td>
                                <td>-</td>
                                <td><?= $val->Keterangan ?></td>
                                <td><?= $val->nama_supplier . '-' . $val->alamat . '-' . $val->no_tlp ?></td>
                            </tr>
                            <tr>
                                <td><?= $val->Tanggal ?> &nbsp;<sup><?= "<br/>" . $val->NoTransaksi ?></sup></td>
                                <td><?= $val->kd_coa_1 . $val->kd_coa_2 . $val->kd_coa_3 . $val->kd_coa_4 . " " . $val->namacoa ?></td>

                                <td>-</td>
                                <td><?= $val->Nilai ?></td>
                                <td></td>
                                <td></td>

                            </tr>

                        <?php endforeach; ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- Large modal -->

    <div class="modal fade bd-example-modal-xl" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Report Saldo</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form autocomplete="off" class="form-inline">

                    <div class="form-group sm-2">
                        <label for="debet" class="sr-only">Pilih coa</label>
                        <select required class="custom-select my-1 mr-sm-2 debet select2" name="debet" id="coa">
                            <option></option>
                            <?php foreach ($coa as  $val) : ?>
                                <option value="<?= $val['m_coa_4_id'] ?>"><?= $val['namacoa'] ?></option>
                            <?php endforeach; ?>
                        </select>

                    </div>
                    &nbsp;
                    <div class="form-group sm-2">
                        <label for="tgl_awal" class="sr-only">Tanggal Awal</label>
                        <input type="text" name="tgl_awal" class="form-control datepicker" placeholder="Tanggal Awal" id="tgl_awal">
                    </div>
                    <div class="form-group mx-sm-3 sm-2">
                        <label for="tgl_akhir" class="sr-only">Tanggal Akhir</label>
                        <input type="text" class="form-control datepicker" id="tgl_akhir" placeholder="Tanggal Akhir">
                    </div>
                    <button type="button" class="btn btn-primary btn-sm btncari">Cari</button>
                </form>
                <div class="table-responsive">
                    <table class="table" id="caridata">
                        <thead>
                            <tr>
                                <th>Rekening</th>
                                <th>Nilai Debit</th>
                                <th>Nilai Kredit</th>
                                <th>Saldo</th>

                            </tr>
                        </thead>
                        <tbody></tbody>
                        <tfoot></tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>
<script>
    // $('.debet').select2({
    //     placeholder: "Pilih Debet",
    //     allowClear: true,
    //     theme: 'bootstrap4',
    // });
    $('body').on('shown.bs.modal', '.modal', function() {
        $(this).find('select').each(function() {
            var dropdownParent = $(document.body);
            if ($(this).parents('.modal.in:first').length !== 0)
                dropdownParent = $(this).parents('.modal.in:first');
            $(this).select2({
                dropdownParent: dropdownParent,
                placeholder: "Pilih COA",
                allowClear: true,
                theme: 'bootstrap4',
            });
        });
    });


    $(".datepicker").datepicker();
    $(document).ready(function() {
        $('.btncari').on('click', function() {

            $('#caridata').show()
            $('#caridata').DataTable().destroy()
            $('#caridata').DataTable({
                // "processing": true,
                // "serverSide": true,
                "ajax": {
                    "url": "<?= base_url() ?>/jurnal_umum/report_saldo",
                    "type": "POST",
                    "data": {
                        "tgl_awal": $('#tgl_awal').val(),
                        "tgl_akhir": $('#tgl_akhir').val(),
                        "coa_id": $('#coa').val(),
                    }
                },
                "columns": [{
                        "data": null,
                        name: null,
                        render: function(data, type, row) {
                            return row.namacoa + "<sup>" + row.kode_coa_1 + row.kode_coa_2 + row.kode_coa_3 + row.kode_coa_4 + "</sup>";
                        }
                    },
                    {
                        "data": "nilai_debet",
                        render: $.fn.dataTable.render.number(".", ".", 2, 'Rp. ')

                    },
                    {
                        "data": "nilai_kredit",
                        render: $.fn.dataTable.render.number(".", ".", 2, 'Rp. ')

                    },
                    {
                        data: null,
                        name: 'spent',
                        render: function(data, type, row) {
                            var a = row.nilai_debet - row.nilai_kredit
                            return $.fn.dataTable.render.number(".", ".", 2, 'Rp. ').display(a)
                            // if (data == null) {
                            //     return 'Rp. 0'

                            // } else {
                            //     return $.fn.dataTable.render.number(".", ".", 0, 'Rp. ').display(data)
                            // }
                        },
                    },

                ]
            });
        });


    });
    $(document).ready(function() {

        $('#datatable').DataTable({
            autoWidth: false,
            "bInfo": false,
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ],

            'columnDefs': [{
                "targets": 0, // your case first column
                "className": "text-center",
                "width": "7%"

            }],
            columns: [{
                    name: 'first',
                    title: 'Tanggal',
                },
                {
                    name: 'second',
                    title: 'Rekening',
                },

                {
                    title: 'Debit',
                    render: $.fn.dataTable.render.number(".", ",", 2, 'Rp. ')
                },
                {
                    title: 'Kredit',
                    render: $.fn.dataTable.render.number(".", ",", 2, 'Rp. ')
                },
                {
                    title: 'Keterangan',
                },
                {
                    name: 'supplier',

                },
            ],
            rowsGroup: [
                'first:name'

            ],

        });

    });

    $(document).on('click', '.delete', function() {
        var $button = $(this);
        var table = $('#datatable').DataTable();
        Swal.fire({
            title: 'Apakah Anda Yakin?',
            text: "Data Tidak Bisa Dikembalikan Lagi",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya!'
        }).then((result) => {

            if (result.isConfirmed) {
                $.ajax({
                    type: "post",
                    url: "<?= base_url('pengajuan/delete_pengajuan') ?>",
                    data: {
                        id: $(this).attr('id')
                    },
                    dataType: "json",
                    success: function(response) {
                        if (response.error === 'error') {
                            Swal.fire({
                                icon: 'error',
                                title: 'gagal',
                                text: response.msg,

                            })
                        }
                        if (response.error === 'sukses') {
                            table.row($button.parents('tr')).remove().draw();
                            Swal.fire(
                                'Berhasil!',
                                'Data Berhasil Dihapus.',
                                'success'
                            )
                        }

                    }
                    // error: function (xhr, ajaxOptions, thrownError) {

                })
            }
        })
    });
</script>
<!-- /.container-fluid -->
<?= $this->endSection(); ?>
<?= $this->extend('templates/formindex'); ?>
<?= $this->section('konten'); ?>
<style>
    .ck-editor__editable_inline {
        min-height: 300px;
    }
</style>

<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>


<!-- Begin Page Content -->
<div class="container">

    <!-- Page Heading -->

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"><?= $page ?></h6>
        </div>
        <div class="card-body">
            <form action="<?= base_url() ?>/jurnal_umum/action_add" class="add" enctype="multipart/form-data" method="post">
                <?= csrf_field(); ?>
                <div class="form-row">
                    <div class="form-group col-lg-4">
                        <label for="kuitansi">No. Bukti Transaksi/No. Kuitansi</label>
                        <input required name="kuitansi" type="text" autofocus class="form-control" id="kuitansi" placeholder="No. Bukti Transaksi/ Kuitansi">

                    </div>
                    <div class="form-group col-lg-4">
                        <label for="mak">Pilih Unit</label>
                        <select required class="custom-select my-1 mr-sm-2 select2" name="unit" id="mak">
                            <option></option>
                            <?php foreach ($coa as $key => $val) : ?>
                                <option value="<?= $val['id_divisi'] ?>"><?= $val['nama_divisi'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>

                    <div class="form-group col-lg-4">
                        <label for="tgl">Tanggal Transaksi</label>
                        <input name="tgltrans" autocomplete="off" type="text" autofocus class="form-control datepicker" id="tgl" placeholder="Tanggal Transaksi">

                    </div>
                    <div class="form-group col-lg-4">
                        <label for="coa">Rekening Debet</label>
                        <select required class="custom-select my-1 mr-sm-2 coa select2" name="debet" id="debet">
                            <option></option>
                            <?php foreach ($coa1 as $key => $val) : ?>
                                <option value="<?= $val->m_coa_4_id ?>"><?= $val->namacoa ?></option>
                            <?php endforeach; ?>
                        </select>

                    </div>
                    <div class="form-group col-lg-4">
                        <label for="coa">Rekening Kredit</label>
                        <select required class="custom-select my-1 mr-sm-2 coa select2" name="kredit" id="kredit">
                            <option></option>
                            <?php foreach ($coa1 as $key => $val) : ?>
                                <option value="<?= $val->m_coa_4_id ?>"><?= $val->namacoa ?></option>
                            <?php endforeach; ?>
                        </select>

                    </div>

                    <div class="form-group col-lg-4">
                        <label for="dana">Nilai</label>
                        <input name="dana" type="text" autocomplete="off" class="form-control" id="rupiah" placeholder="Nilai " required>
                    </div>
                    <div class="form-group col-lg-4">
                        <label for="typesup"> Tipe Suplier</label>
                        <select required class="custom-select my-1 mr-sm-2 coa select2" name="typesup" id="typesup" onchange="ajax()">
                            <option></option>
                            <?php foreach ($sup as $key => $vall) : ?>
                                <option value="<?= $vall->type ?>"><?= $vall->type ?></option>
                            <?php endforeach; ?>
                        </select>

                    </div>
                    <div class="form-group col-lg-4">
                        <label for="sup">Suplier</label>
                        <select required class="custom-select my-1 mr-sm-2 coa select2" name="sup" id="sup">

                        </select>

                    </div>
                    <div class="form-group col-lg-12 ">
                        <label for="rincian">Keterangan</label>
                        <textarea name="rincian" class="form-control" id="rincian" style="height: 100px; width:500px;"></textarea>
                    </div>

                    <br>

                    <div class="col text-center">
                        <button type="submit" class="btn btn-outline-success btnsubmit">Kirim</button>
                        <a href="<?= base_url('jurnal_umum') ?>" class="btn btn-outline-danger btnsubmit">Kembali</a>
                    </div>

                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $(".datepicker").datepicker({

    });
    var rupiah = document.getElementById('rupiah');
    rupiah.addEventListener('keyup', function(e) {
        // tambahkan 'Rp.' pada saat form di ketik
        // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
        rupiah.value = formatRupiah(this.value, 'Rp. ');
    });

    /* Fungsi formatRupiah */
    function formatRupiah(angka, prefix) {
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
            split = number_string.split(','),
            sisa = split[0].length % 3,
            rupiah = split[0].substr(0, sisa),
            ribuan = split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
    }
    $(document).ready(function() {
        $('.add').submit(function(e) {
            e.preventDefault();
            var datas = $('.add').serialize();
            $.ajax({
                type: "post",
                url: $(this).attr('action'),
                data: datas,
                dataType: "json",
                beforeSend: function() {
                    $('.btnsubmit').attr('disable', 'disabled')

                    $('.btnsubmit').html('<i class="fa fa-spin fa-spinner"</i>')
                },
                complete: function() {
                    $('.btnsubmit').removeAttr('disable')

                    $('.btnsubmit').html('simpan')
                },
                success: function(response) {
                    if (response.error === 'error') {
                        alertify.set('notifier', 'position', 'top-right');
                        var msg = alertify.error(response.msg, 0);
                        $('body').one('click', function() {
                            msg.dismiss();
                        });
                    }
                    if (response.error === 'sukses') {
                        alertify.set('notifier', 'position', 'top-right');
                        var msg = alertify.success(response.msg, 0);
                        setTimeout(function() {
                            window.location.href = "<?= base_url() ?>/jurnal_umum";
                        }, 1000)

                    }
                },

            })
        })
    })

    ClassicEditor
        .create(document.querySelector('#editor'))
        .catch(error => {
            console.error(error);
        });
    $(document).ready(function() {
        $('#mak').select2({
            placeholder: "Pilih Unit",
            allowClear: true,
            theme: 'bootstrap4',
        });
        $('#debet').select2({
            placeholder: "Pilih Debet",
            allowClear: true,
            theme: 'bootstrap4',
        });
        $('#kredit').select2({
            placeholder: "Pilih Kredit",
            allowClear: true,
            theme: 'bootstrap4',
        });
        $('#typesup').select2({
            placeholder: "Pilih Tipe Supplier",
            allowClear: true,
            theme: 'bootstrap4',
        });
        $('#sup').select2({
            placeholder: "Pilih Supplier",
            allowClear: true,
            theme: 'bootstrap4',
        });

    });

    function ajax() {
        var sup = $('#typesup').val();
        // var coa_2 = $('#coa2').val();
        $.ajax({
            url: '<?= base_url('jurnal_umum') ?>/supplier',
            method: 'post',
            dataType: 'json',
            data: {
                sup: sup


            },
            success: function(data) {
                if (data.kd == 2) {
                    $("#sup").html(data.coa_2_hasil);

                }
            }
        });
    }
</script>
<!-- /.container-fluid -->
<?= $this->endSection(); ?>
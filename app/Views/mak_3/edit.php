<?= $this->extend('templates/formindex'); ?>
<?= $this->section('konten'); ?>
<style>
    .ck-editor__editable_inline {
        min-height: 300px;
    }
</style>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>



<!-- Begin Page Content -->
<div class="container">

    <!-- Page Heading -->

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"><?= $page ?></h6>
        </div>

        <div class="card-body">
            <form action="<?= base_url() ?>/m_mak_3/action_edit" class="add" method="post">
                <?= csrf_field(); ?>

                <div class="form-row">
                    <div class="form-group col-lg-6">
                        <label for="select">Pilih MAK 1</label>
                        <select required class="custom-select my-1 mr-sm-2 select2" name="mak_1_id" id="select" onchange="ajax()">
                            <option></option>
                            <?php foreach ($coa1 as $key => $val) : ?>
                                <option value="<?= $val->mak_1_id ?>" <?= ($val->mak_1_id == $coa['mak_1_id'] ? ' selected' : '') ?>><?= $val->kode_mak_1 . ' - ' . $val->nama_mak ?></option>

                            <?php endforeach; ?>
                        </select>
                        <input type="hidden" value="<?= $id ?>" name="id">
                    </div>
                    <div class="form-group col-lg-6">
                        <label for="coa2">Pilih MAK 2</label>
                        <select required class="custom-select my-1 mr-sm-2" name="mak_id_2" id="coa2">
                            <option></option>
                            <?php foreach ($coa2 as $val) : ?>
                                <option value="<?= $val['mak_2_id'] ?>" <?= ($val['mak_2_id'] == $coa['mak_2_id'] ? ' selected' : '') ?>><?= $val['kode_mak_2'] . ' - ' . $val['nama_mak_2'] ?></option>

                            <?php endforeach; ?>
                        </select>
                    </div>

                    <div class="form-group col-lg-6">
                        <label for="judul">NAMA MAK 3</label>
                        <input name="nama_mak" type="text" autofocus class="form-control clear" id="coa1" placeholder="Nama MAK 3" value="<?= $coa['nama_mak_3'] ?>">

                    </div>
                    <div class="form-group col-lg-6">
                        <label for="judul">KODE MAK 3</label>
                        <input name="kode_mak_3" type="text" autofocus class="form-control clear" id="coa1" placeholder="Kode MAK 3" value="<?= $coa['kode_mak_3'] ?>">

                    </div>


                    <br>

                    <div class="col text-center">
                        <div id="tampil"></div>
                        <button type="submit" class="btn btn-outline-success btnsubmit">Kirim</button>
                        <a href="<?= base_url('m_mak_3') ?>" class="btn btn-outline-danger btnsubmit">Kembali</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {

        $("#select").change(function() {
            $('#coa2').val(null).trigger('change');
            $('#tampil').empty()
            $('.clear').val('');

        });

        $("#coa2").change(function() {
            $('#tampil').empty()

            $('.clear').val('');

        });
        $("#coa1").focus(function() {
            if ($("#coa2").val() !=='' & $("#select").val() !=='') {
                var data = $("#select option:selected").text();
                data = parseInt(data);
                var data1 = $("#coa2 option:selected").text();
                data1 = parseInt(data1);
                $('#tampil').append('kode MAK 1 dan 2 yang dipilih ' + data + ' ' + data1)

            }
        });

        $('.add').submit(function(e) {
            e.preventDefault();
            $.ajax({
                type: "post",
                url: $(this).attr('action'),
                data: $(this).serialize(),
                dataType: "json",
                beforeSend: function() {
                    $('.btnsubmit').attr('disable', 'disabled')

                    $('.btnsubmit').html('<i class="fa fa-spin fa-spinner"</i>')
                },
                complete: function() {
                    $('.btnsubmit').removeAttr('disable')

                    $('.btnsubmit').html('simpan')
                },
                success: function(response) {
                    if (response.error === 'error') {
                        alertify.set('notifier', 'position', 'top-right');
                        var msg = alertify.error(response.msg, 0);
                        $('body').one('click', function() {
                            msg.dismiss();
                        });
                    }
                    if (response.error === 'sukses') {
                        alertify.set('notifier', 'position', 'top-right');
                        var msg = alertify.success(response.msg, 0);
                        setTimeout(function() {
                            window.location.href = "<?= base_url() ?>/m_mak_3";
                        }, 1000)





                    }
                },
                // error: function (xhr, ajaxOptions, thrownError) {
                //         alert(xhr.status+"\n"+xhr.responseText+"\n"+thrownError);

                //     }
            })
        })
    })

    function ajax() {

        var coa_1 = $('#select').val();
        var coa_2 = $('#coa2').val();
        $.ajax({
            url: '<?= base_url('m_mak_3') ?>/pilihmak',
            method: 'post',
            dataType: 'json',
            data: {
                coa_1: coa_1,
                coa_2: coa_2,

            },
            success: function(data) {
                if (data.kd == 2) {
                    $("#coa2").html(data.coa_2_hasil);

                } else if (data.kd == 3) {
                    $("#kd_rek_3_hasil").html(data.kd_rek_3_hasil);

                } else if (data.kd == 4) {
                    $("#nm_usaha").val(data.hasil_nama);

                }
            }
        });
    }
    jQuery(document).ready(function() {
        $('.select2').select2({
            placeholder: "Pilih MAK 1",
            allowClear: true
        });
        $("#coa2").select2({
            placeholder: "Pilih MAK 2",
            allowClear: true
        });
        $("#select").change(function() {
            $('#coa2').val(null).trigger('change');


        });

        $("#coa2").change(function() {
            $('.clear').val('');

        });

    })
</script>
<!-- /.container-fluid -->
<?= $this->endSection(); ?>
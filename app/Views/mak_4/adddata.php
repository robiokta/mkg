<?= $this->extend('templates/formindex'); ?>
<?= $this->section('konten'); ?>
<style>
    .ck-editor__editable_inline {
        min-height: 300px;
    }
</style>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>


<!-- Begin Page Content -->
<div class="container">

    <!-- Page Heading -->

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"><?= $page ?></h6>
        </div>
        <div class="card-body">
            <form action="<?= base_url() ?>/m_mak_4/action_add" class="add" method="post">
                <?= csrf_field(); ?>
                <div class="form-row">
                    <div class="form-group col-lg-12">
                        <label for="select">Pilih MAK 1</label>
                        <select required class="custom-select my-1 mr-sm-2 select2" name="mak_1_id" id="select" onchange="ajax()">
                            <option></option>
                            <?php foreach ($coa as $key => $val) : ?>
                                <option value="<?= $val->mak_1_id ?>"><?= $val->kode_mak_1 . " - " . $val->nama_mak ?></option>

                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group col-lg-6">
                        <label for="coa2">Pilih MAK 2</label>
                        <select onchange="ajax()" required class="custom-select my-1 mr-sm-2" name="mak_2_id" id="coa2">
                            <option></option>

                        </select>
                    </div>
                    <div class="form-group col-lg-6">
                        <label for="coa2">Pilih MAK 3</label>
                        <select required class="custom-select my-1 mr-sm-2" name="mak_3_id" id="coa3">
                            <option></option>

                        </select>
                    </div>

                    <div class="form-group col-lg-6">
                        <label for="judul">NAMA MAK 4</label>
                        <input name="nama_mak" type="text" autofocus class="form-control clear" id="coa1" placeholder="Nama MAK 4">

                    </div>
                    <div class="form-group col-lg-6">
                        <label for="judul">KODE MAK 4</label>
                        <input name="kode_mak_4" type="text" autofocus class="form-control clear" id="coa1" placeholder="Kode MAK 4">

                    </div>
                    <br>

                    <div class="col text-center">
                    <div id="tampil"></div>
                        <button type="submit" class="btn btn-outline-success btnsubmit">Kirim</button>
                        <a href="<?=base_url('m_mak_4')?>" class="btn btn-outline-danger btnsubmit">Kembali</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('.select2').select2({
            placeholder: "Pilih MAK 1",
            allowClear: true
        });
        $("#coa2").select2({
            placeholder: "Pilih MAK 2",
            allowClear: true
        });
        $("#coa3").select2({
            placeholder: "Pilih MAK 3",
            allowClear: true
        });
    });

    jQuery(document).ready(function() {
        $("#select").change(function() {
            $('#coa2').val(null).trigger('change');
            $('#coa3').val(null).trigger('change');
            $('#tampil').empty()

        });

        $("#coa2").change(function() {
            $('#coa3').val(null).trigger('change');
            $('.clear').val('');
            $('#tampil').empty()

        });
        $("#coa3").change(function() {
         
            $('#tampil').empty()

        });
        $("#coa1").focus(function() {
            if ($("#coa2").val() !== '' & $("#select").val() !== '' & $("#coa3").val() !== '') {
                var data = $("#select option:selected").text();
                data = parseInt(data);
                var data1 = $("#coa2 option:selected").text();
                data1 = parseInt(data1);
                var data2 = $("#coa3 option:selected").text();
                data2 = parseInt(data2);
                $('#tampil').append('kode MAK yang dipilih ' + data + ' ' + data1 + ' ' + data2)
            }
        });


    })
    $(document).ready(function() {

        $('.add').submit(function(e) {
            e.preventDefault();
            $.ajax({
                type: "post",
                url: $(this).attr('action'),
                data: $(this).serialize(),
                dataType: "json",
                beforeSend: function() {
                    $('.btnsubmit').attr('disable', 'disabled')

                    $('.btnsubmit').html('<i class="fa fa-spin fa-spinner"</i>')
                },
                complete: function() {
                    $('.btnsubmit').removeAttr('disable')

                    $('.btnsubmit').html('simpan')
                },
                success: function(response) {
                    if (response.error === 'error') {
                        alertify.set('notifier', 'position', 'top-right');
                        var msg = alertify.error(response.msg, 0);
                        $('body').one('click', function() {
                            msg.dismiss();
                        });
                    }
                    if (response.error === 'sukses') {
                        alertify.set('notifier', 'position', 'top-right');
                        var msg = alertify.success(response.msg, 0);
                        setTimeout(function() {
                            window.location.href = "<?= base_url() ?>/m_mak_4";
                        }, 1000)
                    }
                },
                // error: function (xhr, ajaxOptions, thrownError) {
                //         alert(xhr.status+"\n"+xhr.responseText+"\n"+thrownError);

                //     }
            })
        })
    })

    function ajax() {

        var coa_1 = $('#select').val();
        var coa_2 = $('#coa2').val();
        var coa_3 = $('#coa3').val();
        $.ajax({
            url: '<?= base_url('m_mak_4') ?>/pilihmak',
            method: 'post',
            dataType: 'json',
            data: {
                coa_1: coa_1,
                coa_2: coa_2,
                coa_3: coa_3,

            },
            success: function(data) {
                if (data.kd == 2) {
                    $("#coa2").html(data.coa_2_hasil);

                } else if (data.kd == 3) {
                    $("#coa3").html(data.coa_3_hasil);

                } else if (data.kd == 4) {
                    $("#nm_usaha").val(data.hasil_nama);

                }
            }
        });
    }
</script>
<!-- /.container-fluid -->
<?= $this->endSection(); ?>
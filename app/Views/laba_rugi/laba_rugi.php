<?= $this->extend('templates/index'); ?>
<?= $this->section('konten'); ?>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script src="//cdn.rawgit.com/ashl1/datatables-rowsgroup/v1.0.0/dataTables.rowsGroup.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<style>
    table tbody tr td {
        font-size: 14px;
    }
    table tfoot tr td {
        font-size: 14px;
    }

    table thead tr th {
        font-size: 14px;
    }

    table {
        color: black;
    }
</style>
<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <!-- <h1 class="h3 mb-4 text-gray-800"><?= $page ?></h1> -->

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"><?= $user ?></h6>
        </div>
        <div class="card-body">
            <div autocomplete="off" class="form-inline">
                <div class="form-group sm-2" id="jenis">

                    <select required class="custom-select my-1 mr-sm-2  select2" name="jenis" id="jenis">
                        <option> Pilih Waktu</option>
                        <option value="year">Tahun</option>
                        <option value="bulan">Bulan</option>
                    </select>

                </div>&nbsp;
                <div class="form-group sm-2" style="display: none;" id="year">
                    <label for="debet" class="sr-only">Pilih coa</label>
                    <select required class="custom-select my-1 mr-sm-2 debet select2" name="debet" id="tahun">
                        <option></option>
                        <?php
                        $year = date('Y');
                        $add = $year - 2020;
                        $min = 2018 + $add;
                        $max = $min + 10;
                        for ($i = $min; $i <= $max; $i++) {
                            echo '<option value=' . $i . '>' . $i . '</option>';
                        } ?>
                        <option value="2019">Level 3</option>
                    </select>

                </div>&nbsp;
                <div class="form-group sm-2" style="display: none;" id="bulan">
                    <input type="text" class="form-control datepicker" autocomplete="off" placeholder="Tanggal" name="tgl" id="tgl">

                </div>
                &nbsp;

                <!-- <div class="form-group mx-sm-5 sm-2">
                    <label for="level" class="sr-only">Level</label>
                    <select required class="custom-select my-1 mr-sm-5 debet select2" name="level" id="level">
                        <option></option>
                        <option value="6">Level 1</option>
                        <option value="2">Level 2</option>
                        

                    </select> -->

                <!-- </div> -->
              
                &nbsp;
                <button type="button" class="btn btn-primary btn-sm btncari">Cari</button>
            </div>
            <br>

            <div class="row">
                <div class="form-group col-lg-5">
                    <div class="form-group row">
                        <div class="table-responsive" id="pendapatan">


                        </div>
                    </div>
                </div>
                <div class="form-group col-lg-1">
                    <div class="form-group row">

                    </div>
                </div>
                <div class="form-group col-lg-5">
                    <div class="form-group row">
                        <div class="table-responsive" id="biaya">


                        </div>
                    </div>
                </div>
                <div class="form-group col-lg-12">
                    <div class="form-group row">
                        <div class="table-responsive" id="total">


                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

</div>
<script>
    $('.btncari').click(function() {
        var total_pendapatan = 0;
        var total_biaya = 0;
        var total_saldo_pendapatan_kredit = 0;
        var total_saldo_pendapatan_debet = 0;
        var total_saldo_biaya_debet = 0;
        var total_saldo_biaya_kredit = 0;
        var c = '';
        var c1 = '';
        var pendapatan = 0
        var biaya = 0;
        var hasil = 0;
        var pendapatan = '';
        var biaya = '';
        $.ajax({
            type: "post",
            url: "<?= base_url('laba_rugi/get_rinci') ?>",
            data: {
                koperasi: $('#kop').val(),
                tahun: $('#tahun').val(),
                bulan: $("#tgl ").val()

            },
            async: false,
            dataType: "json",
            success: function(response) {


                pendapatan = response.pendapatan;
                biaya = response.biaya;

                $.each(response.pendapatan, function(index, val) {
                    
                    total_saldo_pendapatan_kredit += Number(val.nilai_akhir);
                 

                    c1 += `<tr><td>` + val.kode_coa_1 + val.kode_coa_2 +val.kode_coa_3 +val.kode_coa_4 + `</td>` +
                        `<td>` + val.namacoa + `</td>` +
                       
                        `<td>` + getNumberWithCommas(val.nilai_akhir) + `</td>` +

                        `</tr>`

                });

                $.each(response.biaya, function(index, value) {
                    
                    total_biaya += Number(value.nilai_akhir)

                    c += `<tr><td>` + value.kode_coa_1 + value.kode_coa_2 +value.kode_coa_3 +value.kode_coa_4 + `</td>` +
                        `<td>` + value.namacoa + `</td>` +
                      
                        `<td>` + getNumberWithCommas(value.nilai_akhir) + `</td>` +

                        `</tr>`

                });

            }
            // error: function (xhr, ajaxOptions, thrownError) {

        })
        // Open this row


        if (pendapatan) {
console.log('aaaaa')

            var tabel_awal = '<h6 class="m-0 font-weight-bold text-primary">Pendapatan</h6> <br><table class="table text-dark">' +
                '<thead><tr><th>Kode Akun</th><th>Nama Akun </th><th>Saldo</th></tr></thead><tbody>';
            var tabel_akhir = '</tbody> <tfoot> <tr><td colspan="2" style="font-weight:bold;">Total</td> <td style="font-weight:bold;"> ' + getNumberWithCommas(total_pendapatan) + '</td></tr></tfoot></table>';
            $('#pendapatan').html(tabel_awal + c1 + tabel_akhir)
            pendapatan = total_pendapatan

        }
        if (biaya) {

            var tabel_awal1 = '<h6 class="m-0 font-weight-bold text-primary">Biaya</h6> <br><table class="table text-dark">' +
                '<thead><tr><th>Kode Akun</th><th>Nama Akun </th><th>Saldo</th></tr></thead><tbody>';
            var tabel_akhir1 = '</tbody> <tfoot> <tr><td colspan="2" style="font-weight:bold;">Total</td> <td style="font-weight:bold;"> ' + getNumberWithCommas(total_biaya) + '</td></tr></tfoot></table>';
            biaya = total_biaya

            $('#biaya').html(tabel_awal1 + c + tabel_akhir1)

        }
        if (pendapatan == '') {
            $('#pendapatan').html('<h4>Tidak Ada Transaksi Di Rekening Pendapatan</h4>')

        }
        if (biaya == '') {
            $('#biaya').html('<h4>Tidak Ada Transaksi Di Rekening Biaya</h4>')
        }


        hasil = pendapatan - biaya
        console.log(pendapatan)
        $('#total').html('<h5 style="text-align: center;background-color: coral; color:black">Laba/rugi Sebesar ' + getNumberWithCommas(hasil) + '</h5>')

    })

    function getNumberWithCommas(number) {
        return formatter = new Intl.NumberFormat('id-ID', {
            style: 'currency',
            currency: 'IDR',


        }).format(number);
    }
    $('#tahun').select2({
        placeholder: "Pilih Tahun",
        allowClear: true,
        theme: 'bootstrap4',
    });

    $('#level').select2({
        placeholder: "Pilih Level",
        allowClear: true,
        theme: 'bootstrap4',
    });
    $('#kop').select2({
        placeholder: "Pilih Koperasi",
        allowClear: true,
        theme: 'bootstrap4',
    });
    $("#tgl").datepicker({});

    $("#jenis").on('change', function() {
        var a = $('select[name=jenis] option').filter(':selected').val()

        if (a == 'year') {
            $("#year").show("slow");
            $("#bulan").hide("slow");
            $("#tgl").val("");
        }
        if (a == 'bulan') {
            $("#bulan").show("slow");
            // $("#year").hide("slow");
            $("#year").show("slow");
            $("#tahun").val("");
        }

    });
</script>
<!-- /.container-fluid -->
<?= $this->endSection(); ?>
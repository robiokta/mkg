<?= $this->extend('templates/formindex'); ?>
<?= $this->section('konten'); ?>

<!-- Begin Page Content -->
<div class="container">
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $page ?></h1>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"></h6>
        </div>
        <div class="card-body">
            <form action="<?= base_url() ?>/master_supplier/action_edit" class="adduser" method="post">
                <?= csrf_field(); ?>
                <div class="form-row">
                    <div class="form-group col-lg-6">
                        <label for="namasup">Nama Supplier</label>
                        <input name="namasup" value="<?= $sup['nama_supplier'] ?>" type="text" autofocus class="form-control" id="namasup" placeholder="Nama Supplier" required>
                        <input type="hidden" name="id" value="<?= $id ?>">
                    </div>
                    <div class="form-group col-lg-6">
                        <label for="username">Nomer Telpon</label>
                        <input name="no_tlp" value="<?= $sup['no_tlp'] ?>" type="text" class="form-control" id="username" placeholder="Nomer Telepon" required>
                    </div>
                    <div class="form-group col-lg-6">
                        <label for="fulname">Alamat</label>
                        <input type="text" value="<?= $sup['alamat'] ?>" class="form-control" id="fullname" placeholder="Alamat Lengkap" name="alamat" required>
                    </div>
                    <div class="form-group col-lg-6">
                        <label for="tipe">Tipe</label>
                        <input type="text" class="form-control" id="tipe" value="<?=$sup['type']?>" placeholder="Tipe" name="tipe" required>
                    </div>
                    <br>

                    <div class="col text-center">
                        <button type="submit" class="btn btn-outline-success btnsubmit">Kirim</button>
                        <a href="<?= base_url('master_supplier') ?>" class="btn btn-outline-danger btnsubmit">Kembali</a>
                    </div>

                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('.adduser').submit(function(e) {
            e.preventDefault();
            $.ajax({
                type: "post",
                url: $(this).attr('action'),
                data: $(this).serialize(),
                dataType: "json",
                beforeSend: function() {
                    $('.btnsubmit').attr('disable', 'disabled')

                    $('.btnsubmit').html('<i class="fa fa-spin fa-spinner"</i>')
                },
                complete: function() {
                    $('.btnsubmit').removeAttr('disable')

                    $('.btnsubmit').html('simpan')
                },
                success: function(response) {
                    if (response.error === 'error') {
                        alertify.set('notifier', 'position', 'top-right');
                        var msg = alertify.error(response.msg, 0);
                        $('body').one('click', function() {
                            msg.dismiss();
                        });
                    }
                    if (response.error === 'sukses') {
                        alertify.set('notifier', 'position', 'top-right');
                        var msg = alertify.success(response.msg, 0);

                        setTimeout(function() {
                            window.location.href = "<?= base_url() ?>/master_supplier"
                        }, 1000);

                    }
                },
                // error: function (xhr, ajaxOptions, thrownError) {
                //         alert(xhr.status+"\n"+xhr.responseText+"\n"+thrownError);

                //     }
            })
        })
    })
</script>
<!-- /.container-fluid -->
<?= $this->endSection(); ?>
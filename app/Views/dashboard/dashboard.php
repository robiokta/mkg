<?= $this->extend('templates/index'); ?>
<?= $this->section('konten'); ?>
<script src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.7/dist/loadingoverlay.min.js"></script>
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>

    </div>

    <div id="animasi" style="display: none;">
        <img src="<?= base_url() ?>/img/animasi.gif">
    </div>
    <!-- Content Row -->
    <?php if (in_groups('admin') | in_groups('menku') | in_groups('manager')) { ?>
        <div class="row">

            <!-- Content Column -->
            <div class="col-lg-12 sm-4">
                <div class="row">
                    <div class="col-lg-3 mb-3 notif">
                        <div class="card bg-primary text-white shadow">
                            <div class="card-body">
                                Pengajuan
                                <div class="text-white-50 small">Jumlah Pengajuan <span class="badge badge-light"><?= $jumlah ?></span></div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="col-lg-3 mb-4">
                    <div class="card bg-success text-white shadow">
                        <div class="card-body">
                            Success
                            <div class="text-white-50 small">#1cc88a</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 mb-4">
                    <div class="card bg-info text-white shadow">
                        <div class="card-body">
                            Info
                            <div class="text-white-50 small">#36b9cc</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 mb-4">
                    <div class="card bg-warning text-white shadow">
                        <div class="card-body">
                            Warning
                            <div class="text-white-50 small">#f6c23e</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 mb-4">
                    <div class="card bg-danger text-white shadow">
                        <div class="card-body">
                            Danger
                            <div class="text-white-50 small">#e74a3b</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 mb-4">
                    <div class="card bg-secondary text-white shadow">
                        <div class="card-body">
                            Secondary
                            <div class="text-white-50 small">#858796</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 mb-4">
                    <div class="card bg-light text-black shadow">
                        <div class="card-body">
                            Light
                            <div class="text-black-50 small">#f8f9fc</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 mb-4">
                    <div class="card bg-dark text-white shadow">
                        <div class="card-body">
                            Dark
                            <div class="text-white-50 small">#5a5c69</div>
                        </div>
                    </div>
                </div> -->
                </div>

            </div>
        </div>

    <?php } ?>

</div>

<!-- Modal -->
<div class="modal fade" id="ModalCenter" tabindex="-1" role="dialog" aria-labelledby="ModalCenter" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">

            <div class="modal-body">
                <div id="alamat">

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Tutup</button>

            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {


        $('.notif').on('click', function() {
            var c1 = ''
            $.ajax({
                type: "post",
                url: "<?= base_url() ?>/dashboard/data",
                data: '',
                dataType: "json",
                async: false,
                beforeSend: function() {
                    $.LoadingOverlaySetup({
                        background: "rgba(0, 0, 0, 0.5)",
                        image: "<?= base_url() ?>/img/animasi.gif",
                        imageAnimation: "1.5s fadein",
                        imageColor: "#ffcc00"
                    });

                    $.LoadingOverlay("show");
                },
                complete: function() {
                    $.LoadingOverlay("hide");

                },
                success: function(response) {
                    $.each(response.pengajuan, function(index, value) {
                        c1 += `<tr><td><a href="<?= base_url() ?>/pengajuan/persetujuan/` + value.id + `">` + value.no_pengajuan + `</a></td>` +
                            `<td>` + value.judul + `</td>` +
                            `</tr>`
                    });

                },
            })
            var tabel_awal1 = '<table id = "modal_detail" class="table table-striped table-hover table-hover dt-responsive display nowrap" cellpadding="5" cellspacing="0" border="0" style="padding-left:25px;width:100%;padding-right:25px;">' +
                '<thead><tr><th>No Pengajuan</th><th>Judul </th></tr></thead><tbody>';
            var tabel_akhir1 = '</tbody></table>';


            $('#alamat').append(tabel_awal1 + c1 + tabel_akhir1)
            var tables = $('#modal_detail').DataTable({
                destroy: true,
                // scrollY: '100px',
                "paging": true,
                "ordering": false,
                "info": false,
                "searching": true
            });
            tables.columns.adjust();
            $('#ModalCenter').modal('show')

        })



        $("#ModalCenter").on("hidden.bs.modal", function() {
            $('#alamat').empty();
        });
    })
</script>
<!-- /.container-fluid -->
<?= $this->endSection(); ?>
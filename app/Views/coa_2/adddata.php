<?= $this->extend('templates/formindex'); ?>
<?= $this->section('konten'); ?>
<style>
    .ck-editor__editable_inline {
        min-height: 300px;
    }
</style>

<!-- Begin Page Content -->
<div class="container">

    <!-- Page Heading -->

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"><?= $page ?></h6>
        </div>
        <div class="card-body">
            <form action="<?= base_url() ?>/m_coa_2/action_add" class="add" method="post">
                <?= csrf_field(); ?>
                <div class="form-row">
                    <div class="form-group col-lg-4">
                        <label for="select">Pilih COA 1</label>
                        <select required class="custom-select my-1 mr-sm-2" name="parent_id" id="select">
                            <option>Pilih COA 1</option>
                            <?php foreach ($coa as $key => $val) : ?>
                                <option value="<?= $val->m_coa_id ?>"><?=$val->kode_coa.' - '. $val->nama_coa ?></option>

                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group col-lg-4">
                        <label for="judul">NAMA COA 2</label>
                        <input name="nama_coa" type="text" autofocus class="form-control clear" id="coa1" placeholder="Nama COA 2">

                    </div>
                    <div class="form-group col-lg-4">
                        <label for="judul">KODE COA 2</label>
                        <input name="kode_coa_2" type="text" autofocus class="form-control clear" id="coa1" placeholder="Kode COA 2">

                    </div>


                    <br>

                    <div class="col text-center">
                        <button type="submit" class="btn btn-outline-success btnsubmit">Kirim</button>
                        <a href="<?=base_url('m_coa_2')?>" class="btn btn-outline-danger btnsubmit">Kembali</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $("#select").change(function() {
            $('.clear').val('');
            
        });


        $('.add').submit(function(e) {
            e.preventDefault();
            $.ajax({
                type: "post",
                url: $(this).attr('action'),
                data: $(this).serialize(),
                dataType: "json",
                beforeSend: function() {
                    $('.btnsubmit').attr('disable', 'disabled')

                    $('.btnsubmit').html('<i class="fa fa-spin fa-spinner"</i>')
                },
                complete: function() {
                    $('.btnsubmit').removeAttr('disable')

                    $('.btnsubmit').html('simpan')
                },
                success: function(response) {
                    if (response.error === 'error') {
                        alertify.set('notifier', 'position', 'top-right');
                        var msg = alertify.error(response.msg, 0);
                        $('body').one('click', function() {
                            msg.dismiss();
                        });
                    }
                    if (response.error === 'sukses') {
                        alertify.set('notifier', 'position', 'top-right');
                        var msg = alertify.success(response.msg, 0);
                        setTimeout(function() {
                            window.location.href = "<?= base_url() ?>/m_coa_2";
                        }, 1000)
                    }
                },
                // error: function (xhr, ajaxOptions, thrownError) {
                //         alert(xhr.status+"\n"+xhr.responseText+"\n"+thrownError);

                //     }
            })
        })
    })
</script>
<!-- /.container-fluid -->
<?= $this->endSection(); ?>
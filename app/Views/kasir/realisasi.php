<?= $this->extend('templates/formindex'); ?>
<?= $this->section('konten'); ?>
<style>
    .ck-editor__editable_inline {
        min-height: 200px;
    }
</style>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>


<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<!-- Begin Page Content -->
<div class="container">

    <!-- Page Heading -->

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"><?= $page ?></h6>
        </div>
        <div class="card-body">
            <form action="<?= base_url() ?>/pengajuan_cair/action_add" class="add" method="post">
                <?= csrf_field(); ?>
                <div class="form-row">
                    <div class="form-group col-lg-6">
                        <label for="kuitansi">No. Bukti Transaksi/No. Kuitansi</label>
                        <input required name="kuitansi" type="text" autofocus class="form-control" id="kuitansi" placeholder="No. Bukti Transaksi/ Kuitansi">
                        <input type="hidden" name="id_rinci" value="<?= $id ?>">
                    </div>
                    <div class="form-group col-lg-6">
                        <label for="unit">Unit</label>
                        <select required class="custom-select my-1 mr-sm-2 select2" name="unit" id="unit">
                            <option></option>
                            <?php foreach ($unit as  $val) : ?>
                                <option value="<?= $val['id_divisi'] ?>"><?= $val['nama_divisi'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <input type="hidden" name="nopeng" value="<?= $kas['no_pengajuan'] ?>">
                    <input type="hidden" name="debit" value="<?= $rinci['coa_4_id'] ?>">

                    <div class="form-group col-lg-6">
                        <label for="kredit">Kredit</label>
                        <select required class="custom-select my-1 mr-sm-2 coa kredit select2" name="kredit" id="kredit">
                            <option></option>
                            <?php foreach ($coa1 as $key => $val) : ?>
                                <option value="<?= $val->m_coa_4_id ?>"><?= $val->namacoa ?></option>
                            <?php endforeach; ?>
                        </select>

                    </div>
                    <div class="form-group col-lg-6">
                        <label for="ralisasi">Nilai Realisasi</label>
                        <input required id="realisasi" name="nilai" type="text" autofocus class="form-control" placeholder="Nilai Realisasi">

                    </div>
                    <div class="form-group col-lg-6">
                        <label for="judul">Tanggal transaksi</label>
                        <input required name="tgl_trans" type="text" class="form-control datepicker" id="coa1" placeholder="Tgl Transaksi">

                    </div>
                    <div class="form-group col-lg-6">
                        <label for="sup">Supplier</label>
                        <select required class="custom-select my-1 mr-sm-2 select2" name="supplier" id="sup">
                            <option></option>
                            <?php foreach ($supplier as $val) : ?>
                                <option value="<?= $val['id_supplier'] ?>"><?= $val['nama_supplier'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <!-- <div class="form-group col-lg-6">
                        <label for="supplier">Supplier</label>
                        <input required id="supplier" name="supplier" type="text" autofocus class="form-control" placeholder="Supplier">
                        <input required id="nope" name="nope" type="text" autofocus class="form-control" placeholder="Nomer Telepon" style="display:none">

                    </div>-->
                    <div class="form-group col-lg-12 ">
                        <label for="rincian">Detail/Rincian</label>
                        <input type="text" class="form-control" id="rincian" placeholder="Detail" name="detail">
                    </div>
                    <br>

                    <div class="col text-center">
                        <button type="submit" class="btn btn-outline-success btnsubmit">Kirim</button>
                        <a href="<?= base_url('pengajuan_cair') ?>" class="btn btn-outline-danger btnsubmit">Kembali</a>
                    </div>

                </div>
            </form>
        </div>
    </div>
</div>
<script>
    var rupiah = document.getElementById('realisasi');
    rupiah.addEventListener('keyup', function(e) {
        // tambahkan 'Rp.' pada saat form di ketik
        // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
        rupiah.value = formatRupiah(this.value, 'Rp. ');
    });

    /* Fungsi formatRupiah */
    function formatRupiah(angka, prefix) {
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
            split = number_string.split(','),
            sisa = split[0].length % 3,
            rupiah = split[0].substr(0, sisa),
            ribuan = split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
    }

    $(document).ready(function() {
        $(".datepicker").datepicker();
        $('.add').submit(function(e) {
            e.preventDefault();
            $.ajax({
                type: "post",
                url: $(this).attr('action'),
                data: $(this).serialize(),
                dataType: "json",
                beforeSend: function() {
                    $('.btnsubmit').attr('disable', 'disabled')

                    $('.btnsubmit').html('<i class="fa fa-spin fa-spinner"</i>')
                },
                complete: function() {
                    $('.btnsubmit').removeAttr('disable')

                    $('.btnsubmit').html('simpan')
                },
                success: function(response) {
                    if (response.error === 'error') {
                        alertify.set('notifier', 'position', 'top-right');
                        var msg = alertify.error(response.msg, 0);
                        $('body').one('click', function() {
                            msg.dismiss();
                        });
                    }
                    if (response.error === 'sukses') {
                        alertify.set('notifier', 'position', 'top-right');
                        var msg = alertify.success(response.msg, 0);
                        setTimeout(function() {
                            window.location.href = "<?= base_url() ?>/pengajuan_cair";
                        }, 1000)
                    }
                },
                // error: function (xhr, ajaxOptions, thrownError) {
                //         alert(xhr.status+"\n"+xhr.responseText+"\n"+thrownError);

                //     }
            })
        })
    })

    // $("#supplier").on("keyup", function() {
    //     var val = $(this).val()
    //     if (val) {
    //         $('#nope').show()
    //     } else {
    //         $('#nope').hide()
    //     }

    // });
    $(document).ready(function() {

        $('#sup').select2({
            placeholder: "Pilih Supplier",
            allowClear: true,
            theme: 'bootstrap4',
        });

        $('#kredit').select2({
            placeholder: "Pilih Kredit",
            allowClear: true,
            theme: 'bootstrap4',
        });
        $('#unit').select2({
            placeholder: "Pilih Unit",
            allowClear: true,
            theme: 'bootstrap4',
        });

    });

    ClassicEditor
        .create(document.querySelector('#editor'))
        .catch(error => {
            console.error(error);
        });
</script>
<!-- /.container-fluid -->
<?= $this->endSection(); ?>
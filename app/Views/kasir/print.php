<html>
<style type="text/css">
    h1,
    h2,
    h3,
    h4,
    h5,
    h6,
    li,
    blockquote,
    p,
    th,
    td {
        font-family: Helvetica, Arial, Verdana, sans-serif;
        /*Trebuchet MS,*/
    }

    h1,
    h2,
    h3,
    h4 {
        /*color: #5E88B6;*/
        font-weight: normal;
    }

    h4,
    h5,
    h6 {
        color: #5E88B6;
    }

    h2 {
        margin: 0 auto auto auto;
        font-size: x-large;
    }

    h3 {
        text-align: center;
    }

    li,
    blockquote,
    p,
    th,
    td {
        font-size: 80%;
    }

    ul {
        list-style: url(/img/bullet.gif) none;
    }

    #footer {
        border-top: 1px solid #CCC;
        text-align: right;
    }

    P.breakhere {
        page-break-after: always
    }
</style>
<title>Cetak Pencairan</title>

<body>
    <?php foreach ($sttk as $key => $value) {
        $nomer = $value->idsttk;
    } ?>
    <?php foreach ($print as $key => $val) : ?>
        <?php
        if ($nomer < 10) {
            $nomer = "00" . $nomer;
        } elseif ($nomer < 100) {
            $nomer = "0" . $nomer;
        }
        ?>
        <table width="100%">
            <tr>
                <th scope="col" "5%" align="right"><img src="<?= base_url() ?>/img/logmkg.jpeg" alt="logo_supra" width="50" height="50" /></th>
                <th scope="col" width="65%" align="left">Mentari Karya Gemilang</th>
                <th scope="col" width="30%" align="right">
                    <h2>STTK </h2>(SURAT TANDA TERIMA KASIR)<br>NO:<?= $nomer ?>/STTK/<?= $val->name ?>/<?= date("d/m/Y", strtotime($val->tgl_acc)) ?><br>Pengeluaran Tunai/Cash
                </th>
            </tr>
            <table style="border: 1pt ridge black; border-collapse:collapse" cellpadding="10">
                <tr style="border: 1pt ridge black; border-collapse:collapse">
                    <td style="border: 1pt ridge black; border-collapse:collapse">Nomor Pengajuan</td>
                    <td style="border: 1pt ridge black; border-collapse:collapse">Tanggal Disetujui</td>
                    <td style="border: 1pt ridge black; border-collapse:collapse">Tahun Anggaran</td>
                    <td style="border: 1pt ridge black; border-collapse:collapse">Unit Kerja</td>
                </tr>
                <tr style="border: 1pt ridge black; border-collapse:collapse">
                    <td style="border: 1pt ridge black; border-collapse:collapse"><?= $val->no_pengajuan ?></td>
                    <td style="border: 1pt ridge black; border-collapse:collapse"><?= $val->tgl_acc ?></td>
                    <td style="border: 1pt ridge black; border-collapse:collapse"><?= date("Y", strtotime($val->tgl_acc)) ?></td>
                    <td style="border: 1pt ridge black; border-collapse:collapse"><?= $val->description ?></td>
                </tr>
            </table>
            </th>
            </tr>
        </table><br>
        <table style="border: 1pt ridge black; border-collapse:collapse" cellpadding="10" width="100%">
            <tr>
                <th style="border: 1pt ridge black; border-collapse:collapse">No</th>
                <th style="border: 1pt ridge black; border-collapse:collapse">Nomor Rekening</th>
                <th style="border: 1pt ridge black; border-collapse:collapse">Pengajuan</th>
                <th style="border: 1pt ridge black; border-collapse:collapse">Kuantitas</th>
                <!-- <th style="border: 1pt ridge black; border-collapse:collapse">Tunai</th> -->
                <th style="border: 1pt ridge black; border-collapse:collapse">Harga Satuan</th>
                <th style="border: 1pt ridge black; border-collapse:collapse">Harga Total</th>
            </tr>
            <td colspan="6" align="left" style="border: 1pt ridge black; border-collapse:collapse; padding-left:40px"><b><?=$val->id_mak_4?></b></td>
            <?php $jumlah = 0;
            $harga = 0;
            $i=1;
            
            foreach ($coa as $key => $value) {
                # code...
            ?>
                <tr>
                    <td style="border: 1pt ridge black; border-collapse:collapse"><?=$i?></td>
                    <td style="border: 1pt ridge black; border-collapse:collapse"><?= $value->kode_coa . ' ' . $value->kode_coa_2 . ' ' . $value->kode_coa_3 . ' ' . $value->kode_coa_4 ?></td>
                    <td style="border: 1pt ridge black; border-collapse:collapse"><?= $val->judul ?>

                    </td>
                    <td align="right" style="border: 1pt ridge black; border-collapse:collapse"><?= @$value->jumlah; ?></td>
                    <!-- <td align="right" style="border: 1pt ridge black; border-collapse:collapse">0</td> -->
                    <td align="right" style="border: 1pt ridge black; border-collapse:collapse"><?= "Rp " . number_format(@$value->harga, 0, ',', '.'); ?></td>
                    <td align="right" style="border: 1pt ridge black; border-collapse:collapse"><?= "Rp " . number_format(@$value->total_harga, 0, ',', '.'); ?></td>
                </tr>
                <?php $jumlah += $value->jumlah;
                $harga += $value->harga;
                $hargatotal=$jumlah*$harga;
                ?>
            <?php $i++; } ?>

            <tr>
                <td colspan="5" align="right" style="border: 1pt ridge black; border-collapse:collapse"><b>TOTAL</b></td>
                
                <td align="right" style="border: 1pt ridge black; border-collapse:collapse"><b><?= "Rp " . number_format(@$hargatotal, 0, ',', '.'); ?></b></td>
            </tr>
            <table>

                <tr>
                    <td colspan="2" align="left">Jl. Karimata 49 Jember, <?= date("d M Y") ?><br><br><br><br></td>
                </tr>
                <tr>
                    <td align="center"><u><?= user()->fullname ?></u></td>
                    <td width="15%"></td>
                    <td align="center"><u><?= $val->fullname . "Devisi ( " . $val->name . " )" ?></u></td>
                </tr>
                <tr>
                    <td align="center">Kasir</td>
                    <td></td>
                    <td align="center">Penerima</td>
                </tr>
            </table>
        <?php endforeach; ?>
</body>
<script>
    window.print()
</script>

</html>
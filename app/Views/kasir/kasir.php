<?= $this->extend('templates/index'); ?>
<?= $this->section('konten'); ?>

<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $page ?></h1>

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"><?= $user ?></h6>
        </div>
        <div class="card-body">
            <a class="btn btn-success float-right" href="<?= base_url('pengajuan/adddata') ?>" role="button"><i class="fas fa-user-plus"></i></a>

            <table class="table table-striped table-hover dt-responsive display nowrap" cellspacing="0" id="datatable">
                <thead>
                    <tr>
                        <th>No Pengajuan</th>
                        <th>Judul</th>
                        <th>Tanggal Pengajuan</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($pengajuan as $val) : ?>
                        <tr>
                            <td><?= $val->no_pengajuan ?></td>
                            <td><?= $val->judul ?></td>
                            
                            <td><?= $val->tgl_pengajuan ?></td>
                            <td><?= $val->status_name ?></td>
                            <td>
                                <a href="<?= base_url('pengajuan_cair/print/' . $val->id_kasir) ?>" class="btn btn-primary active" data-toggle="tooltip" data-placement="top" title="Edit">
                                    <i class="fas fa-print"></i>
                                </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>

                </tbody>
            </table>
        </div>
    </div>




</div>
<script>
    $(document).ready(function() {
        $('#datatable').DataTable({
            autoWidth: false,
            responsive: true
        });

    });

    $(document).on('click', '.delete', function() {
        var $button = $(this);
        var table = $('#datatable').DataTable();
        Swal.fire({
            title: 'Apakah Anda Yakin?',
            text: "Data Tidak Bisa Dikembalikan Lagi",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya!'
        }).then((result) => {

            if (result.isConfirmed) {
                $.ajax({
                    type: "post",
                    url: "<?= base_url('pengajuan/delete_pengajuan') ?>",
                    data: {
                        id: $(this).attr('id')
                    },
                    dataType: "json",
                    success: function(response) {
                        if (response.error === 'error') {
                            Swal.fire({
                                icon: 'error',
                                title: 'gagal',
                                text: response.msg,

                            })
                        }
                        if (response.error === 'sukses') {
                            table.row($button.parents('tr')).remove().draw();
                            Swal.fire(
                                'Berhasil!',
                                'Data Berhasil Dihapus.',
                                'success'
                            )
                        }

                    }
                    // error: function (xhr, ajaxOptions, thrownError) {

                })
            }
        })
    });
</script>
<!-- /.container-fluid -->

<?= $this->endSection(); ?>
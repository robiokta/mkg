<!doctype html>
<html>

<head>
	<meta charset="UTF-8">
	<meta name="robots" content="noindex">

	<title>Whoops!</title>
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,900" rel="stylesheet">
	<link type="text/css" rel="stylesheet" href="<?= base_url() ?>/img/style.css" />
	<style type="text/css">
		<?= preg_replace('#[\r\n\t ]+#', ' ', file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . 'debug.css')) ?>
	</style>
	<link rel="icon" href="<?= base_url() ?>/img/logo_pt.ico">
</head>

<body>

	<div id="notfound">
		<div class="notfound">
			<div class="notfound-404">
				<h1>Oops!</h1>
			</div>
			<h2>404 - Halaman Tidak Ditemukan</h2>
			<p>Halaman Yang anda akses tidak ditemukan atau anda tidak memiliki hak untuk mengakses halaman tersebut.</p>
			<a href="<?= base_url() ?>">Kembali Kehalman Awal</a>
		</div>
	</div>

</body>

</html>
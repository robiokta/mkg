   <?= $this->extend('templates/formindex'); ?>
   <?= $this->section('konten'); ?>
   <style>
       .ck-editor__editable_inline {
           min-height: 300px;
       }
   </style>
      <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
   <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>



   <!-- Begin Page Content -->
   <div class="container">

       <!-- Page Heading -->

       <div class="card shadow mb-4">
           <div class="card-header py-3">
               <h6 class="m-0 font-weight-bold text-primary"><?= $page ?></h6>
           </div>
           <?php foreach ($mak as $key => $val) : ?>
               <?php
                $a = $val->nama_mak;
                $b = $val->kode_mak_1;
                $c = $val->grup_user;
                ?>
           <?php endforeach; ?>
           <div class="card-body">
               <form action="<?= base_url() ?>/m_mak_1/action_edit" class="add" method="post">
                   <?= csrf_field(); ?>

                   <div class="form-row">
                       <div class="form-group col-lg-4">
                           <label for="judul">Nama MAK 1</label>
                           <input name="nama_mak" value="<?= $a ?>" type="text" autofocus class="form-control" id="coa1" placeholder="NAMA MAK 1">
                           <input type="hidden" name="id" value="<?= $id ?>">

                       </div>
                       <div class="form-group col-lg-4">
                           <label for="judul">Kode MAK 1</label>
                           <input name="kode_mak" value="<?= $b ?>" type="text" autofocus class="form-control" id="coa1" placeholder="Kode MAK 1">
                       </div>
                       <div class="form-group col-lg-4">
                           <label for="akses">Akses</label>
                           <select name="akses" id="askses" required class="custom-select my-1 mr-sm-2 select2">
                               <option></option>
                               <?php foreach ($grup as  $val) : ?>
                                   <option value="<?= $val['description'] ?>" <?= ($val['description'] == $c ? ' selected' : '') ?>><?= $val['description'] ?></option>
                               <?php endforeach; ?>
                           </select>
                       </div>
                       <br>

                       <div class="col text-center">
                           <button type="submit" class="btn btn-outline-success btnsubmit">Kirim</button>
                           <a href="<?=base_url('m_mak_1')?>" class="btn btn-outline-danger btnsubmit">Kembali</a>
                       </div>

                   </div>
               </form>
           </div>
       </div>
   </div>
   <script>
       $(document).ready(function() {
        $('.select2').select2({
               placeholder: "Pilih Hak Akses",
               allowClear: true,
               theme: 'bootstrap4',

           });
           $('.add').submit(function(e) {
               e.preventDefault();
               $.ajax({
                   type: "post",
                   url: $(this).attr('action'),
                   data: $(this).serialize(),
                   dataType: "json",
                   beforeSend: function() {
                       $('.btnsubmit').attr('disable', 'disabled')

                       $('.btnsubmit').html('<i class="fa fa-spin fa-spinner"</i>')
                   },
                   complete: function() {
                       $('.btnsubmit').removeAttr('disable')

                       $('.btnsubmit').html('simpan')
                   },
                   success: function(response) {
                       if (response.error === 'error') {
                           alertify.set('notifier', 'position', 'top-right');
                           var msg = alertify.error(response.msg, 0);
                           $('body').one('click', function() {
                               msg.dismiss();
                           });
                       }
                       if (response.error === 'sukses') {
                           alertify.set('notifier', 'position', 'top-right');
                           var msg = alertify.success(response.msg, 0);
                           setTimeout(function() {
                               window.location.href = "<?= base_url() ?>/m_mak_1";
                           }, 1000)

                       }
                   },
                   // error: function (xhr, ajaxOptions, thrownError) {
                   //         alert(xhr.status+"\n"+xhr.responseText+"\n"+thrownError);

                   //     }
               })
           })
       })

    //    ClassicEditor
    //        .create(document.querySelector('#editor'))
    //        .catch(error => {
    //            console.error(error);
    //        });
   </script>
   <!-- /.container-fluid -->
   <?= $this->endSection(); ?>
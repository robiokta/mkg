<?= $this->extend('templates/index'); ?>
<?= $this->section('konten'); ?><link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script src="//cdn.rawgit.com/ashl1/datatables-rowsgroup/v1.0.0/dataTables.rowsGroup.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedcolumns/3.2.6/css/fixedColumns.bootstrap4.min.css" />

<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/fixedcolumns/3.2.6/js/dataTables.fixedColumns.min.js"></script>
<style>
    table tbody tr td {
        font-size: 14px;
    }

    table thead tr th {
        font-size: 14px;
        text-align: center;
    }

    table tfoot tr th {
        font-size: 14px;

    }

    table {
        color: black;
    }

    /* th, td { white-space: nowrap; } */
    
    /* Ensure that the demo table scrolls */
th, td { white-space: nowrap; }
div.dataTables_wrapper {
    width: 100%;
    margin: 0 auto;
}


    /* .dataTables_scrollHead {
        position: sticky !important;
        top: 119px;
        z-index: 99;
        background-color: white;
        box-shadow: 0px 5px 5px 0px rgba(82, 63, 105, 0.08);
    } */
    th,
    td {
        white-space: nowrap;
    }


    table, tbody, tr {
        height: 10px;
    }

    .kiri {
        text-align: left;
    }

    table {
        border-collapse: collapse;
    }

    .vertical-center {
        vertical-align: middle !important;
    }
</style>
<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <!-- <h1 class="h3 mb-4 text-gray-800"><?= $page ?></h1> -->

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"><?= $user ?></h6>
        </div>
        <div class="card-body">
            <div autocomplete="off" class="form-inline">
            <div class="form-group sm-2">
                    <label for="debet" class="sr-only">Pilih coa</label>
                    <select required class="custom-select my-1 mr-sm-2 debet select2" name="debet" id="tahun">
                        <option></option>
                        <?php
                        $year = date('Y');
                        $add = $year - 2020;
                        $min = 2018 + $add;
                        $max = $min + 10;
                        for ($i = $min; $i <= $max; $i++) {
                            echo '<option value=' . $i . '>' . $i . '</option>';
                        } ?>
                        <option value="2019">Level 3</option>
                    </select>

                </div>
                &nbsp;

                <div class="form-group mx-sm-5 sm-2">
                    <label for="level" class="sr-only">Level</label>
                    <select required class="custom-select my-1 mr-sm-5 debet select2" name="level" id="level">
                        <option></option>
                        <option value="6">Level 1</option>
                        <option value="2">Level 2</option>
                        <option value="3">Level 3</option>
                        <option value="4">Level 4</option>
                        <!-- <option value="5">Level 5</option> -->

                    </select>

                </div>
                <button type="button" class="btn btn-primary btn-sm btncari">Cari</button>
            </div>
            <!-- <div class="table-responsive"> -->
            <table class="table table-sm table-bordered text-dark" width="100%" id="table">
                <thead class="bg-blue text-center">
                    <tr>
                        <!-- <th rowspan="2">No</th> -->
                        <th class="vertical-center" rowspan="2" style="text-align:center">Kode Akun</th>
                        <th class="vertical-center" rowspan="2" style="text-align:center;width:100%">Nama Akun</th>
                        <th class="vertical-center" rowspan="2" style="text-align:center">Saldo Tahun Lalu</th>
                        <th colspan="3">Januari</th>
                        <th colspan="3">Februari</th>
                        <th colspan="3">Maret</th>
                        <th colspan="3">April</th>
                        <th colspan="3">Mei</th>
                        <th colspan="3">Juni</th>
                        <th colspan="3">Juli</th>
                        <th colspan="3">Agustus</th>
                        <th colspan="3">September</th>
                        <th colspan="3">Oktober</th>
                        <th colspan="3">November</th>
                        <th colspan="3">Desember</th>
                    </tr>
                    <tr>
                        <th>Debet</th>
                        <th>Kredit</th>
                        <th>Saldo</th>
                        <th>Debet</th>
                        <th>Kredit</th>
                        <th>Saldo</th>
                        <th>Debet</th>
                        <th>Kredit</th>
                        <th>Saldo</th>
                        <th>Debet</th>
                        <th>Kredit</th>
                        <th>Saldo</th>
                        <th>Debet</th>
                        <th>Kredit</th>
                        <th>Saldo</th>
                        <th>Debet</th>
                        <th>Kredit</th>
                        <th>Saldo</th>
                        <th>Debet</th>
                        <th>Kredit</th>
                        <th>Saldo</th>
                        <th>Debet</th>
                        <th>Kredit</th>
                        <th>Saldo</th>
                        <th>Debet</th>
                        <th>Kredit</th>
                        <th>Saldo</th>
                        <th>Debet</th>
                        <th>Kredit</th>
                        <th>Saldo</th>
                        <th>Debet</th>
                        <th>Kredit</th>
                        <th>Saldo</th>
                        <th>Debet</th>
                        <th>Kredit</th>
                        <th>Saldo</th>
                    </tr>
                </thead>
                <tbody class="text-right"></tbody>
                <tfoot>
                <tr>
                        <th colspan="2" style="text-align:right">Total:</th>
                        <th style="text-align:right"></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                </tfoot>
            </table>
            <!-- </div> -->
        </div>
        <!-- Large modal -->


    </div>
    <script>
        // $('.debet').select2({
        //     placeholder: "Pilih Debet",
        //     allowClear: true,
        //     theme: 'bootstrap4',
        $('.select2bs4').select2({
            theme: 'bootstrap4'
        });

        // function datatable() {
        $('#table').DataTable().destroy()
        var table = $('#table').DataTable({
            scrollY: "400px",
            scrollX: true,
            scrollCollapse: true,
            paging: false,
            fixedColumns: {
                leftColumns: 2
            },
            // "order": [],
           
            "info": false,
           
            "processing": true,
            "serverSide": true,

            "pageLength": 15,
            "lengthMenu": [15, 25, 50, 100, 200, 500, 1000, 10000],
            "ajax": {
                "url": "<?= base_url() ?>/neraca_lajur/get_data1",
                "type": "POST",
                "data": function(d) {
                    d.level = $('#level').val();
                    d.tahun = $('#tahun').val();
                    d.koperasi = $('#kop').val();
                }

            },

            "language": {

                "sLengthMenu": "_MENU_ &nbsp;&nbsp;Data Per Halaman &nbsp;&nbsp;",
            },
            "columns": [{
                "className": "text-center tengah",
            }, {
                "className": "kiri tengah",
            }, {
                "className": "kanan",
                render: $.fn.dataTable.render.number(",", ".")
            }, {

                render: $.fn.dataTable.render.number(",", ".")
            }, {

                render: $.fn.dataTable.render.number(",", ".")
            }, {

                render: $.fn.dataTable.render.number(",", ".")
            }, {

                render: $.fn.dataTable.render.number(",", ".")
            }, {

                render: $.fn.dataTable.render.number(",", ".")
            }, {

                render: $.fn.dataTable.render.number(",", ".")
            }, {

                render: $.fn.dataTable.render.number(",", ".")
            }, {

                render: $.fn.dataTable.render.number(",", ".")
            }, {

                render: $.fn.dataTable.render.number(",", ".")
            }, {

                render: $.fn.dataTable.render.number(",", ".")
            }, {

                render: $.fn.dataTable.render.number(",", ".")
            }, {

                render: $.fn.dataTable.render.number(",", ".")
            }, {

                render: $.fn.dataTable.render.number(",", ".")
            }, {

                render: $.fn.dataTable.render.number(",", ".")
            }, {

                render: $.fn.dataTable.render.number(",", ".")
            }, {

                render: $.fn.dataTable.render.number(",", ".")
            }, {

                render: $.fn.dataTable.render.number(",", ".")
            }, {

                render: $.fn.dataTable.render.number(",", ".")
            }, {

                render: $.fn.dataTable.render.number(",", ".")
            }, {

                render: $.fn.dataTable.render.number(",", ".")
            }, {

                render: $.fn.dataTable.render.number(",", ".")
            }, {

                render: $.fn.dataTable.render.number(",", ".")
            }, {

                render: $.fn.dataTable.render.number(",", ".")
            }, {

                render: $.fn.dataTable.render.number(",", ".")
            }, {

                render: $.fn.dataTable.render.number(",", ".")
            }, {

                render: $.fn.dataTable.render.number(",", ".")
            }, {

                render: $.fn.dataTable.render.number(",", ".")
            }, {

                render: $.fn.dataTable.render.number(",", ".")
            }, {

                render: $.fn.dataTable.render.number(",", ".")
            }, {

                render: $.fn.dataTable.render.number(",", ".")
            }, {

                render: $.fn.dataTable.render.number(",", ".")
            }, {

                render: $.fn.dataTable.render.number(",", ".")
            }, {

                render: $.fn.dataTable.render.number(",", ".")
            }, {

                render: $.fn.dataTable.render.number(",", ".")
            }, {

                render: $.fn.dataTable.render.number(",", ".")
            }, {

                render: $.fn.dataTable.render.number(",", ".")
            }, ],


            "footerCallback": function(row, data, start, end, display) {
                var api = this.api(),
                    data;
                // Remove the formatting to get integer data for summation
                var intVal = function(i) {
                    return typeof i === 'string' ?
                        // i.replace(/[\$,.]/g, '') * 1 :
                        //i.replace(/(<font[^>]+>|<li>|<\/font>|[\$,.])/g, '') * 1 :
                        i.replace(/[\$,]/g, '') * 1 :

                        // i.replace(/(<[^>]+>|<[^>]>|<\/[^>]>)/g, '') * 1 :
                        typeof i === 'number' ?
                        i : 0;
                };

                // Total over all pagestotal1 = api.column(4).data().reduce(function(a, b) { return intVal(a) + intVal(b);}, 0);
                total0 = api.column(2).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total1 = api.column(3).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total2 = api.column(4).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total3 = api.column(5).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total4 = api.column(6).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total5 = api.column(7).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total6 = api.column(8).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total7 = api.column(9).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total8 = api.column(10).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total9 = api.column(11).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total10 = api.column(12).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total11 = api.column(13).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total12 = api.column(14).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total13 = api.column(15).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total14 = api.column(16).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total15 = api.column(17).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total16 = api.column(18).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total17 = api.column(19).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total18 = api.column(20).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total19 = api.column(21).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total20 = api.column(22).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total21 = api.column(23).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total22 = api.column(24).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total23 = api.column(25).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total24 = api.column(26).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total25 = api.column(27).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total26 = api.column(28).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total27 = api.column(29).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total28 = api.column(30).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total29 = api.column(31).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total30 = api.column(32).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total31 = api.column(33).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total32 = api.column(34).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total33 = api.column(35).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total34 = api.column(36).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total35 = api.column(37).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                total36 = api.column(38).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);


                // Update footer
                $(api.column(2).footer()).html(form(total0));
                $(api.column(3).footer()).html(form(total1));
                $(api.column(4).footer()).html(form(total2));
                $(api.column(5).footer()).html(form(total3));
                $(api.column(6).footer()).html(form(total4));
                $(api.column(7).footer()).html(form(total5));
                $(api.column(8).footer()).html(form(total6));
                $(api.column(9).footer()).html(form(total7));
                $(api.column(10).footer()).html(form(total8));
                $(api.column(11).footer()).html(form(total9));
                $(api.column(12).footer()).html(form(total10));
                $(api.column(13).footer()).html(form(total11));
                $(api.column(14).footer()).html(form(total12));
                $(api.column(15).footer()).html(form(total13));
                $(api.column(16).footer()).html(form(total14));
                $(api.column(17).footer()).html(form(total15));
                $(api.column(18).footer()).html(form(total16));
                $(api.column(19).footer()).html(form(total17));
                $(api.column(20).footer()).html(form(total18));
                $(api.column(21).footer()).html(form(total19));
                $(api.column(22).footer()).html(form(total20));
                $(api.column(23).footer()).html(form(total21));
                $(api.column(24).footer()).html(form(total22));
                $(api.column(25).footer()).html(form(total23));
                $(api.column(26).footer()).html(form(total24));
                $(api.column(27).footer()).html(form(total25));
                $(api.column(28).footer()).html(form(total26));
                $(api.column(29).footer()).html(form(total27));
                $(api.column(30).footer()).html(form(total28));
                $(api.column(31).footer()).html(form(total29));
                $(api.column(32).footer()).html(form(total30));
                $(api.column(33).footer()).html(form(total31));
                $(api.column(34).footer()).html(form(total32));
                $(api.column(35).footer()).html(form(total33));
                $(api.column(36).footer()).html(form(total34));
                $(api.column(37).footer()).html(form(total35));
                $(api.column(38).footer()).html(form(total36));
            },
          
            "columnDefs": [{
                    // "targets": [0, 3],
                    
                    "defaultContent": "-",
                    "targets": "_all",
                },
                {
                    width: 100,
                    targets: 0
                },
                {
                    width: 150,
                    targets: 1
                }

            ],
           
        });
        $(".dataTables_scrollBody").scroll(function() {
            $('.fixedHeader-floating').css('left', this.getBoundingClientRect().x - parseInt($(this).scrollLeft()) + 'px');
        });

        $('.btncari').click(function() {
            // alert('lll');
            // console.log($('#tahun').val())

            // table.draw();
            // datatable();

            table.draw();
            $(function() {
                // fix to get fixedHeader and scrollX working together in datatables
                $(document).on('preInit.dt', function(e, settings) {
                    if (
                        settings.oInit &&
                        settings.oInit.fixedHeader &&
                        settings.oInit.scrollX
                    ) {
                        $(settings.nScrollBody).scroll(function() {
                            if (!settings._fixedHeader) {
                                return false;
                            }

                            let tableFloatingHeader = settings._fixedHeader.dom.header.floating;
                            if (tableFloatingHeader) {
                                tableFloatingHeader.css('left', this.getBoundingClientRect().x - parseInt($(this).scrollLeft()) + 'px');
                            }
                        });
                    }
                });
            });
        })
        var formatter = new Intl.NumberFormat('id-ID', {
            style: 'currency',
            currency: 'IDR',
        });

        $(".datepicker").datepicker();

        function form(number) {
            var hasil = new Intl.NumberFormat().format(number)
            if (hasil == 'NaN') {
                return hasil = new Intl.NumberFormat().format(0)

            } else {
                return hasil
            }
        }
        $('#tahun').select2({
            placeholder: "Pilih Tahun",
            allowClear: true,
            theme: 'bootstrap4',
        });

        $('#level').select2({
            placeholder: "Pilih Level",
            allowClear: true,
            theme: 'bootstrap4',
        });

    </script>
    <!-- /.container-fluid -->
    <?= $this->endSection(); ?>
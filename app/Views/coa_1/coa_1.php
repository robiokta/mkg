<?= $this->extend('templates/index'); ?>
<?= $this->section('konten'); ?>

<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $page ?></h1>

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"><?= $user ?></h6>
        </div>
        <div class="card-body">
            <a class="btn btn-success float-right" href="<?= base_url('m_coa_1/adddata') ?>" role="button"><i class="fas fa-plus"></i></a>

            <table class="table table-striped table-hover dt-responsive display nowrap" cellspacing="0" id="datatable">
                <thead>
                    <tr>
                        <th>Kode COA</th>
                        <th>Nama COA</th>
                        <th>Action</th>
                        <th>Status</th>

                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($coa as $val) : ?>
                        <tr>
                            <td><?= $val->kode_coa ?></td>
                            <td><?= $val->nama_coa ?></td>
                            
                            <td>


                                <a href="<?= base_url('m_coa_1/edit/' . $val->m_coa_id) ?>" class="btn btn-primary active btn-sm" data-toggle="tooltip" data-placement="top" title="Edit">
                                    <i class="fas fa-edit"></i>
                                </a>
                                <a class="btn btn-danger active delete btn-sm" data-toggle="tooltip" data-placement="top" title="Hapus" id="<?= $val->m_coa_id ?>">
                                    <i class="far fa-trash-alt"></i>
                                </a>
                               
                            </td>
                            <td><select class="form-control form-control-sm status"  id='<?= $val->m_coa_id ?>'>
                                    <option value="active"  <?= ($val->status == 'active' ? ' selected' : '')?>>Active</option>
                                    <option value="inactive" <?= ($val->status == 'inactive' ? ' selected' : '') ?>>Inactive</option>

                                </select></td>
                        </tr>
                    <?php endforeach; ?>

                </tbody>
            </table>
        </div>
    </div>




</div>
<script>
    $(document).ready(function() {
        $('#datatable').DataTable({
            autoWidth: false,
            responsive: true
        });

    });
    $(document).on('change', '.status', function() {
        var $button = $(this);
        var table = $('#datatable').DataTable();
        Swal.fire({
            title: 'Apakah Anda Yakin?',
            text: "Merubah Status COA ?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya!'
        }).then((result) => {

            if (result.isConfirmed) {
                $.ajax({
                    type: "post",
                    url: "<?= base_url('m_coa_1/status') ?>",
                    data: {
                        id: $(this).attr('id'),
                        val: $(this).val()
                    },
                    dataType: "json",
                    success: function(response) {
                        if (response.error === 'error') {
                            Swal.fire({
                                icon: 'error',
                                title: 'gagal',
                                text: response.msg,

                            })
                        }
                        if (response.error === 'sukses') {

                            Swal.fire(
                                'Berhasil!',
                                'Data Berhasil Dubah.',
                                'success'
                            )
                            setTimeout(function() {
                                window.location.reload(1);
                            }, 1000);
                        }

                    }
                    // error: function (xhr, ajaxOptions, thrownError) {

                })
            }
        })
    });

    $(document).on('click', '.delete', function() {
        var $button = $(this);
        var table = $('#datatable').DataTable();
        Swal.fire({
            title: 'Apakah Anda Yakin?',
            text: "Data Tidak Bisa Dikembalikan Lagi",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya!'
        }).then((result) => {

            if (result.isConfirmed) {
                $.ajax({
                    type: "post",
                    url: "<?= base_url('m_coa_1/delete') ?>",
                    data: {
                        id: $(this).attr('id')
                    },
                    dataType: "json",
                    success: function(response) {
                        if (response.error === 'error') {
                            Swal.fire({
                                icon: 'error',
                                title: 'gagal',
                                text: response.msg,

                            })
                        }
                        if (response.error === 'sukses') {
                            table.row($button.parents('tr')).remove().draw();
                            Swal.fire(
                                'Berhasil!',
                                'Data Berhasil Dihapus.',
                                'success'
                            )
                        }

                    }
                    // error: function (xhr, ajaxOptions, thrownError) {

                })
            }
        })
    });
</script>
<!-- /.container-fluid -->
<?= $this->endSection(); ?>
   <?= $this->extend('templates/formindex'); ?>
   <?= $this->section('konten'); ?>
   <style>
       .ck-editor__editable_inline {
           min-height: 300px;
       }
   </style>

   <!-- Begin Page Content -->
   <div class="container">

       <!-- Page Heading -->

       <div class="card shadow mb-4">
           <div class="card-header py-3">
               <h6 class="m-0 font-weight-bold text-primary"><?= $page ?></h6>
           </div>
           
           <div class="card-body">
               <form action="<?= base_url() ?>/user/actionedit" class="add" method="post">
                   <?= csrf_field(); ?>

                   <div class="form-row">
                       <div class="form-group col-lg-6">
                           <label for="judul">Nama</label>
                           <input name="nama" value="<?= user()->fullname ?>" type="text" autofocus class="form-control" id="coa1" placeholder="NAMA ">
                           <input type="hidden" name="id" value="<?= user()->id ?>">
                       </div>
                       <div class="form-group col-lg-6">
                           <label for="judul">Password</label>
                           <input name="password" value="" type="password" autofocus class="form-control" id="coa1" placeholder="PASSWORD">
                       </div>

                       <br>

                       <div class="col text-center">
                           <button type="submit" class="btn btn-outline-success btnsubmit">Kirim</button>
                           <a href="<?=base_url('m_coa_1')?>" class="btn btn-outline-danger btnsubmit">Kembali</a>
                       </div>
                   </div>
               </form>
           </div>
       </div>
   </div>
   <script>
       $(document).ready(function() {
           $('.add').submit(function(e) {
               e.preventDefault();
               $.ajax({
                   type: "post",
                   url: $(this).attr('action'),
                   data: $(this).serialize(),
                   dataType: "json",
                   beforeSend: function() {
                       $('.btnsubmit').attr('disable', 'disabled')

                       $('.btnsubmit').html('<i class="fa fa-spin fa-spinner"</i>')
                   },
                   complete: function() {
                       $('.btnsubmit').removeAttr('disable')

                       $('.btnsubmit').html('simpan')
                   },
                   success: function(response) {
                       if (response.error === 'error') {
                           alertify.set('notifier', 'position', 'top-right');
                           var msg = alertify.error(response.msg, 0);
                           $('body').one('click', function() {
                               msg.dismiss();
                           });
                       }
                       if (response.error === 'sukses') {
                           alertify.set('notifier', 'position', 'top-right');
                           var msg = alertify.success(response.msg, 0);
                           setTimeout(function() {
                               window.location.href = "<?= base_url() ?>/user";
                           }, 1000)





                       }
                   },
                   // error: function (xhr, ajaxOptions, thrownError) {
                   //         alert(xhr.status+"\n"+xhr.responseText+"\n"+thrownError);

                   //     }
               })
           })
       })

       ClassicEditor
           .create(document.querySelector('#editor'))
           .catch(error => {
               console.error(error);
           });
   </script>
   <!-- /.container-fluid -->
   <?= $this->endSection(); ?>
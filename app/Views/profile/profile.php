<?= $this->extend('templates/index'); ?>
<?= $this->section('konten'); ?>

<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $page ?></h1>

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"><?= $user ?></h6>
        </div>
        <div class="card-body">
            <div class="card" style="width: 18rem;">
                <img class="card-img-top" src="<?= (user()->user_img == 'default.svg' ? base_url() . '/img/' . user()->user_img : base_url() . '/img/profile/' . user()->id . '/' . user()->user_img) ?>" alt="Card image cap">
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                    Ubah Gambar
                </button>


                <div class="card-body">

                </div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">Nama : </br><?= user()->fullname ?></li>
                    <li class="list-group-item">Username : </br><?= user()->username ?></li>
                    <li class="list-group-item">Email : </br><?= user()->email ?></li>

                    <div class="card-body">
                        <a href="<?= base_url() ?>/user/edituser" class="card-link btn btn-info">Edit</a>

                    </div>
            </div>

        </div>
    </div>




</div>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?= base_url() ?>/user/upload" method="POST" enctype="multipart/form-data">
                    <?= csrf_field(); ?>
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" name="file_upload" accept=".jpeg,.jpg,.png" id="inputGroupFile02" />
                        <label class="custom-file-label" for="inputGroupFile02">Pilih Gambar</label>
                    </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
        </div>
    </div>
</div>
</form>
<script>
    $('#inputGroupFile02').on('change', function() {
        //get the file name
        var fileName = $(this).val();
        //replace the "Choose a file" label
        $(this).next('.custom-file-label').html(fileName);
    })
</script>
<!-- /.container-fluid -->
<?= $this->endSection(); ?>
<?= $this->extend('templates/index'); ?>
 <?= $this->section('konten'); ?>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script> 
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $page ?></h1>

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"><?= $user ?></h6>
        </div>
        <div class="card-body">
            <form autocomplete="off">

                <div class="form-row align-items-center">
                    <div class="col-3">
                        <label for="tgl_awal" class="sr-only">Tanggal Awal</label>
                        <input type="text" name="tgl_awal" class="form-control datepicker" placeholder="Tanggal Awal" id="tgl_awal">
                    </div>
                    <div class="col-3">
                        <label for="tgl_akhir" class="sr-only">Tanggal Akhir</label>
                        <input type="text" class="form-control datepicker" id="tgl_akhir" placeholder="Tanggal Akhir">
                    </div>
                    <div class="col-3">
                        <label for="level" class="sr-only">Level</label>
                        <select required data-allow-clear="true" class="custom-select  debet select2" name="level" id="level">
                            <option></option>
                            <option value="6">Level 1</option>
                            <option value="2">Level 2</option>
                            <option value="3">Level 3</option>
                            <option value="4">Level 4</option>
                            <!-- <option value="5">Level 5</option> -->

                        </select>

                    </div>
                    <div class="col-3">
                        <label for="level" class="sr-only">Level</label>
                        <select required class="custom-select  debet select2" name="devisi" id="devisi">
                            <option></option>
                            <?php foreach ($dev as  $value) {
                            ?>
                                <option value="<?= $value['id_divisi'] ?>"><?= $value['nama_divisi'] ?></option>

                            <?php
                            } ?>
                            <!-- <option value="5">Level 5</option> -->

                        </select>

                    </div>
                    <div class="col-6">
                        <button type="button" class="btn btn-primary mb-2 btncari">Cari</button>
                    </div>
                </div>
            </form>
            <div class="table-responsive">

            </div>
        </div>
    </div>

</div>
<script>
    $(".datepicker").datepicker();

    function getNumberWithCommas(number) {
        return formatter = new Intl.NumberFormat('id-ID', {
            style: 'currency',
            currency: 'IDR',


        }).format(number);
    }
    $(document).ready(function() {
        $('.btncari').on('click', function() {

            var c = ''
            var div = ''
            var sisa = 0;
            $.ajax({
                type: "post",
                url: "<?= base_url('buku_besar/get_data1') ?>",
                data: {
                    tgl_awal: $('#tgl_awal').val(),
                    tgl_akhir: $('#tgl_akhir').val(),
                    level: $('#level').val(),
                    user: $('#devisi').val()
                },
                async: false,
                dataType: "json",
                success: function(response) {


                    $.each(response.data, function(index, value) {


                        c += `<tr><td>` + value.kode_coa + `</td>` +
                            `<td>` + value.nama_coa + `</td>` +
                            `<td>` + getNumberWithCommas(value.debit) + `</td>` +
                            `<td>` + getNumberWithCommas(value.kredit) + `</td>` +
                            `<td>` + getNumberWithCommas(value.saldo) + `</td>` +
                            `<td> <button type="button" id="` + value.kode_coa + `= onclick="datas(` + value.kode_coa + `)" class="btn btn-sm btn-info text-white details-control">
					<i class="fas fa-info-circle"></i></button></td>`

                    });
                }
                // error: function (xhr, ajaxOptions, thrownError) {

            })
            // Open this row
            var tabel_awal = '<table id = "table" class="table table-striped table-hover display nowrap"  border="0" >' +
                '<thead><tr><th>Kode COA</th><th>Nama Rekening </th><th>Debit </th><th>Kredit</th><th>Jumlah</th><th>details</th></tr></thead><tbody>';
            var tabel_akhir = '</tbody></table>';
            // var d = row.data();
            // row.child(div + tabel_awal + '' + c + '' + tabel_akhir).show();
            var tampil = tabel_awal + '' + c + '' + tabel_akhir

            $('.table-responsive').html(tampil)
            var tables = $('#table').DataTable({
                destroy: true,

                "paging": false,
                "searching": false
            });

            // tr.addClass('shown');


        });

    });
    // function datas(data){

    $(document).on('click', '.details-control', function() {
        var tr = $(this).closest('tr');
        var table = $('#table').DataTable();
        var row = (table).row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {
            var cd = ''
            var div = ''
            var sisa = 0;
            var saldo_awal=0
            var saldo_akhir=0
            var k=''
            $.ajax({
                type: "post",
                url: "<?= base_url('buku_besar/get_detail') ?>",
                data: {
                    tgl_awal: $('#tgl_awal').val(),
                    tgl_akhir: $('#tgl_akhir').val(),
                    level: $('#level').val(),
                    kode_coa: $(this).attr('id'),
                    user: $('#devisi').val()
                },
                async: false,
                dataType: "json",
                success: function(response) {
                    $.each(response.datass, function(index, value) {
                       saldo_awal= value.debit-value.kredit
                       k = `<tr> <td colspan="2" class="text-center">Saldo Awal</td>
 <td style="display: none;"></td><td></td><td></td><td>`+getNumberWithCommas(saldo_awal)+`</td></tr>`

                    });

                    $.each(response.datas, function(index, value) {
                        saldo_akhir+= (value.Type === 'Debit' ?saldo_awal+value.Nilai:saldo_awal-value.Nilai)
                        
                        
                        cd += `<tr>` +
                            `<td>` + value.description + `</td>` +
                            `<td>` + value.Tanggal + `</td>` +
                            `<td> ` + (value.Type === 'Debit' ? getNumberWithCommas(value.Nilai) : '0') + `</td>` +
                            `<td>` + (value.Type === 'kredit' ? getNumberWithCommas(value.Nilai) : '0') + `</td>` +
                            `<td>` + getNumberWithCommas(saldo_akhir) + `</td></tr>`
                            
                            

                    });
                }
                // error: function (xhr, ajaxOptions, thrownError) {

            })
            // Open this row
            var tabel_awal1 = '<table id="child" class="table table-striped table-hover " cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">' +
                '<thead><tr><th>Devisi </th><th>Tanggal </th><th>Debit </th><th>kredit</th><th>saldo</th></tr></thead><tbody>';
            var tabel_akhir1 = '</tbody></table>';
          
            var d1 = row.data();
            row.child(tabel_awal1 + '' +k+''+ cd + '' + tabel_akhir1).show();
            

            var tables = $('#child').DataTable({
                destroy: true,
                scrollY: '100px',
                "ordering": false,
                "paging": false,
                "searching": false
            });

            tr.addClass('shown');
        }
    });

    $('#level').select2({
        placeholder: "Rekening",
        allowClear: true,
        theme: 'bootstrap4',
    });
    $('#devisi').select2({
        placeholder: "Devisi",
        allowClear: true,
        theme: 'bootstrap4',
    });
</script>
<!-- /.container-fluid -->
<?= $this->endSection(); ?>
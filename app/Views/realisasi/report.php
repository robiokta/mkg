<?= $this->extend('templates/index'); ?>
<?= $this->section('konten'); ?>
<style>
    td.details-control {
        /* background: url('https://www.datatables.net/examples/resources/details_open.png') no-repeat center center; */
        cursor: pointer;
    }

    /* tr.shown td.details-control { */
    /* background: url('https://www.datatables.net/examples/resources/details_close.png') no-repeat center center; */
    /* } */
</style>
<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $page ?></h1>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"><?= $user ?></h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="table" class="table table-striped table-hover " cellspacing="0" style="width:100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>No Pengajuan</th>
                            <th>Judul</th>
                            <th>Tanggal Pengajuan</th>
                            <th>Status</th>
                            <th>Devisi</th>
                            <th>Pengajuan</th>
                            <th>Realisasi</th>
                            <th>Sisa</th>
                            <th>Action</th>

                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th colspan="6" style="text-align:right">Total:</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        var table = $('#table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: '<?= base_url() ?>/realisasi/kasir'
            },
            "language": {
                lengthMenu: "Tampilkan _MENU_ Data Per Halaman",
                zeroRecords: "Data Tidak Ditemukan",
                info: "Menampilkan _PAGE_ dari _PAGES_",
                infoEmpty: "Data Tidak ditemukan",
                infoFiltered: "(Penyaringan _MAX_ total data)",
                "search": "Cari",
                "paginate": {
                    "next": "Berikutnya",
                    "previous": "Sebelumnya"
                }
            },
            "footerCallback": function(row, data, start, end, display) {
                var api = this.api(),
                    data;

                // converting to interger to find total
                var intVal = function(i) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '') * 1 :
                        typeof i === 'number' ?
                        i : 0;
                };
                var col6 = api
                    .column(6)
                    .data()
                    .reduce(function(a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);

                var col7 = api
                    .column(7)
                    .data()
                    .reduce(function(a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);


                // Update footer by showing the total with the reference of the column index 

                $(api.column(6).footer()).html($.fn.dataTable.render.number('.', '.', '2', 'Rp.').display(col6));
                $(api.column(7).footer()).html($.fn.dataTable.render.number('.', '.', '2', 'Rp.').display(col7));
            },


            columns: [{
                    "searchable": false,
                    "orderable": false,
                    "targets": 0,
                    "orderable": false,
                    "data": null,
                    "defaultContent": ''
                },
                {
                    data: 'no_pengajuan',
                    name: 'id_pengajuan'
                },
                {
                    data: 'judul',
                    name: 'judul'
                },
                {
                    data: 'tgl_pengajuan',
                    name: 'tgl_pengajuan'
                },
                {
                    data: 'status_name',
                    name: 'status_name'
                },
                {
                    data: 'nama_divisi',
                    name: 'nama_divisi'
                }, {
                    data: 'price',
                    name: 'price',
                    render: $.fn.dataTable.render.number(".", ".", 2, 'Rp. ')
                }, {
                    data: 'spent',
                    name: 'spent',
                    render: function(data, type, row) {
                        if (data == null) {
                            return 'Rp. 0'

                        } else {
                            return $.fn.dataTable.render.number(".", ".", 2, 'Rp. ').display(data)
                        }
                    },
                },
                {
                    data: null,
                    name: 'spent',
                    render: function(data, type, row) {
                        var a = row.price - row.spent
                        return $.fn.dataTable.render.number(".", ".", 2, 'Rp. ').display(a)
                        // if (data == null) {
                        //     return 'Rp. 0'

                        // } else {
                        //     return $.fn.dataTable.render.number(".", ".", 0, 'Rp. ').display(data)
                        // }
                    },
                },
                {
                    "data": "id_pengajuan",
                    orderable: false,
                    render: function(data, type, row, meta) {
                        return `<button type="button" id="${data}" class="btn btn-sm btn-info text-white details-control">
					<i class="fas fa-info-circle"></i></button>
					`;
                    }
                },

            ],

            "order": [
                [1, 'asc']
            ],
            createdRow: function(row, datas, index) {
                if (datas.extn === '') {
                    var td = $(row).find("td:first");
                    td.removeClass('details-control');
                }
            },
            rowCallback: function(row, datas, index) {
                //console.log('rowCallback');
            }
        });

        table.on('order.dt search.dt draw.dt', function() {
            table.column(0, {
                search: 'applied',
                order: 'applied',
                draw: 'applied'
            }).nodes().each(function(cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();
        // Add event listener for opening and closing details
        $('#table tbody').on('click', '.details-control', function() {
            var tr = $(this).closest('tr');
            var row = table.row(tr);

            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            } else {
                var c = ''
                var div = ''
                var sisa = 0;
                $.ajax({
                    type: "post",
                    url: "<?= base_url('realisasi/get_rinci') ?>",
                    data: {
                        id: $(this).attr('id')
                    },
                    async: false,
                    dataType: "json",
                    success: function(response) {

                        $.each(response.rek_mak, function(index, val) {

                            div += `<div>` + val.kode_mak_1 + ` ` + val.kode_mak_2 + ` ` + val.kode_mak_3 + ` ` + val.kode_mak_4 + `</div>` +
                                `<div>` + val.nama_mak_4 + `</div>`

                        });
                        $.each(response.rek_coa, function(index, value) {
                            sisa = value.total_harga - value.realisasi

                            c += `<tr><td>` + value.kode_coa + ` ` + value.kode_coa_2 + ` ` + value.kode_coa_3 + ` ` + value.kode_coa_4 + `</td>` +
                                `<td>` + value.nama_coa_4 + `</td>` +
                                `<td>Rp. ` + getNumberWithCommas(value.total_harga) + `</td>` +
                                `<td>Rp. ` + (value.realisasi == null ? '0' : getNumberWithCommas(value.realisasi)) + `</td>` +
                                `<td>Rp. ` + (sisa == null ? '0' : getNumberWithCommas(sisa)) + `</td>` +

                                `<td> <button type="button" class="btn btn-primary btn-sm" onclick="rincian(` + value.rinci + `)" data-toggle="modal" data-target=".bd-example-modal-lg">
                                <i class="fas fa-file-invoice-dollar"></i></button>
                                </a> </td></tr>`

                        });
                    }
                    // error: function (xhr, ajaxOptions, thrownError) {

                })
                // Open this row
                var tabel_awal = '<table id = "child_details" class="table table-striped table-hover dt-responsive display nowrap" cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">' +
                    '<thead><tr><th>Kode COA</th><th>Nama Rekening </th><th>Disetujui </th><th>Realisasi</th><th>Sisa</th><th>Aksi </th></tr></thead><tbody>';
                var tabel_akhir = '</tbody></table>';
                var d = row.data();
                row.child(div + tabel_awal + '' + c + '' + tabel_akhir).show();

                var tables = $('#child_details').DataTable({
                    destroy: true,
                    scrollY: '100px',
                    "paging": false,
                    "searching": false
                });

                tr.addClass('shown');
            }
        });
    });

    function getNumberWithCommas(number) {
        return formatter = new Intl.NumberFormat('id-ID', {
            style: 'currency',
            currency: 'IDR',


        }).format(number);
    }

    function rincian(id) {
        $('#modal_tabel').empty()
        console.log(id)
        var c1 = ''
        var div = ''
        var sisa = 0;
        $.ajax({
            type: "post",
            url: "<?= base_url('realisasi/get_rincian') ?>",
            data: {
                id: id
            },
            async: false,
            dataType: "json",
            success: function(response) {
                $.each(response.rek_coa, function(index, values) {
                    c1 += `<tr>` + `<td>` + values.no_pengajuan + `</td>` +
                        `<td>` + values.no_trans + `</td>` +
                        `<td>` + values.tgl_realisasi + `</td>` +

                        `<td>` + values.nama_supplier + `</td>` +
                        `<td>Rp.` + getNumberWithCommas(values.nilai_realisasi) + `</td>` +
                        `</tr>`
                });
            }
        })
        // Open this row
        var tabel_awal1 = '<table id = "modal_detail" class="table table-striped table-hover table-hover dt-responsive display nowrap" cellpadding="5" cellspacing="0" border="0" style="padding-left:25px;width:100%;padding-right:25px;">' +
            '<thead><tr><th>No Pengajuan</th><th>No Bukti </th><th>Tanggal Realisasi </th><th>Supplier</th><th>Nilai Realisasi</th></tr></thead><tbody>';
        var tabel_akhir1 = '</tbody></table>';
        $('#modal_tabel').append(tabel_awal1 + c1 + tabel_akhir1)

        var tables = $('#modal_detail').DataTable({
            destroy: true,
            // scrollY: '100px',
            "paging": false,
            // "searching": false
        });
        tables.columns.adjust();
    }
</script>
<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Detail Realisasi</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive" id="modal_tabel">
                </div>

            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
<!-- /.container-fluid -->
<?= $this->endSection(); ?>
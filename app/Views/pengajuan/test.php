<style>
    .modal {
        overflow: auto !important;
    }

    .panel-default {

        overflow: auto;
        height: 450px;
    }
</style>

<div class="row">

    <div class="col-md-12">
        <div class="portlet light portlet-fit portlet-form bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" fa fa-plus font-green"></i>
                    <span class="caption-subject font-green sbold uppercase">Tambah <?php echo $pagetitle ?></span>
                </div>
                <div class="actions">
                </div>
            </div>
            <div class="portlet-body">
                <!-- BEGIN FORM-->
                <form action="<?= @$url ?>/action_add" class="form-horizontal form-add" id="data" role="form" enctype="multipart/form-data" method="POST">
                    <div class="form-body">
                        <div class="alert alert-warning display-hide">
                            <button class="close" data-close="alert"></button> Anda memiliki beberapa kesalahan formulir. Silakan periksa di bawah ini. <br />
                            <span> </span>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label"> Tipe Reklame
                                        <span class="required">*</span>
                                    </label>
                                    <div class="col-md-8">
                                        <select name="billtype" required class="form-control select2 txtx" id="bill">
                                            <option value="">Pilih Reklame</option>
                                            <?php foreach ($show as $key => $val) : ?>
                                                <option value="<?= $val->BILLBOARD_TYPE; ?>"><?= $val->STATUS_NAME; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                        <span class="help-block"></span>
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                                <div id="tempat" class="reset"></div>
                                <script type='text/javascript'>
                                </script>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label"> Jenis Sewa
                                        <span class="required">*</span>
                                    </label>
                                    <div class="col-md-8" id period>
                                        <select name="periode" required class="form-control select2 txtx reset" id="period">
                                            <option value="">Jenis Sewa</option>
                                        </select>
                                        <span class="help-block"></span>
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                                <div id="appen"></div>
                                <div id='myMap' class="peta" style='width: 35vw; height: 50vh;margin-left:28px; '></div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group form-md-line-input peta">
                                    <label class="col-md-4 control-label"> Peta
                                        <span class="required">*</span>
                                    </label>
                                    <div class="col-md-8" id='locationField'>
                                        <input type="text" class="form-control price tx" id="searchBox" placeholder="Denah Lokasi" name="maps">
                                        <span class="help-block"></span>
                                        <input type="hidden" id="latitude" value="" name="latitude">
                                        <input type="hidden" id="longi" value="" name="longitude">
                                        <div id="lang" style="display:none;">Langitude: <span id="langitude"></span> </div>
                                        <div id="long" style="display:none;">Longitude:<span id="longitude"></span> </div>
                                        <div class="form-control-focus"> </div>

                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label"> Foto
                                        <span class="required">*</span>
                                    </label>
                                    <div class="col-md-8">
                                        <div class="input_fields_wrap">
                                            <button class="add_field_button btn blue"><i class="fa fa-plus-square-o" style="font-size:15px;color:white;margin-top:5px;" aria-hidden="true"></i> </button>
                                            <button id="hapus_tombol" class="btn red"><i class="fa fa-minus-square-o" style="font-size:15px;color:white;margin-top:5px;" aria-hidden="true"></i> </button>

                                            <div id="div1">
                                                <input type="text" class="form-control price tx" placeholder="Detail" name="detail[]" required>

                                                &nbsp;
                                                <span class='label label-info' id="select_file" style="margin-bottom:5px"></span>
                                                <span class="help-block"></span>

                                                &nbsp;
                                                <button type="button" style="display:none;" class="btn green" id="btn_preview" data-toggle="modal" data-target="#myModal">Preview</button>

                                                <!-- Modal -->
                                                <div class="modal fade" id="myModal" role="dialog">
                                                    <div class="modal-dialog">

                                                        <!-- Modal content-->
                                                        <div class="modal-content">
                                                            <div style="display:block">
                                                                <button style="display:inline-block" class="btn btn-default btn-xs" type="button" onclick="zoomin()">
                                                                    Perbesar
                                                                </button>

                                                                <button style="display:inline-block" class="btn btn-default btn-xs" type="button" onclick="zoomout()">
                                                                    Perkecil
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div>
                                                                    <div class="panel panel-default">
                                                                        <div class="panel-body">
                                                                            <span class='zoom ex1' id="">
                                                                                <img id="preview" height="100%" width="100%" src="#" class="perbesargambar" alt="your image" />

                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>

                                                <button type="button" class="btn red" style="display:none;" id="btn_photo">Delete</button>
                                                <div class="form-control-focus"> </div>

                                                <a id="btn_chs" class='btn form-control' style=" text-align: left" href='javascript:;'>
                                                    Pilih Foto...
                                                    <input type="file" class="form-control" style='position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;' name="img[]" id="photo" size="40" onchange='$("#select_file").html($(this).val());' accept="image/png, image/jpeg" required>
                                                </a>


                                            </div>
                                        </div>


                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label"> Jangka Waktu
                                        <span class="required">*</span>
                                    </label>
                                    <div class="col-md-8">
                                        <input type="text" data-date-start-date="1d" onChange="ajax();" class="form-control tgl datepicker" autocomplete="off" data-date-format='dd-mm-yyyy' placeholder="Tanggal Awal" required name="tgl_awal" id="tgl_awal">
                                        &nbsp;
                                        <input type="text" class="form-control tgl datepicker" onChange="ajax();" placeholder="Tanggal Akhir" required autocomplete="off" name="tgl_akhir" id="tgl_akhir" data-date-format='dd-mm-yyyy'>
                                        <span class="help-block"></span>
                                        <div class="form-control-focus"></div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label"> Total Pajak
                                        <span class="required">*</span>
                                    </label>
                                    <div class="col-md-8">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="glyphicon" style="font-size: 14px; color: #888; font-weight: 400; margin-left: -10px;">Rp. </i></span>
                                            <input type="text" class="form-control price reset1" placeholder="Pajak" required name="" id="pajakk">
                                            <input type="hidden" id="pajak" name="tax">
                                            <span class="help-block"></span>
                                            <div id="price"> </diV>
                                        </div>
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="text-center">
                            <button type="submit" class="btn blue">Kirim</button>
                            <a href="<?php echo $url ?>" class="btn grey ajaxify">Kembali</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END VALIDATION STATES -->
    </div>
</div>
<div id='output' style="margin-left:10px;float:left;"></div>
<!-- <a href="<?php echo $url ?>" class="ajaxify reload"></a> -->
<script type='text/javascript'>
    $(document).ready(function() {
        var max_fields = 10; //maximum input boxes allowed
        var wrapper = $(".input_fields_wrap"); //Fields wrapper
        var add_button = $(".add_field_button"); //Add button ID

        var x = 1; //initlal text box count
        $(add_button).click(function(e) { //on add input button click
            e.preventDefault();
            if (x < max_fields) { //max input box allowed
                x++; //text box increment
                $(wrapper).append(` <div id="div` + x + `">
					<input type="text" class="form-control price tx"  placeholder="Detail" name="detail[]" required>
											&nbsp;
											<span class='label label-info' id="select_file` + x + `" style="margin-bottom:5px"></span>
											<span class="help-block"></span>

											&nbsp;
											<button type="button" style="display:none;" class="btn green" id="btn_preview` + x + `" data-toggle="modal" data-target="#myModal` + x + `">Preview</button>

											<!-- Modal -->
											<div class="modal fade" id="myModal` + x + `" role="dialog">
												<div class="modal-dialog">

													<!-- Modal content-->
													<div class="modal-content">
													<div style="display:block">
																<button style="display:inline-block" class="btn btn-default btn-xs" type="button" onclick="perbesar(` + x + `)">
																	Perbesar
																</button>

																<button style="display:inline-block" class="btn btn-default btn-xs" type="button" onclick="perkecil(` + x + `)">
																	Perkecil
																</button>
															</div>
														<div class="modal-body">
															<div class="panel panel-default">
																<div class="panel-body">
																	<span class='zoom ex1' id="">
																		<img id="preview` + x + `" src="#" alt="your class="perbesargambar" image" height="100%" width="100%" style="display:none" />
																	<span class='zoom ex1' id="">
																</div>
															</div>
														</div>
														<div class="modal-footer">
															<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
														</div>
													</div>

												</div>
											</div>

											<button type="button" class="btn red" style="display:none;" id="btn_photo` + x + `">Delete</button>
											<div class="form-control-focus"> </div>
											<a id="btn_chs` + x + `" class='btn form-control' style=" text-align: left" href='javascript:;'>
												Pilih Foto...
												<input type="file" class="form-control" style='position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;' name="img[]" id="photo` + x + `" size="40" onchange='$("#select_file` + x + `").html($(this).val());' accept="image/png, image/jpeg" required>
											</a>
											
											</div>`); //add input box

            }

            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        $('#preview' + x).attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }

            $("#photo" + x).change(function() {
                readURL(this);
            });

        });



        $('#hapus_tombol').click(function(e) { //user click on remove text
            e.preventDefault()
            $('#div' + x).remove();
            x--;
        })

    });
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC98KnDnzN_1sRWzetFe98yzDOrcZcEcBo&libraries=places&callback=initMap" async defer></script>
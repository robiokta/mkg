<?= $this->extend('templates/index'); ?>
<?= $this->section('konten'); ?>

<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $page ?></h1>

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"><?= $user ?></h6>
        </div>
        <div class="card-body">
        <?php if (in_groups('admin') | in_groups('unit') ) {
                                
                                ?>
            <a class="btn btn-success float-right" href="<?= base_url('pengajuan/adddata') ?>" role="button"><i class="fas fa-plus"></i></a>
            <?php } ?>

            <table class="table table-striped table-hover dt-responsive" cellspacing="0" id="datatable">
                <thead>
                    <tr>
                        <th>No Pengajuan</th>
                        <th>Judul</th>
                        <th>Pengajuan</th>
                        <th>Tanggal Pengajuan</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($pengajuan as $val) : ?>
                        <tr>
                            <td><?= $val->no_pengajuan ?></td>
                            <td><?= $val->judul ?></td>
                            <td><?= $val->pengajuan ?></td>
                            <td><?= $val->tgl_pengajuan ?></td>
                            <td><?= $val->status_name ?></td>
                            <td>
                                <?php if (in_groups('admin') | in_groups('manager') | in_groups('menku')) {
                                    if (in_groups('manager')) {
                                ?>
                                        <a class="btn btn-success btn-sm " data-toggle="tooltip" data-placement="top" title="Details" href="<?= base_url('pengajuan/persetujuan/' . $val->id) ?>">
                                            <i class="fas fa-check-double"></i>
                                        </a>

                                    <?php
                                    }
                                    else if (in_groups('menku')) {
                                    ?>

                                        <a class="btn btn-success  btn-sm" data-toggle="tooltip" data-placement="top" title="Details" href="<?= base_url('pengajuan/persetujuan/' . $val->id) ?>">
                                            <i class="fas fa-check-double"></i>
                                        </a>

                                    <?php
                                    }
                                    else if (in_groups('admin')) { ?>

                                        <a href="<?= base_url('pengajuan/edit/' . $val->id) ?>" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Edit">
                                            <i class="fas fa-edit"></i>
                                        </a>
                                        <a class="btn btn-danger btn-sm delete" data-toggle="tooltip" data-placement="top" title="Hapus" id="<?= $val->id ?>">
                                            <i class="far fa-trash-alt"></i>
                                        </a>
                                        <a class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="Details" href="<?= base_url('pengajuan/persetujuan/' . $val->id) ?>">
                                            <i class="fas fa-check-double"></i>
                                        </a>
                                    <?php
                                        # code...
                                    }
                                } else  if (in_groups('unit')) {
                                    ?>
                                    <a href="<?= base_url('pengajuan/edit/' . $val->id) ?>" class="btn btn-primary btn-sm <?= ($val->status == 'PO01' || $val->status == 'PO04' ?  'active' : 'disabled') ?>" data-toggle="tooltip" data-placement="top" title="Edit">
                                        <i class="fas fa-edit"></i>
                                    </a>
                                    <a class="btn btn-danger <?= ($val->status == 'PO01' || $val->status == 'PO04' ?  'active' : 'disabled') ?> delete btn-sm" data-toggle="tooltip" data-placement="top" title="Hapus" id="<?= $val->id ?>">
                                        <i class="far fa-trash-alt"></i>
                                    </a>

                                <?php
                                } ?>


                            </td>
                        </tr>
                    <?php endforeach; ?>

                </tbody>
            </table>
        </div>
    </div>




</div>
<script>
    $(document).ready(function() {
        $('#datatable').DataTable({
            autoWidth: false,
            responsive: true
        });

    });

    $(document).on('click', '.delete', function() {
        var $button = $(this);
        var table = $('#datatable').DataTable();
        Swal.fire({
            title: 'Apakah Anda Yakin?',
            text: "Data Tidak Bisa Dikembalikan Lagi",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya!'
        }).then((result) => {

            if (result.isConfirmed) {
                $.ajax({
                    type: "post",
                    url: "<?= base_url('pengajuan/delete_pengajuan') ?>",
                    data: {
                        id: $(this).attr('id')
                    },
                    dataType: "json",
                    success: function(response) {
                        if (response.error === 'error') {
                            Swal.fire({
                                icon: 'error',
                                title: 'gagal',
                                text: response.msg,

                            })
                        }
                        if (response.error === 'sukses') {
                            table.row($button.parents('tr')).remove().draw();
                            Swal.fire(
                                'Berhasil!',
                                'Data Berhasil Dihapus.',
                                'success'
                            )
                        }

                    }
                    // error: function (xhr, ajaxOptions, thrownError) {

                })
            }
        })
    });
</script>
<!-- /.container-fluid -->
<?= $this->endSection(); ?>
<?= $this->extend('templates/formindex'); ?>
<?= $this->section('konten'); ?>
<style>
    .ck-editor__editable_inline {
        min-height: 300px;
    }
</style>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>


<!-- Begin Page Content -->
<div class="container">

    <!-- Page Heading -->

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"><?= $page ?></h6>
        </div>
        <div class="card-body">
            <form action="<?= base_url() ?>/pengajuan/action_add" class="add" enctype="multipart/form-data" method="post">
                <?= csrf_field(); ?>
                <div class="form-row">

                    <div class="form-group col-lg-6">
                        <label for="mak">MAK</label>
                        <select required class="custom-select my-1 mr-sm-2 select2" name="mak_4" id="mak">
                            <option></option>
                            <?php foreach ($coa as $key => $val) : ?>
                                <option value="<?= $val->mak_4_id ?>"><?= $val->namamak ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group col-lg-6">
                        <label for="judul">Judul</label>
                        <input name="judul" type="text" autofocus class="form-control" id="judul" placeholder="Judul">

                    </div>
                    <div class="form-group col-lg-6" id="input_fields_wrap">
                        <label for="coa">COA</label>
                        <select required class="custom-select my-1 mr-sm-2 coa select2" name="coa_4[]" id="coa">
                            <option></option>
                            <?php foreach ($coa1 as $key => $val) : ?>
                                <option value="<?= $val->m_coa_4_id ?>"><?= $val->namacoa ?></option>
                            <?php endforeach; ?>
                        </select>
                        <br>
                        <div class="form-row penjumlahan">
                            <div class="form-group col-lg-6">
                                <label for="jumlah">Jumlah</label>
                                <input name="jumlah[]" type="number" autofocus class="form-control jumlah" id="Jumlah" placeholder="jumlah">
                            </div>
                            <div class="form-group col-lg-6">
                                <label for="harga">Harga Satuan</label>
                                <input name="harga[]" type="text" onkeyup="rupiah(this)" class="form-control dana" id="harga" placeholder="Harga">
                            </div>

                        </div>

                        <button type="button" id="btn" class="btn btn-outline-success input_fields_wrap"><i class="fas fa-plus"></i></button>
                        <button type="button" id="hapus_tombol" class="btn btn-outline-danger input_fields_wrap"><i class="fas fa-minus"></i></button>
                    </div>
                  
                    <div class="form-group col-lg-12">
                        <label for="dana">Total</label>
                        <input name="dana" type="text" class="form-control" id="dana" onchange="rupiah(this)" disabled placeholder="Total Anggaran." required>
                    </div>
                    <div class="form-group col-lg-12 ">
                        <label for="rincian">Deskripsi Pengajuan</label>
                        <textarea name="rincian" class="editor__editable_inline" id="editor"></textarea>
                    </div>

                    <br>

                    <div class="col text-center">
                        <button type="submit" class="btn btn-outline-success btnsubmit">Kirim</button>
                        <a href="<?= base_url('pengajuan') ?>" class="btn btn-outline-danger btnsubmit">Kembali</a>
                    </div>

                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        var max_fields = 10; //maximum input boxes allowed
        var wrapper = $(".input_fields_wrap"); //Fields wrapper
        var add_button = $("#btn"); //Add button ID

        var col = 12
        var x = 1; //initlal text box count
        $(add_button).click(function(e) {
            //on add input button click
            e.preventDefault();

            if (x < max_fields) { //max input box allowed
                x++; //text box increment
                if (x === 2) {
                    col = 6
                }
                $(`<div class="form-group col-lg-6" id="smm` + x + `">
                        <label for="coa">COA</label>
                        <select required class="custom-select my-1 mr-sm-2 coa select2" name="coa_4[]" >
                            <option></option>
                            <?php foreach ($coa1 as $key => $val) : ?>
                                <option value="<?= $val->m_coa_4_id ?>"><?= $val->namacoa ?></option>
                            <?php endforeach; ?>
                        </select>
                       
                        <br>
                        <div class="form-row penjumlahan">

                            <div class="form-group col-lg-6">
                                <label for="jumlah">Jumlah</label>
                                <input name="jumlah[]" type="number" autofocus class="form-control jumlah"  placeholder="jumlah">
                            </div>
                            <div class="form-group col-lg-6">
                                <label for="harga">Harga Satuan</label>
                                <input name="harga[]" type="text" onkeyup="rupiah(this)" class="form-control dana"  placeholder="Harga">
                            </div>

                        </div>
                       
                    </div>`).insertAfter("#input_fields_wrap");
                // $('').append(` `);
            }
            $(".coa").each(function() {

                $('.coa').select2({
                    placeholder: "Pilih COA",
                    allowClear: true,
                    theme: 'bootstrap4',

                });
            });
        });
        $("#hapus_tombol").click(function() {
            $('#smm' + x).remove();

            x--;

        });
    });

    $(document).on("change blur", ".dana", function() {
        var dana = 0
        var satuan = 0
        var hasil = 0

        $("div.penjumlahan").each(function() {
            // get the values from this div:
            var val1 = $('.dana', this).val().split('.').join("")
            val1=val1.split(',').join(".")
            

            var val2 = $('.jumlah', this).val();

            var total = (val1 * 1) * (val2 * 1)
            hasil += total

        });
        hasil = format(hasil)
        $('#dana').val(hasil);

    });


    function rupiah(e) {
        // i am spammy!
        e.value = formatRupiah(e.value, 'Rp. ');
    }

    function format(n, sep, decimals) {
        sep = sep || "."; // Default to period as decimal separator
        decimals = decimals || 2; // Default to 2 decimals

        return n.toLocaleString().split(sep)[0] +
            sep +
            n.toFixed(decimals).split(sep)[1];
    }

    /* Fungsi formatRupiah */
    function formatRupiah(angka, prefix) {
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
            split = number_string.split(','),
            sisa = split[0].length % 3,
            rupiah = split[0].substr(0, sisa),
            ribuan = split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
    }
    $(document).ready(function() {
        $('.add').submit(function(e) {
            e.preventDefault();
            var datas = $('.add').serialize();
            $.ajax({
                type: "post",
                url: $(this).attr('action'),
                data: datas,
                dataType: "json",
                beforeSend: function() {
                    $('.btnsubmit').attr('disable', 'disabled')

                    $('.btnsubmit').html('<i class="fa fa-spin fa-spinner"</i>')
                },
                complete: function() {
                    $('.btnsubmit').removeAttr('disable')

                    $('.btnsubmit').html('simpan')
                },
                success: function(response) {
                    if (response.error === 'error') {
                        alertify.set('notifier', 'position', 'top-right');
                        var msg = alertify.error(response.msg, 0);
                        $('body').one('click', function() {
                            msg.dismiss();
                        });
                    }
                    if (response.error === 'sukses') {
                        alertify.set('notifier', 'position', 'top-right');
                        var msg = alertify.success(response.msg, 0);
                        setTimeout(function() {
                            window.location.href = "<?= base_url() ?>/pengajuan";
                        }, 1000)

                    }
                },

            })
        })
    })

    ClassicEditor
        .create(document.querySelector('#editor'))
        .catch(error => {
            console.error(error);
        });
    $(document).ready(function() {
        $('#mak').select2({
            placeholder: "Pilih MAK",
            allowClear: true,
            theme: 'bootstrap4',
        });
        $('.coa').select2({
            placeholder: "Pilih COA",
            allowClear: true,
            theme: 'bootstrap4',
        });

    });
</script>
<!-- /.container-fluid -->
<?= $this->endSection(); ?>
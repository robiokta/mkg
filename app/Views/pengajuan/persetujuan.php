<?= $this->extend('templates/formindex'); ?>
<?= $this->section('konten'); ?>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>


<style>
    .ck-editor__editable_inline {
        min-height: 300px;
    }

    table {
        border-collapse: collapse;
        color: black;
    }

    tr {
        border-bottom: 1px solid black;
    }
</style>

<!-- Begin Page Content -->
<div class="container">

    <!-- Page Heading -->

    <h1 class="h3 mb-4 text-gray-800"><?= $page ?></h1>
    <div class="card shadow mb-4">

        <?php foreach ($pengajuan as $key => $val) {
        ?>
            <table style="width:100%">
                <tr>
                    <th>Nomer Pengajuan</th>
                    <td><?= $val->no_pengajuan ?> </td>
                </tr>
                <tr>
                    <th>Judul Pengajuan</th>
                    <td><?= $val->judul ?> </td>
                </tr>
                <tr>
                    <th>Rincian</th>
                    <td><?= $val->pengajuan ?> </td>
                </tr>

                <tr>
                    <th>Pemohon</th>
                    <td><?= $val->fullname ?> </td>
                </tr>

                <tr>
                    <th>Devisi</th>
                    <td><?= $val->description ?> </td>
                </tr>
                <tr>
                    <th>Dana</th>
                    <td>
                        <?php foreach ($rinci as $value) {
                            # code...
                        ?>
                            Jumlah Satuan : <?= $value['jumlah'] ?> Harga Satuan Rp. <?= number_format($value['harga'], 2, ',', '.'); ?>
                            <br>
                        <?php } ?>
                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-xs btn-success" data-toggle="modal" data-target="#myModal">
                            <i class="far fa-edit"></i>
                        </button>
                    </td>
                </tr>
            </table>
            <hr>
            <div class="col text-center">
                <a class="btn btn-outline-success acc" id="<?= $val->idpengajuan ?>">Setujui</a>
                <a class="btn btn-outline-danger acc" id="<?= $val->idpengajuan + 2 ?>">Tolak</a>
                <a href="<?= base_url('/pengajuan') ?>" class="btn btn-outline-primary btnsubmit">Kembali</a>
            </div>
    </div>
<?php $a = $val->idpengajuan;
            $b = $val->statuspengajuan;
        } ?>
<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Detail COA</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?= base_url() ?>/pengajuan/ubah" class="add" enctype="multipart/form-data" method="post">
                    <?= csrf_field(); ?>
                    <div class="form-row">
                        <input type="hidden" name="id" value="<?= $id ?>">
                        <?php $i = 1;
                        foreach ($rinci as $isi) {
                            $i++;

                        ?>
                            <div class="form-group col-lg-12" id="smm<?= $i ?>">

                                <label for="coa" class="delete<?= $i ?>">COA</label>
                                <select required class="custom-select my-1 mr-sm-2 coaselect delete<?= $i ?>" name="coa_4[]">
                                    <option></option>
                                    <?php foreach ($coa1 as $key => $val) : ?>
                                        <option value="<?= $val->m_coa_4_id ?>" <?= ($isi['coa_4_id'] == $val->m_coa_4_id ? ' selected' : '') ?>><?= $val->nama_coa ?></option>
                                    <?php endforeach; ?>
                                </select>

                                <div class="form-row delete<?= $i ?>">

                                    <div class="form-group col-lg-6">
                                        <label for="jumlah">Jumlah</label>
                                        <input name="jumlah[]" type="number" value="<?= $isi['jumlah'] ?>" class="form-control jumlah" placeholder="jumlah">
                                    </div>
                                    <div class="form-group col-lg-6">
                                        <label for="harga">Harga Satuan</label>
                                        <input name="harga[]" value="<?= number_format($isi['harga'], 2, ',', '.') ?>" type="text" onkeyup="rupiah(this)" class="form-control dana" placeholder="Harga">
                                    </div>

                                </div>
                            </div><?php $k = $i;
                                } ?>
                        <div class="form-row" id="input_fields_wrap">
                        </div>
                        <button type="button" id="btn" class="btn btn-outline-success input_fields_wrap"><i class="fas fa-plus"></i></button>
                        <button type="button" id="hapus_tombol" class="btn btn-outline-danger input_fields_wrap"><i class="fas fa-minus"></i></button>
                    </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary btnperubahan">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>


<script>
    // $.fn.modal.Constructor.prototype._enforceFocus = function() {

    // };
    $(document).on("change blur", ".dana", function() {
        var dana = 0
        var satuan = 0
        $(".dana").each(function() {

            dana += Number($(this).val().split('.').join(""))
        });
        $(".jumlah").each(function() {

            satuan += Number($(this).val().split('.').join(""))

        });
        hasil = dana * satuan;

        hasil = format(hasil)
        $('#dana').val(hasil)

    });
    $('body').on('shown.bs.modal', '.modal', function() {
        $(this).find('select').each(function() {
            var dropdownParent = $(document.body);
            if ($(this).parents('.modal.in:first').length !== 0)
                dropdownParent = $(this).parents('.modal.in:first');
            $(this).select2({
                dropdownParent: dropdownParent,
                placeholder: "Pilih COA",
                allowClear: true,
                theme: 'bootstrap4',
                // ...
            });
        });
    });
    $(document).ready(function() {


        var max_fields = 10; //maximum input boxes allowed
        var wrapper = $(".input_fields_wrap"); //Fields wrapper
        var add_button = $("#btn"); //Add button ID

        var x = <?= $k ?>; //initlal text box count
        $(add_button).click(function(e) { //on add input button click
            e.preventDefault();

            if (x < max_fields) { //max input box allowed
                x++; //text box increment
                $(`<div class="form-group col-lg-12" id="smm` + x + `">
                        <label for="coa">COA</label>
                        <select required class="custom-select my-1 mr-sm-2 coa select2" name="coa_4[]" >
                            <option></option>
                            <?php foreach ($coa1 as $key => $val) : ?>
                                <option value="<?= $val->m_coa_4_id ?>"><?= $val->nama_coa ?></option>
                            <?php endforeach; ?>
                        </select>
                       
                        <br>
                        <div class="form-row">

                            <div class="form-group col-lg-6">
                                <label for="jumlah">Jumlah</label>
                                <input name="jumlah[]" type="number"  class="form-control jumlah"  placeholder="jumlah">
                            </div>
                            <div class="form-group col-lg-6">
                                <label for="harga">Harga Satuan</label>
                                <input name="harga[]" type="text" onkeyup="rupiah(this)" class="form-control dana" placeholder="Harga">
                            </div>

                        </div>
                       
                    </div>`).insertAfter("#input_fields_wrap");
            }
            $(".coa").each(function() {
                $('.coa').select2({
                    placeholder: "Pilih COA",
                    allowClear: true,
                    theme: 'bootstrap4',

                });
            });

        });
        $("#hapus_tombol").click(function() {
            $('.delete' + x).remove();


            $('#smm' + x).remove();

            x--;

        });
    });

    function rupiah(e) {
        // i am spammy!
        e.value = formatRupiah(e.value, 'Rp. ');
    }

    function format(n, sep, decimals) {
        sep = sep || "."; // Default to period as decimal separator
        decimals = decimals || 2; // Default to 2 decimals

        return n.toLocaleString().split(sep)[0] +
            sep +
            n.toFixed(decimals).split(sep)[1];
    }

    /* Fungsi formatRupiah */
    function formatRupiah(angka, prefix) {
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
            split = number_string.split(','),
            sisa = split[0].length % 3,
            rupiah = split[0].substr(0, sisa),
            ribuan = split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
    }
    $(document).ready(function() {
        $('.add').submit(function(e) {
            e.preventDefault();
            var datas = $('.add').serialize();
            $.ajax({
                type: "post",
                url: $(this).attr('action'),
                data: datas,
                dataType: "json",
                beforeSend: function() {
                    $('.btnperubahan').attr('disable', 'disabled')

                    $('.btnperubahan').html('<i class="fa fa-spin fa-spinner"</i>')
                },
                complete: function() {
                    $('.btnperubahan').removeAttr('disable')

                    $('.btnperubahan').html('simpan')
                },
                success: function(response) {
                    if (response.error === 'error') {
                        alertify.set('notifier', 'position', 'top-right');
                        var msg = alertify.error(response.msg, 0);
                        $('body').one('click', function() {
                            msg.dismiss();
                        });
                    }
                    if (response.error === 'sukses') {
                        alertify.set('notifier', 'position', 'top-right');
                        var msg = alertify.success(response.msg, 0);
                        setTimeout(function() {
                            window.location.reload(true)
                        }, 1000)

                    }
                },
                // error: function (xhr, ajaxOptions, thrownError) {
                //         alert(xhr.status+"\n"+xhr.responseText+"\n"+thrownError);

                //     }
            })
        })
    })

    $(document).on('click', '.acc', function() {
        var $button = $(this);
        var a = <?= $a ?>;
        var b = $(this).attr('id');
        var c = <?= $a + 2 ?>;
        var status = "<?= $b ?>"

        if (a == b) {
            var url = "<?= base_url('pengajuan/acc') ?>"
            <?php if (in_groups('menku')) {
            ?>
                var catatan = "Pengajuan Akan Diteruskan Proses Realisasi"
            <?php
            } else if (in_groups('manager')) { ?>
                var catatan = "Pengajuan Akan Diteruskan Kepada Manager Keuangan"
            <?php

            } ?>


        } else if (b == c) {
            var url = "<?= base_url('pengajuan/decline') ?>"
            var catatan = "Data Akan Dikembalikan"
        }

        Swal.fire({
            title: 'Apakah Anda Yakin?',
            text: catatan,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya!'
        }).then((result) => {

            if (result.isConfirmed) {
                $.ajax({
                    type: "post",
                    url: url,
                    data: {
                        id: <?= $a ?>,
                        status: "<?= $b ?>"
                    },
                    dataType: "json",
                    success: function(response) {
                        if (response.error === 'error') {
                            Swal.fire({
                                icon: 'error',
                                title: 'gagal',
                                text: response.msg,

                            })
                        }
                        if (response.error === 'sukses') {

                            Swal.fire(
                                'Berhasil!',
                                response.msg,
                                'success'
                            )
                            setTimeout(function() {
                                window.location.href = "<?= base_url() ?>/pengajuan";
                            }, 1000)
                        }

                    }
                    // error: function (xhr, ajaxOptions, thrownError) {

                })

            }
        })
    });
</script>
<!-- /.container-fluid -->
<?= $this->endSection(); ?>
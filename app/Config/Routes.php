<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/**
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Dashboard::index');
$routes->get('dashboard/data', 'Dashboard::data',['filter' => 'role:admin,manager,menku,unit']);
$routes->get('userlist', 'Userlist::index', ['filter' => 'role:admin']);
$routes->get('userlist/index', 'Userlist::index', ['filter' => 'role:admin']);
$routes->get('userlist/adduser', 'Userlist::adduser', ['filter' => 'role:admin']);
$routes->get('userlist/user', 'Userlist::user', ['filter' => 'role:admin']);
$routes->get('userlist/edituser/(:num)', 'Userlist::edituser/$1', ['filter' => 'role:admin']);
$routes->get('userlist/action_edit/', 'Userlist::action_edit/', ['filter' => 'role:admin']);


$routes->get('pengajuan', 'Pengajuan::index', ['filter' => 'role:unit,admin,manager,menku']);
$routes->get('pengajuan/index', 'Pengajuan::index', ['filter' => 'role:unit,admin,manager,menku']);


$routes->get('pengajuan/adddata', 'Pengajuan::adddata', ['filter' => 'role:admin,unit']);
$routes->get('pengajuan/action_add', 'Pengajuan::action_add', ['filter' => 'role:unit,admin']);
$routes->get('pengajuan/action_edit', 'Pengajuan::action_edit', ['filter' => 'role:unit,admin']);

$routes->get('pengajuan/edit/(:num)', 'Pengajuan::edit/$1', ['filter' => 'role:unit,admin']);
$routes->get('pengajuan/persetujuan/(:num)', 'Pengajuan::persetujuan/$1', ['filter' => 'role:admin,manager,menku']);

$routes->get('pengajuan/delete_pengajuan', 'Pengajuan::delete_pengajuan', ['filter' => 'role:admin,unit']);
$routes->get('pengajuan/acc', 'Pengajuan::acc', ['filter' => 'role:admin,manager,menku']);
$routes->get('pengajuan/decline', 'Pengajuan::decline', ['filter' => 'role:admin,manager,menku']);

$routes->get('m_coa_1/edit/(:num)', 'M_coa_1::edit/$1', ['filter' => 'role:admin']);
$routes->get('m_coa_1', 'M_coa_1::index', ['filter' => 'role:admin']);
$routes->get('m_coa_1/index', 'M_coa_1::index', ['filter' => 'role:admin']);
$routes->get('m_coa_1/adddata', 'M_coa_1::adddata', ['filter' => 'role:admin']);
$routes->get('m_coa_1/action_add', 'M_coa_1::action_add', ['filter' => 'role:admin']);
$routes->get('m_coa_1/action_edit', 'M_coa_1::action_edit', ['filter' => 'role:admin']);

$routes->get('m_coa_2/edit/(:num)', 'M_coa_2::edit/$1', ['filter' => 'role:admin']);
$routes->get('m_coa_2', 'M_coa_2::index', ['filter' => 'role:admin']);
$routes->get('m_coa_2/index', 'M_coa_2::index', ['filter' => 'role:admin']);
$routes->get('m_coa_2/adddata', 'M_coa_2::adddata', ['filter' => 'role:admin']);
$routes->get('m_coa_2/action_add', 'M_coa_2::action_add', ['filter' => 'role:admin']);
$routes->get('m_coa_2/action_edit', 'M_coa_2::action_edit', ['filter' => 'role:admin']);

$routes->get('m_coa_3/edit/(:num)', 'M_coa_3::edit/$1', ['filter' => 'role:admin']);
$routes->get('m_coa_3', 'M_coa_3::index', ['filter' => 'role:admin']);
$routes->get('m_coa_3/index', 'M_coa_3::index', ['filter' => 'role:admin']);
$routes->get('m_coa_3/adddata', 'M_coa_3::adddata', ['filter' => 'role:admin']);
$routes->get('m_coa_3/action_add', 'M_coa_3::action_add', ['filter' => 'role:admin']);
$routes->get('m_coa_3/action_edit', 'M_coa_3::action_edit', ['filter' => 'role:admin']);

$routes->get('m_coa_4/edit/(:num)', 'M_coa_4::edit/$1', ['filter' => 'role:admin']);
$routes->get('m_coa_4', 'M_coa_4::index', ['filter' => 'role:admin']);
$routes->get('m_coa_4/index', 'M_coa_4::index', ['filter' => 'role:admin']);
$routes->get('m_coa_4/adddata', 'M_coa_4::adddata', ['filter' => 'role:admin']);
$routes->get('m_coa_4/action_add', 'M_coa_4::action_add', ['filter' => 'role:admin']);
$routes->get('m_coa_4/action_edit', 'M_coa_4::action_edit', ['filter' => 'role:admin']);

$routes->get('m_mak_1/edit/(:num)', 'M_mak_1::edit/$1', ['filter' => 'role:admin']);
$routes->get('m_mak_1', 'M_mak_1::index', ['filter' => 'role:admin']);
$routes->get('m_mak_1/index', 'M_mak_1::index', ['filter' => 'role:admin']);
$routes->get('m_mak_1/adddata', 'M_mak_1::adddata', ['filter' => 'role:admin']);
$routes->get('m_mak_1/action_add', 'M_mak_1::action_add', ['filter' => 'role:admin']);
$routes->get('m_mak_1/action_edit', 'M_mak_1::action_edit', ['filter' => 'role:admin']);

$routes->get('m_mak_2/edit/(:num)', 'M_mak_2::edit/$1', ['filter' => 'role:admin']);
$routes->get('m_mak_2', 'M_mak_2::index', ['filter' => 'role:admin']);
$routes->get('m_mak_2/index', 'M_mak_2::index', ['filter' => 'role:admin']);
$routes->get('m_mak_2/adddata', 'M_mak_2::adddata', ['filter' => 'role:admin']);
$routes->get('m_mak_2/action_add', 'M_mak_2::action_add', ['filter' => 'role:admin']);
$routes->get('m_mak_2/action_edit', 'M_mak_2::action_edit', ['filter' => 'role:admin']);

$routes->get('m_mak_3/edit/(:num)', 'M_mak_3::edit/$1', ['filter' => 'role:admin']);
$routes->get('m_mak_3', 'M_mak_3::index', ['filter' => 'role:admin']);
$routes->get('m_mak_3/index', 'M_mak_3::index', ['filter' => 'role:admin']);
$routes->get('m_mak_3/adddata', 'M_mak_3::adddata', ['filter' => 'role:admin']);
$routes->get('m_mak_3/action_add', 'M_mak_3::action_add', ['filter' => 'role:admin']);
$routes->get('m_mak_3/action_edit', 'M_mak_3::action_edit', ['filter' => 'role:admin']);

$routes->get('m_mak_4/edit/(:num)', 'M_mak_4::edit/$1', ['filter' => 'role:admin']);
$routes->get('m_mak_4', 'M_mak_4::index', ['filter' => 'role:admin']);
$routes->get('m_mak_4/index', 'M_mak_4::index', ['filter' => 'role:admin']);
$routes->get('m_mak_4/adddata', 'M_mak_4::adddata', ['filter' => 'role:admin']);
$routes->get('m_mak_4/action_add', 'M_mak_4::action_add', ['filter' => 'role:admin']);
$routes->get('m_mak_4/action_edit', 'M_mak_4::action_edit', ['filter' => 'role:admin']);

$routes->get('pengajuan_cair/index', 'Pengajuan_cair::index', ['filter' => 'role:admin,kasir']);
$routes->get('pengajuan_cair/print', 'Pengajuan_cair::print', ['filter' => 'role:admin,kasir']);
$routes->get('pengajuan_cair/pencairan', 'Pengajuan_cair::pencairan', ['filter' => 'role:admin,kasir']);
$routes->get('pengajuan_cair/kasir', 'Pengajuan_cair::kasir', ['filter' => 'role:admin,kasir']);
$routes->get('pengajuan_cair/get_rinci', 'Pengajuan_cair::get_rinci', ['filter' => 'role:admin,kasir']);
$routes->get('pengajuan_cair/realisai', 'Pengajuan_cair::realisasi', ['filter' => 'role:admin,kasir']);

$routes->get('report_kasir/index', 'Report_kasir::index', ['filter' => 'role:admin,menku']);
$routes->get('report_kasir', 'Report_kasir::index', ['filter' => 'role:admin,menku']);
$routes->get('report_kasir/report', 'Report_kasir::report', ['filter' => 'role:admin,menku']);
$routes->get('report_kasir/report_filter', 'Report_kasir::report_filter', ['filter' => 'role:admin,menku']);


$routes->get('realisasi/index', 'Realisasi::index', ['filter' => 'role:admin,unit']);
$routes->get('realisasi', 'Realisasi::index', ['filter' => 'role:admin,unit']);
$routes->get('realisasi/get_rinci', 'Realisasi::get_rinci', ['filter' => 'role:admin,unit']);
$routes->get('realisasi/get_rincian', 'Realisasi::get_rincian', ['filter' => 'role:admin,unit']);

$routes->get('jurnal_umum/index', 'Jurnal_umum::index', ['filter' => 'role:admin,menku']);
$routes->get('jurnal_umum', 'Jurnal_umum::index', ['filter' => 'role:admin,menku']);
$routes->get('jurnal_umum/adddata', 'Jurnal_umum::adddata', ['filter' => 'role:admin,menku']);
$routes->get('jurnal_umum/action_add', 'Jurnal_umum::action_add', ['filter' => 'role:admin,menku']);



$routes->get('kas_keluar/index', 'Kas_keluar::index', ['filter' => 'role:admin,kasir']);
$routes->get('kas_keluar', 'Kas_keluar::index', ['filter' => 'role:admin,kasir']);
$routes->get('kas_keluar/adddata', 'Kas_keluar::adddata', ['filter' => 'role:admin,kasir']);
$routes->get('kas_keluar/edit', 'Kas_keluar::edit', ['filter' => 'role:admin,kasir']);
$routes->get('kas_keluar/action_add', 'Kas_keluar::action_edit', ['filter' => 'role:admin,kasir']);
$routes->get('kas_keluar/action_add', 'Kas_keluar::action_add', ['filter' => 'role:admin,kasir']);

$routes->get('kas_masuk/index', 'Kas_masuk::index', ['filter' => 'role:admin,kasir']);
$routes->get('kas_masuk', 'Kas_masuk::index', ['filter' => 'role:admin,kasir']);
$routes->get('kas_masuk/adddata', 'Kas_masuk::adddata', ['filter' => 'role:admin,kasir']);
$routes->get('kas_masuk/edit', 'Kas_masuk::edit', ['filter' => 'role:admin,kasir']);
$routes->get('kas_masuk/action_add', 'Kas_masuk::action_edit', ['filter' => 'role:admin,kasir']);
$routes->get('kas_masuk/action_add', 'Kas_masuk::action_add', ['filter' => 'role:admin,kasir']);


/**
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}

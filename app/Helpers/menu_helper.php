<?php

function category_menu2()
{
    $db      = \Config\Database::connect();
    // Select all entries from the menu table
    $query = $db->query("select menu_icon,menu_sequence, m_menu.menu_id as menuid, menu_name, menu_description,
        menu_parent_id from m_menu join menu_role on m_menu.menu_id = menu_role.menu_id join auth_groups_users on menu_role.grup_user=auth_groups_users.group_id where user_id=" . user_id() . " and menu_description !='#' and menu_parent_id=0 order by menu_sequence ASC
");

    // Create a multidimensional array to contain a list of items and parents
    $cat = array(
        'items' => array(),
        'parents' => array()
    );
    // dd($query->getResult());
    // Builds the array lists with data from the menu table
    foreach ($query->getResult() as $cats) {
        // Creates entry into items array with current menu item id ie. $menu['items'][1]
        $cat['items'][$cats->menuid] = $cats;
        // Creates entry into parents array. Parents array contains a list of all items with children
        $cat['parents'][$cats->menu_parent_id][] = $cats->menuid;
    }

    if ($cat) {
        $result = build_category_menu2(0, $cat);
        return $result;
    } else {
        return FALSE;
    }
}

// Menu builder function, parentId 0 is the root
function build_category_menu2($parent, $menu)
{

    $html = "";
    if (isset($menu['parents'][$parent])) {
        $html .= "";
        foreach ($menu['parents'][$parent] as $itemId) {
            if (!isset($menu['parents'][$itemId])) {
                $html .= "<li class='nav-item'><a class='nav-link' href='" . base_url() . '/' . $menu['items'][$itemId]->menu_description . "'><i class='" . $menu['items'][$itemId]->menu_icon . "'></i><span>" . $menu['items'][$itemId]->menu_name . "</span></a></li>";
            }
        }
    }
    return $html;
}


function category_menu()
{
    $db      = \Config\Database::connect();
    // Select all entries from the menu table
    $query = $db->query("select DISTINCT (menu_description), m_menu.menu_id as menuid, menu_name, menu_description,menu_sequence,
        menu_parent_id, menu_icon from m_menu join menu_role on m_menu.menu_id = menu_role.menu_id join auth_groups_users on menu_role.grup_user=auth_groups_users.group_id where user_id=" . user_id() . " and menu_parent_id !=0 or menu_description ='#' order by menu_sequence ASC");

    // Create a multidimensional array to contain a list of items and parents
    $cat = array(
        'items' => array(),
        'parents' => array()
    );
    // dd($query->getResult());
    // Builds the array lists with data from the menu table
    foreach ($query->getResult() as $cats) {
        // Creates entry into items array with current menu item id ie. $menu['items'][1]
        $cat['items'][$cats->menuid] = $cats;
        // Creates entry into parents array. Parents array contains a list of all items with children
        $cat['parents'][$cats->menu_parent_id][] = $cats->menuid;
    }

    if ($cat) {
        $result = build_category_menu(0, $cat);
        return $result;
    } else {
        return FALSE;
    }
}

// Menu builder function, parentId 0 is the root
function build_category_menu($parent, $menu)
{


    $html = "";
    if (isset($menu['parents'][$parent])) {
        foreach ($menu['parents'][$parent] as $itemId) {

            if (isset($menu['parents'][$itemId])) {
                $html .= "<li class='nav-item'><a class='nav-link collapsed' href='#' data-toggle='collapse' data-target='#" . $menu['items'][$itemId]->menu_name . "'
                    aria-expanded='true' aria-controls='collapseUtilities'> <i class='" . $menu['items'][$itemId]->menu_icon . "'></i><span>" . $menu['items'][$itemId]->menu_name . "</span></a>
                    <div id='" . $menu['items'][$itemId]->menu_name . "' class='collapse' aria-labelledby='headingUtilities'
                    data-parent='#accordionSidebar'>
                    <div class='bg-white py-2 collapse-inner rounded'>
                    <h6 class='collapse-header'>Sub Menu:</h6>";
                $html .= build_category_menu3($itemId, $menu);
                // >\n";
            }
        }
        $html .= "</div>
            </div>
        </li>";
    }
    return $html;
}

function build_category_menu3($parent, $menu)
{


    $html = "";
    if (isset($menu['parents'][$parent])) {
        $html .= "<div id='scrollmenu'>";
        foreach ($menu['parents'][$parent] as $itemId) {
            if (!isset($menu['parents'][$itemId])) {
                $html .= "<a class='collapse-item' href='" . base_url() . '/' . $menu['items'][$itemId]->menu_description . "'>" . $menu['items'][$itemId]->menu_name . "</a>";
            }
        }
        $html .= "</div>";
    }
    return $html;
}

<?php

namespace App\Models;

use CodeIgniter\Model;

class Result_model extends Model
{
    public $db;
    public $builder;

    public function __construct()
    {
        parent::__construct();
        $this->db = \Config\Database::connect();
    }

    protected function _get_datatables_query($table, $column_order, $column_search, $order)
    {
        $this->builder = $this->db->table(implode("|",$table[0]));
        $i = 0;
        foreach ($column_search as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->builder->groupStart();
                    $this->builder->like($item, $_POST['search']['value']);
                } else {
                    $this->builder->orLike($item, $_POST['search']['value']);
                }
                if (count($column_search) - 1 == $i)
                    $this->builder->groupEnd();
            }
            $i++;
        }
        if (isset($_POST['order'])) {
            $this->builder->orderBy($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($order)) {
            $order = $order;
            $this->builder->orderBy(key($order), $order[key($order)]);
        }
    }

    public function get_datatables($cjoin, $table, $tablerelasi, $table3, $relation, $relation2, $condition, $column_order, $groupby, $order, $column_search)
    {
        $relasi = count($tablerelasi);
        if ($relasi == 0) {
            $builder = $this->db->table($table);
           
        } elseif ($relasi != 0) {
            $this->builder = $this->db->table(implode("|",$table[0]));
            // $this->builder->select('nilai_realisasi');
            for ($i=1; $i < $relasi; $i++) {
                
                $this->builder->join(implode("|",$table[$i]), implode("|",$tablerelasi[$i]));
            }
            
            // $this->db->join($table2, $relation);
        } 
        // elseif ($cjoin == 3) {
        //     $this->db->from($table);
        //     $this->db->join($table2, $relation);
        //     $this->db->join($table3, $relation2);
        // }
        // if ($condition != '') {
        //     $this->db->where($condition);
        // }

        // if ($groupby != '') {
        //     $this->db->group_by($groupby);
        // }

        // $i = 0;
        
        $query = $this->builder->get(); 
        
        return $query->getResult();
    }

    public function count_filtered($table, $column_order, $column_search, $order, $data = '')
    {
        $this->_get_datatables_query($table, $column_order, $column_search, $order);
        if ($data) {
            $this->builder->where($data);
        }
        $this->builder->get();
        return $this->builder->countAll();
    }

    public function count_all($table, $data = '')
    {
        if ($data) {
            $this->builder->where($data);
        }
        $this->builder->from(implode("|",$table[0]));
        return $this->builder->countAll();
    }
}

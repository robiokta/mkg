<?php

namespace App\Models;

use CodeIgniter\Model;

class Grupmodel extends Model
{
    protected $table = 'auth_groups';

    public function getval($id)
    {
        $sql    = "select users.id userid, username, fullname, email,id_divisi, auth_groups.id as grupid, id_divisi from users join auth_groups_users on users.id =auth_groups_users.user_id join auth_groups on auth_groups_users.group_id=auth_groups.id where users.id=$id";
        $data   = $this->db->query($sql);
        //echo $this->db->last_query();exit();
        return $data->getResult();
    }
    public function lastid()
    {
        $id_divisi=user()->id_divisi;
        $sql    = "select count(id) as id from t_pengajuan where YEAR(tgl_pengajuan) = YEAR(CURDATE()) and id_divisi=$id_divisi ";
        $data   = $this->db->query($sql);
        //echo $this->db->last_query();exit();
        return $data->getResult();
    }
    public function updateuser($data, $id)
    {
        $query = $this->db->table('users')->update($data, array('id' => $id));
        return $query;
    }
    public function updategroup($data, $id)
    {
        $query = $this->db->table('auth_groups_users')->update($data, array('user_id' => $id));
        return $query;
    }
    public function deleteuser($id)
    {
        $query = $this->db->table('users')->delete(array('id' => $id));
        return $query;
    }
    public function delete_pengajuan($data,$id)
    {
        $query = $this->db->table('t_pengajuan')->update($data, array('id' => $id));
        return $query;
    }
    public function insert_pengajuan($data)
    {
        return $this->db->table('t_pengajuan')->insert($data);
    }
    public function insert_kasir($data)
    {
        return $this->db->table('t_kasir')->insert($data);
    }
    public function pengajuan($id)
    {
        $sql    = "select * from t_pengajuan where id=$id and status='PO01'";
        $data   = $this->db->query($sql);
        //echo $this->db->last_query();exit();
        return $data->getResult();
    }
    public function updatepengajuan($data, $id)
    {
        $query = $this->db->table('t_pengajuan')->update($data, array('id' => $id));
        return $query;
    }
    public function acc($data, $id)
    {
        $query = $this->db->table('t_pengajuan')->update($data, array('id' => $id));
        return $query;
    }
    public function kasir($id)
    {
        $sql    = "SELECT fullname,description, name,id_mak_4, judul,no_pengajuan, tgl_pengajuan,id_pengajuan, tgl_acc  FROM t_kasir join t_pengajuan on t_kasir.id_pengajuan = t_pengajuan.id join users on users.id=t_pengajuan.user_id join auth_groups_users on auth_groups_users.user_id=users.id join auth_groups on auth_groups.id=auth_groups_users.group_id where id_kasir=$id ";
        $data   = $this->db->query($sql);
        //echo $this->db->last_query();exit();
        return $data->getResult();
    }
    public function updatekasir($data, $id)
    {
        $query = $this->db->table('t_kasir')->update($data, array('id_kasir' => $id));
        return $query;
    }
    public function insert_sttk($data)
    {
        return $this->db->table('sttk')->insert($data);
    }
    public function nosttk()
    {
        $sql    = "SELECT count(id) idsttk from sttk where tgl_klik = date('Y')";
        $data   = $this->db->query($sql);
        //echo $this->db->last_query();exit();
        return $data->getResult();
    }
    public function coa_1()
    {
        $sql    = "SELECT * FROM m_coa";
        $data   = $this->db->query($sql);
        //echo $this->db->last_query();exit();
        return $data->getResult();
    }
    public function coa_2()
    {
        $sql    = "SELECT *,m_coa_2.nama_coa as namacoa, m_coa_2.status as statuss FROM m_coa_2 join m_coa on m_coa_2.m_coa_id=m_coa.m_coa_id order by kode_coa_2 asc" ;
        $data   = $this->db->query($sql);
        //echo $this->db->last_query();exit();
        return $data->getResult();
    }
    public function insert_coa_1($data)
    {
        return $this->db->table('m_coa')->insert($data);
    }
    public function insert_mak_1($data)
    {
        return $this->db->table('mak_1')->insert($data);
    }

    public function insert_coa_2($data)
    {
        return $this->db->table('m_coa_2')->insert($data);
    }
    public function delete_coa($id)
    {
        $query = $this->db->table('m_coa')->delete(array('m_coa_id' => $id));
        return $query;
    }
    public function delete_coa_2($id)
    {
        $query = $this->db->table('m_coa_2')->delete(array('m_coa_2_id' => $id));
        return $query;
    }

    public function coa($id)
    {
        $sql    = "select * from m_coa where m_coa_id=$id";
        $data   = $this->db->query($sql);
        //echo $this->db->last_query();exit();
        return $data->getResult();
    }
    public function mak($id)
    {
        $sql    = "select * from mak_1 where mak_1_id=$id";
        $data   = $this->db->query($sql);
        //echo $this->db->last_query();exit();
        return $data->getResult();
    }
    public function coa2($id)
    {
        $sql    = "select * from m_coa_2 where m_coa_2_id=$id";
        $data   = $this->db->query($sql);
        //echo $this->db->last_query();exit();
        return $data->getResult();
    }
    
    public function updatecoa($data, $id)
    {
        $query = $this->db->table('m_coa')->update($data, array('m_coa_id' => $id));
        return $query;
    }
    public function updatemak($data, $id)
    {
        $query = $this->db->table('mak_1')->update($data, array('mak_1_id' => $id));
        return $query;
    }
    public function updatecoa2($data, $id)
    {
        $query = $this->db->table('m_coa_2')->update($data, array('m_coa_2_id' => $id));
        return $query;
    }
    public function coa22($id)
    {
        $sql    = "select * from m_coa_2 where m_coa_id=$id";
        $data   = $this->db->query($sql);
        //echo $this->db->last_query();exit();
        return $data->getResult();
    }
    public function mak_1()
    {
        $sql    = "SELECT * FROM mak_1";
        $data   = $this->db->query($sql);
        //echo $this->db->last_query();exit();
        return $data->getResult();
    }
    public function query($query)
    {
        $sql    = "$query";
        $data   = $this->db->query($sql);
        //echo $this->db->last_query();exit();
        return $data->getResult();
    }

}

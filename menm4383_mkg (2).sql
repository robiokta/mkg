-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 27, 2022 at 03:47 PM
-- Server version: 10.2.41-MariaDB-cll-lve
-- PHP Version: 7.3.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `root_mkg`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `resetmvdetailbukubesar` (IN `rc` INT(0))  NO SQL
BEGIN
	  TRUNCATE TABLE mv_detailbukubesar;
     INSERT INTO mv_detailbukubesar
     select * from detail_bukubesar;
     
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `resetmvneraca` (IN `rc` INT(0))  NO SQL
BEGIN
	  TRUNCATE TABLE mv_neraca_lajur;
     INSERT INTO mv_neraca_lajur
     select * from neraca;
     
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `resetmvneraca_x` (IN `rc` INT(0))  NO SQL
BEGIN
	  TRUNCATE TABLE mv_neraca_lajur_x;
     INSERT INTO mv_neraca_lajur_x
     select * from neraca_3;
     
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `resetmvneraca_y` (IN `rc` INT(0))  NO SQL
BEGIN
	  TRUNCATE TABLE mv_neraca_lajur_y;
     INSERT INTO mv_neraca_lajur_y
     select * from neraca_lajur_x;
     
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `auth_activation_attempts`
--

CREATE TABLE `auth_activation_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  `user_agent` varchar(255) NOT NULL,
  `token` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `auth_groups`
--

CREATE TABLE `auth_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth_groups`
--

INSERT INTO `auth_groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'unit', 'UMJT'),
(3, 'unit', 'UMJE'),
(4, 'manager', 'Manager'),
(5, 'menku', 'Menager Keuangan'),
(6, 'kasir', 'Kasir'),
(7, 'manajer_keuangan', 'Manager Keuangan'),
(8, 'unit', 'UMJOP'),
(9, 'unit', 'Direktur'),
(10, 'unit', 'GFA'),
(11, 'manager', 'Manager Trans UMJ'),
(12, 'manager', 'Manager Engineering'),
(13, 'manager', 'Manager Offset'),
(14, 'manager', 'Manager FGA'),
(15, 'unit', 'P3LM'),
(16, 'manager', 'Manager P3LM'),
(17, 'unit', 'Yasmin');

-- --------------------------------------------------------

--
-- Table structure for table `auth_groups_permissions`
--

CREATE TABLE `auth_groups_permissions` (
  `group_id` int(11) UNSIGNED NOT NULL DEFAULT 0,
  `permission_id` int(11) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `auth_groups_users`
--

CREATE TABLE `auth_groups_users` (
  `id_user_gruopus` int(11) NOT NULL,
  `group_id` int(11) UNSIGNED NOT NULL DEFAULT 0,
  `user_id` int(11) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth_groups_users`
--

INSERT INTO `auth_groups_users` (`id_user_gruopus`, `group_id`, `user_id`) VALUES
(1, 1, 34),
(21, 1, 54),
(2, 2, 35),
(3, 3, 33),
(4, 4, 37),
(5, 5, 38),
(6, 6, 30),
(19, 6, 52),
(7, 8, 36),
(10, 9, 45),
(11, 10, 46),
(12, 11, 43),
(15, 12, 48),
(13, 13, 44),
(14, 14, 47),
(16, 15, 49),
(18, 16, 51),
(20, 17, 53);

-- --------------------------------------------------------

--
-- Table structure for table `auth_logins`
--

CREATE TABLE `auth_logins` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `user_id` int(11) UNSIGNED DEFAULT NULL,
  `date` datetime NOT NULL,
  `success` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth_logins`
--

INSERT INTO `auth_logins` (`id`, `ip_address`, `email`, `user_id`, `date`, `success`) VALUES
(1, '127.0.0.1', 'robiokta', NULL, '2020-12-13 21:14:25', 0),
(2, '127.0.0.1', 'robiokta', NULL, '2020-12-13 21:14:37', 0),
(3, '127.0.0.1', 'robi.andri.oktafianto@gmail.com', 2, '2020-12-13 21:15:22', 1),
(4, '127.0.0.1', 'robi.andri.oktafianto@gmail.com', 2, '2020-12-13 22:17:45', 1),
(5, '127.0.0.1', 'robi.andri.oktafianto@gmail.com', 2, '2020-12-13 22:18:13', 1),
(6, '127.0.0.1', 'robi.andri.oktafianto@gmail.com', 2, '2020-12-13 22:18:44', 1),
(7, '127.0.0.1', 'robi.andri.oktafianto@gmail.com', NULL, '2020-12-13 22:21:16', 0),
(8, '127.0.0.1', 'robi.andri.oktafianto@gmail.com', 2, '2020-12-13 22:21:26', 1),
(9, '127.0.0.1', 'robi.andri.oktafianto@gmail.com', 2, '2020-12-13 22:22:10', 1),
(10, '127.0.0.1', 'robi.andri.oktafianto@gmail.com', 2, '2020-12-13 22:22:26', 1),
(11, '127.0.0.1', 'robi.andri.oktafianto@gmail.com', 2, '2020-12-13 22:27:02', 1),
(12, '127.0.0.1', 'robi.andri.oktafianto@gmail.com', 2, '2020-12-13 22:30:06', 1),
(13, '127.0.0.1', 'robi.andri.oktafianto@gmail.com', 2, '2020-12-13 22:34:32', 1),
(14, '127.0.0.1', 'robi.andri.oktafianto@gmail.com', 2, '2020-12-13 22:34:54', 1),
(15, '127.0.0.1', 'robi.andri.oktafianto@gmail.com', 2, '2020-12-14 00:00:24', 1),
(16, '127.0.0.1', 'robi.andri.oktafianto@gmail.com', 2, '2020-12-14 00:00:43', 1),
(17, '127.0.0.1', 'robiraka', 3, '2020-12-14 00:18:18', 0),
(18, '127.0.0.1', 'robi.raka@gmail.com', 3, '2020-12-14 00:18:49', 1),
(19, '127.0.0.1', 'robi.andri.oktafianto@gmail.com', 2, '2020-12-14 00:19:00', 1),
(20, '127.0.0.1', 'robi.raka@gmail.com', 3, '2020-12-14 00:23:46', 1),
(21, '127.0.0.1', 'robi.andri.oktafianto@gmail.com', 2, '2020-12-14 00:25:53', 1),
(22, '127.0.0.1', 'robi.raka@gmail.com', 3, '2020-12-14 00:27:35', 1),
(23, '127.0.0.1', 'robi.raka@gmail.com', 3, '2020-12-14 00:29:15', 1),
(24, '127.0.0.1', 'robi.andri.oktafianto@gmail.com', 2, '2020-12-14 00:31:14', 1),
(25, '127.0.0.1', 'robi.raka@gmail.com', 3, '2020-12-14 00:35:42', 1),
(26, '127.0.0.1', 'robi.andri.oktafianto@gmail.com', 2, '2020-12-14 00:40:40', 1),
(27, '127.0.0.1', 'robi.andri.oktafianto@gmail.com', 2, '2020-12-14 00:49:35', 1),
(28, '127.0.0.1', 'robi.andri.oktafianto@gmail.com', 2, '2020-12-14 00:49:42', 1),
(29, '127.0.0.1', 'robi.raka@gmail.com', 3, '2020-12-14 00:51:03', 1),
(30, '127.0.0.1', 'robi.andri.oktafianto@gmail.com', 2, '2020-12-14 00:56:25', 1),
(31, '127.0.0.1', 'robi.raka@gmail.com', 3, '2020-12-14 01:03:19', 1),
(32, '127.0.0.1', 'robi.andri.oktafianto@gmail.com', 2, '2020-12-14 01:03:52', 1),
(33, '127.0.0.1', 'robi.andri.oktafianto@gmail.com', 2, '2020-12-14 02:21:55', 1),
(34, '127.0.0.1', 'robi.andri.oktafianto@gmail.com', 2, '2020-12-14 05:32:24', 1),
(35, '127.0.0.1', 'robi.andri.oktafianto@gmail.com', 2, '2020-12-14 05:37:30', 1),
(36, '127.0.0.1', 'robi.andri.oktafianto@gmail.com', 2, '2020-12-14 18:55:55', 1),
(37, '127.0.0.1', 'robi', NULL, '2020-12-14 20:49:41', 0),
(38, '127.0.0.1', 'robi@mail.com', 9, '2020-12-14 20:49:52', 1),
(39, '127.0.0.1', 'robi.andri.oktafianto@gmail.com', 2, '2020-12-14 21:40:55', 1),
(40, '127.0.0.1', 'robi@gmail.com', 16, '2020-12-14 22:32:24', 1),
(41, '127.0.0.1', 'robi.andri.oktafianto@gmail.com', 2, '2020-12-14 22:36:47', 1),
(42, '127.0.0.1', 'robi.andri.oktafianto@gmail.com', 2, '2020-12-14 22:52:14', 1),
(43, '127.0.0.1', 'robi.andri.oktafianto@gmail.com', 2, '2020-12-15 18:51:09', 1),
(44, '127.0.0.1', 'robi.andri.oktafianto@gmail.com', 2, '2020-12-15 19:51:05', 1),
(45, '127.0.0.1', 'robi.andri.oktafianto@gmail.com', 2, '2020-12-16 04:51:46', 1),
(46, '127.0.0.1', 'robiraka', NULL, '2020-12-16 05:01:09', 0),
(47, '127.0.0.1', 'robi.raka@gmail.com', 3, '2020-12-16 05:01:17', 1),
(48, '127.0.0.1', 'robi.andri.oktafianto@gmail.com', 2, '2020-12-16 05:12:18', 1),
(49, '127.0.0.1', 'robi.andri.oktafianto@gmail.com', 2, '2020-12-16 06:45:50', 1),
(50, '127.0.0.1', 'robi@mail.com', 20, '2020-12-16 07:11:31', 1),
(51, '127.0.0.1', 'robi.raka@gmail.com', 3, '2020-12-16 07:12:03', 1),
(52, '127.0.0.1', 'robi.raka@gmail.com', 3, '2020-12-16 07:24:20', 1),
(53, '127.0.0.1', 'robiokta', NULL, '2020-12-16 07:27:07', 0),
(54, '127.0.0.1', 'robiokta', NULL, '2020-12-16 07:27:14', 0),
(55, '127.0.0.1', 'robi.andri.oktafianto@gmail.com', 2, '2020-12-16 07:27:21', 1),
(56, '127.0.0.1', 'robiokta', NULL, '2020-12-16 08:51:12', 0),
(57, '127.0.0.1', 'robiokta', NULL, '2020-12-16 08:51:21', 0),
(58, '127.0.0.1', 'robiokta', NULL, '2020-12-16 08:51:29', 0),
(59, '127.0.0.1', 'robi.raka@gmail.com', 3, '2020-12-16 08:51:40', 1),
(60, '127.0.0.1', 'robiokta', NULL, '2020-12-16 08:52:01', 0),
(61, '127.0.0.1', 'robi.andri.oktafianto@gmail.com', 2, '2020-12-16 08:52:08', 1),
(62, '127.0.0.1', 'robi.andri.oktafianto@gmail.com', 2, '2020-12-16 09:07:03', 1),
(63, '127.0.0.1', 'robi.andri.oktafianto@gmail.com', 2, '2020-12-16 19:16:40', 1),
(64, '127.0.0.1', 'robi.andri.oktafianto@gmail.com', 2, '2020-12-16 21:44:06', 1),
(65, '127.0.0.1', 'robi.andri.oktafianto@gmail.com', 2, '2020-12-17 00:40:04', 1),
(66, '127.0.0.1', 'robi@gmaill.com', 27, '2020-12-17 01:53:21', 1),
(67, '127.0.0.1', 'robi.andri.oktafianto@gmail.com', 2, '2020-12-17 01:56:44', 1),
(68, '127.0.0.1', 'robi@gmaill.com', 27, '2020-12-17 01:59:11', 1),
(69, '127.0.0.1', 'robi.andri.oktafianto@gmail.com', 2, '2020-12-17 02:04:11', 1),
(70, '127.0.0.1', 'robiokta', NULL, '2020-12-17 19:22:44', 0),
(71, '127.0.0.1', 'robi.andri.oktafianto@gmail.com', 2, '2020-12-17 19:22:51', 1),
(72, '127.0.0.1', 'robiraka@mail.com', 28, '2020-12-17 21:08:12', 1),
(73, '127.0.0.1', 'robiokta', NULL, '2020-12-17 21:34:08', 0),
(74, '127.0.0.1', 'robi.andri.oktafianto@gmail.com', 2, '2020-12-17 21:34:15', 1),
(75, '127.0.0.1', 'robi.andri.oktafianto@gmail.com', 2, '2020-12-18 00:32:29', 1),
(76, '127.0.0.1', 'robiraka@mail.com', 28, '2020-12-18 01:03:58', 1),
(77, '127.0.0.1', 'robi@mail.com', NULL, '2020-12-18 01:19:21', 0),
(78, '127.0.0.1', 'robi@mail.com', NULL, '2020-12-18 01:19:30', 0),
(79, '127.0.0.1', 'robi@gmail.com', 16, '2020-12-18 01:20:06', 1),
(80, '127.0.0.1', 'robiii', NULL, '2020-12-18 01:27:01', 0),
(81, '127.0.0.1', 'robi@gmaill.com', 27, '2020-12-18 01:27:08', 1),
(82, '127.0.0.1', 'robiraka@mail.com', 28, '2020-12-18 01:27:55', 1),
(83, '127.0.0.1', 'robi@gmail.com', 16, '2020-12-18 01:28:34', 1),
(84, '127.0.0.1', 'robi.andri.oktafianto@gmail.com', 2, '2020-12-18 01:35:12', 1),
(85, '127.0.0.1', 'direktur', NULL, '2020-12-18 01:36:39', 0),
(86, '127.0.0.1', 'direktur@gmail.com', 29, '2020-12-18 01:36:47', 1),
(87, '127.0.0.1', 'robi@gmail.com', 16, '2020-12-18 01:52:13', 1),
(88, '127.0.0.1', 'direktur@gmail.com', 29, '2020-12-18 01:53:50', 1),
(89, '127.0.0.1', 'robi@gmail.com', 16, '2020-12-18 01:54:49', 1),
(90, '127.0.0.1', 'direktur', NULL, '2020-12-18 02:07:57', 0),
(91, '127.0.0.1', 'direktur@gmail.com', 29, '2020-12-18 02:08:03', 1),
(92, '127.0.0.1', 'robi.andri.oktafianto@gmail.com', 2, '2020-12-18 02:08:25', 1),
(93, '127.0.0.1', 'direktur@gmail.com', 29, '2020-12-20 19:33:56', 1),
(94, '127.0.0.1', 'robi.andri.oktafianto@gmail.com', 2, '2020-12-20 19:37:04', 1),
(95, '127.0.0.1', 'kasir@gmail.com', 30, '2020-12-20 19:49:25', 1),
(96, '127.0.0.1', 'robi.andri.oktafianto@gmail.com', 2, '2020-12-20 19:57:43', 1),
(97, '127.0.0.1', 'robiraka@mail.com', 28, '2020-12-20 19:59:48', 1),
(98, '127.0.0.1', 'kasir', NULL, '2020-12-20 19:59:58', 0),
(99, '127.0.0.1', 'kasir@gmail.com', 30, '2020-12-20 20:00:04', 1),
(100, '127.0.0.1', 'kasir@gmail.com', 30, '2020-12-20 20:13:53', 1),
(101, '127.0.0.1', 'robiraka@mail.com', 28, '2020-12-20 20:14:15', 1),
(102, '127.0.0.1', 'kasir@gmail.com', 30, '2020-12-20 20:15:43', 1),
(103, '127.0.0.1', 'kasir@gmail.com', 30, '2020-12-21 00:33:53', 1),
(104, '127.0.0.1', 'kasir@gmail.com', 30, '2020-12-21 00:49:59', 1),
(105, '127.0.0.1', 'direktur@gmail.com', 29, '2020-12-21 00:52:56', 1),
(106, '127.0.0.1', 'kasir@gmail.com', 30, '2020-12-21 00:53:22', 1),
(107, '127.0.0.1', 'robiokta', NULL, '2020-12-22 06:16:17', 0),
(108, '127.0.0.1', 'robi.andri.oktafianto@gmail.com', 2, '2020-12-22 06:16:23', 1),
(109, '127.0.0.1', 'unit@gmail.com', 31, '2020-12-22 06:18:05', 1),
(110, '127.0.0.1', 'manager@mail.com', 32, '2020-12-22 06:18:50', 1),
(111, '127.0.0.1', 'direktur@gmail.com', 29, '2020-12-22 06:19:21', 1),
(112, '127.0.0.1', 'kasir', NULL, '2020-12-22 06:19:41', 0),
(113, '127.0.0.1', 'kasri', NULL, '2020-12-22 06:19:48', 0),
(114, '127.0.0.1', 'kasir', NULL, '2020-12-22 06:19:53', 0),
(115, '127.0.0.1', 'kasir', NULL, '2020-12-22 06:20:01', 0),
(116, '127.0.0.1', 'kasir@gmail.com', 30, '2020-12-22 06:20:32', 1),
(117, '127.0.0.1', 'unit@gmail.com', 31, '2020-12-22 21:11:46', 1),
(118, '127.0.0.1', 'unit', NULL, '2020-12-22 21:15:00', 0),
(119, '127.0.0.1', 'unit@gmail.com', 31, '2020-12-22 21:15:06', 1),
(120, '127.0.0.1', 'manager@mail.com', 32, '2020-12-22 21:38:09', 1),
(121, '127.0.0.1', 'direktur@gmail.com', 29, '2020-12-22 21:38:48', 1),
(122, '127.0.0.1', 'kasir@gmail.com', 30, '2020-12-22 21:42:03', 1),
(123, '127.0.0.1', 'unit@gmail.com', 31, '2020-12-22 21:47:29', 1),
(124, '127.0.0.1', 'manager@mail.com', 32, '2020-12-22 21:48:08', 1),
(125, '127.0.0.1', 'direktur@gmail.com', 29, '2020-12-22 21:49:12', 1),
(126, '127.0.0.1', 'robi.andri.oktafianto@gmail.com', 2, '2020-12-30 07:55:37', 1),
(127, '127.0.0.1', 'robi.andri.oktafianto@gmail.com', 2, '2020-12-30 21:31:50', 1),
(128, '127.0.0.1', 'robi.andri.oktafianto@gmail.com', 2, '2021-01-01 22:07:46', 1),
(129, '127.0.0.1', 'robi.andri.oktafianto@gmail.com', 2, '2021-01-02 01:03:33', 1),
(130, '127.0.0.1', 'robi.andri.oktafianto@gmail.com', 2, '2021-01-03 19:08:12', 1),
(131, '127.0.0.1', 'robi.andri.oktafianto@gmail.com', 2, '2021-01-04 00:18:29', 1),
(132, '127.0.0.1', 'robi.andri.oktafianto@gmail.com', 2, '2021-01-04 18:50:04', 1),
(133, '127.0.0.1', 'robi.andri.oktafianto@gmail.com', 2, '2021-01-04 21:53:59', 1),
(134, '127.0.0.1', 'robi.andri.oktafianto@gmail.com', 2, '2021-01-06 20:02:06', 1),
(135, '127.0.0.1', 'robi.andri.oktafianto@gmail.com', 2, '2021-01-06 20:26:37', 1),
(136, '127.0.0.1', 'robi.andri.oktafianto@gmail.com', 2, '2021-01-07 01:20:53', 1),
(137, '127.0.0.1', 'robi.andri.oktafianto@gmail.com', 2, '2021-01-07 19:06:10', 1),
(138, '127.0.0.1', 'kasir@gmail.com', 30, '2021-01-07 20:51:27', 1),
(139, '127.0.0.1', 'robiokta', NULL, '2021-01-07 20:52:17', 0),
(140, '127.0.0.1', 'robi.andri.oktafianto@gmail.com', 2, '2021-01-07 20:52:26', 1),
(141, '127.0.0.1', 'robi.andri.oktafianto@gmail.com', 2, '2021-01-08 00:10:47', 1),
(142, '::1', 'robiokta', NULL, '2021-01-10 19:56:37', 0),
(143, '::1', 'robiokta', NULL, '2021-01-10 19:56:43', 0),
(144, '::1', 'robi.andri.oktafianto@gmail.com', 2, '2021-01-10 19:56:49', 1),
(145, '::1', 'robi.andri.oktafianto@gmail.com', 2, '2021-01-10 20:34:44', 1),
(146, '::1', 'robi.andri.oktafianto@gmail.com', 2, '2021-01-10 22:29:43', 1),
(147, '::1', 'robi.andri.oktafianto@gmail.com', 2, '2021-01-10 22:32:20', 1),
(148, '::1', 'robi.andri.oktafianto@gmail.com', 2, '2021-01-10 23:41:49', 1),
(149, '::1', 'robi.andri.oktafianto@gmail.com', 2, '2021-01-11 18:45:53', 1),
(150, '::1', 'robi.andri.oktafianto@gmail.com', 2, '2021-01-12 01:20:19', 1),
(151, '::1', 'robi.andri.oktafianto@gmail.com', 2, '2021-01-12 01:41:52', 1),
(152, '::1', 'robi.andri.oktafianto@gmail.com', 2, '2021-01-12 02:40:19', 1),
(153, '::1', 'robi.andri.oktafianto@gmail.com', 2, '2021-01-12 18:50:34', 1),
(154, '::1', 'robi.andri.oktafianto@gmail.com', 2, '2021-01-13 00:37:59', 1),
(155, '::1', 'robi.andri.oktafianto@gmail.com', 2, '2021-01-13 02:39:54', 1),
(156, '::1', 'robi.andri.oktafianto@gmail.com', 2, '2021-01-13 18:50:17', 1),
(157, '::1', 'manager@mail.com', 32, '2021-01-13 21:31:12', 1),
(158, '::1', 'manager@mail.com', 32, '2021-01-13 22:30:53', 1),
(159, '::1', 'robiokta', NULL, '2021-01-14 00:28:45', 0),
(160, '::1', 'robi.andri.oktafianto@gmail.com', 2, '2021-01-14 00:28:51', 1),
(161, '::1', 'direktur@gmail.com', 29, '2021-01-14 00:44:53', 1),
(162, '::1', 'kasir', NULL, '2021-01-14 00:45:17', 0),
(163, '::1', 'kasir', NULL, '2021-01-14 00:45:26', 0),
(164, '::1', 'kasir@gmail.com', 30, '2021-01-14 00:45:32', 1),
(165, '::1', 'kasir@gmail.com', 30, '2021-01-14 01:29:17', 1),
(166, '::1', 'robi.andri.oktafianto@gmail.com', 2, '2021-01-14 02:14:20', 1),
(167, '::1', 'kasir@gmail.com', 30, '2021-01-14 02:29:16', 1),
(168, '::1', 'robi.andri.oktafianto@gmail.com', 2, '2021-01-14 03:45:43', 1),
(169, '::1', 'manager@mail.com', 32, '2021-01-14 03:50:46', 1),
(170, '::1', 'direktur@gmail.com', 29, '2021-01-14 03:51:10', 1),
(171, '::1', 'kasir', NULL, '2021-01-14 03:51:43', 0),
(172, '::1', 'kasir@gmail.com', 30, '2021-01-14 03:51:49', 1),
(173, '::1', 'robi.andri.oktafianto@gmail.com', 2, '2021-01-14 18:52:08', 1),
(174, '::1', 'kasir', NULL, '2021-01-14 18:53:23', 0),
(175, '::1', 'kasir@gmail.com', 30, '2021-01-14 18:53:31', 1),
(176, '::1', 'kasir@gmail.com', 30, '2021-01-15 04:01:21', 1),
(177, '::1', 'kasir@gmail.com', 30, '2021-01-17 18:50:39', 1),
(178, '::1', 'kasir@gmail.com', 30, '2021-01-17 19:45:52', 1),
(179, '::1', 'kasir@gmail.com', 30, '2021-01-18 18:44:48', 1),
(180, '::1', 'robi.andri.oktafianto@gmail.com', 2, '2021-01-18 18:58:35', 1),
(181, '::1', 'robi.andri.oktafianto@gmail.com', 2, '2021-01-19 01:34:54', 1),
(182, '::1', 'kasir@gmail.com', 30, '2021-01-19 19:20:15', 1),
(183, '::1', 'robi.andri.oktafianto@gmail.com', 2, '2021-01-19 20:02:32', 1),
(184, '::1', 'manager@mail.com', 32, '2021-01-19 20:03:15', 1),
(185, '::1', 'robi.andri.oktafianto@gmail.com', 2, '2021-01-19 20:04:54', 1),
(186, '::1', 'direktur@gmail.com', 29, '2021-01-19 20:07:12', 1),
(187, '::1', 'kasir@gmail.com', 30, '2021-01-19 20:09:54', 1),
(188, '::1', 'robiokta', NULL, '2021-01-19 20:27:07', 0),
(189, '::1', 'robi.andri.oktafianto@gmail.com', 2, '2021-01-19 20:27:13', 1),
(190, '::1', 'kasir@gmail.com', 30, '2021-01-19 21:10:57', 1),
(191, '::1', 'manager@mail.com', 32, '2021-01-19 22:12:52', 1),
(192, '::1', 'robi.andri.oktafianto@gmail.com', 2, '2021-01-19 22:13:49', 1),
(193, '::1', 'direktur', NULL, '2021-01-19 22:34:43', 0),
(194, '::1', 'direktur', NULL, '2021-01-19 22:34:50', 0),
(195, '::1', 'direktur@gmail.com', 29, '2021-01-19 22:34:54', 1),
(196, '::1', 'direktur@gmail.com', 29, '2021-01-19 22:36:03', 1),
(197, '::1', 'direktur@gmail.com', 29, '2021-01-19 22:40:43', 1),
(198, '::1', 'unit@gmail.com', 31, '2021-01-19 23:31:40', 1),
(199, '::1', 'manager@mail.com', 32, '2021-01-19 23:32:39', 1),
(200, '::1', 'kasir@gmail.com', 30, '2021-01-19 23:48:48', 1),
(201, '::1', 'direktur', NULL, '2021-01-19 23:50:33', 0),
(202, '::1', 'direktur@gmail.com', 29, '2021-01-19 23:50:39', 1),
(203, '::1', 'unit@gmail.com', 31, '2021-01-19 23:53:56', 1),
(204, '::1', 'robi.andri.oktafianto@gmail.com', 2, '2021-01-20 19:04:24', 1),
(205, '::1', 'enginering@mail.com', 33, '2021-01-20 19:55:40', 1),
(206, '::1', 'robi.andri.oktafianto@gmail.com', 2, '2021-01-20 20:03:51', 1),
(207, '::1', 'enginering@mail.com', 33, '2021-01-20 20:58:48', 1),
(208, '::1', 'manager@mail.com', 32, '2021-01-20 22:05:36', 1),
(209, '::1', 'enginering@mail.com', 33, '2021-01-21 19:20:05', 1),
(210, '::1', 'manager@mail.com', 32, '2021-01-21 19:34:32', 1),
(211, '::1', 'direktur', NULL, '2021-01-21 19:34:54', 0),
(212, '::1', 'direktur', NULL, '2021-01-21 19:35:01', 0),
(213, '::1', 'direktur@gmail.com', 29, '2021-01-21 19:35:21', 1),
(214, '::1', 'kasir', NULL, '2021-01-21 19:38:40', 0),
(215, '::1', 'kasir@gmail.com', 30, '2021-01-21 19:38:46', 1),
(216, '::1', 'robi.andri.oktafianto@gmail.com', 2, '2021-01-21 19:44:10', 1),
(217, '::1', 'menku', NULL, '2021-01-21 19:55:25', 0),
(218, '::1', 'direktur@gmail.com', 29, '2021-01-21 19:55:31', 1),
(219, '::1', 'kasir@gmail.com', 30, '2021-01-21 21:09:57', 1),
(220, '::1', 'robi.andri.oktafianto@gmail.com', 2, '2021-01-22 00:32:44', 1),
(221, '::1', 'kasir@gmail.com', 30, '2021-01-22 03:47:40', 1),
(222, '::1', 'enginering@mail.com', 33, '2021-01-22 03:49:14', 1),
(223, '::1', 'manager@mail.com', 32, '2021-01-22 04:03:52', 1),
(224, '::1', 'enginering@mail.com', 33, '2021-01-22 04:04:16', 1),
(225, '::1', 'manager@mail.com', 32, '2021-01-22 04:05:40', 1),
(226, '::1', 'manager@mail.com', 32, '2021-01-22 04:06:15', 1),
(227, '::1', 'direktur@gmail.com', 29, '2021-01-22 04:11:09', 1),
(228, '::1', 'kasir', NULL, '2021-01-24 06:47:13', 0),
(229, '::1', 'kasir@gmail.com', 30, '2021-01-24 06:47:20', 1),
(230, '::1', 'menku', NULL, '2021-01-24 07:39:38', 0),
(231, '::1', 'direktur@gmail.com', 29, '2021-01-24 07:39:44', 1),
(232, '::1', 'menku', NULL, '2021-01-24 18:46:06', 0),
(233, '::1', 'menku', NULL, '2021-01-24 18:46:14', 0),
(234, '::1', 'direktur@gmail.com', 29, '2021-01-24 18:46:21', 1),
(235, '::1', 'enginering', NULL, '2021-01-24 20:00:08', 0),
(236, '::1', 'enginering@mail.com', 33, '2021-01-24 20:00:12', 1),
(237, '::1', 'kasir@gmail.com', 30, '2021-01-24 20:29:38', 1),
(238, '::1', 'direktur@gmail.com', 29, '2021-01-24 21:05:43', 1),
(239, '::1', 'enginering', NULL, '2021-01-24 21:15:33', 0),
(240, '::1', 'enginering', NULL, '2021-01-24 21:15:38', 0),
(241, '::1', 'enginering@mail.com', 33, '2021-01-24 21:15:44', 1),
(242, '::1', 'direktur@gmail.com', 29, '2021-01-24 21:20:45', 1),
(243, '::1', 'weoqi', NULL, '2021-01-24 21:24:50', 0),
(244, '::1', 'asd', NULL, '2021-01-24 21:26:52', 0),
(245, '::1', 'robi.andri.oktafianto@gmail.com', 2, '2021-01-24 21:29:14', 1),
(246, '::1', 'robiokta', NULL, '2021-01-24 21:29:49', 0),
(247, '::1', 'direktur@gmail.com', 29, '2021-01-24 21:31:21', 1),
(248, '::1', 'robi.andri.oktafianto@gmail.com', 34, '2021-01-24 21:32:35', 1),
(249, '::1', 'robi.andri.oktafianto@gmail.com', 34, '2021-01-24 21:32:41', 1),
(250, '::1', 'trans@gmail.com', 35, '2021-01-24 21:40:58', 1),
(251, '::1', 'enginering@mail.com', 33, '2021-01-24 21:41:32', 1),
(252, '::1', 'manager', NULL, '2021-01-24 21:42:43', 0),
(253, '::1', 'manager@gmail.com', 37, '2021-01-24 21:42:49', 1),
(254, '::1', 'menku', NULL, '2021-01-24 21:43:08', 0),
(255, '::1', 'menku@gmail.com', 38, '2021-01-24 21:43:14', 1),
(256, '::1', 'kasir@gmail.com', 30, '2021-01-24 21:43:51', 1),
(257, '::1', 'robi.andri.oktafianto@gmail.com', 34, '2021-01-25 19:07:39', 1),
(258, '::1', 'robi.andri.oktafianto@gmail.com', 34, '2021-01-26 00:02:00', 1),
(259, '::1', 'robi.andri.oktafianto@gmail.com', 34, '2021-01-26 19:27:33', 1),
(260, '::1', 'enginering@mail.com', 33, '2021-01-26 19:51:10', 1),
(261, '::1', 'robi.andri.oktafianto@gmail.com', 34, '2021-01-26 20:15:04', 1),
(262, '103.76.27.52', 'robi.andri.oktafianto@gmail.com', 34, '2021-01-27 00:03:18', 1),
(263, '103.76.27.52', 'kasir@gmail.com', 30, '2021-01-27 01:09:22', 1),
(264, '103.76.27.52', 'enginering@mail.com', 33, '2021-01-27 03:05:46', 1),
(265, '103.76.27.52', 'robi.andri.oktafianto@gmail.com', 34, '2021-01-27 03:08:01', 1),
(266, '103.76.27.52', 'manager', NULL, '2021-01-27 03:11:40', 0),
(267, '103.76.27.52', 'manager@gmail.com', 37, '2021-01-27 03:11:46', 1),
(268, '103.76.27.52', 'menku@gmail.com', 38, '2021-01-27 03:12:18', 1),
(269, '103.76.27.52', 'kasir@gmail.com', 30, '2021-01-27 03:12:43', 1),
(270, '103.76.27.52', 'kasir@gmail.com', 30, '2021-01-27 03:12:52', 1),
(271, '103.76.27.52', 'menku', NULL, '2021-01-27 03:15:42', 0),
(272, '103.76.27.52', 'menku@gmail.com', 38, '2021-01-27 03:15:47', 1),
(273, '103.76.27.52', 'robi.andri.oktafianto@gmail.com', 34, '2021-01-27 03:17:03', 1),
(274, '103.76.27.52', 'robi.andri.oktafianto@gmail.com', 34, '2021-01-27 03:17:41', 1),
(275, '103.76.27.52', 'robi.andri.oktafianto@gmail.com', 34, '2021-01-27 04:22:00', 1),
(276, '103.76.27.52', 'robi.andri.oktafianto@gmail.com', 34, '2021-01-27 04:22:06', 1),
(277, '103.76.27.52', 'robi.andri.oktafianto@gmail.com', 34, '2021-01-27 04:58:12', 1),
(278, '103.76.27.52', 'robi.andri.oktafianto@gmail.com', 34, '2021-01-27 19:03:18', 1),
(279, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-01-28 01:02:50', 1),
(280, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-01-29 03:56:48', 1),
(281, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-02-01 00:45:33', 1),
(282, '202.67.40.26', 'robi.andri.oktafianto@gmail.com', 34, '2021-02-01 08:12:17', 1),
(283, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-02-01 20:02:18', 1),
(284, '125.166.119.40', 'robi.andri.oktafianto@gmail.com', 34, '2021-02-02 01:23:42', 1),
(285, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-02-02 01:35:43', 1),
(286, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-02-02 19:41:46', 1),
(287, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-02-02 19:44:43', 1),
(288, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-02-02 19:59:07', 1),
(289, '125.166.119.117', 'robi.andri.oktafianto@gmail.com', 34, '2021-02-03 01:16:57', 1),
(290, '182.253.93.195', 'enginering@mail.com', 33, '2021-02-03 01:21:47', 1),
(291, '125.166.119.117', 'enginering@mail.com', 33, '2021-02-03 01:22:07', 1),
(292, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-02-03 01:25:01', 1),
(293, '125.166.119.117', 'dir@gmail.com', 45, '2021-02-03 01:25:57', 1),
(294, '125.166.119.117', 'enginering@mail.com', 33, '2021-02-03 01:28:10', 1),
(295, '125.166.119.117', 'dir@gmail.com', 45, '2021-02-03 01:28:30', 1),
(296, '125.166.119.117', 'enginering@mail.com', 33, '2021-02-03 01:28:39', 1),
(297, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-02-03 01:29:11', 1),
(298, '125.166.119.117', 'manager@gmail.com', 37, '2021-02-03 01:31:13', 1),
(299, '125.166.119.117', 'mankeu', NULL, '2021-02-03 01:34:22', 0),
(300, '125.166.119.117', 'menkeu', NULL, '2021-02-03 01:34:33', 0),
(301, '125.166.119.117', 'menkeu', NULL, '2021-02-03 01:34:47', 0),
(302, '182.253.93.195', 'menkeu', NULL, '2021-02-03 01:35:00', 0),
(303, '182.253.93.195', 'menku@gmail.com', 38, '2021-02-03 01:35:13', 1),
(304, '125.166.119.117', 'menku@gmail.com', 38, '2021-02-03 01:35:18', 1),
(305, '125.166.119.117', 'kasir@gmail.com', 30, '2021-02-03 01:35:47', 1),
(306, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-02-03 01:35:54', 1),
(307, '125.166.119.117', 'robi.andri.oktafianto@gmail.com', 34, '2021-02-03 01:55:07', 1),
(308, '182.253.93.195', 'offset@gmail.com', 36, '2021-02-03 01:57:26', 1),
(309, '182.253.93.195', 'manager@gmail.com', 37, '2021-02-03 02:21:03', 1),
(310, '182.253.93.195', 'menku@gmail.com', 38, '2021-02-03 02:22:08', 1),
(311, '182.253.93.195', 'trans', NULL, '2021-02-03 02:23:54', 0),
(312, '182.253.93.195', 'trans@gmail.com', 35, '2021-02-03 02:24:17', 1),
(313, '182.253.93.195', 'admgfa@gmail.com', 46, '2021-02-03 02:29:50', 1),
(314, '182.253.93.195', 'enginering@mail.com', 33, '2021-02-03 02:30:12', 1),
(315, '182.253.93.195', 'admgfa@gmail.com', 46, '2021-02-03 02:30:41', 1),
(316, '182.253.93.195', 'kasir@gmail.com', 30, '2021-02-03 02:32:08', 1),
(317, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-02-03 02:32:28', 1),
(318, '182.253.93.195', 'enginering@mail.com', 33, '2021-02-03 02:44:32', 1),
(319, '182.253.93.195', 'kasir@gmail.com', 30, '2021-02-03 02:45:48', 1),
(320, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-02-03 02:53:07', 1),
(321, '182.253.93.195', 'trans@gmail.com', 35, '2021-02-03 02:54:42', 1),
(322, '182.253.93.195', 'admgfa@gmail.com', 46, '2021-02-03 03:21:26', 1),
(323, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-02-03 18:44:01', 1),
(324, '182.253.93.195', 'admgfa@gmail.com', 46, '2021-02-03 18:44:49', 1),
(325, '182.253.93.195', 'manager@gmail.com', 37, '2021-02-03 19:06:18', 1),
(326, '182.253.93.195', 'menku@gmail.com', 38, '2021-02-03 19:07:58', 1),
(327, '36.90.226.70', 'robi.andri.oktafianto@gmail.com', 34, '2021-02-03 19:11:33', 1),
(328, '182.253.93.195', 'enginering@mail.com', 33, '2021-02-03 20:24:57', 1),
(329, '182.253.93.195', 'enginering@mail.com', 33, '2021-02-03 20:26:25', 1),
(330, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-02-03 21:24:07', 1),
(331, '182.253.93.195', 'manager@gmail.com', 37, '2021-02-03 21:39:15', 1),
(332, '182.253.93.195', 'admgfa@gmail.com', 46, '2021-02-03 21:57:21', 1),
(333, '182.253.93.195', 'menku@gmail.com', 38, '2021-02-03 22:17:55', 1),
(334, '182.253.93.195', 'manager@gmail.com', 37, '2021-02-03 22:19:58', 1),
(335, '182.253.93.195', 'kasir@gmail.com', 30, '2021-02-03 22:26:39', 1),
(336, '182.253.93.195', 'kasir@gmail.com', 30, '2021-02-04 01:05:20', 1),
(337, '182.253.93.195', 'manageroffset', NULL, '2021-02-04 01:08:42', 0),
(338, '182.253.93.195', 'manageroffset@gmail.com', 44, '2021-02-04 01:09:14', 1),
(339, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-02-04 01:09:39', 1),
(340, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-02-04 01:10:42', 1),
(341, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-02-04 01:15:41', 1),
(342, '182.253.93.195', 'managerfga@gmail.com', 47, '2021-02-04 01:17:17', 1),
(343, '182.253.93.195', 'admfga', NULL, '2021-02-04 01:24:46', 0),
(344, '182.253.93.195', 'admgfa@gmail.com', 46, '2021-02-04 01:24:54', 1),
(345, '182.253.93.195', 'managerfga@gmail.com', 47, '2021-02-04 01:25:34', 1),
(346, '182.253.93.195', 'menku@gmail.com', 38, '2021-02-04 01:26:00', 1),
(347, '182.253.93.195', 'trans@gmail.com', 35, '2021-02-04 01:30:53', 1),
(348, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-02-04 01:31:27', 1),
(349, '182.253.93.195', 'manageroffset@gmail.com', 44, '2021-02-04 01:37:04', 1),
(350, '182.253.93.195', 'offset@gmail.com', 36, '2021-02-04 01:39:24', 1),
(351, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-02-04 01:44:34', 1),
(352, '182.253.93.195', 'managerengineering@gmail.com', 48, '2021-02-04 01:46:53', 1),
(353, '182.253.93.195', 'manager@gmail.com', 37, '2021-02-04 03:20:39', 1),
(354, '182.253.93.195', 'admgfa@gmail.com', 46, '2021-02-04 03:23:21', 1),
(355, '182.253.93.195', 'managerfga@gmail.com', 47, '2021-02-04 03:25:35', 1),
(356, '182.253.93.195', 'menku@gmail.com', 38, '2021-02-04 03:25:53', 1),
(357, '182.253.93.195', 'kasir@gmail.com', 30, '2021-02-04 03:27:14', 1),
(358, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-02-04 03:28:15', 1),
(359, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-02-04 18:40:08', 1),
(360, '182.253.93.195', 'kasir@gmail.com', 30, '2021-02-04 19:45:31', 1),
(361, '182.253.93.195', 'kasir@gmail.com', 30, '2021-02-04 22:17:52', 1),
(362, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-02-05 00:07:57', 1),
(363, '182.253.93.195', 'kasir@gmail.com', 30, '2021-02-05 01:55:41', 1),
(364, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-02-05 03:23:59', 1),
(365, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-02-07 19:58:30', 1),
(366, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-02-08 02:45:23', 1),
(367, '182.253.93.195', 'admgfa@gmail.com', 46, '2021-02-08 23:50:25', 1),
(368, '182.253.93.195', 'enginering@mail.com', 33, '2021-02-09 00:01:07', 1),
(369, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-02-09 00:15:13', 1),
(370, '182.253.93.195', 'manager@gmail.com', 37, '2021-02-09 00:16:43', 1),
(371, '112.215.173.52', 'robi.andri.oktafianto@gmail.com', 34, '2021-02-09 21:58:28', 1),
(372, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-02-09 22:01:31', 1),
(373, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-02-11 01:22:43', 1),
(374, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-02-11 01:23:52', 1),
(375, '182.253.93.195', 'robiokta', NULL, '2021-02-11 01:33:46', 0),
(376, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-02-11 01:34:19', 1),
(377, '182.253.93.195', 'robi', NULL, '2021-02-11 01:38:12', 0),
(378, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-02-11 01:38:23', 1),
(379, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-02-11 03:10:37', 1),
(380, '140.213.57.244', 'robi.andri.oktafianto@gmail.com', 34, '2021-02-13 15:57:42', 1),
(381, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-02-16 01:44:52', 1),
(382, '115.178.227.232', 'robi.andri.oktafianto@gmail.com', 34, '2021-02-20 07:39:46', 1),
(383, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-02-22 21:37:48', 1),
(384, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-03-12 00:50:44', 1),
(385, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-03-14 22:34:47', 1),
(386, '182.1.101.133', 'robi.andri.oktafianto@gmail.com', 34, '2021-04-03 05:22:16', 1),
(387, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-04-07 02:46:13', 1),
(388, '182.253.93.195', 'menku', NULL, '2021-04-07 19:42:01', 0),
(389, '182.253.93.195', 'menku@gmail.com', 38, '2021-04-07 19:42:08', 1),
(390, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-04-07 20:36:48', 1),
(391, '182.253.93.195', 'p3lm@gmail.com', 49, '2021-04-07 20:43:42', 1),
(392, '182.253.93.195', 'managerp3lm@gmail.com', 51, '2021-04-07 20:44:00', 1),
(393, '182.253.93.195', 'managerp3lm@gmail.com', 51, '2021-04-07 20:44:11', 1),
(394, '182.253.93.195', 'menku@gmail.com', 38, '2021-04-07 20:58:27', 1),
(395, '182.253.93.195', 'robiokta', NULL, '2021-04-07 21:06:37', 0),
(396, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-04-07 21:06:57', 1),
(397, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-04-07 21:09:18', 1),
(398, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-04-08 00:40:05', 1),
(399, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-04-11 19:44:19', 1),
(400, '182.253.93.195', 'robiokta', NULL, '2021-04-11 20:10:23', 0),
(401, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-04-11 20:10:53', 1),
(402, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-04-14 19:38:45', 1),
(403, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-04-14 19:46:38', 1),
(404, '182.1.115.120', 'robiokta', NULL, '2021-04-15 01:00:20', 0),
(405, '182.1.115.120', 'robiokta', NULL, '2021-04-15 01:00:35', 0),
(406, '182.1.115.120', 'robi.andri.oktafianto@gmail.com', 34, '2021-04-15 01:03:19', 1),
(407, '182.253.93.195', 'kasir', NULL, '2021-04-15 01:09:00', 0),
(408, '182.253.93.195', 'kasir@gmail.com', 30, '2021-04-15 01:09:10', 1),
(409, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-04-15 02:17:10', 1),
(410, '182.253.93.195', 'kasir@gmail.com', 30, '2021-04-15 02:22:43', 1),
(411, '114.125.86.90', 'robi.andri.oktafianto@gmail.com', 34, '2021-04-22 02:56:08', 1),
(412, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-04-22 20:14:45', 1),
(413, '182.253.93.195', 'kasir', NULL, '2021-04-25 22:39:36', 0),
(414, '182.253.93.195', 'kasir@gmail.com', 30, '2021-04-25 22:39:43', 1),
(415, '182.253.93.195', 'kasir@gmail.com', 30, '2021-04-25 22:57:01', 1),
(416, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-04-29 19:47:53', 1),
(417, '182.253.93.195', 'robiokta', NULL, '2021-05-03 21:29:33', 0),
(418, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-05-03 21:29:45', 1),
(419, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-05-04 20:43:26', 1),
(420, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-05-05 21:53:39', 1),
(421, '182.253.93.195', 'robiokta', NULL, '2021-05-06 02:13:51', 0),
(422, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-05-06 02:14:02', 1),
(423, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-05-07 02:06:55', 1),
(424, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-05-18 19:54:09', 1),
(425, '125.166.117.176', 'robi.andri.oktafianto@gmail.com', 34, '2021-05-18 19:58:50', 1),
(426, '125.166.118.249', 'robi.andri.oktafianto@gmail.com', 34, '2021-05-18 22:17:00', 1),
(427, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-05-20 02:48:33', 1),
(428, '182.253.93.195', 'trans@gmail.com', 35, '2021-05-20 03:11:54', 1),
(429, '182.253.93.195', 'trans@gmail.com', 35, '2021-05-21 03:22:31', 1),
(430, '182.253.93.195', 'admgfa@gmail.com', 46, '2021-05-21 03:27:17', 1),
(431, '125.166.118.148', 'robi.andri.oktafianto@gmail.com', 34, '2021-05-21 03:30:11', 1),
(432, '125.166.118.148', 'robi.andri.oktafianto@gmail.com', 34, '2021-05-21 03:35:36', 1),
(433, '182.253.93.195', 'kasir@gmail.com', 30, '2021-05-21 03:35:47', 1),
(434, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-05-21 03:36:58', 1),
(435, '182.253.93.195', 'admgfa@gmail.com', 46, '2021-05-21 03:39:38', 1),
(436, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-05-21 03:40:50', 1),
(437, '182.253.93.195', 'kasir@gmail.com', 30, '2021-05-21 03:41:39', 1),
(438, '182.253.93.195', 'menku@gmail.com', 38, '2021-05-21 03:57:03', 1),
(439, '182.253.93.195', 'kasir', NULL, '2021-05-21 03:59:28', 0),
(440, '182.253.93.195', 'kasir@gmail.com', 30, '2021-05-21 03:59:33', 1),
(441, '182.253.93.195', 'menku@gmail.com', 38, '2021-05-21 04:13:19', 1),
(442, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-05-21 04:20:38', 1),
(443, '182.253.93.195', 'kasir@gmail.com', 30, '2021-05-21 04:25:22', 1),
(444, '182.253.93.195', 'menku@gmail.com', 38, '2021-05-21 04:27:35', 1),
(445, '36.90.227.62', 'robi.andri.oktafianto@gmail.com', 34, '2021-05-22 14:42:46', 1),
(446, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-05-24 01:23:24', 1),
(447, '182.253.93.195', 'trans@gmail.com', 35, '2021-05-24 01:33:36', 1),
(448, '182.253.93.195', 'trans@gmail.com', 35, '2021-05-24 01:33:45', 1),
(449, '182.253.93.195', 'admgfa@gmail.com', 46, '2021-05-24 01:34:16', 1),
(450, '182.253.93.195', 'admgfa@gmail.com', 46, '2021-05-24 01:35:40', 1),
(451, '182.253.93.195', 'admgfa@gmail.com', 46, '2021-05-24 01:35:51', 1),
(452, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-05-24 01:41:59', 1),
(453, '182.253.93.195', 'offset@gmail.com', 36, '2021-05-24 03:04:14', 1),
(454, '182.253.93.195', 'trans@gmail.com', 35, '2021-05-24 03:34:27', 1),
(455, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-05-25 01:58:44', 1),
(456, '125.166.118.140', 'robi.andri.oktafianto@gmail.com', 34, '2021-05-25 01:58:48', 1),
(457, '125.166.119.89', 'robi.andri.oktafianto@gmail.com', 34, '2021-05-25 04:58:48', 1),
(458, '125.166.119.89', 'kasir', NULL, '2021-05-25 04:59:35', 0),
(459, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-05-27 01:01:33', 1),
(460, '182.253.93.195', 'trans@gmail.com', 35, '2021-05-27 04:21:01', 1),
(461, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-05-27 04:33:04', 1),
(462, '182.253.93.195', 'robiokta', NULL, '2021-05-27 20:31:57', 0),
(463, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-05-27 20:32:07', 1),
(464, '114.125.119.188', 'robi.andri.oktafianto@gmail.com', 34, '2021-05-27 23:33:34', 1),
(465, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-05-28 01:00:39', 1),
(466, '125.166.118.140', 'robi.andri.oktafianto@gmail.com', 34, '2021-05-28 01:17:46', 1),
(467, '182.253.93.195', 'trans@gmail.com', 35, '2021-05-28 01:32:23', 1),
(468, '112.215.153.89', 'kasir@gmail.com', 30, '2021-05-28 01:33:20', 1),
(469, '182.253.93.195', 'kasir@gmail.com', 30, '2021-05-28 01:54:17', 1),
(470, '182.253.93.195', 'robiokta', NULL, '2021-05-28 02:10:12', 0),
(471, '182.253.93.195', 'robiokta', NULL, '2021-05-28 02:10:45', 0),
(472, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-05-28 02:11:07', 1),
(473, '182.253.93.195', 'trans@gmail.com', 35, '2021-05-28 02:40:15', 1),
(474, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-05-28 02:43:37', 1),
(475, '182.253.93.195', 'kasir@gmail.com', 30, '2021-05-28 02:45:16', 1),
(476, '182.253.93.195', 'trans@gmail.com', 35, '2021-05-28 03:43:39', 1),
(477, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-05-30 19:45:08', 1),
(478, '182.253.93.195', 'robiokta', NULL, '2021-05-31 04:28:48', 0),
(479, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-05-31 04:29:04', 1),
(480, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-05-31 04:51:30', 1),
(481, '182.253.93.195', 'robiokta', NULL, '2021-06-01 21:41:32', 0),
(482, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-06-01 21:41:42', 1),
(483, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-06-01 21:41:45', 1),
(484, '182.253.93.195', 'trans@gmail.com', 35, '2021-06-02 02:00:38', 1),
(485, '103.76.27.52', 'trans@gmail.com', 35, '2021-06-02 21:37:54', 1),
(486, '182.253.93.195', 'trans@gmail.com', 35, '2021-06-03 01:33:10', 1),
(487, '103.76.27.52', 'trans@gmail.com', 35, '2021-06-06 21:50:57', 1),
(488, '182.253.93.195', 'robi', NULL, '2021-06-06 22:05:52', 0),
(489, '182.253.93.195', 'robiokta', NULL, '2021-06-06 22:06:19', 0),
(490, '182.253.93.195', 'robiokta', NULL, '2021-06-06 22:06:35', 0),
(491, '182.253.93.195', 'robiokta', NULL, '2021-06-06 22:06:49', 0),
(492, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-06-06 22:11:17', 1),
(493, '182.253.93.195', 'kasir@gmail.com', 30, '2021-06-06 22:11:48', 1),
(494, '112.215.240.36', 'kasir@gmail.com', 30, '2021-06-06 22:31:37', 1),
(495, '112.215.240.36', 'engineering', NULL, '2021-06-06 22:35:42', 0),
(496, '112.215.240.36', 'engineering', NULL, '2021-06-06 22:35:52', 0),
(497, '112.215.240.36', 'robiokta', NULL, '2021-06-06 22:36:04', 0),
(498, '112.215.240.36', 'robiokta', NULL, '2021-06-06 22:36:19', 0),
(499, '112.215.240.36', 'robi.andri.oktafianto@gmail.com', 34, '2021-06-06 22:36:38', 1),
(500, '112.215.240.36', 'enginering@mail.com', 33, '2021-06-06 22:37:32', 1),
(501, '182.253.93.195', 'engineering', NULL, '2021-06-06 22:39:54', 0),
(502, '182.253.93.195', 'enginering@mail.com', 33, '2021-06-06 22:40:14', 1),
(503, '112.215.240.36', 'robi.andri.oktafianto@gmail.com', 34, '2021-06-06 22:44:35', 1),
(504, '112.215.240.36', 'admgfa@gmail.com', 46, '2021-06-06 22:53:47', 1),
(505, '182.253.93.195', 'admgfa@gmail.com', 46, '2021-06-06 22:57:23', 1),
(506, '182.253.93.195', 'enginering@mail.com', 33, '2021-06-07 20:57:52', 1),
(507, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-06-07 23:47:28', 1),
(508, '182.253.93.195', 'enginering@mail.com', 33, '2021-06-08 01:59:27', 1),
(509, '182.253.93.195', 'kasir@gmail.com', 30, '2021-06-08 03:30:24', 1),
(510, '182.253.93.195', 'admgfa@gmail.com', 46, '2021-06-08 03:31:15', 1),
(511, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-06-08 03:42:19', 1),
(512, '182.253.93.195', 'enginering@mail.com', 33, '2021-06-08 19:53:44', 1),
(513, '182.253.93.195', 'admgfa@gmail.com', 46, '2021-06-08 21:10:45', 1),
(514, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-06-08 21:38:01', 1),
(515, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-06-09 01:05:49', 1),
(516, '182.253.93.195', 'admgfa@gmail.com', 46, '2021-06-09 01:14:32', 1),
(517, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-06-09 03:45:38', 1),
(518, '182.253.93.195', 'enginering@mail.com', 33, '2021-06-09 19:36:24', 1),
(519, '182.253.93.195', 'admgfa@gmail.com', 46, '2021-06-09 20:27:40', 1),
(520, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-06-09 22:26:06', 1),
(521, '182.253.93.195', 'p3lm@gmail.com', 49, '2021-06-09 22:32:22', 1),
(522, '182.253.93.195', 'enginering@mail.com', 33, '2021-06-09 23:03:49', 1),
(523, '182.253.93.195', 'kasir@gmail.com', 30, '2021-06-10 00:03:39', 1),
(524, '182.253.93.195', 'enginering@mail.com', 33, '2021-06-10 02:23:47', 1),
(525, '182.253.93.195', 'enginering@mail.com', 33, '2021-06-11 03:43:14', 1),
(526, '182.1.111.237', 'enginering@mail.com', 33, '2021-06-11 22:07:26', 1),
(527, '114.125.117.141', 'enginering@mail.com', 33, '2021-06-13 20:49:40', 1),
(528, '182.253.93.195', 'enginering@mail.com', 33, '2021-06-13 22:21:04', 1),
(529, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-06-13 23:10:02', 1),
(530, '103.144.18.244', 'trans@gmail.com', 35, '2021-06-15 06:30:00', 1),
(531, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-06-16 21:18:18', 1),
(532, '114.125.72.52', 'enginering@mail.com', 33, '2021-06-16 21:37:53', 1),
(533, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-06-17 03:45:06', 1),
(534, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-06-17 20:15:59', 1),
(535, '103.76.27.52', 'trans@gmail.com', 35, '2021-06-17 21:07:11', 1),
(536, '125.166.117.156', 'robi.andri.oktafianto@gmail.com', 34, '2021-06-17 21:41:21', 1),
(537, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-06-17 21:43:56', 1),
(538, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-06-18 01:31:53', 1),
(539, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-06-18 02:48:16', 1),
(540, '103.76.27.52', 'trans@gmail.com', 35, '2021-06-20 22:06:19', 1),
(541, '103.76.27.52', 'trans@gmail.com', 35, '2021-06-21 01:35:32', 1),
(542, '103.76.27.52', 'trans@gmail.com', 35, '2021-06-21 21:15:33', 1),
(543, '103.76.27.52', 'trans@gmail.com', 35, '2021-06-21 21:17:05', 1),
(544, '103.76.27.52', 'trans@gmail.com', 35, '2021-06-22 22:23:01', 1),
(545, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-06-23 21:20:20', 1),
(546, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-06-24 01:23:56', 1),
(547, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-06-28 03:55:39', 1),
(548, '182.253.93.195', 'trans@gmail.com', 35, '2021-07-01 01:00:24', 1),
(549, '182.253.93.195', 'trans@gmail.com', 35, '2021-07-01 02:50:24', 1),
(550, '203.78.117.199', 'robi.andri.oktafianto@gmail.com', 34, '2021-07-03 08:04:51', 1),
(551, '103.76.27.52', 'trans@gmail.com', 35, '2021-07-22 23:20:32', 1),
(552, '125.166.118.199', 'trans@gmail.com', 35, '2021-07-22 23:21:29', 1),
(553, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-07-22 23:21:52', 1),
(554, '182.253.93.195', 'managertrans', NULL, '2021-07-22 23:22:15', 0),
(555, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-07-22 23:22:20', 1),
(556, '103.76.27.52', 'robi.andri.oktafianto@gmail.com', 34, '2021-07-22 23:23:00', 1),
(557, '182.253.93.195', 'devtrans', NULL, '2021-07-22 23:23:03', 0),
(558, '182.253.93.195', 'trans@gmail.com', 35, '2021-07-22 23:23:09', 1),
(559, '125.166.118.199', 'robi.andri.oktafianto@gmail.com', 34, '2021-07-22 23:23:52', 1),
(560, '182.253.93.195', 'kasir@gmail.com', 30, '2021-07-22 23:45:41', 1),
(561, '182.253.93.195', 'kasir@gmail.com', 30, '2021-07-22 23:46:54', 1),
(562, '103.76.27.52', 'robi.andri.oktafianto@gmail.com', 34, '2021-07-23 00:01:57', 1),
(563, '125.166.118.199', 'robi.andri.oktafianto@gmail.com', 34, '2021-07-23 00:03:10', 1),
(564, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-07-23 01:02:27', 1),
(565, '182.253.93.195', 'diyan_diana', NULL, '2021-07-23 01:09:09', 0),
(566, '182.253.93.195', 'diyan_diana', NULL, '2021-07-23 01:09:23', 0),
(567, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-07-23 01:09:33', 1),
(568, '182.253.93.195', 'diyan.diana60@gmail.com', 52, '2021-07-23 01:10:09', 1),
(569, '182.253.93.195', 'kasir@gmail.com', 30, '2021-07-26 20:11:47', 1),
(570, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-07-26 21:03:02', 1),
(571, '182.253.93.195', 'kasir', NULL, '2021-07-26 21:15:01', 0),
(572, '182.253.93.195', 'kasir@gmail.com', 30, '2021-07-26 21:15:06', 1),
(573, '182.253.93.195', 'menkeu', NULL, '2021-07-26 21:15:33', 0),
(574, '182.253.93.195', 'menkeu', NULL, '2021-07-26 21:15:39', 0),
(575, '182.253.93.195', 'menku@gmail.com', 38, '2021-07-26 21:15:59', 1),
(576, '182.253.93.195', 'kasir@gmail.com', 30, '2021-07-27 01:47:30', 1),
(577, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-07-27 03:22:27', 1),
(578, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-07-27 03:31:52', 1),
(579, '182.253.93.195', 'kasir@gmail.com', 30, '2021-07-28 03:16:54', 1),
(580, '182.253.93.195', 'kasir@gmail.com', 30, '2021-07-28 19:42:14', 1),
(581, '125.166.118.215', 'robi.andri.oktafianto@gmail.com', 34, '2021-07-29 00:33:00', 1),
(582, '182.253.93.195', 'kasir@gmail.com', 30, '2021-07-29 02:36:25', 1),
(583, '182.253.93.195', 'kasir@gmail.com', 30, '2021-07-29 03:03:45', 1),
(584, '182.253.93.195', 'kasir@gmail.com', 30, '2021-08-06 01:19:06', 1),
(585, '182.253.93.195', 'kasir@gmail.com', 30, '2021-08-08 20:18:36', 1),
(586, '103.76.27.52', 'offset@gmail.com', 36, '2021-08-08 22:05:07', 1),
(587, '103.76.27.52', 'offset', NULL, '2021-08-08 22:24:38', 0),
(588, '103.76.27.52', 'offset@gmail.com', 36, '2021-08-08 22:24:55', 1),
(589, '182.253.93.195', 'KASIR', NULL, '2021-08-09 02:18:27', 0),
(590, '182.253.93.195', 'kasir@gmail.com', 30, '2021-08-09 02:18:35', 1),
(591, '182.253.93.195', 'kasir@gmail.com', 30, '2021-08-11 22:07:11', 1),
(592, '182.253.93.195', 'kasir@gmail.com', 30, '2021-08-12 20:51:33', 1),
(593, '182.253.93.195', 'kasir@gmail.com', 30, '2021-08-15 20:24:35', 1),
(594, '125.166.119.11', 'robi.andri.oktafianto@gmail.com', 34, '2021-08-17 23:38:24', 1),
(595, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-08-18 00:05:04', 1),
(596, '182.253.93.195', 'trans@gmail.com', 35, '2021-08-18 02:06:07', 1),
(597, '182.253.93.195', 'kasir@gmail.com', 30, '2021-08-18 21:30:37', 1),
(598, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-08-20 01:12:42', 1),
(599, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-09-06 04:00:36', 1),
(600, '182.253.93.195', 'kasir@gmail.com', 30, '2021-09-21 20:51:27', 1),
(601, '182.253.93.195', 'kasir@gmail.com', 30, '2021-09-22 02:50:58', 1),
(602, '182.253.93.195', 'kasir@gmail.com', 30, '2021-09-23 21:47:22', 1),
(603, '182.253.93.195', 'kasir@gmail.com', 30, '2021-09-24 00:46:27', 1),
(604, '103.76.27.52', 'robi.andri.oktafianto@gmail.com', 34, '2021-09-26 20:39:57', 1),
(605, '182.253.93.195', 'kasir', NULL, '2021-10-03 21:24:16', 0),
(606, '182.253.93.195', 'kasir@gmail.com', 30, '2021-10-03 21:24:21', 1),
(607, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-10-03 22:35:56', 1),
(608, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-10-04 20:13:02', 1),
(609, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-10-04 20:13:31', 1),
(610, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-02 20:09:19', 1),
(611, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-02 21:08:50', 1),
(612, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-02 21:08:54', 1),
(613, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-02 21:09:39', 1),
(614, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-02 21:13:57', 1),
(615, '182.1.67.234', 'kasir@gmail.com', 30, '2021-11-02 21:30:19', 1),
(616, '182.253.93.195', 'robiokta', NULL, '2021-11-02 22:36:52', 0),
(617, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-02 22:38:02', 1),
(618, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-02 22:38:08', 1),
(619, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-02 22:40:45', 1),
(620, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-02 22:41:18', 1),
(621, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-02 22:41:20', 1),
(622, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-02 22:57:20', 1),
(623, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-02 22:57:32', 1),
(624, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-02 22:59:06', 1),
(625, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-02 22:59:11', 1),
(626, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-02 23:00:05', 1),
(627, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-02 23:01:20', 1),
(628, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-02 23:18:14', 1),
(629, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-02 23:18:25', 1),
(630, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-02 23:19:07', 1),
(631, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-02 23:19:14', 1),
(632, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-02 23:19:19', 1),
(633, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-02 23:20:19', 1),
(634, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-02 23:21:23', 1),
(635, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-02 23:21:28', 1),
(636, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-02 23:21:50', 1),
(637, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-02 23:23:28', 1),
(638, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-02 23:25:59', 1),
(639, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-02 23:27:02', 1),
(640, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-02 23:30:10', 1),
(641, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-02 23:30:24', 1),
(642, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-02 23:35:53', 1),
(643, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-02 23:36:58', 1),
(644, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-02 23:38:45', 1),
(645, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-02 23:39:33', 1),
(646, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-02 23:39:41', 1),
(647, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-02 23:41:58', 1),
(648, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-02 23:41:59', 1),
(649, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-02 23:42:26', 1),
(650, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-02 23:42:26', 1),
(651, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-02 23:42:26', 1),
(652, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-02 23:42:27', 1),
(653, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-02 23:43:57', 1),
(654, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-02 23:45:12', 1),
(655, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-02 23:45:14', 1),
(656, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 00:17:54', 1),
(657, '182.253.93.195', 'robiokta', NULL, '2021-11-03 00:18:41', 0),
(658, '182.253.93.195', 'robiokta', NULL, '2021-11-03 00:19:19', 0),
(659, '182.253.93.195', 'robiokta', NULL, '2021-11-03 00:19:34', 0),
(660, '182.253.93.195', 'robiokta', NULL, '2021-11-03 00:19:42', 0),
(661, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 00:20:10', 1),
(662, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 00:20:43', 1),
(663, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 00:22:04', 1),
(664, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 01:25:45', 1);
INSERT INTO `auth_logins` (`id`, `ip_address`, `email`, `user_id`, `date`, `success`) VALUES
(665, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 02:07:27', 1),
(666, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 02:09:53', 1),
(667, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 02:10:43', 1),
(668, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 02:12:25', 1),
(669, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 02:14:29', 1),
(670, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 02:16:52', 1),
(671, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 02:20:01', 1),
(672, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 02:24:07', 1),
(673, '182.253.93.195', 'robiokta', NULL, '2021-11-03 02:24:41', 0),
(674, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 02:28:38', 1),
(675, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 02:29:12', 1),
(676, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 02:29:48', 1),
(677, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 02:30:54', 1),
(678, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 02:33:59', 1),
(679, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 02:41:18', 1),
(680, '182.253.93.195', 'robiokta', NULL, '2021-11-03 02:41:46', 0),
(681, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 03:15:41', 1),
(682, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 03:17:52', 1),
(683, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 03:17:54', 1),
(684, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 03:17:54', 1),
(685, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 03:17:54', 1),
(686, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 03:18:52', 1),
(687, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 03:25:56', 1),
(688, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 03:29:50', 1),
(689, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 03:35:03', 1),
(690, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 03:35:26', 1),
(691, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 03:36:25', 1),
(692, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 03:43:09', 1),
(693, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 03:43:53', 1),
(694, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 03:47:49', 1),
(695, '118.99.84.185', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 03:48:03', 1),
(696, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 03:53:07', 1),
(697, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 03:56:52', 1),
(698, '118.99.84.185', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 03:57:29', 1),
(699, '118.99.84.185', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 03:57:35', 1),
(700, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 03:58:42', 1),
(701, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 03:59:03', 1),
(702, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 03:59:06', 1),
(703, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 03:59:48', 1),
(704, '182.253.93.195', 'robiokta', NULL, '2021-11-03 04:05:21', 0),
(705, '182.253.93.195', 'robiokta', NULL, '2021-11-03 04:05:47', 0),
(706, '182.253.93.195', 'robiokta', NULL, '2021-11-03 04:05:48', 0),
(707, '182.253.93.195', 'robiokta', NULL, '2021-11-03 04:05:48', 0),
(708, '182.253.93.195', 'robiokta', NULL, '2021-11-03 04:05:49', 0),
(709, '182.253.93.195', 'robiokta', NULL, '2021-11-03 04:05:50', 0),
(710, '182.253.93.195', 'robiokta', NULL, '2021-11-03 04:06:15', 0),
(711, '182.253.93.195', 'robiokta', NULL, '2021-11-03 04:07:31', 0),
(712, '182.253.93.195', 'robiokta', NULL, '2021-11-03 04:08:00', 0),
(713, '182.253.93.195', 'robiokta', NULL, '2021-11-03 04:10:27', 0),
(714, '182.253.93.195', 'robiokta', NULL, '2021-11-03 04:10:28', 0),
(715, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 04:10:35', 1),
(716, '182.253.93.195', 'robiokta', NULL, '2021-11-03 04:11:02', 0),
(717, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 04:11:32', 1),
(718, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 04:12:36', 1),
(719, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 04:24:18', 1),
(720, '118.99.84.185', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 04:31:28', 1),
(721, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 20:16:02', 1),
(722, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 20:18:18', 1),
(723, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 20:19:42', 1),
(724, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 20:20:43', 1),
(725, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 20:29:06', 1),
(726, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 21:12:22', 1),
(727, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 21:12:34', 1),
(728, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 21:12:37', 1),
(729, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 21:12:37', 1),
(730, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 21:12:38', 1),
(731, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 21:12:38', 1),
(732, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 21:12:38', 1),
(733, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 21:12:50', 1),
(734, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 21:13:06', 1),
(735, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 21:13:34', 1),
(736, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 21:14:34', 1),
(737, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 21:14:37', 1),
(738, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 21:15:13', 1),
(739, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 21:15:21', 1),
(740, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 21:15:29', 1),
(741, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 21:15:31', 1),
(742, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 21:15:32', 1),
(743, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 21:16:03', 1),
(744, '118.99.84.185', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 21:16:04', 1),
(745, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 21:16:20', 1),
(746, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 21:17:55', 1),
(747, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 21:17:58', 1),
(748, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 21:17:59', 1),
(749, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 21:17:59', 1),
(750, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 21:17:59', 1),
(751, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 21:18:00', 1),
(752, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 21:18:00', 1),
(753, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 21:18:22', 1),
(754, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 21:20:11', 1),
(755, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 21:23:28', 1),
(756, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 21:23:57', 1),
(757, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 21:24:01', 1),
(758, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 21:24:07', 1),
(759, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 21:24:09', 1),
(760, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 21:24:10', 1),
(761, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 21:24:10', 1),
(762, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 21:24:11', 1),
(763, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 21:24:12', 1),
(764, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 21:24:13', 1),
(765, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 21:24:13', 1),
(766, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 21:25:14', 1),
(767, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 21:26:05', 1),
(768, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 21:27:24', 1),
(769, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 21:33:00', 1),
(770, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 21:49:11', 1),
(771, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 22:59:36', 1),
(772, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 22:59:37', 1),
(773, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 23:15:15', 1),
(774, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 23:15:15', 1),
(775, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-03 23:15:17', 1),
(776, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-04 01:55:37', 1),
(777, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-04 01:57:07', 1),
(778, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-04 01:59:05', 1),
(779, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-04 01:59:07', 1),
(780, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-04 02:04:05', 1),
(781, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-04 02:04:16', 1),
(782, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-04 02:04:20', 1),
(783, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-04 02:04:20', 1),
(784, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-04 02:04:21', 1),
(785, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-04 02:04:21', 1),
(786, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-04 02:04:22', 1),
(787, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-04 02:04:22', 1),
(788, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-04 02:04:23', 1),
(789, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-04 02:04:43', 1),
(790, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-04 02:04:53', 1),
(791, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-04 02:06:14', 1),
(792, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-04 02:07:33', 1),
(793, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-04 02:09:36', 1),
(794, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-04 02:49:48', 1),
(795, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-04 02:51:42', 1),
(796, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-04 02:53:24', 1),
(797, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-07 20:31:36', 1),
(798, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-07 20:34:08', 1),
(799, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-07 20:36:10', 1),
(800, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-07 20:43:21', 1),
(801, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-07 21:40:01', 1),
(802, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-07 21:40:58', 1),
(803, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-07 21:42:26', 1),
(804, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-07 21:51:23', 1),
(805, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-07 22:05:07', 1),
(806, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-07 22:13:12', 1),
(807, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-07 22:14:48', 1),
(808, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-07 22:17:53', 1),
(809, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-07 22:21:46', 1),
(810, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-07 22:26:50', 1),
(811, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-08 02:26:19', 1),
(812, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-08 02:27:59', 1),
(813, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-08 02:28:28', 1),
(814, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-08 02:28:47', 1),
(815, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-08 02:28:47', 1),
(816, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-08 02:40:33', 1),
(817, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-08 02:41:44', 1),
(818, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-08 02:43:27', 1),
(819, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-08 02:44:45', 1),
(820, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-08 02:48:38', 1),
(821, '182.253.93.195', 'managertrans', NULL, '2021-11-08 02:50:51', 0),
(822, '182.253.93.195', 'managertrans', NULL, '2021-11-08 02:52:42', 0),
(823, '182.253.93.195', 'managertrans', NULL, '2021-11-08 02:59:05', 0),
(824, '182.253.93.195', 'managertrans', NULL, '2021-11-08 02:59:33', 0),
(825, '182.253.93.195', 'managertrans', NULL, '2021-11-08 03:01:39', 0),
(826, '182.253.93.195', 'managertrans', NULL, '2021-11-08 03:04:51', 0),
(827, '182.253.93.195', 'managertrans', NULL, '2021-11-08 03:06:33', 0),
(828, '182.253.93.195', 'managertrans', NULL, '2021-11-08 03:09:31', 0),
(829, '182.253.93.195', 'managertrans', NULL, '2021-11-08 03:13:55', 0),
(830, '182.253.93.195', 'managertrans', NULL, '2021-11-08 03:13:55', 0),
(831, '182.253.93.195', 'managertrans', NULL, '2021-11-08 03:13:56', 0),
(832, '182.253.93.195', 'managertrans', NULL, '2021-11-08 03:15:06', 0),
(833, '182.253.93.195', 'managertrans', NULL, '2021-11-08 03:17:03', 0),
(834, '182.253.93.195', 'managertrans', NULL, '2021-11-08 03:18:23', 0),
(835, '182.253.93.195', 'managertrans', NULL, '2021-11-08 03:19:54', 0),
(836, '182.253.93.195', 'managertrans', NULL, '2021-11-08 03:20:51', 0),
(837, '182.253.93.195', 'managertrans', NULL, '2021-11-08 03:21:47', 0),
(838, '182.253.93.195', 'managertrans', NULL, '2021-11-08 03:22:48', 0),
(839, '182.253.93.195', 'managertrans', NULL, '2021-11-08 03:23:09', 0),
(840, '182.253.93.195', 'managertrans', NULL, '2021-11-08 03:23:11', 0),
(841, '182.253.93.195', 'managertrans', NULL, '2021-11-08 03:23:49', 0),
(842, '182.253.93.195', 'managertrans', NULL, '2021-11-08 03:23:56', 0),
(843, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-08 03:24:18', 1),
(844, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-08 03:24:58', 1),
(845, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-08 03:27:08', 1),
(846, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-08 03:28:15', 1),
(847, '182.253.93.195', 'managertrans', NULL, '2021-11-08 03:29:28', 0),
(848, '36.90.226.45', 'managertrans@gmail.com', 43, '2021-11-08 03:44:58', 1),
(849, '36.90.226.45', 'managertrans@gmail.com', 43, '2021-11-08 03:48:18', 1),
(850, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-08 18:51:06', 1),
(851, '182.253.93.195', 'kasir@gmail.com', 30, '2021-11-08 18:55:55', 1),
(852, '182.253.93.195', 'kasir@gmail.com', 30, '2021-11-08 23:46:47', 1),
(853, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-09 19:07:08', 1),
(854, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-09 20:48:29', 1),
(855, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-09 20:49:17', 1),
(856, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-09 20:50:30', 1),
(857, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-09 20:52:47', 1),
(858, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-10 00:31:09', 1),
(859, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-10 00:33:31', 1),
(860, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-10 00:37:45', 1),
(861, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-10 00:56:02', 1),
(862, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-10 00:58:37', 1),
(863, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-10 01:02:27', 1),
(864, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-10 01:03:51', 1),
(865, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-10 01:15:55', 1),
(866, '118.99.84.184', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-10 01:40:59', 1),
(867, '182.1.92.34', 'managertrans@gmail.com', 43, '2021-11-10 01:45:10', 1),
(868, '182.1.92.34', 'managertrans@gmail.com', 43, '2021-11-10 01:49:46', 1),
(869, '182.1.92.34', 'managertrans@gmail.com', 43, '2021-11-10 01:52:40', 1),
(870, '182.1.92.34', 'managertrans@gmail.com', 43, '2021-11-10 02:08:46', 1),
(871, '182.1.92.34', 'managertrans@gmail.com', 43, '2021-11-10 02:15:26', 1),
(872, '182.1.92.34', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-10 02:20:54', 1),
(873, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-10 02:46:32', 1),
(874, '182.253.93.195', 'kasir@gmail.com', 30, '2021-11-10 18:47:23', 1),
(875, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-10 19:08:04', 1),
(876, '182.253.93.195', 'robiokta13', NULL, '2021-11-10 20:12:49', 0),
(877, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-10 20:20:30', 1),
(878, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-10 20:22:02', 1),
(879, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-10 20:51:30', 1),
(880, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-10 21:19:12', 1),
(881, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-10 21:19:57', 1),
(882, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-10 21:21:54', 1),
(883, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-10 21:38:33', 1),
(884, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-10 22:23:10', 1),
(885, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-10 22:24:33', 1),
(886, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-10 22:25:32', 1),
(887, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-10 22:41:41', 1),
(888, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-10 22:42:44', 1),
(889, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-10 22:45:09', 1),
(890, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-10 23:00:44', 1),
(891, '182.253.93.195', 'robiokta', NULL, '2021-11-10 23:01:12', 0),
(892, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-10 23:01:38', 1),
(893, '182.253.93.195', 'robiokta', NULL, '2021-11-11 00:27:01', 0),
(894, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-11 00:30:20', 1),
(895, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-11 00:31:59', 1),
(896, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-11 00:33:40', 1),
(897, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-11 00:34:03', 1),
(898, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-11 00:35:20', 1),
(899, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-11 00:53:41', 1),
(900, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-11 00:57:59', 1),
(901, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-11 00:58:41', 1),
(902, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-11 02:08:32', 1),
(903, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-11 02:26:50', 1),
(904, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-11 02:28:01', 1),
(905, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-11 02:34:25', 1),
(906, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-11 02:37:28', 1),
(907, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-11 02:49:29', 1),
(908, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-11 02:51:00', 1),
(909, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-11 02:54:27', 1),
(910, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-11 03:01:31', 1),
(911, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-11 03:03:25', 1),
(912, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-11 03:04:18', 1),
(913, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-11 03:13:23', 1),
(914, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-11 03:13:23', 1),
(915, '182.253.93.195', 'kasir@gmail.com', 30, '2021-11-11 19:27:30', 1),
(916, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-11 19:38:37', 1),
(917, '182.253.93.195', 'kasir@gmail.com', 30, '2021-11-12 00:38:10', 1),
(918, '182.253.93.195', 'kasir@gmail.com', 30, '2021-11-12 00:43:00', 1),
(919, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-12 01:07:43', 1),
(920, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-12 01:10:34', 1),
(921, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-12 01:12:18', 1),
(922, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-12 01:21:02', 1),
(923, '182.253.93.195', 'robiokta', NULL, '2021-11-12 01:21:29', 0),
(924, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-12 01:22:11', 1),
(925, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-12 01:23:55', 1),
(926, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-12 01:24:11', 1),
(927, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-12 01:24:32', 1),
(928, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-12 01:35:35', 1),
(929, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-12 01:39:15', 1),
(930, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-12 01:45:12', 1),
(931, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-12 01:46:09', 1),
(932, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-12 01:47:02', 1),
(933, '182.253.93.195', 'kasir@gmail.com', 30, '2021-11-14 20:30:58', 1),
(934, '182.253.93.195', 'kasir@gmail.com', 30, '2021-11-14 21:54:27', 1),
(935, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-15 19:06:40', 1),
(936, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-15 19:08:52', 1),
(937, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-15 19:13:13', 1),
(938, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-15 19:20:45', 1),
(939, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-15 19:25:15', 1),
(940, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-15 19:28:35', 1),
(941, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-15 19:29:22', 1),
(942, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-15 19:33:22', 1),
(943, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-16 00:43:39', 1),
(944, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-16 00:44:49', 1),
(945, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-16 00:46:31', 1),
(946, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-16 01:15:26', 1),
(947, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-16 01:16:17', 1),
(948, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-16 01:18:30', 1),
(949, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-16 01:20:16', 1),
(950, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-16 01:23:58', 1),
(951, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-16 01:25:01', 1),
(952, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-16 01:34:35', 1),
(953, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-16 01:37:15', 1),
(954, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-16 01:38:08', 1),
(955, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-16 01:51:38', 1),
(956, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-16 02:02:16', 1),
(957, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-16 02:04:16', 1),
(958, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-16 02:18:06', 1),
(959, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-16 02:45:53', 1),
(960, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-16 02:47:49', 1),
(961, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-16 02:53:06', 1),
(962, '182.253.93.195', 'kasir@gmail.com', 30, '2021-11-16 18:50:07', 1),
(963, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-16 23:56:47', 1),
(964, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-17 00:34:30', 1),
(965, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-17 00:34:31', 1),
(966, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-17 00:38:34', 1),
(967, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-17 00:44:17', 1),
(968, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-17 00:44:22', 1),
(969, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-17 00:44:37', 1),
(970, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-17 00:47:03', 1),
(971, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-17 01:34:39', 1),
(972, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-17 02:31:03', 1),
(973, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-17 02:32:57', 1),
(974, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-17 02:35:00', 1),
(975, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-17 02:38:20', 1),
(976, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-17 02:53:44', 1),
(977, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-17 02:55:10', 1),
(978, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-17 02:55:42', 1),
(979, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-17 03:08:45', 1),
(980, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-17 03:16:33', 1),
(981, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-17 03:18:59', 1),
(982, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-17 03:22:15', 1),
(983, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-17 03:23:04', 1),
(984, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-17 03:23:04', 1),
(985, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-17 03:24:00', 1),
(986, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-17 19:10:45', 1),
(987, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-17 19:19:41', 1),
(988, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-17 19:22:57', 1),
(989, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-17 19:27:03', 1),
(990, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-17 19:32:56', 1),
(991, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-17 19:33:47', 1),
(992, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-17 19:40:56', 1),
(993, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-17 19:42:11', 1),
(994, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-17 19:42:50', 1),
(995, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-17 19:46:04', 1),
(996, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-17 19:50:13', 1),
(997, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-17 19:51:56', 1),
(998, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-17 20:47:40', 1),
(999, '182.253.93.195', 'menku@gmail.com', 38, '2021-11-17 20:48:30', 1),
(1000, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-17 20:53:53', 1),
(1001, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-17 21:06:34', 1),
(1002, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-17 21:25:21', 1),
(1003, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-17 21:33:37', 1),
(1004, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-17 21:33:39', 1),
(1005, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-17 21:34:35', 1),
(1006, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-17 21:35:19', 1),
(1007, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-17 21:47:26', 1),
(1008, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-17 21:49:33', 1),
(1009, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-18 00:32:37', 1),
(1010, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-18 00:33:31', 1),
(1011, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-18 00:50:42', 1),
(1012, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-18 01:30:17', 1),
(1013, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-18 01:30:17', 1),
(1014, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-18 01:30:28', 1),
(1015, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-18 01:30:40', 1),
(1016, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-18 01:30:45', 1),
(1017, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-18 01:31:02', 1),
(1018, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-18 02:06:03', 1),
(1019, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-18 02:07:48', 1),
(1020, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-18 02:08:38', 1),
(1021, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-18 02:18:48', 1),
(1022, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-18 02:52:43', 1),
(1023, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-18 02:53:11', 1),
(1024, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-18 02:55:04', 1),
(1025, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-18 02:55:30', 1),
(1026, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-18 02:58:16', 1),
(1027, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-18 02:59:23', 1),
(1028, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-18 03:00:13', 1),
(1029, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-18 03:00:23', 1),
(1030, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-18 03:26:00', 1),
(1031, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-18 19:05:30', 1),
(1032, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-18 19:32:58', 1),
(1033, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-18 19:36:48', 1),
(1034, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-18 19:36:49', 1),
(1035, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-18 19:39:36', 1),
(1036, '182.1.64.47', 'managertrans@gmail.com', 43, '2021-11-18 19:39:45', 1),
(1037, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-18 19:43:55', 1),
(1038, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-18 19:43:59', 1),
(1039, '182.1.66.111', 'managertrans@gmail.com', 43, '2021-11-18 19:44:34', 1),
(1040, '182.1.66.111', 'managertrans@gmail.com', 43, '2021-11-18 19:44:44', 1),
(1041, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-18 19:53:44', 1),
(1042, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-18 20:05:18', 1),
(1043, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-18 20:16:55', 1),
(1044, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-18 20:22:33', 1),
(1045, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-18 20:23:12', 1),
(1046, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-18 20:24:39', 1),
(1047, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-18 20:25:52', 1),
(1048, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-18 20:30:04', 1),
(1049, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-18 20:35:57', 1),
(1050, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-18 20:39:44', 1),
(1051, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-18 20:53:00', 1),
(1052, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-18 21:21:57', 1),
(1053, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-18 21:27:40', 1),
(1054, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-18 21:27:41', 1),
(1055, '182.1.82.229', 'managertrans@gmail.com', 43, '2021-11-18 21:32:01', 1),
(1056, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-18 21:34:04', 1),
(1057, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-18 21:35:58', 1),
(1058, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-18 21:37:55', 1),
(1059, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-19 02:50:13', 1),
(1060, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-19 02:50:40', 1),
(1061, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-19 02:50:40', 1),
(1062, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-19 02:50:43', 1),
(1063, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-19 02:50:43', 1),
(1064, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-19 02:50:51', 1),
(1065, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-19 02:50:57', 1),
(1066, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-19 02:51:21', 1),
(1067, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-19 02:53:25', 1),
(1068, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-19 03:07:40', 1),
(1069, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-19 03:08:18', 1),
(1070, '182.253.93.195', 'kasir@gmail.com', 30, '2021-11-21 18:40:40', 1),
(1071, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-21 19:18:05', 1),
(1072, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-21 20:14:49', 1),
(1073, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-21 20:54:01', 1),
(1074, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-21 21:10:12', 1),
(1075, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-21 21:10:34', 1),
(1076, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-21 21:11:10', 1),
(1077, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-21 21:11:11', 1),
(1078, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-21 21:47:14', 1),
(1079, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-21 21:49:35', 1),
(1080, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-21 21:54:02', 1),
(1081, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-21 22:02:38', 1),
(1082, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-21 22:02:41', 1),
(1083, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-21 22:03:35', 1),
(1084, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-21 22:13:38', 1),
(1085, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-21 22:15:30', 1),
(1086, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-21 22:24:44', 1),
(1087, '182.253.93.195', 'managertrans@gmail.com', 43, '2021-11-21 22:34:10', 1),
(1088, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-22 00:24:33', 1),
(1089, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-22 00:54:24', 1),
(1090, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-22 00:55:58', 1),
(1091, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-22 01:13:47', 1),
(1092, '182.253.93.195', 'kasir@gmail.com', 30, '2021-11-22 01:15:05', 1),
(1093, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-22 01:15:19', 1),
(1094, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-22 01:16:36', 1),
(1095, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-22 01:19:09', 1),
(1096, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-22 01:24:52', 1),
(1097, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-22 01:27:37', 1),
(1098, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-22 01:33:06', 1),
(1099, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-22 01:36:40', 1),
(1100, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-22 01:36:50', 1),
(1101, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-22 01:39:33', 1),
(1102, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-22 01:39:35', 1),
(1103, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-22 01:48:29', 1),
(1104, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-22 01:49:54', 1),
(1105, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-22 01:50:13', 1),
(1106, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-22 02:00:37', 1),
(1107, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-22 02:03:49', 1),
(1108, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-22 02:03:49', 1),
(1109, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-22 02:05:35', 1),
(1110, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-22 02:05:35', 1),
(1111, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-22 02:09:29', 1),
(1112, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-22 02:09:29', 1),
(1113, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-22 02:13:05', 1),
(1114, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-22 02:17:41', 1),
(1115, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-22 02:17:41', 1),
(1116, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-22 02:21:28', 1),
(1117, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-22 02:24:55', 1),
(1118, '182.253.93.195', 'kasir@gmail.com', 30, '2021-11-22 19:21:06', 1),
(1119, '182.253.93.195', 'kasir@gmail.com', 30, '2021-11-22 20:48:48', 1),
(1120, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-22 21:17:22', 1),
(1121, '182.253.93.195', 'kasir@gmail.com', 30, '2021-11-22 21:20:16', 1),
(1122, '182.253.93.195', 'kasir@gmail.com', 30, '2021-11-22 23:34:59', 1),
(1123, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-23 00:51:58', 1),
(1124, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-23 00:52:38', 1),
(1125, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-23 01:55:46', 1),
(1126, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-23 01:55:46', 1),
(1127, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-23 02:29:05', 1),
(1128, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-23 02:40:20', 1),
(1129, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-23 02:45:55', 1),
(1130, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-23 03:14:16', 1),
(1131, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-23 03:15:30', 1),
(1132, '36.90.227.206', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-23 03:33:02', 1),
(1133, '182.253.93.195', 'kasir@gmail.com', 30, '2021-11-23 20:08:08', 1),
(1134, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-23 22:10:23', 1),
(1135, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-23 22:19:58', 1),
(1136, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-23 22:23:48', 1),
(1137, '36.90.226.143', 'robiokta', NULL, '2021-11-24 21:34:35', 0),
(1138, '36.90.226.143', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-24 21:34:50', 1),
(1139, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-25 19:33:52', 1),
(1140, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-25 19:33:53', 1),
(1141, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-25 19:33:53', 1),
(1142, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-28 20:51:43', 1),
(1143, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-28 21:05:15', 1),
(1144, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-28 21:05:31', 1),
(1145, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-29 01:24:49', 1),
(1146, '182.253.93.195', 'kasir@gmail.com', 30, '2021-11-29 01:32:19', 1),
(1147, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-29 01:56:15', 1),
(1148, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-29 01:56:46', 1),
(1149, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-29 02:14:34', 1),
(1150, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-29 02:26:46', 1),
(1151, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-29 02:27:44', 1),
(1152, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-29 02:28:36', 1),
(1153, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-29 02:29:28', 1),
(1154, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-29 02:31:46', 1),
(1155, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-29 02:32:37', 1),
(1156, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-29 02:36:03', 1),
(1157, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-29 02:37:54', 1),
(1158, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-29 02:38:45', 1),
(1159, '182.253.93.195', 'kasir@gmail.com', 30, '2021-11-29 19:13:47', 1),
(1160, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-29 20:06:38', 1),
(1161, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-11-29 23:54:35', 1),
(1162, '182.1.82.89', 'robi.andri.oktafianto@gmail.com', 34, '2021-12-01 07:14:46', 1),
(1163, '182.1.82.89', 'robi.andri.oktafianto@gmail.com', 34, '2021-12-01 07:15:23', 1),
(1164, '182.1.82.89', 'robi.andri.oktafianto@gmail.com', 34, '2021-12-01 07:19:12', 1),
(1165, '36.90.227.252', 'robi.andri.oktafianto@gmail.com', 34, '2021-12-10 07:37:38', 1),
(1166, '36.90.227.252', 'robi.andri.oktafianto@gmail.com', 34, '2021-12-10 07:44:09', 1),
(1167, '118.99.83.53', 'robi.andri.oktafianto@gmail.com', 34, '2021-12-11 08:18:20', 1),
(1168, '118.99.83.53', 'robi.andri.oktafianto@gmail.com', 34, '2021-12-11 08:36:20', 1),
(1169, '118.99.83.53', 'robi.andri.oktafianto@gmail.com', 34, '2021-12-11 08:41:06', 1),
(1170, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-12-12 18:39:33', 1),
(1171, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-12-13 03:10:56', 1),
(1172, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-12-13 19:47:32', 1),
(1173, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-12-14 01:43:18', 1),
(1174, '182.253.93.195', 'robiokta', NULL, '2021-12-14 18:33:24', 0),
(1175, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-12-14 18:33:37', 1),
(1176, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-12-14 19:28:52', 1),
(1177, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-12-14 19:32:34', 1),
(1178, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-12-14 19:33:52', 1),
(1179, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-12-14 19:34:09', 1),
(1180, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-12-14 19:40:50', 1),
(1181, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-12-14 19:41:28', 1),
(1182, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-12-14 19:42:19', 1),
(1183, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-12-14 19:42:59', 1),
(1184, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-12-14 19:43:40', 1),
(1185, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-12-14 19:45:29', 1),
(1186, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-12-14 19:46:43', 1),
(1187, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-12-14 20:05:13', 1),
(1188, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-12-14 20:09:09', 1),
(1189, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-12-14 20:13:16', 1),
(1190, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-12-14 20:19:53', 1),
(1191, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-12-14 20:26:27', 1),
(1192, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-12-14 20:28:23', 1),
(1193, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-12-14 20:31:03', 1),
(1194, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-12-14 20:41:56', 1),
(1195, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-12-14 20:42:14', 1),
(1196, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-12-14 20:46:06', 1),
(1197, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-12-14 20:48:09', 1),
(1198, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-12-14 20:50:01', 1),
(1199, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-12-14 21:09:11', 1),
(1200, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-12-14 21:09:46', 1),
(1201, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-12-14 21:10:45', 1),
(1202, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-12-14 21:11:41', 1),
(1203, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-12-14 21:12:21', 1),
(1204, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-12-15 21:09:21', 1),
(1205, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2021-12-16 03:12:58', 1),
(1206, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2022-01-03 02:03:26', 1),
(1207, '182.253.93.195', 'engineering', NULL, '2022-01-03 02:04:09', 0),
(1208, '182.253.93.195', 'enginering@mail.com', 33, '2022-01-03 02:10:14', 1),
(1209, '182.253.93.195', 'admgfa@gmail.com', 46, '2022-01-03 02:10:41', 1),
(1210, '182.253.93.195', 'trans@gmail.com', 35, '2022-01-03 02:11:18', 1),
(1211, '182.253.93.195', 'offset@gmail.com', 36, '2022-01-03 02:12:17', 1),
(1212, '182.253.93.195', 'adminyasmine', NULL, '2022-01-03 02:14:36', 0),
(1213, '182.253.93.195', 'adminyasmin@gmial.com', 53, '2022-01-03 02:14:47', 1),
(1214, '182.253.93.195', 'adminyasmin@gmial.com', 53, '2022-01-03 02:23:47', 1),
(1215, '182.253.93.195', 'engineering', NULL, '2022-01-03 02:24:03', 0),
(1216, '182.253.93.195', 'engineering', NULL, '2022-01-03 02:24:12', 0),
(1217, '182.253.93.195', 'enginering@mail.com', 33, '2022-01-03 02:24:25', 1),
(1218, '182.253.93.195', 'engineering', NULL, '2022-01-03 02:33:56', 0),
(1219, '182.253.93.195', 'enginering@mail.com', 33, '2022-01-03 02:34:22', 1),
(1220, '182.253.93.195', 'trans@gmail.com', 35, '2022-01-03 02:39:42', 1),
(1221, '182.253.93.195', 'adminyasmin@gmial.com', 53, '2022-01-03 02:53:55', 1),
(1222, '182.253.93.195', 'admgfa@gmail.com', 46, '2022-01-04 01:22:58', 1),
(1223, '182.253.93.195', 'offset@gmail.com', 36, '2022-01-04 01:29:24', 1),
(1224, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2022-01-04 01:31:00', 1),
(1225, '182.253.93.195', 'adminyasmin@gmial.com', 53, '2022-01-04 01:33:32', 1),
(1226, '182.253.93.195', 'admgfa@gmail.com', 46, '2022-01-04 01:33:32', 1),
(1227, '182.253.93.195', ' admyasmin', NULL, '2022-01-04 01:35:53', 0),
(1228, '182.253.93.195', ' admyasmin', NULL, '2022-01-04 01:36:01', 0),
(1229, '182.253.93.195', 'adminyasmin@gmial.com', 53, '2022-01-04 01:36:28', 1),
(1230, '182.253.93.195', 'admgfa@gmail.com', 46, '2022-01-04 20:07:26', 1),
(1231, '182.253.93.195', 'enginering@mail.com', 33, '2022-01-04 20:43:51', 1),
(1232, '182.253.93.195', 'offset@gmail.com', 36, '2022-01-04 20:56:49', 1),
(1233, '182.253.93.195', 'trans@gmail.com', 35, '2022-01-04 20:58:34', 1),
(1234, '182.253.93.195', 'adminyasmin@gmial.com', 53, '2022-01-04 21:10:45', 1),
(1235, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2022-01-04 21:29:22', 1),
(1236, '182.253.93.195', 'admgfa@gmail.com', 46, '2022-01-04 21:31:18', 1),
(1237, '182.253.93.195', 'admgfa@gmail.com', 46, '2022-01-04 22:04:33', 1),
(1238, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2022-01-04 22:19:22', 1),
(1239, '182.253.93.195', 'adminmkg@gmail.com', 54, '2022-01-04 22:21:17', 1),
(1240, '182.253.93.195', 'trans@gmail.com', 35, '2022-01-06 20:32:21', 1),
(1241, '182.253.93.195', 'adminmkg@gmail.com', 54, '2022-01-06 20:43:51', 1),
(1242, '182.253.93.195', 'adminmkg@gmail.com', 54, '2022-01-06 20:45:16', 1),
(1243, '182.253.93.195', 'kasir@gmail.com', 30, '2022-01-06 20:52:33', 1),
(1244, '182.253.93.195', 'adminmkg@gmail.com', 54, '2022-01-07 00:06:47', 1),
(1245, '182.253.93.195', 'adminmkg@gmail.com', 54, '2022-01-07 00:07:36', 1),
(1246, '182.253.93.195', 'kasir@gmail.com', 30, '2022-01-07 00:08:08', 1),
(1247, '182.253.93.195', 'adminyasmin@gmial.com', 53, '2022-01-07 00:20:55', 1),
(1248, '182.253.93.195', 'trans@gmail.com', 35, '2022-01-07 00:25:23', 1),
(1249, '182.253.93.195', 'admmkg', NULL, '2022-01-07 00:33:05', 0),
(1250, '182.253.93.195', 'robi.andri.oktafianto@gmail.com', 34, '2022-01-07 00:33:16', 1),
(1251, '182.253.93.195', 'trans@gmail.com', 35, '2022-01-07 00:35:19', 1),
(1252, '182.253.93.195', 'enginering@mail.com', 33, '2022-01-07 00:54:42', 1),
(1253, '182.253.93.195', 'adminmkg@gmail.com', 54, '2022-01-07 01:15:48', 1),
(1254, '182.253.93.195', 'kasir@gmail.com', 30, '2022-01-07 01:20:24', 1),
(1255, '182.253.93.195', 'admgfa@gmail.com', 46, '2022-01-10 01:09:23', 1),
(1256, '182.253.93.195', 'admsimpad', NULL, '2022-01-10 01:36:31', 0),
(1257, '182.253.93.195', 'adminmkg@gmail.com', 54, '2022-01-10 01:36:54', 1),
(1258, '182.253.93.195', 'admgfa@gmail.com', 46, '2022-01-10 02:49:53', 1),
(1259, '182.253.93.195', 'adminmkg@gmail.com', 54, '2022-01-10 02:51:31', 1),
(1260, '182.253.93.195', 'kasir@gmail.com', 30, '2022-01-10 02:53:15', 1),
(1261, '182.253.93.195', 'adminyasmin@gmial.com', 53, '2022-01-10 03:01:08', 1),
(1262, '182.253.93.195', 'admgfa@gmail.com', 46, '2022-01-10 03:01:49', 1),
(1263, '182.253.93.195', 'kasir@gmail.com', 30, '2022-01-10 03:06:02', 1),
(1264, '182.253.93.195', 'adminyasmin@gmial.com', 53, '2022-01-10 21:28:36', 1),
(1265, '182.253.93.195', 'enginering@mail.com', 33, '2022-01-10 21:31:01', 1),
(1266, '182.253.93.195', 'trans@gmail.com', 35, '2022-01-10 21:41:16', 1),
(1267, '182.253.93.195', 'trans@gmail.com', 35, '2022-01-11 21:27:09', 1);

-- --------------------------------------------------------

--
-- Table structure for table `auth_permissions`
--

CREATE TABLE `auth_permissions` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth_permissions`
--

INSERT INTO `auth_permissions` (`id`, `name`, `description`) VALUES
(1, 'manage-user', 'Manage all user'),
(2, 'manage-profile', 'Manage Profile');

-- --------------------------------------------------------

--
-- Table structure for table `auth_reset_attempts`
--

CREATE TABLE `auth_reset_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `email` varchar(255) NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  `user_agent` varchar(255) NOT NULL,
  `token` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `auth_tokens`
--

CREATE TABLE `auth_tokens` (
  `id` int(11) UNSIGNED NOT NULL,
  `selector` varchar(255) NOT NULL,
  `hashedValidator` varchar(255) NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `expires` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `auth_users_permissions`
--

CREATE TABLE `auth_users_permissions` (
  `user_id` int(11) UNSIGNED NOT NULL DEFAULT 0,
  `permission_id` int(11) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Stand-in structure for view `coa`
-- (See below for the actual view)
--
CREATE TABLE `coa` (
`kode_coa_1` int(11)
,`m_coa_id` int(11)
,`coa_2_id` int(11)
,`kode_coa_2` int(11)
,`coa_3_id` int(11)
,`kode_coa_3` int(11)
,`m_coa_4_id` int(11)
,`kode_coa_4` int(11)
,`namacoa` varchar(100)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `coa2`
-- (See below for the actual view)
--
CREATE TABLE `coa2` (
`m_coa_2_id` int(11)
,`kode_coa_2` int(11)
,`kode_coa_1` int(11)
,`namacoa` varchar(100)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `coa3`
-- (See below for the actual view)
--
CREATE TABLE `coa3` (
`coa_3_id` int(11)
,`kode_coa_1` int(11)
,`kode_coa_2` int(11)
,`kode_coa_3` int(11)
,`namacoa` varchar(100)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `detail_bukubesar`
-- (See below for the actual view)
--
CREATE TABLE `detail_bukubesar` (
`kode_coa_1` int(11)
,`kode_coa_2` int(11)
,`kode_coa_3` int(11)
,`kode_coa_4` int(11)
,`namacoa` varchar(100)
,`Tanggal` date
,`Nilai` double
,`IdUser` int(11)
,`type` varchar(6)
,`description` varchar(255)
,`IdUnit` int(11)
);

-- --------------------------------------------------------

--
-- Table structure for table `mak_1`
--

CREATE TABLE `mak_1` (
  `mak_1_id` int(11) NOT NULL,
  `nama_mak` varchar(100) NOT NULL,
  `kode_mak_1` int(11) NOT NULL,
  `grup_user` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mak_1`
--

INSERT INTO `mak_1` (`mak_1_id`, `nama_mak`, `kode_mak_1`, `grup_user`) VALUES
(1, 'DIREKTUR', 1, '0'),
(2, 'TRANSPORTASI', 2, 'UMJT'),
(3, 'ENGINEERING', 3, 'UMJE'),
(4, 'OFFSET & STATIONARY', 4, 'UMJOP'),
(5, 'FINANCE & GENERAL AFFAIR', 5, 'GFA'),
(6, 'PAUD YASMIN', 6, 'Yasmin');

-- --------------------------------------------------------

--
-- Table structure for table `mak_2`
--

CREATE TABLE `mak_2` (
  `mak_2_id` int(11) NOT NULL,
  `mak_1_id` int(11) NOT NULL,
  `nama_mak_2` varchar(100) NOT NULL,
  `kode_mak_2` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mak_2`
--

INSERT INTO `mak_2` (`mak_2_id`, `mak_1_id`, `nama_mak_2`, `kode_mak_2`) VALUES
(1, 1, 'DIREKTUR', 1),
(2, 2, 'TRANSPORTASI', 2),
(3, 3, 'ENGINEERING', 3),
(4, 4, 'OFFSET & STATIONARY', 4),
(5, 5, 'FINANCE & GENERAL AFFAIR', 5),
(6, 6, 'PAUD YASMIN', 6);

-- --------------------------------------------------------

--
-- Table structure for table `mak_3`
--

CREATE TABLE `mak_3` (
  `mak_3_id` int(11) NOT NULL,
  `mak_2_id` int(11) NOT NULL,
  `mak_1_id` int(11) NOT NULL,
  `nama_mak_3` varchar(100) NOT NULL,
  `kode_mak_3` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mak_3`
--

INSERT INTO `mak_3` (`mak_3_id`, `mak_2_id`, `mak_1_id`, `nama_mak_3`, `kode_mak_3`) VALUES
(1, 1, 1, 'DIREKTUR', 1),
(2, 2, 2, 'TRANSPORTASI', 2),
(3, 3, 3, 'ENGINEERING', 3),
(4, 4, 4, 'OFFSET & STATIONARY', 4),
(5, 5, 5, 'FINANCE & GENERAL AFFAIR', 5),
(6, 6, 6, 'PAUD YASMIN', 6);

-- --------------------------------------------------------

--
-- Table structure for table `mak_4`
--

CREATE TABLE `mak_4` (
  `mak_4_id` int(11) NOT NULL,
  `mak_1_id` int(11) NOT NULL,
  `mak_2_id` int(11) NOT NULL,
  `mak_3_id` int(11) NOT NULL,
  `nama_mak_4` varchar(100) NOT NULL,
  `kode_mak_4` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mak_4`
--

INSERT INTO `mak_4` (`mak_4_id`, `mak_1_id`, `mak_2_id`, `mak_3_id`, `nama_mak_4`, `kode_mak_4`) VALUES
(1, 5, 5, 5, 'Gaji Pokok dan Tunjangan Pegawai', 1),
(2, 2, 2, 2, 'Trip / Pariwisata', 1),
(3, 2, 2, 2, 'Pegurusan Legalitas dan Perijinan', 2),
(4, 3, 3, 3, 'Proyek Konstruksi / Pekerjaan Sipil', 1),
(5, 3, 3, 3, 'Pekerjaan Jasa Perawatan Rumah Tangga', 2),
(6, 4, 4, 4, 'Pengadaan Barang Dagangan', 1),
(7, 5, 5, 5, 'Pengadaan Aset dan Inventaris', 2),
(8, 5, 5, 5, 'Perawatan dan Perbaikan Gedung Kantor', 3),
(9, 5, 5, 5, 'Operasional Managerial', 4),
(10, 2, 2, 2, 'Operasional Managerial', 4),
(11, 3, 3, 3, 'Operasional Kantor', 3),
(12, 3, 3, 3, '	Pengadaan Barang Dagangan', 4),
(13, 3, 3, 3, 'Operasional Manager', 5),
(14, 6, 6, 6, 'Penerimaan Uang Sekolah', 1),
(15, 5, 5, 5, 'Biaya Tenaga Kerja', 5),
(16, 5, 5, 5, 'Pembayaran Biaya Listrik', 6),
(17, 2, 2, 2, 'Perawatan dan Perbaikan Kendaraan', 5),
(18, 5, 5, 5, 'Biaya Lahan pertanian', 7),
(19, 5, 5, 5, 'Biaya Rumah Tangga', 8),
(20, 5, 5, 5, 'Biaya ATK dan Materai', 9);

-- --------------------------------------------------------

--
-- Table structure for table `menu_api`
--

CREATE TABLE `menu_api` (
  `id` int(11) NOT NULL,
  `nama_menu` varchar(25) NOT NULL,
  `link_menu` varchar(30) NOT NULL,
  `activity_name` varchar(100) NOT NULL,
  `id_divisi` int(11) NOT NULL,
  `role` varchar(10) NOT NULL,
  `img_menu` text NOT NULL,
  `warna` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu_api`
--

INSERT INTO `menu_api` (`id`, `nama_menu`, `link_menu`, `activity_name`, `id_divisi`, `role`, `img_menu`, `warna`) VALUES
(1, 'Realisasi', 'pengajuan_cair', 'RealisasiActivity', 0, 'admin', 'https://apisimpad.mentarikaryagemilang.co.id/img/icons/book.png', '#42f548'),
(2, 'Realisasi', 'pengajuan_cair', 'PengajuanCariActivity', 0, 'admin', 'https://apisimpad.mentarikaryagemilang.co.id/img/icons/book.png', '#42f548'),
(3, 'Realisasi', 'pengajuan_cair', 'PengajuanCariActivity', 0, 'admin', 'https://apisimpad.mentarikaryagemilang.co.id/img/icons/book.png', '#42f548'),
(4, 'Realisasi', 'pengajuan_cair', 'PengajuanCariActivity', 0, 'admin', 'https://apisimpad.mentarikaryagemilang.co.id/img/icons/book.png', '#42f548'),
(5, 'Realisasi', 'pengajuan_cair', 'PengajuanCariActivity', 0, 'admin', 'https://apisimpad.mentarikaryagemilang.co.id/img/icons/book.png', '#42f548'),
(6, 'Realisasi', 'pengajuan_cair', 'PengajuanCariActivity', 0, 'admin', 'https://apisimpad.mentarikaryagemilang.co.id/img/icons/book.png', '#42f548');

-- --------------------------------------------------------

--
-- Table structure for table `menu_role`
--

CREATE TABLE `menu_role` (
  `id_menu_role` int(11) NOT NULL,
  `grup_user` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu_role`
--

INSERT INTO `menu_role` (`id_menu_role`, `grup_user`, `menu_id`) VALUES
(1, 1, 2),
(2, 1, 3),
(3, 1, 7),
(4, 2, 7),
(5, 4, 7),
(7, 1, 9),
(8, 1, 10),
(9, 6, 9),
(10, 6, 10),
(11, 1, 11),
(12, 1, 12),
(13, 1, 13),
(14, 1, 14),
(15, 1, 15),
(16, 1, 16),
(17, 1, 17),
(18, 1, 18),
(19, 1, 19),
(21, 2, 20),
(22, 1, 20),
(23, 3, 20),
(24, 3, 7),
(25, 5, 7),
(26, 5, 19),
(27, 5, 9),
(28, 1, 21),
(29, 1, 22),
(30, 2, 22),
(31, 3, 22),
(32, 4, 22),
(33, 5, 22),
(34, 6, 22),
(35, 8, 22),
(36, 8, 20),
(37, 8, 7),
(39, 10, 20),
(40, 10, 7),
(41, 10, 22),
(42, 11, 22),
(43, 11, 7),
(44, 13, 22),
(45, 13, 7),
(46, 14, 22),
(47, 14, 7),
(48, 12, 22),
(49, 12, 7),
(50, 1, 23),
(51, 15, 22),
(52, 15, 7),
(53, 15, 20),
(54, 16, 22),
(55, 16, 7),
(56, 5, 23),
(57, 1, 24),
(58, 1, 25),
(59, 1, 26),
(60, 6, 24),
(61, 6, 25),
(62, 6, 26),
(63, 1, 27),
(64, 5, 27),
(65, 1, 28),
(66, 1, 29),
(67, 1, 30),
(68, 5, 30),
(69, 1, 31),
(70, 5, 31),
(71, 17, 22),
(72, 17, 7),
(73, 17, 20);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `version` varchar(255) NOT NULL,
  `class` text NOT NULL,
  `group` varchar(255) NOT NULL,
  `namespace` varchar(255) NOT NULL,
  `time` int(11) NOT NULL,
  `batch` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `version`, `class`, `group`, `namespace`, `time`, `batch`) VALUES
(1, '2017-11-20-223112', 'Myth\\Auth\\Database\\Migrations\\CreateAuthTables', 'default', 'Myth\\Auth', 1607913727, 1);

-- --------------------------------------------------------

--
-- Table structure for table `mv_detailbukubesar`
--

CREATE TABLE `mv_detailbukubesar` (
  `kode_coa_1` int(11) NOT NULL,
  `kode_coa_2` int(11) NOT NULL,
  `kode_coa_3` int(11) NOT NULL,
  `kode_coa_4` int(11) NOT NULL,
  `namacoa` varchar(100) NOT NULL,
  `Tanggal` date NOT NULL,
  `Nilai` double NOT NULL,
  `IdUser` int(11) NOT NULL,
  `Type` varchar(10) NOT NULL,
  `description` varchar(100) NOT NULL,
  `IdUnit` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mv_detailbukubesar`
--

INSERT INTO `mv_detailbukubesar` (`kode_coa_1`, `kode_coa_2`, `kode_coa_3`, `kode_coa_4`, `namacoa`, `Tanggal`, `Nilai`, `IdUser`, `Type`, `description`, `IdUnit`) VALUES
(1, 1, 2, 3, 'Mandiri - 1430000679009 (Tabungan)', '2021-01-01', 900000000, 34, 'Debit', 'Menager Keuangan', 5),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-01', 100000000, 34, 'Debit', 'Menager Keuangan', 5),
(1, 1, 5, 7, 'Piutang Operasional', '2021-01-05', 2500000, 34, 'Debit', 'Menager Keuangan', 5),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-01-05', 2265000, 34, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-01-05', 2345000, 34, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-01-05', 1365000, 34, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-01-06', 14300000, 34, 'Debit', 'UMJT', 2),
(1, 1, 3, 1, 'Persediaan Barang Konstruksi', '2021-05-20', 3022000, 30, 'Debit', 'UMJT', 2),
(1, 1, 3, 1, 'Persediaan Barang Konstruksi', '2021-05-21', 4040000, 30, 'Debit', 'UMJT', 2),
(1, 1, 3, 1, 'Persediaan Barang Konstruksi', '2021-05-21', 2899000, 30, 'Debit', 'UMJT', 2),
(1, 1, 3, 1, 'Persediaan Barang Konstruksi', '2021-05-24', 1306000, 30, 'Debit', 'UMJT', 2),
(1, 1, 3, 1, 'Persediaan Barang Konstruksi', '2021-05-24', 2000000, 30, 'Debit', 'UMJT', 2),
(1, 1, 3, 1, 'Persediaan Barang Konstruksi', '2021-05-24', 1000000, 30, 'Debit', 'UMJT', 2),
(1, 1, 3, 1, 'Persediaan Barang Konstruksi', '2021-05-25', 19366000, 30, 'Debit', 'UMJT', 2),
(1, 1, 3, 1, 'Persediaan Barang Konstruksi', '2021-05-28', 4975000, 30, 'Debit', 'UMJT', 2),
(1, 1, 3, 1, 'Persediaan Barang Konstruksi', '2021-05-28', 1956500, 30, 'Debit', 'UMJT', 2),
(6, 8, 1, 2, 'Biaya Rumah Tangga', '2021-05-21', 79000, 30, 'Debit', 'Menager Keuangan', 5),
(6, 1, 1, 1, 'Gaji Pokok ', '2021-06-04', 250000, 30, 'Debit', 'Menager Keuangan', 5),
(6, 8, 1, 1, 'Biaya ATK dan Materai', '2021-05-27', 890300, 30, 'Debit', 'Menager Keuangan', 5),
(1, 1, 3, 1, 'Persediaan Barang Konstruksi', '2021-05-18', 2000000, 30, 'Debit', 'UMJT', 2),
(1, 1, 3, 1, 'Persediaan Barang Konstruksi', '2021-05-20', 1690000, 30, 'Debit', 'UMJT', 2),
(1, 1, 5, 7, 'Piutang Operasional', '2021-01-12', 74000, 30, 'Debit', 'Menager Keuangan', 5),
(1, 1, 5, 7, 'Piutang Operasional', '2021-01-21', 501170, 30, 'Debit', 'Menager Keuangan', 5),
(1, 1, 5, 7, 'Piutang Operasional', '2021-01-16', 804300, 30, 'Debit', 'Menager Keuangan', 5),
(1, 1, 5, 7, 'Piutang Operasional', '2021-01-22', 100000, 30, 'Debit', 'Menager Keuangan', 5),
(1, 1, 5, 7, 'Piutang Operasional', '2021-01-22', 45000, 30, 'Debit', 'Menager Keuangan', 5),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-02-02', 800000, 30, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-01-14', 8940000, 30, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-01-14', 1130000, 30, 'Debit', 'UMJT', 2),
(1, 5, 4, 1, 'Inventaris Golongan 1', '2021-01-21', 1200000, 30, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-01-21', 2543000, 30, 'Debit', 'UMJT', 2),
(5, 3, 1, 1, 'Tenaga Kerja Harian', '2021-01-21', 1103000, 30, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-08-28', 373500, 30, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-01-11', 546500, 30, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-01-11', 256500, 30, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-01-12', 471000, 30, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-08-29', 1664000, 30, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-01-11', 2000000, 30, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-01-13', 2525000, 30, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-01-01', 493500, 30, 'Debit', 'UMJT', 2),
(1, 1, 5, 7, 'Piutang Operasional', '2021-02-01', 121000, 30, 'Debit', 'Administrator', 1),
(6, 8, 1, 8, 'Biaya Konsumsi Kantor', '2021-02-02', 1116000, 30, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-02-02', 772100, 30, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-02-03', 432000, 30, 'Debit', 'UMJT', 2),
(1, 1, 5, 7, 'Piutang Operasional', '2021-02-03', 100000, 30, 'Debit', 'Administrator', 1),
(1, 1, 5, 7, 'Piutang Operasional', '2021-02-03', 3560000, 30, 'Debit', 'Administrator', 1),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-02-03', 4000000, 30, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-02-04', 1404000, 30, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-02-04', 1041000, 30, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-02-05', 17293500, 30, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-02-05', 2220000, 30, 'Debit', 'UMJT', 2),
(5, 3, 1, 1, 'Tenaga Kerja Harian', '2021-02-05', 4620000, 30, 'Debit', 'UMJT', 2),
(1, 1, 5, 7, 'Piutang Operasional', '2021-02-10', 56000, 30, 'Debit', 'Administrator', 1),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-02-10', 163500, 30, 'Debit', 'UMJT', 2),
(5, 3, 1, 1, 'Tenaga Kerja Harian', '2021-02-11', 3900000, 30, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-02-15', 1000000, 30, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-02-17', 250000, 30, 'Debit', 'UMJT', 2),
(1, 1, 5, 7, 'Piutang Operasional', '2021-02-19', 600000, 30, 'Debit', 'Administrator', 1),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-02-19', 15242300, 30, 'Debit', 'UMJT', 2),
(5, 3, 1, 1, 'Tenaga Kerja Harian', '2021-02-19', 830000, 30, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-02-25', 19622300, 30, 'Debit', 'UMJT', 2),
(5, 3, 1, 1, 'Tenaga Kerja Harian', '2021-02-26', 3450000, 30, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-01-05', 2345000, 30, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-01-05', 1365000, 30, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-01-06', 2265000, 30, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-01-06', 14300000, 30, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-01-06', 1860000, 30, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-01-06', 4800000, 30, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-01-06', 1710000, 30, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-01-06', 6000000, 30, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-01-08', 4330000, 30, 'Debit', 'UMJT', 2),
(5, 4, 1, 1, 'Tenaga Kerja Paket ', '2021-01-08', 3000000, 30, 'Debit', 'UMJT', 2),
(5, 3, 1, 1, 'Tenaga Kerja Harian', '2021-01-09', 1500000, 30, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-01-09', 6645000, 30, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-01-12', 1350000, 30, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-01-12', 4470000, 30, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-01-12', 1727500, 30, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-01-13', 4200000, 30, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-01-14', 1425000, 30, 'Debit', 'UMJT', 2),
(5, 3, 1, 1, 'Tenaga Kerja Harian', '2021-01-15', 1408000, 30, 'Debit', 'UMJT', 2),
(5, 3, 1, 1, 'Tenaga Kerja Harian', '2021-01-15', 1169000, 30, 'Debit', 'UMJT', 2),
(5, 3, 1, 1, 'Tenaga Kerja Harian', '2021-01-16', 239000, 30, 'Debit', 'UMJT', 2),
(5, 3, 1, 1, 'Tenaga Kerja Harian', '2021-01-16', 1400000, 30, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-01-16', 1758627, 30, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-01-18', 1450000, 30, 'Debit', 'UMJT', 2),
(5, 3, 1, 1, 'Tenaga Kerja Harian', '2021-01-18', 600000, 30, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-01-18', 1146000, 30, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-01-19', 138000, 30, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-01-19', 214500, 30, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-01-19', 328000, 30, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-01-19', 630000, 30, 'Debit', 'UMJT', 2),
(6, 1, 2, 1, 'Biaya Kesehatan Karyawan', '2021-01-19', 1946373, 30, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-01-19', 842500, 30, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-01-20', 168000, 30, 'Debit', 'UMJT', 2),
(5, 3, 1, 1, 'Tenaga Kerja Harian', '2021-01-22', 2071000, 30, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-09-23', 750000, 30, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-01-20', 2129868, 30, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-01-28', 6383500, 30, 'Debit', 'UMJT', 2),
(5, 3, 1, 1, 'Tenaga Kerja Harian', '2021-01-29', 2780000, 30, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-01-30', 9500000, 30, 'Debit', 'UMJT', 2),
(1, 1, 3, 1, 'Persediaan Barang Konstruksi', '2021-04-19', 5311500, 30, 'Debit', 'UMJT', 2),
(1, 1, 3, 1, 'Persediaan Barang Konstruksi', '2021-04-19', 1315000, 30, 'Debit', 'UMJT', 2),
(1, 1, 3, 1, 'Persediaan Barang Konstruksi', '2021-04-20', 4575000, 30, 'Debit', 'UMJT', 2),
(1, 1, 3, 1, 'Persediaan Barang Konstruksi', '2021-04-21', 8442500, 30, 'Debit', 'UMJT', 2),
(1, 1, 3, 1, 'Persediaan Barang Konstruksi', '2021-04-22', 1688000, 30, 'Debit', 'UMJT', 2),
(1, 1, 3, 1, 'Persediaan Barang Konstruksi', '2021-04-23', 8297000, 30, 'Debit', 'UMJT', 2),
(1, 1, 3, 1, 'Persediaan Barang Konstruksi', '2021-04-23', 6764500, 30, 'Debit', 'UMJT', 2),
(1, 1, 3, 1, 'Persediaan Barang Konstruksi', '2021-04-26', 1108000, 30, 'Debit', 'UMJT', 2),
(1, 1, 3, 1, 'Persediaan Barang Konstruksi', '2021-04-26', 3245000, 30, 'Debit', 'UMJT', 2),
(1, 1, 3, 1, 'Persediaan Barang Konstruksi', '2021-04-29', 19749000, 30, 'Debit', 'UMJT', 2),
(1, 1, 3, 1, 'Persediaan Barang Konstruksi', '2021-04-27', 2832000, 30, 'Debit', 'UMJT', 2),
(1, 1, 3, 1, 'Persediaan Barang Konstruksi', '2021-04-27', 2700000, 30, 'Debit', 'UMJT', 2),
(1, 1, 3, 1, 'Persediaan Barang Konstruksi', '2021-04-30', 9300000, 30, 'Debit', 'UMJT', 2),
(1, 1, 3, 1, 'Persediaan Barang Konstruksi', '2021-04-30', 9914000, 30, 'Debit', 'UMJT', 2),
(1, 1, 3, 1, 'Persediaan Barang Konstruksi', '2021-05-07', 5167500, 30, 'Debit', 'UMJT', 2),
(1, 1, 3, 1, 'Persediaan Barang Konstruksi', '2021-05-07', 9240000, 30, 'Debit', 'UMJT', 2),
(1, 1, 3, 1, 'Persediaan Barang Konstruksi', '2021-05-10', 275000, 30, 'Debit', 'UMJT', 2),
(1, 1, 3, 1, 'Persediaan Barang Konstruksi', '2021-05-11', 2066000, 30, 'Debit', 'UMJT', 2),
(1, 1, 3, 1, 'Persediaan Barang Konstruksi', '2021-05-11', 2590000, 30, 'Debit', 'UMJT', 2),
(1, 1, 3, 1, 'Persediaan Barang Konstruksi', '2021-06-04', 2525000, 30, 'Debit', 'UMJT', 2),
(5, 4, 1, 1, 'Tenaga Kerja Paket ', '2021-01-08', 800000, 30, 'Debit', 'UMJT', 2),
(5, 3, 1, 1, 'Tenaga Kerja Harian', '2021-01-09', 3839000, 30, 'Debit', 'UMJT', 2),
(5, 3, 1, 1, 'Tenaga Kerja Harian', '2021-01-15', 325000, 30, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-01-21', 1490000, 30, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-01-26', 2500000, 30, 'Debit', 'UMJT', 2),
(6, 1, 5, 2, 'Biaya Uang Makan Dinas', '2021-02-08', 100000, 30, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-03-03', 17190000, 30, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-03-05', 4865000, 30, 'Debit', 'UMJT', 2),
(5, 3, 1, 1, 'Tenaga Kerja Harian', '2021-03-05', 5510000, 30, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-03-05', 3000000, 30, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-03-15', 2153500, 30, 'Debit', 'UMJT', 2),
(5, 3, 1, 1, 'Tenaga Kerja Harian', '2021-03-12', 4428000, 30, 'Debit', 'UMJT', 2),
(6, 8, 1, 6, 'Biaya Rekening Internet dan Pulsa ', '2021-03-16', 278000, 30, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-03-16', 1345000, 30, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-03-16', 1770000, 30, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-03-17', 1150000, 30, 'Debit', 'UMJT', 2),
(1, 1, 1, 2, 'Kas Kecil', '2021-03-17', 20000000, 30, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-03-18', 2150500, 30, 'Debit', 'UMJT', 2),
(5, 3, 1, 1, 'Tenaga Kerja Harian', '2021-03-19', 4482000, 30, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-03-19', 1875500, 30, 'Debit', 'UMJT', 2),
(5, 3, 1, 1, 'Tenaga Kerja Harian', '2021-03-26', 1550000, 30, 'Debit', 'UMJT', 2),
(5, 3, 1, 1, 'Tenaga Kerja Harian', '2021-03-26', 4375000, 30, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-03-26', 5289000, 30, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-03-26', 1300000, 30, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-03-29', 3000000, 30, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-03-30', 3272500, 30, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-03-30', 462000, 30, 'Debit', 'UMJT', 2),
(1, 1, 1, 2, 'Kas Kecil', '2021-03-30', 20000000, 30, 'Debit', 'UMJT', 2),
(6, 8, 1, 1, 'Biaya ATK dan Materai', '2021-04-01', 498000, 30, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-03-31', 17267000, 30, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-04-01', 2200000, 30, 'Debit', 'UMJT', 2),
(5, 3, 1, 1, 'Tenaga Kerja Harian', '2021-04-02', 4626000, 30, 'Debit', 'UMJT', 2),
(5, 1, 2, 4, 'Pembelian Barang Dagang Engineering', '2021-04-01', 2250000, 30, 'Debit', 'UMJT', 2),
(1, 1, 3, 1, 'Persediaan Barang Konstruksi', '2021-04-06', 3715500, 30, 'Debit', 'UMJT', 2),
(1, 1, 3, 1, 'Persediaan Barang Konstruksi', '2021-04-07', 8080000, 30, 'Debit', 'UMJT', 2),
(1, 1, 3, 1, 'Persediaan Barang Konstruksi', '2021-04-09', 4190000, 30, 'Debit', 'UMJT', 2),
(1, 1, 3, 1, 'Persediaan Barang Konstruksi', '2021-04-07', 210000, 30, 'Debit', 'UMJT', 2),
(1, 1, 3, 1, 'Persediaan Barang Konstruksi', '2021-04-08', 10860000, 30, 'Debit', 'UMJT', 2),
(1, 1, 3, 1, 'Persediaan Barang Konstruksi', '2021-04-09', 7901000, 30, 'Debit', 'UMJT', 2),
(1, 1, 3, 1, 'Persediaan Barang Konstruksi', '2021-04-12', 2030000, 30, 'Debit', 'UMJT', 2),
(1, 1, 3, 1, 'Persediaan Barang Konstruksi', '2021-04-16', 250000, 30, 'Debit', 'UMJT', 2),
(1, 1, 3, 1, 'Persediaan Barang Konstruksi', '2021-04-16', 4780000, 30, 'Debit', 'UMJT', 2),
(1, 1, 3, 1, 'Persediaan Barang Konstruksi', '2021-04-16', 5977500, 30, 'Debit', 'UMJT', 2),
(1, 1, 3, 1, 'Persediaan Barang Konstruksi', '2021-05-04', 5266500, 30, 'Debit', 'UMJT', 2),
(1, 1, 3, 1, 'Persediaan Barang Konstruksi', '2021-05-04', 1850000, 30, 'Debit', 'UMJT', 2),
(1, 1, 3, 1, 'Persediaan Barang Konstruksi', '2021-06-04', 5145000, 30, 'Debit', 'UMJT', 2),
(1, 1, 3, 1, 'Persediaan Barang Konstruksi', '2021-06-04', 4756000, 30, 'Debit', 'UMJT', 2),
(1, 1, 3, 1, 'Persediaan Barang Konstruksi', '2021-06-07', 20058000, 30, 'Debit', 'UMJT', 2),
(1, 1, 3, 1, 'Persediaan Barang Konstruksi', '2021-06-11', 2070000, 30, 'Debit', 'UMJT', 2),
(1, 1, 5, 7, 'Piutang Operasional', '2021-01-06', 184500, 30, 'Debit', 'Menager Keuangan', 5),
(1, 1, 5, 7, 'Piutang Operasional', '2021-01-06', 716500, 30, 'Debit', 'Menager Keuangan', 5),
(1, 1, 5, 7, 'Piutang Operasional', '2021-01-16', 18360000, 30, 'Debit', 'Menager Keuangan', 5),
(1, 1, 5, 7, 'Piutang Operasional', '2021-01-19', 5358423, 30, 'Debit', 'Menager Keuangan', 5),
(1, 1, 5, 7, 'Piutang Operasional', '2021-01-26', 4197700, 30, 'Debit', 'Menager Keuangan', 5),
(1, 1, 5, 7, 'Piutang Operasional', '2021-01-26', 300000, 30, 'Debit', 'Menager Keuangan', 5),
(1, 1, 5, 7, 'Piutang Operasional', '2021-01-27', 3000000, 30, 'Debit', 'Menager Keuangan', 5),
(1, 1, 5, 7, 'Piutang Operasional', '2021-01-27', 1414260, 30, 'Debit', 'Menager Keuangan', 5),
(1, 1, 5, 7, 'Piutang Operasional', '2021-01-27', 100000, 30, 'Debit', 'Menager Keuangan', 5),
(1, 1, 5, 7, 'Piutang Operasional', '2021-01-27', 16100, 30, 'Debit', 'Menager Keuangan', 5),
(1, 1, 5, 7, 'Piutang Operasional', '2021-01-29', 495000, 30, 'Debit', 'Menager Keuangan', 5),
(6, 1, 1, 1, 'Gaji Pokok ', '2021-03-01', 41487740, 30, 'Debit', 'Menager Keuangan', 5),
(5, 3, 1, 1, 'Tenaga Kerja Harian', '2021-03-01', 700000, 30, 'Debit', 'Menager Keuangan', 5),
(6, 8, 1, 5, 'Biaya BBM', '2021-03-01', 184200, 30, 'Debit', 'Menager Keuangan', 5),
(6, 1, 2, 1, 'Biaya Kesehatan Karyawan', '2021-03-05', 8884140, 30, 'Debit', 'Menager Keuangan', 5),
(6, 1, 1, 1, 'Gaji Pokok ', '2021-03-01', 150000, 30, 'Debit', 'Menager Keuangan', 5),
(6, 7, 1, 1, 'Biaya Program CSR', '2021-03-01', 1000000, 30, 'Debit', 'Menager Keuangan', 5),
(6, 8, 1, 2, 'Biaya Rumah Tangga', '2021-03-09', 46600, 30, 'Debit', 'Menager Keuangan', 5),
(6, 8, 1, 2, 'Biaya Rumah Tangga', '2021-03-12', 100000, 30, 'Debit', 'Menager Keuangan', 5),
(6, 8, 1, 1, 'Biaya ATK dan Materai', '2021-03-16', 31000, 30, 'Debit', 'Menager Keuangan', 5),
(6, 8, 1, 2, 'Biaya Rumah Tangga', '2021-03-25', 23200, 30, 'Debit', 'Menager Keuangan', 5),
(6, 1, 1, 1, 'Gaji Pokok ', '2021-03-01', 350000, 30, 'Debit', 'Menager Keuangan', 5),
(6, 8, 1, 7, 'Biaya Rekening Listrik', '2021-03-15', 412506, 30, 'Debit', 'Menager Keuangan', 5),
(6, 3, 1, 2, 'Biaya Sarana Promosi', '2021-03-16', 613000, 30, 'Debit', 'Menager Keuangan', 5),
(6, 8, 1, 2, 'Biaya Rumah Tangga', '2021-03-25', 285000, 30, 'Debit', 'Menager Keuangan', 5),
(6, 3, 1, 1, 'Biaya Kegiatan Promosi', '2021-03-25', 1200000, 30, 'Debit', 'Menager Keuangan', 5),
(6, 1, 2, 1, 'Biaya Kesehatan Karyawan', '2021-03-27', 440980, 30, 'Debit', 'Menager Keuangan', 5),
(6, 7, 1, 2, 'Sumbangan', '2021-04-05', 250000, 30, 'Debit', 'Menager Keuangan', 5),
(6, 1, 1, 1, 'Gaji Pokok ', '2021-04-01', 1000000, 30, 'Debit', 'Menager Keuangan', 5),
(6, 1, 1, 1, 'Gaji Pokok ', '2021-04-01', 250000, 30, 'Debit', 'Menager Keuangan', 5),
(6, 8, 1, 2, 'Biaya Rumah Tangga', '2021-04-05', 187500, 30, 'Debit', 'Menager Keuangan', 5),
(6, 7, 1, 2, 'Sumbangan', '2021-04-05', 750000, 30, 'Debit', 'Menager Keuangan', 5),
(6, 8, 1, 4, 'Biaya Meeting', '2021-04-08', 90000, 30, 'Debit', 'Menager Keuangan', 5),
(6, 1, 1, 2, 'Biaya BPJS', '2021-04-12', 16800, 30, 'Debit', 'Menager Keuangan', 5),
(6, 3, 1, 2, 'Biaya Sarana Promosi', '2021-04-15', 350000, 30, 'Debit', 'Menager Keuangan', 5),
(6, 7, 1, 2, 'Sumbangan', '2021-04-15', 100000, 30, 'Debit', 'Menager Keuangan', 5),
(6, 1, 1, 2, 'Biaya BPJS', '2021-04-15', 6265351, 30, 'Debit', 'Menager Keuangan', 5),
(6, 1, 2, 1, 'Biaya Kesehatan Karyawan', '2021-04-10', 2944575, 30, 'Debit', 'Menager Keuangan', 5),
(6, 3, 1, 1, 'Biaya Kegiatan Promosi', '2021-04-19', 1750000, 30, 'Debit', 'Menager Keuangan', 5),
(6, 3, 1, 1, 'Biaya Kegiatan Promosi', '2021-04-19', 1000000, 30, 'Debit', 'Menager Keuangan', 5),
(6, 8, 1, 6, 'Biaya Rekening Internet dan Pulsa ', '2021-04-19', 51500, 30, 'Debit', 'Menager Keuangan', 5),
(6, 3, 1, 1, 'Biaya Kegiatan Promosi', '2021-04-20', 1200000, 30, 'Debit', 'Menager Keuangan', 5),
(6, 7, 1, 2, 'Sumbangan', '2021-04-26', 1500000, 30, 'Debit', 'Menager Keuangan', 5),
(6, 8, 1, 2, 'Biaya Rumah Tangga', '2021-04-21', 293000, 30, 'Debit', 'Menager Keuangan', 5),
(6, 3, 1, 1, 'Biaya Kegiatan Promosi', '2021-04-21', 1300000, 30, 'Debit', 'Menager Keuangan', 5),
(1, 5, 5, 1, 'Inventaris', '2021-04-22', 5999000, 30, 'Debit', 'Menager Keuangan', 5),
(6, 2, 1, 4, 'Biaya Perawatan Inventaris', '2021-04-29', 60000, 30, 'Debit', 'Menager Keuangan', 5),
(6, 1, 1, 1, 'Gaji Pokok ', '2021-04-30', 39766850, 30, 'Debit', 'Menager Keuangan', 5),
(6, 1, 1, 1, 'Gaji Pokok ', '2021-04-30', 250000, 30, 'Debit', 'Menager Keuangan', 5),
(6, 1, 1, 3, 'Biaya Tunjanngan Hari Raya', '2021-05-04', 27134803, 30, 'Debit', 'Menager Keuangan', 5),
(6, 1, 2, 1, 'Biaya Kesehatan Karyawan', '2021-05-11', 270000, 30, 'Debit', 'Menager Keuangan', 5),
(6, 1, 1, 1, 'Gaji Pokok ', '2021-05-31', 39861076, 30, 'Debit', 'Menager Keuangan', 5),
(1, 1, 5, 7, 'Piutang Operasional', '2021-01-20', 2416633, 30, 'Debit', 'Administrator', 1),
(1, 1, 5, 7, 'Piutang Operasional', '2021-01-07', 235000, 30, 'Debit', 'Administrator', 1),
(1, 1, 5, 7, 'Piutang Operasional', '2021-01-08', 900000, 30, 'Debit', 'Administrator', 1),
(1, 1, 5, 7, 'Piutang Operasional', '2021-01-08', 2278329, 30, 'Debit', 'Administrator', 1),
(1, 1, 5, 7, 'Piutang Operasional', '2021-01-08', 450000, 30, 'Debit', 'Administrator', 1),
(1, 1, 5, 7, 'Piutang Operasional', '2021-01-09', 520000, 30, 'Debit', 'Administrator', 1),
(1, 1, 5, 7, 'Piutang Operasional', '2021-01-09', 50000, 30, 'Debit', 'Administrator', 1),
(1, 1, 5, 7, 'Piutang Operasional', '2021-01-09', 260000, 30, 'Debit', 'Administrator', 1),
(1, 1, 5, 7, 'Piutang Operasional', '2021-01-09', 912758, 30, 'Debit', 'Administrator', 1),
(1, 1, 5, 7, 'Piutang Operasional', '2021-01-26', 126730, 30, 'Debit', 'Administrator', 1),
(1, 1, 5, 7, 'Piutang Operasional', '2021-01-26', 523000, 30, 'Debit', 'Administrator', 1),
(1, 1, 5, 7, 'Piutang Operasional', '2021-01-26', 1199000, 30, 'Debit', 'Administrator', 1),
(1, 1, 5, 7, 'Piutang Operasional', '2021-02-09', 16000, 30, 'Debit', 'Administrator', 1),
(6, 8, 1, 2, 'Biaya Rumah Tangga', '2021-03-02', 300000, 30, 'Debit', 'Administrator', 1),
(6, 8, 1, 6, 'Biaya Rekening Internet dan Pulsa ', '2021-03-08', 101500, 30, 'Debit', 'Administrator', 1),
(5, 2, 1, 1, 'Biaya Premi Pariwisata', '2021-03-07', 260000, 30, 'Debit', 'Administrator', 1),
(5, 2, 1, 1, 'Biaya Premi Pariwisata', '2021-03-07', 50000, 30, 'Debit', 'Administrator', 1),
(5, 2, 1, 1, 'Biaya Premi Pariwisata', '2021-03-07', 200000, 30, 'Debit', 'Administrator', 1),
(5, 2, 1, 1, 'Biaya Premi Pariwisata', '2021-03-07', 130000, 30, 'Debit', 'Administrator', 1),
(6, 1, 5, 2, 'Biaya Uang Makan Dinas', '2021-03-07', 120000, 30, 'Debit', 'Administrator', 1),
(6, 8, 1, 5, 'Biaya BBM', '2021-03-07', 700000, 30, 'Debit', 'Administrator', 1),
(5, 2, 1, 1, 'Biaya Premi Pariwisata', '2021-03-08', 260000, 30, 'Debit', 'Administrator', 1),
(5, 2, 1, 1, 'Biaya Premi Pariwisata', '2021-03-08', 200000, 30, 'Debit', 'Administrator', 1),
(5, 2, 1, 1, 'Biaya Premi Pariwisata', '2021-03-08', 130000, 30, 'Debit', 'Administrator', 1),
(6, 1, 5, 2, 'Biaya Uang Makan Dinas', '2021-03-08', 120000, 30, 'Debit', 'Administrator', 1),
(6, 8, 1, 5, 'Biaya BBM', '2021-03-08', 721518, 30, 'Debit', 'Administrator', 1),
(5, 2, 1, 1, 'Biaya Premi Pariwisata', '2021-03-10', 151200, 30, 'Debit', 'Administrator', 1),
(6, 8, 1, 6, 'Biaya Rekening Internet dan Pulsa ', '2021-03-10', 60000, 30, 'Debit', 'Administrator', 1),
(1, 5, 5, 1, 'Inventaris', '2021-03-12', 150000, 30, 'Debit', 'Administrator', 1),
(6, 8, 1, 6, 'Biaya Rekening Internet dan Pulsa ', '2021-03-15', 75000, 30, 'Debit', 'Administrator', 1),
(6, 8, 1, 8, 'Biaya Konsumsi Kantor', '2021-03-15', 145000, 30, 'Debit', 'Administrator', 1),
(5, 2, 1, 1, 'Biaya Premi Pariwisata', '2021-03-13', 1419306, 30, 'Debit', 'Administrator', 1),
(5, 2, 1, 1, 'Biaya Premi Pariwisata', '2021-03-14', 1435805, 30, 'Debit', 'Administrator', 1),
(5, 2, 1, 1, 'Biaya Premi Pariwisata', '2021-03-15', 1085483, 30, 'Debit', 'Administrator', 1),
(5, 2, 1, 1, 'Biaya Premi Pariwisata', '2021-03-16', 1484701, 30, 'Debit', 'Administrator', 1),
(5, 2, 1, 1, 'Biaya Premi Pariwisata', '2021-03-16', 1432000, 30, 'Debit', 'Administrator', 1),
(5, 2, 1, 1, 'Biaya Premi Pariwisata', '2021-03-16', 1473234, 30, 'Debit', 'Administrator', 1),
(5, 2, 1, 1, 'Biaya Premi Pariwisata', '2021-03-14', 1458266, 30, 'Debit', 'Administrator', 1),
(5, 2, 1, 1, 'Biaya Premi Pariwisata', '2021-03-19', 1467093, 30, 'Debit', 'Administrator', 1),
(5, 2, 1, 1, 'Biaya Premi Pariwisata', '2021-03-15', 1472843, 30, 'Debit', 'Administrator', 1),
(6, 8, 1, 8, 'Biaya Konsumsi Kantor', '2021-03-23', 200000, 30, 'Debit', 'Administrator', 1),
(5, 2, 1, 1, 'Biaya Premi Pariwisata', '2021-03-25', 780000, 30, 'Debit', 'Administrator', 1),
(5, 2, 1, 1, 'Biaya Premi Pariwisata', '2021-03-25', 390000, 30, 'Debit', 'Administrator', 1),
(6, 8, 1, 5, 'Biaya BBM', '2021-03-25', 1299135, 30, 'Debit', 'Administrator', 1),
(6, 8, 1, 6, 'Biaya Rekening Internet dan Pulsa ', '2021-03-25', 100000, 30, 'Debit', 'Administrator', 1),
(5, 2, 1, 1, 'Biaya Premi Pariwisata', '2021-03-26', 180000, 30, 'Debit', 'Administrator', 1),
(5, 2, 1, 1, 'Biaya Premi Pariwisata', '2021-03-26', 90000, 30, 'Debit', 'Administrator', 1),
(6, 8, 1, 5, 'Biaya BBM', '2021-03-26', 443526, 30, 'Debit', 'Administrator', 1),
(6, 8, 1, 6, 'Biaya Rekening Internet dan Pulsa ', '2021-03-26', 30000, 30, 'Debit', 'Administrator', 1),
(5, 2, 1, 1, 'Biaya Premi Pariwisata', '2021-03-26', 300000, 30, 'Debit', 'Administrator', 1),
(5, 2, 1, 1, 'Biaya Premi Pariwisata', '2021-03-26', 150000, 30, 'Debit', 'Administrator', 1),
(6, 8, 1, 5, 'Biaya BBM', '2021-03-26', 850000, 30, 'Debit', 'Administrator', 1),
(6, 8, 1, 6, 'Biaya Rekening Internet dan Pulsa ', '2021-03-26', 50000, 30, 'Debit', 'Administrator', 1),
(5, 2, 1, 1, 'Biaya Premi Pariwisata', '2021-03-26', 1427625, 30, 'Debit', 'Administrator', 1),
(5, 2, 1, 1, 'Biaya Premi Pariwisata', '2021-03-26', 1448703, 30, 'Debit', 'Administrator', 1),
(6, 8, 1, 5, 'Biaya BBM', '2021-03-26', 300000, 30, 'Debit', 'Administrator', 1),
(1, 1, 5, 7, 'Piutang Operasional', '2021-01-12', 3505700, 30, 'Debit', 'UMJE', 3),
(1, 1, 5, 7, 'Piutang Operasional', '2021-01-25', 2250000, 30, 'Debit', 'UMJE', 3),
(6, 1, 1, 2, 'Biaya BPJS', '2021-06-08', 11464603, 30, 'Debit', 'Menager Keuangan', 5),
(6, 8, 1, 7, 'Biaya Rekening Listrik', '2021-06-08', 1040500, 30, 'Debit', 'Menager Keuangan', 5),
(6, 8, 1, 7, 'Biaya Rekening Listrik', '2021-04-10', 568923, 30, 'Debit', 'Menager Keuangan', 5),
(6, 1, 1, 2, 'Biaya BPJS', '2021-06-17', 6282151, 30, 'Debit', 'Menager Keuangan', 5),
(3, 1, 1, 1, 'Saham', '2021-01-01', 900000000, 34, 'kredit', 'Menager Keuangan', 5),
(3, 1, 1, 1, 'Saham', '2021-01-01', 100000000, 34, 'kredit', 'Menager Keuangan', 5),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-05', 2500000, 34, 'kredit', 'Menager Keuangan', 5),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-05', 2265000, 34, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-05', 2345000, 34, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-05', 1365000, 34, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-06', 14300000, 34, 'kredit', 'UMJT', 2),
(1, 1, 2, 2, 'BSI - 7149685288 (Operasional Kasir)', '2021-05-20', 3022000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 2, 'BSI - 7149685288 (Operasional Kasir)', '2021-05-21', 4040000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 2, 'BSI - 7149685288 (Operasional Kasir)', '2021-05-21', 2899000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 2, 'BSI - 7149685288 (Operasional Kasir)', '2021-05-24', 1306000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 2, 'BSI - 7149685288 (Operasional Kasir)', '2021-05-24', 2000000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 2, 'BSI - 7149685288 (Operasional Kasir)', '2021-05-24', 1000000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 2, 'BSI - 7149685288 (Operasional Kasir)', '2021-05-25', 19366000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 2, 'BSI - 7149685288 (Operasional Kasir)', '2021-05-28', 4975000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 2, 'BSI - 7149685288 (Operasional Kasir)', '2021-05-28', 1956500, 30, 'kredit', 'UMJT', 2),
(1, 1, 1, 2, 'Kas Kecil', '2021-05-21', 79000, 30, 'kredit', 'Menager Keuangan', 5),
(1, 1, 2, 2, 'BSI - 7149685288 (Operasional Kasir)', '2021-06-04', 250000, 30, 'kredit', 'Menager Keuangan', 5),
(1, 1, 2, 2, 'BSI - 7149685288 (Operasional Kasir)', '2021-05-27', 890300, 30, 'kredit', 'Menager Keuangan', 5),
(1, 1, 2, 2, 'BSI - 7149685288 (Operasional Kasir)', '2021-05-18', 2000000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 2, 'BSI - 7149685288 (Operasional Kasir)', '2021-05-20', 1690000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-12', 74000, 30, 'kredit', 'Menager Keuangan', 5),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-21', 501170, 30, 'kredit', 'Menager Keuangan', 5),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-16', 804300, 30, 'kredit', 'Menager Keuangan', 5),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-22', 100000, 30, 'kredit', 'Menager Keuangan', 5),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-22', 45000, 30, 'kredit', 'Menager Keuangan', 5),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-02-02', 800000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-14', 8940000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-14', 1130000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-21', 1200000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-21', 2543000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-21', 1103000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-08-28', 373500, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-11', 546500, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-11', 256500, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-12', 471000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-08-29', 1664000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-11', 2000000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-13', 2525000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-01', 493500, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-02-01', 121000, 30, 'kredit', 'Administrator', 1),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-02-02', 1116000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-02-02', 772100, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-02-03', 432000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-02-03', 100000, 30, 'kredit', 'Administrator', 1),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-02-03', 3560000, 30, 'kredit', 'Administrator', 1),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-02-03', 4000000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-02-04', 1404000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-02-04', 1041000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-02-05', 17293500, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-02-05', 2220000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-02-05', 4620000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-02-10', 56000, 30, 'kredit', 'Administrator', 1),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-02-10', 163500, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-02-11', 3900000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-02-15', 1000000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-02-17', 250000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-02-19', 600000, 30, 'kredit', 'Administrator', 1),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-02-19', 15242300, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-02-19', 830000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-02-25', 19622300, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-02-26', 3450000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-05', 2345000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-05', 1365000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-06', 2265000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-06', 14300000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-06', 1860000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-06', 4800000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-06', 1710000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-06', 6000000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-08', 4330000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-08', 3000000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-09', 1500000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-09', 6645000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-12', 1350000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-12', 4470000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-12', 1727500, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-13', 4200000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-14', 1425000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-15', 1408000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-15', 1169000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-16', 239000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-16', 1400000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-16', 1758627, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-18', 1450000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-18', 600000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-18', 1146000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-19', 138000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-19', 214500, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-19', 328000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-19', 630000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-19', 1946373, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-19', 842500, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-20', 168000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-22', 2071000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-09-23', 750000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-20', 2129868, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-28', 6383500, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-29', 2780000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-30', 9500000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-04-19', 5311500, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-04-19', 1315000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-04-20', 4575000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-04-21', 8442500, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-04-22', 1688000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-04-23', 8297000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-04-23', 6764500, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-04-26', 1108000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-04-26', 3245000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-04-29', 19749000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-04-27', 2832000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-04-27', 2700000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-04-30', 9300000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-04-30', 9914000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-05-07', 5167500, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-05-07', 9240000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-05-10', 275000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-05-11', 2066000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-05-11', 2590000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 2, 'BSI - 7149685288 (Operasional Kasir)', '2021-06-04', 2525000, 30, 'kredit', 'UMJT', 2),
(1, 1, 1, 2, 'Kas Kecil', '2021-01-08', 800000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-09', 3839000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-15', 325000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-21', 1490000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-26', 2500000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-02-08', 100000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 3, 'Mandiri - 1430000679009 (Tabungan)', '2021-03-03', 17190000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-03-05', 4865000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-03-05', 5510000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-03-05', 3000000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-03-15', 2153500, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-03-12', 4428000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-03-16', 278000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-03-16', 1345000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-03-16', 1770000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-03-17', 1150000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-03-17', 20000000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-03-18', 2150500, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-03-19', 4482000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-03-19', 1875500, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-03-26', 1550000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-03-26', 4375000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-03-26', 5289000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-03-26', 1300000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-03-29', 3000000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-03-30', 3272500, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-03-30', 462000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-03-30', 20000000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-04-01', 498000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-03-31', 17267000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-04-01', 2200000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-04-02', 4626000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-04-01', 2250000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-04-06', 3715500, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-04-07', 8080000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-04-09', 4190000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-04-07', 210000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-04-08', 10860000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-04-09', 7901000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-04-12', 2030000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-04-16', 250000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-04-16', 4780000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-04-16', 5977500, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-05-04', 5266500, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-05-04', 1850000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 2, 'BSI - 7149685288 (Operasional Kasir)', '2021-06-04', 5145000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 2, 'BSI - 7149685288 (Operasional Kasir)', '2021-06-04', 4756000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 2, 'BSI - 7149685288 (Operasional Kasir)', '2021-06-07', 20058000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 2, 'BSI - 7149685288 (Operasional Kasir)', '2021-06-11', 2070000, 30, 'kredit', 'UMJT', 2),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-06', 184500, 30, 'kredit', 'Menager Keuangan', 5),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-06', 716500, 30, 'kredit', 'Menager Keuangan', 5),
(1, 1, 2, 3, 'Mandiri - 1430000679009 (Tabungan)', '2021-01-16', 18360000, 30, 'kredit', 'Menager Keuangan', 5),
(1, 1, 2, 3, 'Mandiri - 1430000679009 (Tabungan)', '2021-01-19', 5358423, 30, 'kredit', 'Menager Keuangan', 5),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-26', 4197700, 30, 'kredit', 'Menager Keuangan', 5),
(1, 1, 1, 2, 'Kas Kecil', '2021-01-26', 300000, 30, 'kredit', 'Menager Keuangan', 5),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-27', 3000000, 30, 'kredit', 'Menager Keuangan', 5),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-27', 1414260, 30, 'kredit', 'Menager Keuangan', 5),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-27', 100000, 30, 'kredit', 'Menager Keuangan', 5),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-27', 16100, 30, 'kredit', 'Menager Keuangan', 5),
(1, 1, 1, 2, 'Kas Kecil', '2021-01-29', 495000, 30, 'kredit', 'Menager Keuangan', 5),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-03-01', 41487740, 30, 'kredit', 'Menager Keuangan', 5),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-03-01', 700000, 30, 'kredit', 'Menager Keuangan', 5),
(1, 1, 1, 2, 'Kas Kecil', '2021-03-01', 184200, 30, 'kredit', 'Menager Keuangan', 5),
(1, 1, 2, 3, 'Mandiri - 1430000679009 (Tabungan)', '2021-03-05', 8884140, 30, 'kredit', 'Menager Keuangan', 5),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-03-01', 150000, 30, 'kredit', 'Menager Keuangan', 5),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-03-01', 1000000, 30, 'kredit', 'Menager Keuangan', 5),
(1, 1, 1, 2, 'Kas Kecil', '2021-03-09', 46600, 30, 'kredit', 'Menager Keuangan', 5),
(1, 1, 1, 2, 'Kas Kecil', '2021-03-12', 100000, 30, 'kredit', 'Menager Keuangan', 5),
(1, 1, 1, 2, 'Kas Kecil', '2021-03-16', 31000, 30, 'kredit', 'Menager Keuangan', 5),
(1, 1, 1, 2, 'Kas Kecil', '2021-03-25', 23200, 30, 'kredit', 'Menager Keuangan', 5),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-03-01', 350000, 30, 'kredit', 'Menager Keuangan', 5),
(1, 1, 1, 2, 'Kas Kecil', '2021-03-15', 412506, 30, 'kredit', 'Menager Keuangan', 5),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-03-16', 613000, 30, 'kredit', 'Menager Keuangan', 5),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-03-25', 285000, 30, 'kredit', 'Menager Keuangan', 5),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-03-25', 1200000, 30, 'kredit', 'Menager Keuangan', 5),
(1, 1, 2, 3, 'Mandiri - 1430000679009 (Tabungan)', '2021-03-27', 440980, 30, 'kredit', 'Menager Keuangan', 5),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-04-05', 250000, 30, 'kredit', 'Menager Keuangan', 5),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-04-01', 1000000, 30, 'kredit', 'Menager Keuangan', 5),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-04-01', 250000, 30, 'kredit', 'Menager Keuangan', 5),
(1, 1, 1, 2, 'Kas Kecil', '2021-04-05', 187500, 30, 'kredit', 'Menager Keuangan', 5),
(1, 1, 1, 2, 'Kas Kecil', '2021-04-05', 750000, 30, 'kredit', 'Menager Keuangan', 5),
(1, 1, 1, 2, 'Kas Kecil', '2021-04-08', 90000, 30, 'kredit', 'Menager Keuangan', 5),
(1, 1, 2, 3, 'Mandiri - 1430000679009 (Tabungan)', '2021-04-12', 16800, 30, 'kredit', 'Menager Keuangan', 5),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-04-15', 350000, 30, 'kredit', 'Menager Keuangan', 5),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-04-15', 100000, 30, 'kredit', 'Menager Keuangan', 5),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-04-15', 6265351, 30, 'kredit', 'Menager Keuangan', 5),
(1, 1, 2, 3, 'Mandiri - 1430000679009 (Tabungan)', '2021-04-10', 2944575, 30, 'kredit', 'Menager Keuangan', 5),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-04-19', 1750000, 30, 'kredit', 'Menager Keuangan', 5),
(1, 1, 2, 3, 'Mandiri - 1430000679009 (Tabungan)', '2021-04-19', 1000000, 30, 'kredit', 'Menager Keuangan', 5),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-04-19', 51500, 30, 'kredit', 'Menager Keuangan', 5),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-04-20', 1200000, 30, 'kredit', 'Menager Keuangan', 5),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-04-26', 1500000, 30, 'kredit', 'Menager Keuangan', 5),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-04-21', 293000, 30, 'kredit', 'Menager Keuangan', 5),
(1, 1, 2, 3, 'Mandiri - 1430000679009 (Tabungan)', '2021-04-21', 1300000, 30, 'kredit', 'Menager Keuangan', 5),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-04-22', 5999000, 30, 'kredit', 'Menager Keuangan', 5),
(1, 1, 2, 2, 'BSI - 7149685288 (Operasional Kasir)', '2021-04-29', 60000, 30, 'kredit', 'Menager Keuangan', 5),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-04-30', 39766850, 30, 'kredit', 'Menager Keuangan', 5),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-04-30', 250000, 30, 'kredit', 'Menager Keuangan', 5),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-05-04', 27134803, 30, 'kredit', 'Menager Keuangan', 5),
(1, 1, 1, 2, 'Kas Kecil', '2021-05-11', 270000, 30, 'kredit', 'Menager Keuangan', 5),
(1, 1, 2, 3, 'Mandiri - 1430000679009 (Tabungan)', '2021-05-31', 39861076, 30, 'kredit', 'Menager Keuangan', 5),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-20', 2416633, 30, 'kredit', 'Administrator', 1),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-07', 235000, 30, 'kredit', 'Administrator', 1),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-08', 900000, 30, 'kredit', 'Administrator', 1),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-08', 2278329, 30, 'kredit', 'Administrator', 1),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-08', 450000, 30, 'kredit', 'Administrator', 1),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-09', 520000, 30, 'kredit', 'Administrator', 1),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-09', 50000, 30, 'kredit', 'Administrator', 1),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-09', 260000, 30, 'kredit', 'Administrator', 1),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-09', 912758, 30, 'kredit', 'Administrator', 1),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-26', 126730, 30, 'kredit', 'Administrator', 1),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-26', 523000, 30, 'kredit', 'Administrator', 1),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-26', 1199000, 30, 'kredit', 'Administrator', 1),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-02-09', 16000, 30, 'kredit', 'Administrator', 1),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-03-02', 300000, 30, 'kredit', 'Administrator', 1),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-03-08', 101500, 30, 'kredit', 'Administrator', 1),
(1, 1, 2, 3, 'Mandiri - 1430000679009 (Tabungan)', '2021-03-07', 260000, 30, 'kredit', 'Administrator', 1),
(1, 1, 2, 3, 'Mandiri - 1430000679009 (Tabungan)', '2021-03-07', 50000, 30, 'kredit', 'Administrator', 1),
(1, 1, 2, 3, 'Mandiri - 1430000679009 (Tabungan)', '2021-03-07', 200000, 30, 'kredit', 'Administrator', 1),
(1, 1, 2, 3, 'Mandiri - 1430000679009 (Tabungan)', '2021-03-07', 130000, 30, 'kredit', 'Administrator', 1),
(1, 1, 2, 3, 'Mandiri - 1430000679009 (Tabungan)', '2021-03-07', 120000, 30, 'kredit', 'Administrator', 1),
(1, 1, 2, 3, 'Mandiri - 1430000679009 (Tabungan)', '2021-03-07', 700000, 30, 'kredit', 'Administrator', 1);
INSERT INTO `mv_detailbukubesar` (`kode_coa_1`, `kode_coa_2`, `kode_coa_3`, `kode_coa_4`, `namacoa`, `Tanggal`, `Nilai`, `IdUser`, `Type`, `description`, `IdUnit`) VALUES
(1, 1, 2, 3, 'Mandiri - 1430000679009 (Tabungan)', '2021-03-08', 260000, 30, 'kredit', 'Administrator', 1),
(1, 1, 2, 3, 'Mandiri - 1430000679009 (Tabungan)', '2021-03-08', 200000, 30, 'kredit', 'Administrator', 1),
(1, 1, 2, 3, 'Mandiri - 1430000679009 (Tabungan)', '2021-03-08', 130000, 30, 'kredit', 'Administrator', 1),
(1, 1, 2, 3, 'Mandiri - 1430000679009 (Tabungan)', '2021-03-08', 120000, 30, 'kredit', 'Administrator', 1),
(1, 1, 2, 3, 'Mandiri - 1430000679009 (Tabungan)', '2021-03-08', 721518, 30, 'kredit', 'Administrator', 1),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-03-10', 151200, 30, 'kredit', 'Administrator', 1),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-03-10', 60000, 30, 'kredit', 'Administrator', 1),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-03-12', 150000, 30, 'kredit', 'Administrator', 1),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-03-15', 75000, 30, 'kredit', 'Administrator', 1),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-03-15', 145000, 30, 'kredit', 'Administrator', 1),
(1, 1, 2, 3, 'Mandiri - 1430000679009 (Tabungan)', '2021-03-13', 1419306, 30, 'kredit', 'Administrator', 1),
(1, 1, 2, 3, 'Mandiri - 1430000679009 (Tabungan)', '2021-03-14', 1435805, 30, 'kredit', 'Administrator', 1),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-03-15', 1085483, 30, 'kredit', 'Administrator', 1),
(1, 1, 1, 2, 'Kas Kecil', '2021-03-16', 1484701, 30, 'kredit', 'Administrator', 1),
(1, 1, 1, 2, 'Kas Kecil', '2021-03-16', 1432000, 30, 'kredit', 'Administrator', 1),
(1, 1, 1, 2, 'Kas Kecil', '2021-03-16', 1473234, 30, 'kredit', 'Administrator', 1),
(1, 1, 2, 3, 'Mandiri - 1430000679009 (Tabungan)', '2021-03-14', 1458266, 30, 'kredit', 'Administrator', 1),
(1, 1, 1, 2, 'Kas Kecil', '2021-03-19', 1467093, 30, 'kredit', 'Administrator', 1),
(1, 1, 2, 3, 'Mandiri - 1430000679009 (Tabungan)', '2021-03-15', 1472843, 30, 'kredit', 'Administrator', 1),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-03-23', 200000, 30, 'kredit', 'Administrator', 1),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-03-25', 780000, 30, 'kredit', 'Administrator', 1),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-03-25', 390000, 30, 'kredit', 'Administrator', 1),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-03-25', 1299135, 30, 'kredit', 'Administrator', 1),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-03-25', 100000, 30, 'kredit', 'Administrator', 1),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-03-26', 180000, 30, 'kredit', 'Administrator', 1),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-03-26', 90000, 30, 'kredit', 'Administrator', 1),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-03-26', 443526, 30, 'kredit', 'Administrator', 1),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-03-26', 30000, 30, 'kredit', 'Administrator', 1),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-03-26', 300000, 30, 'kredit', 'Administrator', 1),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-03-26', 150000, 30, 'kredit', 'Administrator', 1),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-03-26', 850000, 30, 'kredit', 'Administrator', 1),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-03-26', 50000, 30, 'kredit', 'Administrator', 1),
(1, 1, 1, 2, 'Kas Kecil', '2021-03-26', 1427625, 30, 'kredit', 'Administrator', 1),
(1, 1, 1, 2, 'Kas Kecil', '2021-03-26', 1448703, 30, 'kredit', 'Administrator', 1),
(1, 1, 1, 2, 'Kas Kecil', '2021-03-26', 300000, 30, 'kredit', 'Administrator', 1),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-12', 3505700, 30, 'kredit', 'UMJE', 3),
(1, 1, 2, 1, 'BSI - 7138474822 (Bendahara 19)', '2021-01-25', 2250000, 30, 'kredit', 'UMJE', 3),
(1, 1, 2, 3, 'Mandiri - 1430000679009 (Tabungan)', '2021-06-08', 11464603, 30, 'kredit', 'Menager Keuangan', 5),
(1, 1, 1, 2, 'Kas Kecil', '2021-06-08', 1040500, 30, 'kredit', 'Menager Keuangan', 5),
(1, 1, 2, 3, 'Mandiri - 1430000679009 (Tabungan)', '2021-04-10', 568923, 30, 'kredit', 'Menager Keuangan', 5),
(1, 1, 2, 3, 'Mandiri - 1430000679009 (Tabungan)', '2021-06-17', 6282151, 30, 'kredit', 'Menager Keuangan', 5);

-- --------------------------------------------------------

--
-- Table structure for table `mv_neraca_lajur`
--

CREATE TABLE `mv_neraca_lajur` (
  `id` int(11) NOT NULL,
  `Tanggal` varchar(20) NOT NULL,
  `Nilai_debit` double NOT NULL,
  `Nilai_kredit` double NOT NULL,
  `saldo` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mv_neraca_lajur`
--

INSERT INTO `mv_neraca_lajur` (`id`, `Tanggal`, `Nilai_debit`, `Nilai_kredit`, `saldo`) VALUES
(2, '2021-03', 40000000, 8567600, 31432400),
(2, '2021-03', 40000000, 1040500, 38959500),
(2, '2021-03', 40000000, 349000, 39651000),
(2, '2021-03', 40000000, 1027500, 38972500),
(2, '2021-03', 40000000, 9830862, 30169138),
(2, '2021-03', 40000000, 1595000, 38405000),
(2, '2021-06', 3499000, 8567600, -5068600),
(2, '2021-06', 3499000, 1040500, 2458500),
(2, '2021-06', 3499000, 349000, 3150000),
(2, '2021-06', 3499000, 1027500, 2471500),
(2, '2021-06', 3499000, 9830862, -6331862),
(2, '2021-06', 3499000, 1595000, 1904000),
(2, '2021-07', 23169700, 8567600, 14602100),
(2, '2021-07', 23169700, 1040500, 22129200),
(2, '2021-07', 23169700, 349000, 22820700),
(2, '2021-07', 23169700, 1027500, 22142200),
(2, '2021-07', 23169700, 9830862, 13338838),
(2, '2021-07', 23169700, 1595000, 21574700),
(2, '2021-08', 16059000, 8567600, 7491400),
(2, '2021-08', 16059000, 1040500, 15018500),
(2, '2021-08', 16059000, 349000, 15710000),
(2, '2021-08', 16059000, 1027500, 15031500),
(2, '2021-08', 16059000, 9830862, 6228138),
(2, '2021-08', 16059000, 1595000, 14464000),
(2, '2021-09', 2289150, 8567600, -6278450),
(2, '2021-09', 2289150, 1040500, 1248650),
(2, '2021-09', 2289150, 349000, 1940150),
(2, '2021-09', 2289150, 1027500, 1261650),
(2, '2021-09', 2289150, 9830862, -7541712),
(2, '2021-09', 2289150, 1595000, 694150),
(2, '2021-10', 1748900, 8567600, -6818700),
(2, '2021-10', 1748900, 1040500, 708400),
(2, '2021-10', 1748900, 349000, 1399900),
(2, '2021-10', 1748900, 1027500, 721400),
(2, '2021-10', 1748900, 9830862, -8081962),
(2, '2021-10', 1748900, 1595000, 153900),
(2, '2022-01', 8567600, 8567600, 0),
(2, '2022-01', 8567600, 1040500, 7527100),
(2, '2022-01', 8567600, 349000, 8218600),
(2, '2022-01', 8567600, 1027500, 7540100),
(2, '2022-01', 8567600, 9830862, -1263262),
(2, '2022-01', 8567600, 1595000, 6972600),
(3, '2021-01', 100000000, 750000, 99250000),
(3, '2021-01', 100000000, 2037500, 97962500),
(3, '2021-01', 100000000, 53589803, 46410197),
(3, '2021-01', 100000000, 201835201, -101835201),
(3, '2021-01', 100000000, 162239584, -62239584),
(3, '2021-01', 100000000, 82709700, 17290300),
(3, '2021-01', 100000000, 178513048, -78513048),
(3, '2021-08', 36498041, 750000, 35748041),
(3, '2021-08', 36498041, 2037500, 34460541),
(3, '2021-08', 36498041, 53589803, -17091762),
(3, '2021-08', 36498041, 201835201, -165337160),
(3, '2021-08', 36498041, 162239584, -125741543),
(3, '2021-08', 36498041, 82709700, -46211659),
(3, '2021-08', 36498041, 178513048, -142015007),
(3, '2021-09', 22150000, 750000, 21400000),
(3, '2021-09', 22150000, 2037500, 20112500),
(3, '2021-09', 22150000, 53589803, -31439803),
(3, '2021-09', 22150000, 201835201, -179685201),
(3, '2021-09', 22150000, 162239584, -140089584),
(3, '2021-09', 22150000, 82709700, -60559700),
(3, '2021-09', 22150000, 178513048, -156363048),
(4, '2021-06', 67055096, 12565000, 54490096),
(4, '2021-06', 67055096, 32895000, 34160096),
(4, '2021-06', 67055096, 71354000, -4298904),
(4, '2021-06', 67055096, 45144800, 21910296),
(4, '2021-06', 67055096, 60000, 66995096),
(4, '2021-07', 118107000, 12565000, 105542000),
(4, '2021-07', 118107000, 32895000, 85212000),
(4, '2021-07', 118107000, 71354000, 46753000),
(4, '2021-07', 118107000, 45144800, 72962200),
(4, '2021-07', 118107000, 60000, 118047000),
(4, '2021-08', 88816000, 12565000, 76251000),
(4, '2021-08', 88816000, 32895000, 55921000),
(4, '2021-08', 88816000, 71354000, 17462000),
(4, '2021-08', 88816000, 45144800, 43671200),
(4, '2021-08', 88816000, 60000, 88756000),
(4, '2021-09', 2267627, 12565000, -10297373),
(4, '2021-09', 2267627, 32895000, -30627373),
(4, '2021-09', 2267627, 71354000, -69086373),
(4, '2021-09', 2267627, 45144800, -42877173),
(4, '2021-09', 2267627, 60000, 2207627),
(4, '2021-10', 4195474, 12565000, -8369526),
(4, '2021-10', 4195474, 32895000, -28699526),
(4, '2021-10', 4195474, 71354000, -67158526),
(4, '2021-10', 4195474, 45144800, -40949326),
(4, '2021-10', 4195474, 60000, 4135474),
(4, '2022-01', 12565000, 12565000, 0),
(4, '2022-01', 12565000, 32895000, -20330000),
(4, '2022-01', 12565000, 71354000, -58789000),
(4, '2022-01', 12565000, 45144800, -32579800),
(4, '2022-01', 12565000, 60000, 12505000),
(5, '2021-01', 900000000, 17746754, 882253246),
(5, '2021-01', 900000000, 39861076, 860138924),
(5, '2021-01', 900000000, 5830298, 894169702),
(5, '2021-01', 900000000, 35192858, 864807142),
(5, '2021-01', 900000000, 23718423, 876281577),
(5, '2021-08', 11645282, 17746754, -6101472),
(5, '2021-08', 11645282, 39861076, -28215794),
(5, '2021-08', 11645282, 5830298, 5814984),
(5, '2021-08', 11645282, 35192858, -23547576),
(5, '2021-08', 11645282, 23718423, -12073141),
(5, '2021-09', 16845408, 17746754, -901346),
(5, '2021-09', 16845408, 39861076, -23015668),
(5, '2021-09', 16845408, 5830298, 11015110),
(5, '2021-09', 16845408, 35192858, -18347450),
(5, '2021-09', 16845408, 23718423, -6873015),
(5, '2021-10', 33031235, 17746754, 15284481),
(5, '2021-10', 33031235, 39861076, -6829841),
(5, '2021-10', 33031235, 5830298, 27200937),
(5, '2021-10', 33031235, 35192858, -2161623),
(5, '2021-10', 33031235, 23718423, 9312812),
(11, '2021-01', 53794103, 0, 53794103),
(11, '2021-02', 4453000, 0, 4453000),
(12, '2021-04', 133235500, 34384500, 98851000),
(12, '2021-04', 133235500, 21298096, 111937404),
(12, '2021-05', 70709500, 34384500, 36325000),
(12, '2021-05', 70709500, 21298096, 49411404),
(12, '2021-06', 34554000, 34384500, 169500),
(12, '2021-06', 34554000, 21298096, 13255904),
(20, '2021-01', 1200000, 0, 1200000),
(21, '2021-03', 150000, 0, 150000),
(21, '2021-04', 5999000, 0, 5999000),
(46, '2021-03', 18876259, 0, 18876259),
(47, '2021-01', 16434000, 12933000, 3501000),
(47, '2021-01', 16434000, 38599000, -22165000),
(47, '2021-01', 16434000, 10300000, 6134000),
(47, '2021-02', 12800000, 12933000, -133000),
(47, '2021-02', 12800000, 38599000, -25799000),
(47, '2021-02', 12800000, 10300000, 2500000),
(47, '2021-03', 21045000, 12933000, 8112000),
(47, '2021-03', 21045000, 38599000, -17554000),
(47, '2021-03', 21045000, 10300000, 10745000),
(47, '2021-04', 4626000, 12933000, -8307000),
(47, '2021-04', 4626000, 38599000, -33973000),
(47, '2021-04', 4626000, 10300000, -5674000),
(48, '2021-01', 3800000, 6400000, -2600000),
(48, '2021-01', 3800000, 6850000, -3050000),
(50, '2021-03', 41987740, 37822173, 4165567),
(50, '2021-04', 41266850, 37822173, 3444677),
(50, '2021-05', 39861076, 37822173, 2038903),
(50, '2021-06', 250000, 37822173, -37572173),
(53, '2021-01', 1946373, 1155000, 791373),
(53, '2021-01', 1946373, 1807000, 139373),
(53, '2021-03', 9325120, 1155000, 8170120),
(53, '2021-03', 9325120, 1807000, 7518120),
(53, '2021-04', 2944575, 1155000, 1789575),
(53, '2021-04', 2944575, 1807000, 1137575),
(53, '2021-05', 270000, 1155000, -885000),
(53, '2021-05', 270000, 1807000, -1537000),
(57, '2021-02', 100000, 1899600, -1799600),
(57, '2021-03', 240000, 1899600, -1659600),
(62, '2021-04', 60000, 0, 60000),
(64, '2021-03', 1200000, 1650000, -450000),
(64, '2021-04', 5250000, 1650000, 3600000),
(65, '2021-03', 613000, 0, 613000),
(65, '2021-04', 350000, 0, 350000),
(82, '2021-03', 1000000, 0, 1000000),
(83, '2021-04', 2600000, 1000000, 1600000),
(84, '2021-03', 31000, 322500, -291500),
(84, '2021-03', 31000, 635500, -604500),
(84, '2021-04', 498000, 322500, 175500),
(84, '2021-04', 498000, 635500, -137500),
(84, '2021-05', 890300, 322500, 567800),
(84, '2021-05', 890300, 635500, 254800),
(85, '2021-03', 754800, 738400, 16400),
(85, '2021-03', 754800, 575650, 179150),
(85, '2021-03', 754800, 932000, -177200),
(85, '2021-03', 754800, 274500, 480300),
(85, '2021-04', 480500, 738400, -257900),
(85, '2021-04', 480500, 575650, -95150),
(85, '2021-04', 480500, 932000, -451500),
(85, '2021-04', 480500, 274500, 206000),
(85, '2021-05', 79000, 738400, -659400),
(85, '2021-05', 79000, 575650, -496650),
(85, '2021-05', 79000, 932000, -853000),
(85, '2021-05', 79000, 274500, -195500),
(87, '2021-04', 90000, 60000, 30000),
(87, '2021-04', 90000, 765050, -675050),
(88, '2021-03', 4498379, 0, 4498379),
(95, '2021-03', 694500, 128000, 566500),
(95, '2021-03', 694500, 257000, 437500),
(95, '2021-04', 51500, 128000, -76500),
(95, '2021-04', 51500, 257000, -205500),
(96, '2021-03', 412506, 1172974, -760468),
(96, '2021-03', 412506, 468627, -56121),
(96, '2021-03', 412506, 320700, 91806),
(96, '2021-04', 568923, 1172974, -604051),
(96, '2021-04', 568923, 468627, 100296),
(96, '2021-04', 568923, 320700, 248223),
(96, '2021-06', 1040500, 1172974, -132474),
(96, '2021-06', 1040500, 468627, 571873),
(96, '2021-06', 1040500, 320700, 719800),
(97, '2021-02', 1116000, 0, 1116000),
(97, '2021-03', 345000, 0, 345000),
(98, '2021-01', 126651995, 0, 126651995),
(98, '2021-02', 64240700, 0, 64240700),
(98, '2021-03', 66090000, 0, 66090000),
(98, '2021-04', 4450000, 0, 4450000),
(98, '2021-08', 2037500, 0, 2037500),
(98, '2021-09', 750000, 0, 750000),
(104, '2021-04', 6282151, 33031235, -26749084),
(104, '2021-04', 6282151, 16845408, -10563257),
(104, '2021-06', 17746754, 33031235, -15284481),
(104, '2021-06', 17746754, 16845408, 901346),
(105, '2021-05', 27134803, 0, 27134803),
(0, '2021-07', 0, 14340000, -14340000),
(0, '2021-10', 0, 1750000, -1750000),
(2, '2021-01', 8567600, 1595000, 6972600),
(2, '2021-01', 1748900, 1595000, 153900),
(2, '2021-01', 2289150, 1595000, 694150),
(2, '2021-01', 16059000, 1595000, 14464000),
(2, '2021-01', 23169700, 1595000, 21574700),
(2, '2021-01', 3499000, 1595000, 1904000),
(2, '2021-01', 40000000, 1595000, 38405000),
(2, '2021-03', 8567600, 9830862, -1263262),
(2, '2021-03', 1748900, 9830862, -8081962),
(2, '2021-03', 2289150, 9830862, -7541712),
(2, '2021-03', 16059000, 9830862, 6228138),
(2, '2021-03', 23169700, 9830862, 13338838),
(2, '2021-03', 3499000, 9830862, -6331862),
(2, '2021-04', 8567600, 1027500, 7540100),
(2, '2021-04', 1748900, 1027500, 721400),
(2, '2021-04', 2289150, 1027500, 1261650),
(2, '2021-04', 16059000, 1027500, 15031500),
(2, '2021-04', 23169700, 1027500, 22142200),
(2, '2021-04', 3499000, 1027500, 2471500),
(2, '2021-04', 40000000, 1027500, 38972500),
(2, '2021-05', 8567600, 349000, 8218600),
(2, '2021-05', 1748900, 349000, 1399900),
(2, '2021-05', 2289150, 349000, 1940150),
(2, '2021-05', 16059000, 349000, 15710000),
(2, '2021-05', 23169700, 349000, 22820700),
(2, '2021-05', 3499000, 349000, 3150000),
(2, '2021-05', 40000000, 349000, 39651000),
(2, '2021-06', 8567600, 1040500, 7527100),
(2, '2021-06', 1748900, 1040500, 708400),
(2, '2021-06', 2289150, 1040500, 1248650),
(2, '2021-06', 16059000, 1040500, 15018500),
(2, '2021-06', 23169700, 1040500, 22129200),
(2, '2021-06', 40000000, 1040500, 38959500),
(2, '2022-01', 1748900, 8567600, -6818700),
(2, '2022-01', 2289150, 8567600, -6278450),
(2, '2022-01', 16059000, 8567600, 7491400),
(2, '2022-01', 23169700, 8567600, 14602100),
(2, '2022-01', 3499000, 8567600, -5068600),
(2, '2022-01', 40000000, 8567600, 31432400),
(3, '2021-01', 22150000, 178513048, -156363048),
(3, '2021-01', 36498041, 178513048, -142015007),
(3, '2021-02', 22150000, 82709700, -60559700),
(3, '2021-02', 36498041, 82709700, -46211659),
(3, '2021-02', 100000000, 82709700, 17290300),
(3, '2021-03', 22150000, 162239584, -140089584),
(3, '2021-03', 36498041, 162239584, -125741543),
(3, '2021-03', 100000000, 162239584, -62239584),
(3, '2021-04', 22150000, 201835201, -179685201),
(3, '2021-04', 36498041, 201835201, -165337160),
(3, '2021-04', 100000000, 201835201, -101835201),
(3, '2021-05', 22150000, 53589803, -31439803),
(3, '2021-05', 36498041, 53589803, -17091762),
(3, '2021-05', 100000000, 53589803, 46410197),
(3, '2021-08', 22150000, 2037500, 20112500),
(3, '2021-08', 100000000, 2037500, 97962500),
(3, '2021-09', 36498041, 750000, 35748041),
(3, '2021-09', 100000000, 750000, 99250000),
(4, '2021-04', 12565000, 60000, 12505000),
(4, '2021-04', 4195474, 60000, 4135474),
(4, '2021-04', 2267627, 60000, 2207627),
(4, '2021-04', 88816000, 60000, 88756000),
(4, '2021-04', 118107000, 60000, 118047000),
(4, '2021-04', 67055096, 60000, 66995096),
(4, '2021-05', 12565000, 45144800, -32579800),
(4, '2021-05', 4195474, 45144800, -40949326),
(4, '2021-05', 2267627, 45144800, -42877173),
(4, '2021-05', 88816000, 45144800, 43671200),
(4, '2021-05', 118107000, 45144800, 72962200),
(4, '2021-05', 67055096, 45144800, 21910296),
(4, '2021-06', 12565000, 71354000, -58789000),
(4, '2021-06', 4195474, 71354000, -67158526),
(4, '2021-06', 2267627, 71354000, -69086373),
(4, '2021-06', 88816000, 71354000, 17462000),
(4, '2021-06', 118107000, 71354000, 46753000),
(4, '2021-07', 12565000, 32895000, -20330000),
(4, '2021-07', 4195474, 32895000, -28699526),
(4, '2021-07', 2267627, 32895000, -30627373),
(4, '2021-07', 88816000, 32895000, 55921000),
(4, '2021-07', 67055096, 32895000, 34160096),
(4, '2022-01', 4195474, 12565000, -8369526),
(4, '2022-01', 2267627, 12565000, -10297373),
(4, '2022-01', 88816000, 12565000, 76251000),
(4, '2022-01', 118107000, 12565000, 105542000),
(4, '2022-01', 67055096, 12565000, 54490096),
(5, '2021-01', 33031235, 23718423, 9312812),
(5, '2021-01', 16845408, 23718423, -6873015),
(5, '2021-01', 11645282, 23718423, -12073141),
(5, '2021-03', 33031235, 35192858, -2161623),
(5, '2021-03', 16845408, 35192858, -18347450),
(5, '2021-03', 11645282, 35192858, -23547576),
(5, '2021-03', 900000000, 35192858, 864807142),
(5, '2021-04', 33031235, 5830298, 27200937),
(5, '2021-04', 16845408, 5830298, 11015110),
(5, '2021-04', 11645282, 5830298, 5814984),
(5, '2021-04', 900000000, 5830298, 894169702),
(5, '2021-05', 33031235, 39861076, -6829841),
(5, '2021-05', 16845408, 39861076, -23015668),
(5, '2021-05', 11645282, 39861076, -28215794),
(5, '2021-05', 900000000, 39861076, 860138924),
(5, '2021-06', 33031235, 17746754, 15284481),
(5, '2021-06', 16845408, 17746754, -901346),
(5, '2021-06', 11645282, 17746754, -6101472),
(5, '2021-06', 900000000, 17746754, 882253246),
(12, '2021-06', 70709500, 21298096, 49411404),
(12, '2021-06', 133235500, 21298096, 111937404),
(12, '2021-07', 34554000, 34384500, 169500),
(12, '2021-07', 70709500, 34384500, 36325000),
(12, '2021-07', 133235500, 34384500, 98851000),
(34, '2021-01', 0, 1000000000, -1000000000),
(44, '2021-06', 0, 1496000, -1496000),
(44, '2021-07', 0, 13887500, -13887500),
(44, '2021-08', 0, 60206000, -60206000),
(47, '2021-06', 4626000, 10300000, -5674000),
(47, '2021-06', 21045000, 10300000, 10745000),
(47, '2021-06', 12800000, 10300000, 2500000),
(47, '2021-06', 16434000, 10300000, 6134000),
(47, '2021-07', 4626000, 38599000, -33973000),
(47, '2021-07', 21045000, 38599000, -17554000),
(47, '2021-07', 12800000, 38599000, -25799000),
(47, '2021-07', 16434000, 38599000, -22165000),
(47, '2021-08', 4626000, 12933000, -8307000),
(47, '2021-08', 21045000, 12933000, 8112000),
(47, '2021-08', 12800000, 12933000, -133000),
(47, '2021-08', 16434000, 12933000, 3501000),
(48, '2021-07', 3800000, 6850000, -3050000),
(48, '2021-08', 3800000, 6400000, -2600000),
(49, '2021-08', 0, 25076000, -25076000),
(50, '2021-08', 250000, 37822173, -37572173),
(50, '2021-08', 39861076, 37822173, 2038903),
(50, '2021-08', 41266850, 37822173, 3444677),
(50, '2021-08', 41987740, 37822173, 4165567),
(51, '2021-09', 0, 250000, -250000),
(53, '2021-09', 270000, 1807000, -1537000),
(53, '2021-09', 2944575, 1807000, 1137575),
(53, '2021-09', 9325120, 1807000, 7518120),
(53, '2021-09', 1946373, 1807000, 139373),
(53, '2021-10', 270000, 1155000, -885000),
(53, '2021-10', 2944575, 1155000, 1789575),
(53, '2021-10', 9325120, 1155000, 8170120),
(53, '2021-10', 1946373, 1155000, 791373),
(56, '2021-09', 0, 160000, -160000),
(57, '2021-08', 240000, 1899600, -1659600),
(57, '2021-08', 100000, 1899600, -1799600),
(63, '2021-08', 0, 6984500, -6984500),
(64, '2021-09', 5250000, 1650000, 3600000),
(64, '2021-09', 1200000, 1650000, -450000),
(83, '2021-09', 2600000, 1000000, 1600000),
(83, '2021-10', 2600000, 1000000, 1600000),
(84, '2021-06', 890300, 635500, 254800),
(84, '2021-06', 498000, 635500, -137500),
(84, '2021-06', 31000, 635500, -604500),
(84, '2021-09', 890300, 322500, 567800),
(84, '2021-09', 498000, 322500, 175500),
(84, '2021-09', 31000, 322500, -291500),
(85, '2021-06', 79000, 274500, -195500),
(85, '2021-06', 480500, 274500, 206000),
(85, '2021-06', 754800, 274500, 480300),
(85, '2021-08', 79000, 932000, -853000),
(85, '2021-08', 480500, 932000, -451500),
(85, '2021-08', 754800, 932000, -177200),
(85, '2021-09', 79000, 575650, -496650),
(85, '2021-09', 480500, 575650, -95150),
(85, '2021-09', 754800, 575650, 179150),
(85, '2021-10', 79000, 738400, -659400),
(85, '2021-10', 480500, 738400, -257900),
(85, '2021-10', 754800, 738400, 16400),
(86, '2021-09', 0, 66000, -66000),
(87, '2021-08', 90000, 765050, -675050),
(87, '2021-09', 90000, 60000, 30000),
(95, '2021-09', 51500, 257000, -205500),
(95, '2021-09', 694500, 257000, 437500),
(95, '2021-10', 51500, 128000, -76500),
(95, '2021-10', 694500, 128000, 566500),
(96, '2021-07', 1040500, 320700, 719800),
(96, '2021-07', 568923, 320700, 248223),
(96, '2021-07', 412506, 320700, 91806),
(96, '2021-09', 1040500, 468627, 571873),
(96, '2021-09', 568923, 468627, 100296),
(96, '2021-09', 412506, 468627, -56121),
(96, '2021-10', 1040500, 1172974, -132474),
(96, '2021-10', 568923, 1172974, -604051),
(96, '2021-10', 412506, 1172974, -760468),
(100, '2021-09', 0, 20000000, -20000000),
(103, '2021-09', 0, 90000, -90000),
(104, '2021-09', 17746754, 16845408, 901346),
(104, '2021-09', 6282151, 16845408, -10563257),
(104, '2021-10', 17746754, 33031235, -15284481),
(104, '2021-10', 6282151, 33031235, -26749084);

-- --------------------------------------------------------

--
-- Table structure for table `mv_neraca_lajur_x`
--

CREATE TABLE `mv_neraca_lajur_x` (
  `kode_coa_1` int(11) NOT NULL,
  `kode_coa_2` int(11) NOT NULL,
  `kode_coa_3` int(11) NOT NULL,
  `kode_coa_4` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `Tanggal` varchar(20) NOT NULL,
  `Nilai_debit` double NOT NULL,
  `Nilai_kredit` double NOT NULL,
  `saldo` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mv_neraca_lajur_x`
--

INSERT INTO `mv_neraca_lajur_x` (`kode_coa_1`, `kode_coa_2`, `kode_coa_3`, `kode_coa_4`, `id`, `Tanggal`, `Nilai_debit`, `Nilai_kredit`, `saldo`) VALUES
(1, 1, 1, 2, 2, '2021-03', 40000000, 8567600, 31432400),
(1, 1, 1, 2, 2, '2021-03', 40000000, 1040500, 38959500),
(1, 1, 1, 2, 2, '2021-03', 40000000, 349000, 39651000),
(1, 1, 1, 2, 2, '2021-03', 40000000, 1027500, 38972500),
(1, 1, 1, 2, 2, '2021-03', 40000000, 9830862, 30169138),
(1, 1, 1, 2, 2, '2021-03', 40000000, 1595000, 38405000),
(1, 1, 1, 2, 2, '2021-06', 3499000, 8567600, -5068600),
(1, 1, 1, 2, 2, '2021-06', 3499000, 1040500, 2458500),
(1, 1, 1, 2, 2, '2021-06', 3499000, 349000, 3150000),
(1, 1, 1, 2, 2, '2021-06', 3499000, 1027500, 2471500),
(1, 1, 1, 2, 2, '2021-06', 3499000, 9830862, -6331862),
(1, 1, 1, 2, 2, '2021-06', 3499000, 1595000, 1904000),
(1, 1, 1, 2, 2, '2021-07', 23169700, 8567600, 14602100),
(1, 1, 1, 2, 2, '2021-07', 23169700, 1040500, 22129200),
(1, 1, 1, 2, 2, '2021-07', 23169700, 349000, 22820700),
(1, 1, 1, 2, 2, '2021-07', 23169700, 1027500, 22142200),
(1, 1, 1, 2, 2, '2021-07', 23169700, 9830862, 13338838),
(1, 1, 1, 2, 2, '2021-07', 23169700, 1595000, 21574700),
(1, 1, 1, 2, 2, '2021-08', 16059000, 8567600, 7491400),
(1, 1, 1, 2, 2, '2021-08', 16059000, 1040500, 15018500),
(1, 1, 1, 2, 2, '2021-08', 16059000, 349000, 15710000),
(1, 1, 1, 2, 2, '2021-08', 16059000, 1027500, 15031500),
(1, 1, 1, 2, 2, '2021-08', 16059000, 9830862, 6228138),
(1, 1, 1, 2, 2, '2021-08', 16059000, 1595000, 14464000),
(1, 1, 1, 2, 2, '2021-09', 2289150, 8567600, -6278450),
(1, 1, 1, 2, 2, '2021-09', 2289150, 1040500, 1248650),
(1, 1, 1, 2, 2, '2021-09', 2289150, 349000, 1940150),
(1, 1, 1, 2, 2, '2021-09', 2289150, 1027500, 1261650),
(1, 1, 1, 2, 2, '2021-09', 2289150, 9830862, -7541712),
(1, 1, 1, 2, 2, '2021-09', 2289150, 1595000, 694150),
(1, 1, 1, 2, 2, '2021-10', 1748900, 8567600, -6818700),
(1, 1, 1, 2, 2, '2021-10', 1748900, 1040500, 708400),
(1, 1, 1, 2, 2, '2021-10', 1748900, 349000, 1399900),
(1, 1, 1, 2, 2, '2021-10', 1748900, 1027500, 721400),
(1, 1, 1, 2, 2, '2021-10', 1748900, 9830862, -8081962),
(1, 1, 1, 2, 2, '2021-10', 1748900, 1595000, 153900),
(1, 1, 1, 2, 2, '2022-01', 8567600, 8567600, 0),
(1, 1, 1, 2, 2, '2022-01', 8567600, 1040500, 7527100),
(1, 1, 1, 2, 2, '2022-01', 8567600, 349000, 8218600),
(1, 1, 1, 2, 2, '2022-01', 8567600, 1027500, 7540100),
(1, 1, 1, 2, 2, '2022-01', 8567600, 9830862, -1263262),
(1, 1, 1, 2, 2, '2022-01', 8567600, 1595000, 6972600),
(1, 1, 2, 1, 3, '2021-01', 100000000, 750000, 99250000),
(1, 1, 2, 1, 3, '2021-01', 100000000, 2037500, 97962500),
(1, 1, 2, 1, 3, '2021-01', 100000000, 53589803, 46410197),
(1, 1, 2, 1, 3, '2021-01', 100000000, 201835201, -101835201),
(1, 1, 2, 1, 3, '2021-01', 100000000, 162239584, -62239584),
(1, 1, 2, 1, 3, '2021-01', 100000000, 82709700, 17290300),
(1, 1, 2, 1, 3, '2021-01', 100000000, 178513048, -78513048),
(1, 1, 2, 1, 3, '2021-08', 36498041, 750000, 35748041),
(1, 1, 2, 1, 3, '2021-08', 36498041, 2037500, 34460541),
(1, 1, 2, 1, 3, '2021-08', 36498041, 53589803, -17091762),
(1, 1, 2, 1, 3, '2021-08', 36498041, 201835201, -165337160),
(1, 1, 2, 1, 3, '2021-08', 36498041, 162239584, -125741543),
(1, 1, 2, 1, 3, '2021-08', 36498041, 82709700, -46211659),
(1, 1, 2, 1, 3, '2021-08', 36498041, 178513048, -142015007),
(1, 1, 2, 1, 3, '2021-09', 22150000, 750000, 21400000),
(1, 1, 2, 1, 3, '2021-09', 22150000, 2037500, 20112500),
(1, 1, 2, 1, 3, '2021-09', 22150000, 53589803, -31439803),
(1, 1, 2, 1, 3, '2021-09', 22150000, 201835201, -179685201),
(1, 1, 2, 1, 3, '2021-09', 22150000, 162239584, -140089584),
(1, 1, 2, 1, 3, '2021-09', 22150000, 82709700, -60559700),
(1, 1, 2, 1, 3, '2021-09', 22150000, 178513048, -156363048),
(1, 1, 2, 2, 4, '2021-06', 67055096, 12565000, 54490096),
(1, 1, 2, 2, 4, '2021-06', 67055096, 32895000, 34160096),
(1, 1, 2, 2, 4, '2021-06', 67055096, 71354000, -4298904),
(1, 1, 2, 2, 4, '2021-06', 67055096, 45144800, 21910296),
(1, 1, 2, 2, 4, '2021-06', 67055096, 60000, 66995096),
(1, 1, 2, 2, 4, '2021-07', 118107000, 12565000, 105542000),
(1, 1, 2, 2, 4, '2021-07', 118107000, 32895000, 85212000),
(1, 1, 2, 2, 4, '2021-07', 118107000, 71354000, 46753000),
(1, 1, 2, 2, 4, '2021-07', 118107000, 45144800, 72962200),
(1, 1, 2, 2, 4, '2021-07', 118107000, 60000, 118047000),
(1, 1, 2, 2, 4, '2021-08', 88816000, 12565000, 76251000),
(1, 1, 2, 2, 4, '2021-08', 88816000, 32895000, 55921000),
(1, 1, 2, 2, 4, '2021-08', 88816000, 71354000, 17462000),
(1, 1, 2, 2, 4, '2021-08', 88816000, 45144800, 43671200),
(1, 1, 2, 2, 4, '2021-08', 88816000, 60000, 88756000),
(1, 1, 2, 2, 4, '2021-09', 2267627, 12565000, -10297373),
(1, 1, 2, 2, 4, '2021-09', 2267627, 32895000, -30627373),
(1, 1, 2, 2, 4, '2021-09', 2267627, 71354000, -69086373),
(1, 1, 2, 2, 4, '2021-09', 2267627, 45144800, -42877173),
(1, 1, 2, 2, 4, '2021-09', 2267627, 60000, 2207627),
(1, 1, 2, 2, 4, '2021-10', 4195474, 12565000, -8369526),
(1, 1, 2, 2, 4, '2021-10', 4195474, 32895000, -28699526),
(1, 1, 2, 2, 4, '2021-10', 4195474, 71354000, -67158526),
(1, 1, 2, 2, 4, '2021-10', 4195474, 45144800, -40949326),
(1, 1, 2, 2, 4, '2021-10', 4195474, 60000, 4135474),
(1, 1, 2, 2, 4, '2022-01', 12565000, 12565000, 0),
(1, 1, 2, 2, 4, '2022-01', 12565000, 32895000, -20330000),
(1, 1, 2, 2, 4, '2022-01', 12565000, 71354000, -58789000),
(1, 1, 2, 2, 4, '2022-01', 12565000, 45144800, -32579800),
(1, 1, 2, 2, 4, '2022-01', 12565000, 60000, 12505000),
(1, 1, 2, 3, 5, '2021-01', 900000000, 17746754, 882253246),
(1, 1, 2, 3, 5, '2021-01', 900000000, 39861076, 860138924),
(1, 1, 2, 3, 5, '2021-01', 900000000, 5830298, 894169702),
(1, 1, 2, 3, 5, '2021-01', 900000000, 35192858, 864807142),
(1, 1, 2, 3, 5, '2021-01', 900000000, 23718423, 876281577),
(1, 1, 2, 3, 5, '2021-08', 11645282, 17746754, -6101472),
(1, 1, 2, 3, 5, '2021-08', 11645282, 39861076, -28215794),
(1, 1, 2, 3, 5, '2021-08', 11645282, 5830298, 5814984),
(1, 1, 2, 3, 5, '2021-08', 11645282, 35192858, -23547576),
(1, 1, 2, 3, 5, '2021-08', 11645282, 23718423, -12073141),
(1, 1, 2, 3, 5, '2021-09', 16845408, 17746754, -901346),
(1, 1, 2, 3, 5, '2021-09', 16845408, 39861076, -23015668),
(1, 1, 2, 3, 5, '2021-09', 16845408, 5830298, 11015110),
(1, 1, 2, 3, 5, '2021-09', 16845408, 35192858, -18347450),
(1, 1, 2, 3, 5, '2021-09', 16845408, 23718423, -6873015),
(1, 1, 2, 3, 5, '2021-10', 33031235, 17746754, 15284481),
(1, 1, 2, 3, 5, '2021-10', 33031235, 39861076, -6829841),
(1, 1, 2, 3, 5, '2021-10', 33031235, 5830298, 27200937),
(1, 1, 2, 3, 5, '2021-10', 33031235, 35192858, -2161623),
(1, 1, 2, 3, 5, '2021-10', 33031235, 23718423, 9312812),
(1, 1, 5, 7, 11, '2021-01', 53794103, 0, 53794103),
(1, 1, 5, 7, 11, '2021-02', 4453000, 0, 4453000),
(1, 1, 3, 1, 12, '2021-04', 133235500, 34384500, 98851000),
(1, 1, 3, 1, 12, '2021-04', 133235500, 21298096, 111937404),
(1, 1, 3, 1, 12, '2021-05', 70709500, 34384500, 36325000),
(1, 1, 3, 1, 12, '2021-05', 70709500, 21298096, 49411404),
(1, 1, 3, 1, 12, '2021-06', 34554000, 34384500, 169500),
(1, 1, 3, 1, 12, '2021-06', 34554000, 21298096, 13255904),
(1, 5, 4, 1, 20, '2021-01', 1200000, 0, 1200000),
(1, 5, 5, 1, 21, '2021-03', 150000, 0, 150000),
(1, 5, 5, 1, 21, '2021-04', 5999000, 0, 5999000),
(5, 2, 1, 1, 46, '2021-03', 18876259, 0, 18876259),
(5, 3, 1, 1, 47, '2021-01', 16434000, 12933000, 3501000),
(5, 3, 1, 1, 47, '2021-01', 16434000, 38599000, -22165000),
(5, 3, 1, 1, 47, '2021-01', 16434000, 10300000, 6134000),
(5, 3, 1, 1, 47, '2021-02', 12800000, 12933000, -133000),
(5, 3, 1, 1, 47, '2021-02', 12800000, 38599000, -25799000),
(5, 3, 1, 1, 47, '2021-02', 12800000, 10300000, 2500000),
(5, 3, 1, 1, 47, '2021-03', 21045000, 12933000, 8112000),
(5, 3, 1, 1, 47, '2021-03', 21045000, 38599000, -17554000),
(5, 3, 1, 1, 47, '2021-03', 21045000, 10300000, 10745000),
(5, 3, 1, 1, 47, '2021-04', 4626000, 12933000, -8307000),
(5, 3, 1, 1, 47, '2021-04', 4626000, 38599000, -33973000),
(5, 3, 1, 1, 47, '2021-04', 4626000, 10300000, -5674000),
(5, 4, 1, 1, 48, '2021-01', 3800000, 6400000, -2600000),
(5, 4, 1, 1, 48, '2021-01', 3800000, 6850000, -3050000),
(6, 1, 1, 1, 50, '2021-03', 41987740, 37822173, 4165567),
(6, 1, 1, 1, 50, '2021-04', 41266850, 37822173, 3444677),
(6, 1, 1, 1, 50, '2021-05', 39861076, 37822173, 2038903),
(6, 1, 1, 1, 50, '2021-06', 250000, 37822173, -37572173),
(6, 1, 2, 1, 53, '2021-01', 1946373, 1155000, 791373),
(6, 1, 2, 1, 53, '2021-01', 1946373, 1807000, 139373),
(6, 1, 2, 1, 53, '2021-03', 9325120, 1155000, 8170120),
(6, 1, 2, 1, 53, '2021-03', 9325120, 1807000, 7518120),
(6, 1, 2, 1, 53, '2021-04', 2944575, 1155000, 1789575),
(6, 1, 2, 1, 53, '2021-04', 2944575, 1807000, 1137575),
(6, 1, 2, 1, 53, '2021-05', 270000, 1155000, -885000),
(6, 1, 2, 1, 53, '2021-05', 270000, 1807000, -1537000),
(6, 1, 5, 2, 57, '2021-02', 100000, 1899600, -1799600),
(6, 1, 5, 2, 57, '2021-03', 240000, 1899600, -1659600),
(6, 2, 1, 4, 62, '2021-04', 60000, 0, 60000),
(6, 3, 1, 1, 64, '2021-03', 1200000, 1650000, -450000),
(6, 3, 1, 1, 64, '2021-04', 5250000, 1650000, 3600000),
(6, 3, 1, 2, 65, '2021-03', 613000, 0, 613000),
(6, 3, 1, 2, 65, '2021-04', 350000, 0, 350000),
(6, 7, 1, 1, 82, '2021-03', 1000000, 0, 1000000),
(6, 7, 1, 2, 83, '2021-04', 2600000, 1000000, 1600000),
(6, 8, 1, 1, 84, '2021-03', 31000, 322500, -291500),
(6, 8, 1, 1, 84, '2021-03', 31000, 635500, -604500),
(6, 8, 1, 1, 84, '2021-04', 498000, 322500, 175500),
(6, 8, 1, 1, 84, '2021-04', 498000, 635500, -137500),
(6, 8, 1, 1, 84, '2021-05', 890300, 322500, 567800),
(6, 8, 1, 1, 84, '2021-05', 890300, 635500, 254800),
(6, 8, 1, 2, 85, '2021-03', 754800, 738400, 16400),
(6, 8, 1, 2, 85, '2021-03', 754800, 575650, 179150),
(6, 8, 1, 2, 85, '2021-03', 754800, 932000, -177200),
(6, 8, 1, 2, 85, '2021-03', 754800, 274500, 480300),
(6, 8, 1, 2, 85, '2021-04', 480500, 738400, -257900),
(6, 8, 1, 2, 85, '2021-04', 480500, 575650, -95150),
(6, 8, 1, 2, 85, '2021-04', 480500, 932000, -451500),
(6, 8, 1, 2, 85, '2021-04', 480500, 274500, 206000),
(6, 8, 1, 2, 85, '2021-05', 79000, 738400, -659400),
(6, 8, 1, 2, 85, '2021-05', 79000, 575650, -496650),
(6, 8, 1, 2, 85, '2021-05', 79000, 932000, -853000),
(6, 8, 1, 2, 85, '2021-05', 79000, 274500, -195500),
(6, 8, 1, 4, 87, '2021-04', 90000, 60000, 30000),
(6, 8, 1, 4, 87, '2021-04', 90000, 765050, -675050),
(6, 8, 1, 5, 88, '2021-03', 4498379, 0, 4498379),
(6, 8, 1, 6, 95, '2021-03', 694500, 128000, 566500),
(6, 8, 1, 6, 95, '2021-03', 694500, 257000, 437500),
(6, 8, 1, 6, 95, '2021-04', 51500, 128000, -76500),
(6, 8, 1, 6, 95, '2021-04', 51500, 257000, -205500),
(6, 8, 1, 7, 96, '2021-03', 412506, 1172974, -760468),
(6, 8, 1, 7, 96, '2021-03', 412506, 468627, -56121),
(6, 8, 1, 7, 96, '2021-03', 412506, 320700, 91806),
(6, 8, 1, 7, 96, '2021-04', 568923, 1172974, -604051),
(6, 8, 1, 7, 96, '2021-04', 568923, 468627, 100296),
(6, 8, 1, 7, 96, '2021-04', 568923, 320700, 248223),
(6, 8, 1, 7, 96, '2021-06', 1040500, 1172974, -132474),
(6, 8, 1, 7, 96, '2021-06', 1040500, 468627, 571873),
(6, 8, 1, 7, 96, '2021-06', 1040500, 320700, 719800),
(6, 8, 1, 8, 97, '2021-02', 1116000, 0, 1116000),
(6, 8, 1, 8, 97, '2021-03', 345000, 0, 345000),
(5, 1, 2, 4, 98, '2021-01', 126651995, 0, 126651995),
(5, 1, 2, 4, 98, '2021-02', 64240700, 0, 64240700),
(5, 1, 2, 4, 98, '2021-03', 66090000, 0, 66090000),
(5, 1, 2, 4, 98, '2021-04', 4450000, 0, 4450000),
(5, 1, 2, 4, 98, '2021-08', 2037500, 0, 2037500),
(5, 1, 2, 4, 98, '2021-09', 750000, 0, 750000),
(6, 1, 1, 2, 104, '2021-04', 6282151, 33031235, -26749084),
(6, 1, 1, 2, 104, '2021-04', 6282151, 16845408, -10563257),
(6, 1, 1, 2, 104, '2021-06', 17746754, 33031235, -15284481),
(6, 1, 1, 2, 104, '2021-06', 17746754, 16845408, 901346),
(6, 1, 1, 3, 105, '2021-05', 27134803, 0, 27134803),
(0, 0, 0, 0, 0, '2021-07', 0, 14340000, -14340000),
(0, 0, 0, 0, 0, '2021-10', 0, 1750000, -1750000),
(1, 1, 1, 2, 2, '2021-01', 8567600, 1595000, 6972600),
(1, 1, 1, 2, 2, '2021-01', 1748900, 1595000, 153900),
(1, 1, 1, 2, 2, '2021-01', 2289150, 1595000, 694150),
(1, 1, 1, 2, 2, '2021-01', 16059000, 1595000, 14464000),
(1, 1, 1, 2, 2, '2021-01', 23169700, 1595000, 21574700),
(1, 1, 1, 2, 2, '2021-01', 3499000, 1595000, 1904000),
(1, 1, 1, 2, 2, '2021-01', 40000000, 1595000, 38405000),
(1, 1, 1, 2, 2, '2021-03', 8567600, 9830862, -1263262),
(1, 1, 1, 2, 2, '2021-03', 1748900, 9830862, -8081962),
(1, 1, 1, 2, 2, '2021-03', 2289150, 9830862, -7541712),
(1, 1, 1, 2, 2, '2021-03', 16059000, 9830862, 6228138),
(1, 1, 1, 2, 2, '2021-03', 23169700, 9830862, 13338838),
(1, 1, 1, 2, 2, '2021-03', 3499000, 9830862, -6331862),
(1, 1, 1, 2, 2, '2021-04', 8567600, 1027500, 7540100),
(1, 1, 1, 2, 2, '2021-04', 1748900, 1027500, 721400),
(1, 1, 1, 2, 2, '2021-04', 2289150, 1027500, 1261650),
(1, 1, 1, 2, 2, '2021-04', 16059000, 1027500, 15031500),
(1, 1, 1, 2, 2, '2021-04', 23169700, 1027500, 22142200),
(1, 1, 1, 2, 2, '2021-04', 3499000, 1027500, 2471500),
(1, 1, 1, 2, 2, '2021-04', 40000000, 1027500, 38972500),
(1, 1, 1, 2, 2, '2021-05', 8567600, 349000, 8218600),
(1, 1, 1, 2, 2, '2021-05', 1748900, 349000, 1399900),
(1, 1, 1, 2, 2, '2021-05', 2289150, 349000, 1940150),
(1, 1, 1, 2, 2, '2021-05', 16059000, 349000, 15710000),
(1, 1, 1, 2, 2, '2021-05', 23169700, 349000, 22820700),
(1, 1, 1, 2, 2, '2021-05', 3499000, 349000, 3150000),
(1, 1, 1, 2, 2, '2021-05', 40000000, 349000, 39651000),
(1, 1, 1, 2, 2, '2021-06', 8567600, 1040500, 7527100),
(1, 1, 1, 2, 2, '2021-06', 1748900, 1040500, 708400),
(1, 1, 1, 2, 2, '2021-06', 2289150, 1040500, 1248650),
(1, 1, 1, 2, 2, '2021-06', 16059000, 1040500, 15018500),
(1, 1, 1, 2, 2, '2021-06', 23169700, 1040500, 22129200),
(1, 1, 1, 2, 2, '2021-06', 40000000, 1040500, 38959500),
(1, 1, 1, 2, 2, '2022-01', 1748900, 8567600, -6818700),
(1, 1, 1, 2, 2, '2022-01', 2289150, 8567600, -6278450),
(1, 1, 1, 2, 2, '2022-01', 16059000, 8567600, 7491400),
(1, 1, 1, 2, 2, '2022-01', 23169700, 8567600, 14602100),
(1, 1, 1, 2, 2, '2022-01', 3499000, 8567600, -5068600),
(1, 1, 1, 2, 2, '2022-01', 40000000, 8567600, 31432400),
(1, 1, 2, 1, 3, '2021-01', 22150000, 178513048, -156363048),
(1, 1, 2, 1, 3, '2021-01', 36498041, 178513048, -142015007),
(1, 1, 2, 1, 3, '2021-02', 22150000, 82709700, -60559700),
(1, 1, 2, 1, 3, '2021-02', 36498041, 82709700, -46211659),
(1, 1, 2, 1, 3, '2021-02', 100000000, 82709700, 17290300),
(1, 1, 2, 1, 3, '2021-03', 22150000, 162239584, -140089584),
(1, 1, 2, 1, 3, '2021-03', 36498041, 162239584, -125741543),
(1, 1, 2, 1, 3, '2021-03', 100000000, 162239584, -62239584),
(1, 1, 2, 1, 3, '2021-04', 22150000, 201835201, -179685201),
(1, 1, 2, 1, 3, '2021-04', 36498041, 201835201, -165337160),
(1, 1, 2, 1, 3, '2021-04', 100000000, 201835201, -101835201),
(1, 1, 2, 1, 3, '2021-05', 22150000, 53589803, -31439803),
(1, 1, 2, 1, 3, '2021-05', 36498041, 53589803, -17091762),
(1, 1, 2, 1, 3, '2021-05', 100000000, 53589803, 46410197),
(1, 1, 2, 1, 3, '2021-08', 22150000, 2037500, 20112500),
(1, 1, 2, 1, 3, '2021-08', 100000000, 2037500, 97962500),
(1, 1, 2, 1, 3, '2021-09', 36498041, 750000, 35748041),
(1, 1, 2, 1, 3, '2021-09', 100000000, 750000, 99250000),
(1, 1, 2, 2, 4, '2021-04', 12565000, 60000, 12505000),
(1, 1, 2, 2, 4, '2021-04', 4195474, 60000, 4135474),
(1, 1, 2, 2, 4, '2021-04', 2267627, 60000, 2207627),
(1, 1, 2, 2, 4, '2021-04', 88816000, 60000, 88756000),
(1, 1, 2, 2, 4, '2021-04', 118107000, 60000, 118047000),
(1, 1, 2, 2, 4, '2021-04', 67055096, 60000, 66995096),
(1, 1, 2, 2, 4, '2021-05', 12565000, 45144800, -32579800),
(1, 1, 2, 2, 4, '2021-05', 4195474, 45144800, -40949326),
(1, 1, 2, 2, 4, '2021-05', 2267627, 45144800, -42877173),
(1, 1, 2, 2, 4, '2021-05', 88816000, 45144800, 43671200),
(1, 1, 2, 2, 4, '2021-05', 118107000, 45144800, 72962200),
(1, 1, 2, 2, 4, '2021-05', 67055096, 45144800, 21910296),
(1, 1, 2, 2, 4, '2021-06', 12565000, 71354000, -58789000),
(1, 1, 2, 2, 4, '2021-06', 4195474, 71354000, -67158526),
(1, 1, 2, 2, 4, '2021-06', 2267627, 71354000, -69086373),
(1, 1, 2, 2, 4, '2021-06', 88816000, 71354000, 17462000),
(1, 1, 2, 2, 4, '2021-06', 118107000, 71354000, 46753000),
(1, 1, 2, 2, 4, '2021-07', 12565000, 32895000, -20330000),
(1, 1, 2, 2, 4, '2021-07', 4195474, 32895000, -28699526),
(1, 1, 2, 2, 4, '2021-07', 2267627, 32895000, -30627373),
(1, 1, 2, 2, 4, '2021-07', 88816000, 32895000, 55921000),
(1, 1, 2, 2, 4, '2021-07', 67055096, 32895000, 34160096),
(1, 1, 2, 2, 4, '2022-01', 4195474, 12565000, -8369526),
(1, 1, 2, 2, 4, '2022-01', 2267627, 12565000, -10297373),
(1, 1, 2, 2, 4, '2022-01', 88816000, 12565000, 76251000),
(1, 1, 2, 2, 4, '2022-01', 118107000, 12565000, 105542000),
(1, 1, 2, 2, 4, '2022-01', 67055096, 12565000, 54490096),
(1, 1, 2, 3, 5, '2021-01', 33031235, 23718423, 9312812),
(1, 1, 2, 3, 5, '2021-01', 16845408, 23718423, -6873015),
(1, 1, 2, 3, 5, '2021-01', 11645282, 23718423, -12073141),
(1, 1, 2, 3, 5, '2021-03', 33031235, 35192858, -2161623),
(1, 1, 2, 3, 5, '2021-03', 16845408, 35192858, -18347450),
(1, 1, 2, 3, 5, '2021-03', 11645282, 35192858, -23547576),
(1, 1, 2, 3, 5, '2021-03', 900000000, 35192858, 864807142),
(1, 1, 2, 3, 5, '2021-04', 33031235, 5830298, 27200937),
(1, 1, 2, 3, 5, '2021-04', 16845408, 5830298, 11015110),
(1, 1, 2, 3, 5, '2021-04', 11645282, 5830298, 5814984),
(1, 1, 2, 3, 5, '2021-04', 900000000, 5830298, 894169702),
(1, 1, 2, 3, 5, '2021-05', 33031235, 39861076, -6829841),
(1, 1, 2, 3, 5, '2021-05', 16845408, 39861076, -23015668),
(1, 1, 2, 3, 5, '2021-05', 11645282, 39861076, -28215794),
(1, 1, 2, 3, 5, '2021-05', 900000000, 39861076, 860138924),
(1, 1, 2, 3, 5, '2021-06', 33031235, 17746754, 15284481),
(1, 1, 2, 3, 5, '2021-06', 16845408, 17746754, -901346),
(1, 1, 2, 3, 5, '2021-06', 11645282, 17746754, -6101472),
(1, 1, 2, 3, 5, '2021-06', 900000000, 17746754, 882253246),
(1, 1, 3, 1, 12, '2021-06', 70709500, 21298096, 49411404),
(1, 1, 3, 1, 12, '2021-06', 133235500, 21298096, 111937404),
(1, 1, 3, 1, 12, '2021-07', 34554000, 34384500, 169500),
(1, 1, 3, 1, 12, '2021-07', 70709500, 34384500, 36325000),
(1, 1, 3, 1, 12, '2021-07', 133235500, 34384500, 98851000),
(3, 1, 1, 1, 34, '2021-01', 0, 1000000000, -1000000000),
(5, 1, 2, 2, 44, '2021-06', 0, 1496000, -1496000),
(5, 1, 2, 2, 44, '2021-07', 0, 13887500, -13887500),
(5, 1, 2, 2, 44, '2021-08', 0, 60206000, -60206000),
(5, 3, 1, 1, 47, '2021-06', 4626000, 10300000, -5674000),
(5, 3, 1, 1, 47, '2021-06', 21045000, 10300000, 10745000),
(5, 3, 1, 1, 47, '2021-06', 12800000, 10300000, 2500000),
(5, 3, 1, 1, 47, '2021-06', 16434000, 10300000, 6134000),
(5, 3, 1, 1, 47, '2021-07', 4626000, 38599000, -33973000),
(5, 3, 1, 1, 47, '2021-07', 21045000, 38599000, -17554000),
(5, 3, 1, 1, 47, '2021-07', 12800000, 38599000, -25799000),
(5, 3, 1, 1, 47, '2021-07', 16434000, 38599000, -22165000),
(5, 3, 1, 1, 47, '2021-08', 4626000, 12933000, -8307000),
(5, 3, 1, 1, 47, '2021-08', 21045000, 12933000, 8112000),
(5, 3, 1, 1, 47, '2021-08', 12800000, 12933000, -133000),
(5, 3, 1, 1, 47, '2021-08', 16434000, 12933000, 3501000),
(5, 4, 1, 1, 48, '2021-07', 3800000, 6850000, -3050000),
(5, 4, 1, 1, 48, '2021-08', 3800000, 6400000, -2600000),
(0, 0, 0, 0, 49, '2021-08', 0, 25076000, -25076000),
(6, 1, 1, 1, 50, '2021-08', 250000, 37822173, -37572173),
(6, 1, 1, 1, 50, '2021-08', 39861076, 37822173, 2038903),
(6, 1, 1, 1, 50, '2021-08', 41266850, 37822173, 3444677),
(6, 1, 1, 1, 50, '2021-08', 41987740, 37822173, 4165567),
(0, 0, 0, 0, 51, '2021-09', 0, 250000, -250000),
(6, 1, 2, 1, 53, '2021-09', 270000, 1807000, -1537000),
(6, 1, 2, 1, 53, '2021-09', 2944575, 1807000, 1137575),
(6, 1, 2, 1, 53, '2021-09', 9325120, 1807000, 7518120),
(6, 1, 2, 1, 53, '2021-09', 1946373, 1807000, 139373),
(6, 1, 2, 1, 53, '2021-10', 270000, 1155000, -885000),
(6, 1, 2, 1, 53, '2021-10', 2944575, 1155000, 1789575),
(6, 1, 2, 1, 53, '2021-10', 9325120, 1155000, 8170120),
(6, 1, 2, 1, 53, '2021-10', 1946373, 1155000, 791373),
(6, 1, 5, 1, 56, '2021-09', 0, 160000, -160000),
(6, 1, 5, 2, 57, '2021-08', 240000, 1899600, -1659600),
(6, 1, 5, 2, 57, '2021-08', 100000, 1899600, -1799600),
(6, 2, 2, 1, 63, '2021-08', 0, 6984500, -6984500),
(6, 3, 1, 1, 64, '2021-09', 5250000, 1650000, 3600000),
(6, 3, 1, 1, 64, '2021-09', 1200000, 1650000, -450000),
(6, 7, 1, 2, 83, '2021-09', 2600000, 1000000, 1600000),
(6, 7, 1, 2, 83, '2021-10', 2600000, 1000000, 1600000),
(6, 8, 1, 1, 84, '2021-06', 890300, 635500, 254800),
(6, 8, 1, 1, 84, '2021-06', 498000, 635500, -137500),
(6, 8, 1, 1, 84, '2021-06', 31000, 635500, -604500),
(6, 8, 1, 1, 84, '2021-09', 890300, 322500, 567800),
(6, 8, 1, 1, 84, '2021-09', 498000, 322500, 175500),
(6, 8, 1, 1, 84, '2021-09', 31000, 322500, -291500),
(6, 8, 1, 2, 85, '2021-06', 79000, 274500, -195500),
(6, 8, 1, 2, 85, '2021-06', 480500, 274500, 206000),
(6, 8, 1, 2, 85, '2021-06', 754800, 274500, 480300),
(6, 8, 1, 2, 85, '2021-08', 79000, 932000, -853000),
(6, 8, 1, 2, 85, '2021-08', 480500, 932000, -451500),
(6, 8, 1, 2, 85, '2021-08', 754800, 932000, -177200),
(6, 8, 1, 2, 85, '2021-09', 79000, 575650, -496650),
(6, 8, 1, 2, 85, '2021-09', 480500, 575650, -95150),
(6, 8, 1, 2, 85, '2021-09', 754800, 575650, 179150),
(6, 8, 1, 2, 85, '2021-10', 79000, 738400, -659400),
(6, 8, 1, 2, 85, '2021-10', 480500, 738400, -257900),
(6, 8, 1, 2, 85, '2021-10', 754800, 738400, 16400),
(6, 8, 1, 3, 86, '2021-09', 0, 66000, -66000),
(6, 8, 1, 4, 87, '2021-08', 90000, 765050, -675050),
(6, 8, 1, 4, 87, '2021-09', 90000, 60000, 30000),
(6, 8, 1, 6, 95, '2021-09', 51500, 257000, -205500),
(6, 8, 1, 6, 95, '2021-09', 694500, 257000, 437500),
(6, 8, 1, 6, 95, '2021-10', 51500, 128000, -76500),
(6, 8, 1, 6, 95, '2021-10', 694500, 128000, 566500),
(6, 8, 1, 7, 96, '2021-07', 1040500, 320700, 719800),
(6, 8, 1, 7, 96, '2021-07', 568923, 320700, 248223),
(6, 8, 1, 7, 96, '2021-07', 412506, 320700, 91806),
(6, 8, 1, 7, 96, '2021-09', 1040500, 468627, 571873),
(6, 8, 1, 7, 96, '2021-09', 568923, 468627, 100296),
(6, 8, 1, 7, 96, '2021-09', 412506, 468627, -56121),
(6, 8, 1, 7, 96, '2021-10', 1040500, 1172974, -132474),
(6, 8, 1, 7, 96, '2021-10', 568923, 1172974, -604051),
(6, 8, 1, 7, 96, '2021-10', 412506, 1172974, -760468),
(3, 1, 1, 0, 100, '2021-09', 0, 20000000, -20000000),
(1, 5, 4, 2, 103, '2021-09', 0, 90000, -90000),
(6, 1, 1, 2, 104, '2021-09', 17746754, 16845408, 901346),
(6, 1, 1, 2, 104, '2021-09', 6282151, 16845408, -10563257),
(6, 1, 1, 2, 104, '2021-10', 17746754, 33031235, -15284481),
(6, 1, 1, 2, 104, '2021-10', 6282151, 33031235, -26749084);

-- --------------------------------------------------------

--
-- Table structure for table `mv_neraca_lajur_y`
--

CREATE TABLE `mv_neraca_lajur_y` (
  `kode_coa_1` int(11) NOT NULL,
  `kode_coa_2` int(11) NOT NULL,
  `kode_coa_3` int(11) NOT NULL,
  `kode_coa_4` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `Tanggal` varchar(20) NOT NULL,
  `Nilai_debit` double NOT NULL,
  `Nilai_kredit` double NOT NULL,
  `saldo` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mv_neraca_lajur_y`
--

INSERT INTO `mv_neraca_lajur_y` (`kode_coa_1`, `kode_coa_2`, `kode_coa_3`, `kode_coa_4`, `id`, `Tanggal`, `Nilai_debit`, `Nilai_kredit`, `saldo`) VALUES
(1, 1, 1, 2, 2, '2021', 86765750, 8567600, 78198150),
(1, 1, 1, 2, 2, '2021', 86765750, 13842862, 72922888),
(1, 1, 1, 2, 2, '2022', 8567600, 8567600, 0),
(1, 1, 1, 2, 2, '2022', 8567600, 13842862, -5275262),
(1, 1, 2, 1, 3, '2021', 158648041, 681674836, -523026795),
(1, 1, 2, 2, 4, '2021', 280441197, 12565000, 267876197),
(1, 1, 2, 2, 4, '2021', 280441197, 149453800, 130987397),
(1, 1, 2, 2, 4, '2022', 12565000, 12565000, 0),
(1, 1, 2, 2, 4, '2022', 12565000, 149453800, -136888800),
(1, 1, 2, 3, 5, '2021', 961521925, 122349409, 839172516),
(1, 1, 5, 7, 11, '2021', 58247103, 0, 58247103),
(1, 1, 3, 1, 12, '2021', 238499000, 55682596, 182816404),
(1, 5, 4, 1, 20, '2021', 1200000, 0, 1200000),
(1, 5, 5, 1, 21, '2021', 6149000, 0, 6149000),
(5, 2, 1, 1, 46, '2021', 18876259, 0, 18876259),
(5, 3, 1, 1, 47, '2021', 54905000, 61832000, -6927000),
(5, 4, 1, 1, 48, '2021', 3800000, 13250000, -9450000),
(6, 1, 1, 1, 50, '2021', 123365666, 37822173, 85543493),
(6, 1, 2, 1, 53, '2021', 14486068, 2962000, 11524068),
(6, 1, 5, 2, 57, '2021', 340000, 1899600, -1559600),
(6, 2, 1, 4, 62, '2021', 60000, 0, 60000),
(6, 3, 1, 1, 64, '2021', 6450000, 1650000, 4800000),
(6, 3, 1, 2, 65, '2021', 963000, 0, 963000),
(6, 7, 1, 1, 82, '2021', 1000000, 0, 1000000),
(6, 7, 1, 2, 83, '2021', 2600000, 2000000, 600000),
(6, 8, 1, 1, 84, '2021', 1419300, 958000, 461300),
(6, 8, 1, 2, 85, '2021', 1314300, 2520550, -1206250),
(6, 8, 1, 4, 87, '2021', 90000, 825050, -735050),
(6, 8, 1, 5, 88, '2021', 4498379, 0, 4498379),
(6, 8, 1, 6, 95, '2021', 746000, 385000, 361000),
(6, 8, 1, 7, 96, '2021', 2021929, 1962301, 59628),
(6, 8, 1, 8, 97, '2021', 1461000, 0, 1461000),
(5, 1, 2, 4, 98, '2021', 264220195, 0, 264220195),
(6, 1, 1, 2, 104, '2021', 24028905, 49876643, -25847738),
(6, 1, 1, 3, 105, '2021', 27134803, 0, 27134803),
(0, 0, 0, 0, 0, '2021', 0, 16090000, -16090000),
(1, 1, 1, 2, 2, '2021', 8567600, 13842862, -5275262),
(1, 1, 1, 2, 2, '2022', 86765750, 8567600, 78198150),
(1, 1, 2, 2, 4, '2021', 12565000, 149453800, -136888800),
(1, 1, 2, 2, 4, '2022', 280441197, 12565000, 267876197),
(3, 1, 1, 1, 34, '2021', 0, 1000000000, -1000000000),
(5, 1, 2, 2, 44, '2021', 0, 75589500, -75589500),
(0, 0, 0, 0, 49, '2021', 0, 25076000, -25076000),
(0, 0, 0, 0, 51, '2021', 0, 250000, -250000),
(6, 1, 5, 1, 56, '2021', 0, 160000, -160000),
(6, 2, 2, 1, 63, '2021', 0, 6984500, -6984500),
(6, 8, 1, 3, 86, '2021', 0, 66000, -66000),
(3, 1, 1, 0, 100, '2021', 0, 20000000, -20000000),
(1, 5, 4, 2, 103, '2021', 0, 90000, -90000);

-- --------------------------------------------------------

--
-- Table structure for table `m_coa`
--

CREATE TABLE `m_coa` (
  `m_coa_id` int(11) NOT NULL,
  `nama_coa` varchar(100) NOT NULL,
  `kode_coa` int(11) NOT NULL,
  `status` varchar(20) NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_coa`
--

INSERT INTO `m_coa` (`m_coa_id`, `nama_coa`, `kode_coa`, `status`) VALUES
(1, 'AKTIVA', 1, 'active'),
(2, 'HUTANG', 2, 'active'),
(3, 'MODAL', 3, 'active'),
(4, 'PENDAPATAN', 4, 'active'),
(5, 'HARGA POKOK PENJUALAN', 5, 'active'),
(6, 'BIAYA ', 6, 'active'),
(7, 'PENDAPATAN DAN BIAYA DILUAR USAHA', 7, 'active');

-- --------------------------------------------------------

--
-- Table structure for table `m_coa_2`
--

CREATE TABLE `m_coa_2` (
  `m_coa_2_id` int(11) NOT NULL,
  `kode_coa_2` int(11) NOT NULL,
  `m_coa_id` int(11) NOT NULL,
  `nama_coa` varchar(100) NOT NULL,
  `status` varchar(20) NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_coa_2`
--

INSERT INTO `m_coa_2` (`m_coa_2_id`, `kode_coa_2`, `m_coa_id`, `nama_coa`, `status`) VALUES
(1, 1, 1, 'Aktiva Lancar', 'active'),
(2, 2, 1, 'Aktiva Tidak Lancar', 'active'),
(3, 3, 1, 'Akumulasi Penyusutan', 'active'),
(4, 1, 2, 'Hutang Jangka Pendek', 'active'),
(5, 2, 2, 'Hutang Jangka Panjang', 'active'),
(6, 1, 3, 'Saham', 'active'),
(7, 2, 3, 'Laba Ditahan', 'active'),
(8, 3, 3, 'Laba Berjalan', 'active'),
(9, 1, 4, 'Penjualan ', 'active'),
(10, 2, 4, 'Pendapatan Jasa', 'active'),
(11, 3, 4, 'Pendapatan Sewa', 'active'),
(12, 1, 5, 'Pembelian', 'active'),
(13, 2, 5, 'Premi', 'active'),
(14, 3, 5, 'Tenaga Kerja Langsung', 'active'),
(15, 4, 5, 'Tenaga Kerja Langsung', 'active'),
(16, 5, 5, 'Tenaga Kerja Langsung', 'active'),
(17, 1, 6, 'Biaya Pegawai', 'active'),
(18, 2, 6, 'Biaya Perawatan', 'active'),
(19, 3, 6, 'Biaya Marketing', 'active'),
(20, 4, 6, 'Biaya Penyusutan', 'active'),
(21, 5, 6, 'Biaya Bank, Pajak & Perijinan', 'active'),
(22, 6, 6, 'Biaya SDM', 'active'),
(23, 7, 6, 'Biaya CSR', 'active'),
(24, 8, 6, 'Biaya Administrasi & Umum', 'active'),
(25, 1, 7, 'Pendapatan Diluar Usaha', 'active'),
(26, 2, 7, 'Biaya Diluar Usaha', 'active'),
(27, 5, 1, 'Aset Tidak Lancar', 'active'),
(28, 9, 6, 'Biaya Lain-lainnya', 'active'),
(29, 9, 4, 'Pendapatan Lain-lainnya', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `m_coa_3`
--

CREATE TABLE `m_coa_3` (
  `m_coa_3_id` int(11) NOT NULL,
  `id_coa_1` int(11) NOT NULL,
  `m_coa_2_id` int(11) NOT NULL,
  `kode_coa_3` int(11) NOT NULL,
  `nama_coa` varchar(100) NOT NULL,
  `status` varchar(20) NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_coa_3`
--

INSERT INTO `m_coa_3` (`m_coa_3_id`, `id_coa_1`, `m_coa_2_id`, `kode_coa_3`, `nama_coa`, `status`) VALUES
(1, 1, 1, 1, 'KAS', 'active'),
(2, 1, 1, 2, 'BANK', 'active'),
(4, 1, 1, 5, 'Piutang', 'active'),
(5, 1, 1, 3, 'Persedian', 'active'),
(6, 1, 1, 4, 'Biaya Dibayar Dimuka', 'active'),
(7, 1, 2, 1, 'Tanah', 'active'),
(8, 1, 2, 2, 'Bangunan', 'active'),
(9, 1, 2, 3, 'Kendaraan', 'active'),
(10, 1, 2, 4, 'Inventaris', 'active'),
(11, 1, 2, 5, 'Inventaris', 'active'),
(12, 1, 3, 1, 'Akumulasi Penyusutan Bangunan', 'active'),
(13, 1, 3, 2, 'Akumulasi Penyusutan Kendaraan', 'active'),
(14, 1, 3, 3, 'Akumulasi Penyusutan Mesin', 'active'),
(15, 1, 3, 4, 'Akumulasi Penyusutan Inventaris', 'active'),
(16, 2, 4, 1, 'Hutang Dagang', 'active'),
(17, 2, 4, 2, 'Hutang Dagang', 'active'),
(18, 2, 4, 3, 'Hutang Pihak Ketiga Jangka Pendek', 'active'),
(19, 2, 4, 4, 'Hutang Bank Jangka Pendek', 'active'),
(20, 2, 5, 1, 'Hutang Bank Jangka Panjang', 'active'),
(21, 2, 5, 2, 'Hutang Pihak Ketiga Jangka Panjang', 'active'),
(22, 3, 6, 1, 'Saham', 'active'),
(23, 3, 7, 1, 'Laba Ditahan', 'active'),
(24, 3, 8, 1, 'Laba Berjalan', 'active'),
(25, 4, 9, 1, 'Penjualan Barang Dagang', 'active'),
(26, 4, 10, 1, 'Pendapatan Jasa ', 'active'),
(27, 4, 11, 1, 'Pendapatan Sewa', 'active'),
(28, 5, 12, 2, 'Pembelian Barang Dagang', 'active'),
(29, 5, 13, 1, 'Premi', 'active'),
(30, 5, 14, 1, 'Tenaga Kerja Harian', 'active'),
(31, 5, 15, 1, 'Tenaga Kerja Paket', 'active'),
(32, 5, 16, 3, 'Tenaga Kerja Paket', 'active'),
(33, 6, 17, 1, 'Biaya Gaji dan Tunjangan ', 'active'),
(34, 6, 17, 2, 'Biaya Kesehatan', 'active'),
(35, 6, 17, 3, 'Biaya Pensiun', 'active'),
(36, 6, 17, 4, 'Biaya Natura', 'active'),
(37, 6, 17, 5, 'Perjalanan Dinas', 'active'),
(38, 6, 18, 1, 'Biaya Perawatan Aset', 'active'),
(39, 6, 18, 2, 'Biaya Instalasi', 'active'),
(40, 6, 19, 1, 'Biaya Promosi', 'active'),
(41, 6, 19, 2, 'Biaya Penjualan', 'active'),
(42, 6, 19, 3, 'Biaya Penanganan Keluhan Pelanggan', 'active'),
(43, 6, 20, 1, 'Biaya Penyusutan Bangunan', 'active'),
(44, 6, 20, 2, 'Biaya Penyusutan Kendaraan', 'active'),
(45, 6, 20, 3, 'Biaya Penyusutan Mesin', 'active'),
(46, 6, 20, 4, 'Biaya Penyusutan Inventaris', 'active'),
(47, 6, 21, 1, 'Biaya Bank ', 'active'),
(48, 6, 21, 2, 'Biaya  Pajak', 'active'),
(49, 6, 21, 3, 'Biaya Perijinan', 'active'),
(50, 6, 22, 1, 'Biaya Pengembangan SDM', 'active'),
(51, 6, 22, 2, 'Biaya Recruitment SDM', 'active'),
(52, 6, 23, 1, 'Biaya CSR', 'active'),
(53, 6, 24, 1, 'Biaya Administrasi & Umum', 'active'),
(54, 7, 25, 1, 'Pendapatan Diluar Usaha', 'active'),
(55, 7, 26, 1, 'Biaya Diluar Usaha', 'active'),
(58, 5, 12, 1, 'Pembelian Barang Pembantu', 'active'),
(59, 1, 27, 1, 'Tanah', 'active'),
(60, 1, 27, 2, 'Bangunan', 'active'),
(61, 1, 27, 3, 'Kendaraan', 'active'),
(62, 1, 27, 4, 'Inventaris', 'active'),
(63, 6, 28, 9, 'Biaya Lain-lainnya', 'active'),
(64, 4, 29, 9, 'Pendapatan Lain-lainnya', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `m_coa_4`
--

CREATE TABLE `m_coa_4` (
  `m_coa_4_id` int(11) NOT NULL,
  `m_coa_1_id` int(11) NOT NULL,
  `m_coa_2_id` int(11) NOT NULL,
  `m_coa_3_id` int(11) NOT NULL,
  `nama_coa` varchar(100) NOT NULL,
  `kode_coa_4` int(11) NOT NULL,
  `status` varchar(20) NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_coa_4`
--

INSERT INTO `m_coa_4` (`m_coa_4_id`, `m_coa_1_id`, `m_coa_2_id`, `m_coa_3_id`, `nama_coa`, `kode_coa_4`, `status`) VALUES
(1, 1, 1, 1, 'Kas', 1, 'active'),
(2, 1, 1, 1, 'Kas Kecil', 2, 'active'),
(3, 1, 1, 2, 'BSI - 7138474822 (Bendahara 19)', 1, 'active'),
(4, 1, 1, 2, 'BSI - 7149685288 (Operasional Kasir)', 2, 'active'),
(5, 1, 1, 2, 'Mandiri - 1430000679009 (Tabungan)', 3, 'active'),
(6, 1, 1, 4, 'Piutang Customer Transport', 1, 'active'),
(7, 1, 1, 4, 'Piutang Customer Engineering', 2, 'active'),
(8, 1, 1, 4, 'Piutang Customer Offset', 3, 'active'),
(9, 1, 1, 4, 'Piutang Pihak Ketiga', 4, 'active'),
(10, 1, 1, 4, 'Piutang Karyawan', 6, 'active'),
(11, 1, 1, 4, 'Piutang Operasional', 7, 'active'),
(12, 1, 1, 5, 'Persediaan Barang Konstruksi', 1, 'active'),
(13, 1, 1, 5, 'Persediaan Barang Dagang', 2, 'active'),
(14, 1, 1, 5, 'Persediaan Barang Jadi', 3, 'active'),
(15, 1, 1, 5, 'Persediaan Bahan Pembantu', 4, 'active'),
(16, 1, 15, 1, 'Biaya Dibayar Dimuka', 1, 'inactive'),
(17, 1, 21, 1, 'Tanah', 1, 'active'),
(18, 1, 21, 2, 'Bangunan', 1, 'active'),
(19, 1, 21, 9, 'Kendaraan', 1, 'active'),
(20, 1, 27, 10, 'Inventaris Golongan 1', 1, 'active'),
(21, 1, 21, 11, 'Inventaris', 1, 'active'),
(22, 1, 3, 12, 'Akumulasi Penyusutan Bangunan', 1, 'active'),
(23, 1, 3, 13, 'Akumulasi Penyusutan Kendaraan', 1, 'active'),
(24, 1, 3, 14, 'Akumulasi Penyusutan Mesin', 1, 'active'),
(25, 1, 3, 15, 'Akumulasi Penyusutan Inventaris', 1, 'active'),
(26, 2, 4, 16, 'Hutang Supplier', 1, 'active'),
(27, 2, 4, 16, 'Hutang Ekspedisi', 2, 'active'),
(28, 2, 4, 17, 'Hutang Mitra', 1, 'active'),
(29, 2, 4, 18, 'Hutang Leasing', 1, 'active'),
(30, 2, 4, 18, 'Hutang Pihak Ketiga Jangka Pendek', 2, 'active'),
(31, 2, 4, 19, 'Hutang Bank Jangka Pendek', 1, 'active'),
(32, 2, 5, 20, 'Hutang Bank Jangka Panjang', 1, 'active'),
(33, 2, 5, 21, 'Hutang Pihak Ketiga Jangka Panjang', 1, 'active'),
(34, 3, 6, 22, 'Saham', 1, 'active'),
(35, 3, 7, 23, 'Laba Ditahan', 1, 'active'),
(36, 3, 8, 24, 'Laba Berjalan', 1, 'active'),
(37, 4, 9, 25, 'Penjualan Offset', 1, 'active'),
(38, 4, 9, 25, 'Penjualan Stationary', 2, 'active'),
(39, 4, 9, 25, 'Penjualan Produk Engineering', 3, 'active'),
(40, 4, 10, 26, 'Pendapatan Jasa Konstruksi', 1, 'active'),
(41, 4, 10, 26, 'Pendapatan Jasa Travel', 2, 'active'),
(42, 4, 11, 27, 'Pendapatan Sewa Bus', 1, 'active'),
(43, 5, 12, 28, 'Pembelian Barang Dagang Offset', 1, 'active'),
(44, 5, 12, 28, 'Pembelian Barang Dagang Konstruksi', 2, 'active'),
(45, 5, 12, 28, 'Pembelian Barang Pembantu', 1, 'active'),
(46, 5, 13, 29, 'Biaya Premi Pariwisata', 1, 'active'),
(47, 5, 14, 30, 'Tenaga Kerja Harian', 1, 'active'),
(48, 5, 15, 31, 'Tenaga Kerja Paket ', 1, 'active'),
(49, 5, 16, 32, 'Tenaga Kerja Borongan', 2, 'inactive'),
(50, 6, 17, 33, 'Gaji Pokok ', 1, 'active'),
(51, 6, 17, 33, 'Tunjangan Pegawai', 1, 'inactive'),
(52, 6, 17, 36, 'Bonus Karyawan', 1, 'active'),
(53, 6, 17, 34, 'Biaya Kesehatan Karyawan', 1, 'active'),
(54, 6, 17, 35, 'Biaya Pensiun', 1, 'active'),
(55, 6, 17, 36, 'Biaya Natura', 1, 'inactive'),
(56, 6, 17, 37, 'Biaya Transportasi Dinas', 1, 'active'),
(57, 6, 17, 37, 'Biaya Uang Makan Dinas', 2, 'active'),
(58, 6, 17, 37, 'Biaya Penginapan', 3, 'active'),
(59, 6, 18, 38, 'Biaya Perawatan Gedung', 1, 'active'),
(60, 6, 18, 38, 'Biaya Perawatan Mesin', 2, 'active'),
(61, 6, 18, 38, 'Biaya Perawatan Kendaraan', 3, 'active'),
(62, 6, 18, 38, 'Biaya Perawatan Inventaris', 4, 'active'),
(63, 6, 18, 39, 'Biaya Instalasi', 1, 'active'),
(64, 6, 19, 40, 'Biaya Kegiatan Promosi', 1, 'active'),
(65, 6, 19, 40, 'Biaya Sarana Promosi', 2, 'active'),
(66, 6, 19, 41, 'Biaya Program Penjualan', 1, 'active'),
(67, 6, 19, 41, 'Biaya Reward Distributor', 2, 'active'),
(68, 6, 19, 42, 'Biaya Penanganan Keluhan Pelanggan', 1, 'active'),
(69, 6, 20, 43, 'Biaya Penyusutan Bangunan', 1, 'active'),
(70, 6, 20, 44, 'Biaya Penyusutan Kendaraan', 1, 'active'),
(71, 6, 20, 45, 'Biaya Penyusutan Mesin', 1, 'active'),
(72, 6, 20, 46, 'Biaya Penyusutan Inventaris', 1, 'active'),
(73, 6, 21, 47, 'Biaya Administrasi Bank', 1, 'active'),
(74, 6, 21, 48, 'Biaya Pajak PPh 21, 23, Final', 1, 'active'),
(75, 6, 21, 48, 'Biaya Pajak PPh 25, 26', 2, 'active'),
(76, 6, 21, 48, 'Biaya Pajak PPN', 3, 'active'),
(77, 6, 21, 48, 'Biaya HAKI dan PVT', 4, 'active'),
(78, 6, 21, 49, 'Biaya Perijinan', 1, 'active'),
(79, 6, 22, 50, 'Pengembangan Skill', 1, 'active'),
(80, 6, 22, 50, 'Pengembangan Spiritual', 2, 'active'),
(81, 6, 22, 51, 'Biaya Recruitment SDM', 1, 'active'),
(82, 6, 23, 52, 'Biaya Program CSR', 1, 'active'),
(83, 6, 23, 52, 'Sumbangan', 2, 'active'),
(84, 6, 24, 53, 'Biaya ATK dan Materai', 1, 'active'),
(85, 6, 24, 53, 'Biaya Rumah Tangga', 2, 'active'),
(86, 6, 24, 53, 'Biaya Pengiriman Paket', 3, 'active'),
(87, 6, 24, 53, 'Biaya Meeting', 4, 'active'),
(88, 6, 24, 53, 'Biaya BBM', 5, 'active'),
(89, 7, 25, 54, 'Penjualan Aktiva Tetap', 1, 'active'),
(90, 7, 25, 54, 'Pendapatan Bunga Bank', 2, 'active'),
(91, 7, 25, 54, 'Pendapatan Lain-lain', 3, 'active'),
(92, 7, 26, 53, 'Kerugian Penjualan Aktiva Tetap', 1, 'active'),
(93, 7, 26, 53, 'Zakat', 2, 'active'),
(94, 7, 26, 53, 'Sodaqoh', 3, 'active'),
(95, 6, 24, 53, 'Biaya Rekening Internet dan Pulsa ', 6, 'active'),
(96, 6, 24, 53, 'Biaya Rekening Listrik', 7, 'active'),
(97, 6, 24, 53, 'Biaya Konsumsi Kantor', 8, 'active'),
(98, 5, 12, 28, 'Pembelian Barang Dagang Engineering', 4, 'active'),
(99, 1, 1, 2, 'Mandiri - 1430023234279 (Giro Operasional)', 4, 'active'),
(100, 3, 6, 22, 'Modal Awal', 0, 'active'),
(101, 1, 1, 2, 'BSI - 7149685515 (Tabungan)', 5, 'active'),
(102, 1, 1, 2, 'BCA - 0240862859 (Operasional)', 6, 'active'),
(103, 1, 27, 62, 'Inventaris Golongan 2', 2, 'active'),
(104, 6, 17, 33, 'Biaya BPJS', 2, 'active'),
(105, 6, 17, 33, 'Biaya Tunjanngan Hari Raya', 3, 'active'),
(106, 6, 28, 63, 'Biaya Lahan Pertanian', 9, 'active'),
(107, 4, 29, 64, 'Pendapatan Pertanian', 9, 'active');

-- --------------------------------------------------------

--
-- Table structure for table `m_divisi`
--

CREATE TABLE `m_divisi` (
  `id_divisi` int(11) NOT NULL,
  `nama_divisi` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_divisi`
--

INSERT INTO `m_divisi` (`id_divisi`, `nama_divisi`) VALUES
(1, 'UMJ Trans'),
(2, 'Engineering'),
(3, 'Offset'),
(4, 'P3LM'),
(5, 'GFA'),
(6, 'PAUD Yasmine');

-- --------------------------------------------------------

--
-- Table structure for table `m_menu`
--

CREATE TABLE `m_menu` (
  `menu_id` int(11) NOT NULL,
  `menu_name` varchar(75) NOT NULL,
  `menu_description` varchar(100) NOT NULL,
  `menu_sequence` int(11) NOT NULL,
  `menu_parent_id` int(11) NOT NULL,
  `menu_icon` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_menu`
--

INSERT INTO `m_menu` (`menu_id`, `menu_name`, `menu_description`, `menu_sequence`, `menu_parent_id`, `menu_icon`) VALUES
(2, 'Master', '#', 3, 0, 'fas fa-folder-minus'),
(3, 'Manage User', 'userlist', 17, 2, 'fas fa-users'),
(7, 'Pengajuan', 'pengajuan', 2, 0, 'far fa-file-alt'),
(9, 'Keluaran', '#', 4, 0, 'fas fa-book-reader'),
(10, 'Realisasi Anggaran', 'pengajuan_cair', 5, 9, 'fas fa-receipt'),
(11, 'Master COA 1', 'm_coa_1', 8, 2, 'fas fa-steam'),
(12, 'Master COA 2', 'm_coa_2', 9, 2, 'fas fa-steam'),
(13, 'Master COA 3', 'm_coa_3', 10, 2, 'fas fa-steam'),
(14, 'Master COA 4', 'm_coa_4', 11, 2, 'fas fa-steam'),
(15, 'Master MAK 1', 'm_mak_1', 12, 2, 'fas fa-steam'),
(16, 'Master MAK 2', 'm_mak_2', 13, 2, 'fas fa-steam'),
(17, 'Master MAK 3', 'm_mak_3', 14, 2, 'fas fa-steam'),
(18, 'Master MAK 4', 'm_mak_4', 15, 2, 'fas fa-steam'),
(19, 'Report Kasir', 'report_kasir', 6, 9, 'fas fa-clipboard-list'),
(20, 'Realisasi Anggaran', 'realisasi', 7, 0, 'fas fa-receipt'),
(21, 'Master Supplier', 'master_supplier', 16, 2, ''),
(22, 'Dashboard', 'dashboard', 1, 0, 'fas fa-home'),
(23, 'Jurnal Umum ', 'jurnal_umum', 18, 0, 'fas fa-book'),
(24, 'Kas', '#', 4, 0, 'fas fa-cash-register'),
(25, 'Kas Masuk', 'kas_masuk', 19, 24, 'fas fa-cash-register'),
(26, 'Kas Keluar', 'kas_keluar', 19, 24, 'fas fa-cash-register'),
(27, 'Neraca', '#', 5, 0, 'fas fa-balance-scale'),
(28, 'Neraca Lajur Bulanan', 'neraca_lajur_bulanan', 1, 27, ''),
(29, 'Neraca Lajur', 'neraca_lajur', 2, 27, ''),
(30, 'Buku Besar', 'buku_besar', 28, 0, 'fas fa-swatchbook'),
(31, 'Laba Rugi', 'laba_rugi', 29, 0, 'fas fa-wallet');

-- --------------------------------------------------------

--
-- Table structure for table `m_supplier`
--

CREATE TABLE `m_supplier` (
  `id_supplier` int(11) NOT NULL,
  `nama_supplier` varchar(100) NOT NULL,
  `no_tlp` varchar(15) NOT NULL,
  `alamat` text NOT NULL,
  `type` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_supplier`
--

INSERT INTO `m_supplier` (`id_supplier`, `nama_supplier`, `no_tlp`, `alamat`, `type`) VALUES
(2, 'toko', '-', 'jember', 'supplier'),
(3, 'toko kaca', '234-2934-0', 'jember', 'supplier'),
(4, 'Manager Engineering', '46546433', 'alamat', 'customer'),
(5, 'Manager Offset', '234234', '24234545', 'customer'),
(6, 'Toko Thomas Jember', '123', '-', 'supplier'),
(7, 'Manager UMJTrans Jember', '123', '-', 'supplier'),
(8, 'Manager FGA', '12345', '-', ''),
(11, 'Hanafi', '123', '-', 'pegawai'),
(12, 'Akhmad Suharto', '123', '-', 'pegawai'),
(13, 'Taufiq Timur Warisaji', '123', '-', 'pegawai'),
(14, 'Bagus Indra Tjayadhi', '123', '-', 'pegawai'),
(15, 'Adhitya Surya Manggala', '123', '-', 'pegawai'),
(16, 'Hardian Oktavianto', '123', '-', 'pegawai'),
(17, 'Fachmi Behesti', '123', '-', 'pegawai'),
(18, 'Robi Andri Oktafianto', '123', '-', 'pegawai'),
(19, 'Elok Nurul Istiqomah', '123', '-', 'pegawai'),
(20, 'Kholida Kumara Dewi', '123', '-', 'pegawai'),
(21, 'Eka Dimas Ariadi Saputra', '123', '-', 'pegawai'),
(22, 'Fafan Yoga Affandi', '123', '-', 'pegawai'),
(23, 'Diah Diana Putri', '123', '-', 'pegawai'),
(24, 'Ahmad Zaini', '123', '-', 'pegawai'),
(25, 'Akhmad Muhaimin', '123', '-', 'pegawai'),
(26, 'Beny Auri Hermawan', '123', '-', 'pegawai'),
(27, 'Didik Hariyadi', '123', '-', 'pegawai'),
(28, 'Edy Hariyono', '123', '-', 'pegawai'),
(29, 'Lukman Hakim', '123', '-', 'pegawai'),
(30, 'Muhamad Nur Fauzi', '123', '-', 'pegawai'),
(31, 'Wahyu Hidayat', '123', '-', 'pegawai'),
(32, 'Amaliya Firdaus', '123', '-', 'pegawai'),
(33, 'Ahmad Firman Rusandi', '123', '-', 'pegawai'),
(34, 'Syechul Hardiansyah Permana', '123', '-', 'pegawai'),
(35, 'Arsin', '123', '-', 'pegawai'),
(36, 'M. Yasin Mustakim', '123', '-', 'pegawai'),
(37, 'Agus Salem', '123', '-', 'pegawai'),
(38, 'Febri Imam Puji Santoso', '123', '-', 'pegawai'),
(39, 'Samsul Hadi', '123', '-', 'pegawai'),
(40, 'Syabil Abi Nuaim Daman', '123', '-', 'pegawai'),
(41, 'Muhammad Ariefin', '123', '-', 'pegawai'),
(42, 'Zulfa Agustina', '123', '-', 'pegawai'),
(43, 'Muhammad Abdul Gafur', '123', '-', 'pegawai'),
(44, 'Mohammad Taufik', '123', '-', 'pegawai'),
(45, 'Muhammad Ayub', '123', '-', 'pegawai'),
(46, 'Lilla Noervita Andiyani', '123', '-', 'pegawai'),
(47, 'Ratna Nurwindasari', '123', '-', 'pegawai'),
(48, 'Niken Silvia Nur Ashari', '123', '-', 'pegawai'),
(49, 'Universitas Muhammadiyah Jember', '123', '-', 'Mitra'),
(50, 'Unit GFA', '-', '-', 'Divisi'),
(51, 'Tukang', '-', 'UM Jember', 'pegawai'),
(52, 'Kas Engineering', '-', 'Um Jember', 'Divisi'),
(53, 'Supir UMJ', '-', 'UM Jember', 'Divisi'),
(54, 'Tenaga Ahli', '-', 'Um jember', 'Divisi'),
(55, 'BPJS', '-', '-', 'Divisi'),
(56, 'Pekerja Freelance', '-', '-', 'Freelance'),
(57, 'Kru Trans', '-', '-', 'Divisi'),
(58, 'Karyawan', '-', '-', 'Karyawan'),
(59, 'Lembaga', '-', '-', 'Lembaga'),
(60, 'Magang', '-', '-', 'Magang'),
(61, 'Marketing', '-', '-', 'Marketing'),
(62, 'Purusahaan Listrik Negara (PLN)', '-', '-', 'suplier'),
(63, 'Pihak Ke 3', '-', '- ', 'suplier'),
(64, 'PT BCA (Benih Citra Asia)', '-', '-', 'suplier');

-- --------------------------------------------------------

--
-- Stand-in structure for view `neraca`
-- (See below for the actual view)
--
CREATE TABLE `neraca` (
`ID` int(11)
,`Tanggal` varchar(7)
,`Nilai_debet` double
,`Nilai_kredit` double
,`saldo` double
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `neraca_3`
-- (See below for the actual view)
--
CREATE TABLE `neraca_3` (
`kode_coa_1` int(11)
,`kode_coa_2` int(11)
,`kode_coa_3` int(11)
,`kode_coa_4` int(11)
,`ID` int(11)
,`Tanggal` varchar(7)
,`Nilai_debet` double
,`Nilai_kredit` double
,`saldo` double
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `neraca_lajur_x`
-- (See below for the actual view)
--
CREATE TABLE `neraca_lajur_x` (
`kode_coa_1` int(11)
,`kode_coa_2` int(11)
,`kode_coa_3` int(11)
,`kode_coa_4` int(11)
,`ID` int(11)
,`Tanggal` varchar(4)
,`Nilai_debet` double
,`Nilai_kredit` double
,`saldo` double
);

-- --------------------------------------------------------

--
-- Table structure for table `sttk`
--

CREATE TABLE `sttk` (
  `id` int(11) NOT NULL,
  `tgl_klik` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_jurnalumum`
--

CREATE TABLE `t_jurnalumum` (
  `IdJurnal` int(11) NOT NULL,
  `idkascoa` int(11) NOT NULL,
  `IdUnit` int(11) NOT NULL,
  `NoTransaksi` varchar(150) NOT NULL,
  `Tanggal` date NOT NULL,
  `NoRekening` varchar(150) NOT NULL,
  `Rekening` varchar(150) NOT NULL,
  `Uraian` longtext NOT NULL,
  `IdDebet` int(11) NOT NULL,
  `IdKredit` int(11) NOT NULL,
  `Nilai` double NOT NULL,
  `Keterangan` varchar(150) NOT NULL,
  `IdUser` int(8) NOT NULL,
  `TglSave` timestamp NOT NULL DEFAULT current_timestamp(),
  `NU` enum('N','Y','SHU') NOT NULL DEFAULT 'N',
  `MTransaksi` varchar(5) NOT NULL,
  `NA` enum('N','Y') NOT NULL,
  `IdSup` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_jurnalumum`
--

INSERT INTO `t_jurnalumum` (`IdJurnal`, `idkascoa`, `IdUnit`, `NoTransaksi`, `Tanggal`, `NoRekening`, `Rekening`, `Uraian`, `IdDebet`, `IdKredit`, `Nilai`, `Keterangan`, `IdUser`, `TglSave`, `NU`, `MTransaksi`, `NA`, `IdSup`, `jumlah`) VALUES
(1, 0, 5, '28/05/2021/GFA/1', '2021-01-01', '', '', '', 5, 34, 900000000, '', 34, '2021-05-28 07:22:47', 'N', '', 'N', 49, 1),
(4, 0, 5, '28/05/2021/GFA/4', '2021-01-01', '', '', '', 3, 34, 100000000, '', 34, '2021-05-28 07:53:10', 'N', '', 'N', 49, 1),
(8, 11, 5, '001/SPM/GFA/05/01/2021', '2021-01-05', '', '', '', 11, 3, 2500000, '', 34, '2021-05-28 08:52:12', 'N', '', 'N', 19, 1),
(9, 0, 2, '001/SPM/UMJE/05/01/2021', '2021-01-05', '', '', '', 98, 3, 2265000, '', 34, '2021-06-07 03:23:38', 'N', '', 'N', 0, 2265000),
(10, 0, 2, '002/SPM/UMJE/05/01/2021', '2021-01-05', '', '', '', 98, 3, 2345000, '', 34, '2021-06-07 03:29:16', 'N', '', 'N', 0, 1),
(11, 0, 2, '003/SPM/UMJE/05/01/2021', '2021-01-05', '', '', '', 98, 3, 1365000, '', 34, '2021-06-07 03:31:13', 'N', '', 'N', 0, 1),
(12, 0, 2, '004/SPM/UMJE/06/01/2021', '2021-01-06', '', '', '', 98, 3, 14300000, '', 34, '2021-06-07 03:33:06', 'N', '', 'N', 0, 1),
(13, 4, 2, '149/SPM/UMJE/20/05/2021', '2021-05-20', '', '', '', 12, 4, 3022000, '', 30, '2021-07-27 01:20:30', 'N', '', 'N', 34, 1),
(14, 4, 2, '151/SPM/UMJE/21/05/2021', '2021-05-21', '', '', '', 12, 4, 4040000, '', 30, '2021-07-27 01:34:44', 'N', '', 'N', 50, 1),
(15, 4, 2, '152/SPM/UMJE/21/05/2021', '2021-05-21', '', '', '', 12, 4, 2899000, '', 30, '2021-07-27 01:41:44', 'N', '', 'N', 2, 1),
(16, 4, 2, '153/SPM/UMJE/22/05/2021', '2021-05-24', '', '', '', 12, 4, 1306000, '', 30, '2021-07-27 01:45:52', 'N', '', 'N', 34, 1),
(17, 4, 2, '154/SPM/UMJE/22/05/2021', '2021-05-24', '', '', '', 12, 4, 2000000, '', 30, '2021-07-27 01:55:24', 'N', '', 'N', 2, 1),
(18, 4, 2, '155/SPM/UMJE/24/05/2021', '2021-05-24', '', '', '', 12, 4, 1000000, '', 30, '2021-07-27 01:56:16', 'N', '', 'N', 2, 1),
(19, 4, 2, '156/SPM/UMJE/24/05/2021', '2021-05-25', '', '', '', 12, 4, 19366000, '', 30, '2021-07-27 02:09:41', 'N', '', 'N', 2, 1),
(20, 4, 2, '157/SPM/UMJE/28/05/2021', '2021-05-28', '', '', '', 12, 4, 4975000, '', 30, '2021-07-27 02:49:49', 'N', '', 'N', 50, 1),
(21, 4, 2, '158/SPM/UMJE/28/05/2021', '2021-05-28', '', '', '', 12, 4, 1956500, '', 30, '2021-07-27 02:53:51', 'N', '', 'N', 2, 1),
(22, 2, 5, '096/SPM/GFA/21/05/2021', '2021-05-21', '', '', '', 85, 2, 79000, '', 30, '2021-07-27 07:43:41', 'N', '', 'N', 2, 1),
(23, 4, 5, '100/SPM/GFA/02/06/2021', '2021-06-04', '', '', '', 50, 4, 250000, '', 30, '2021-07-27 07:47:41', 'N', '', 'N', 50, 1),
(24, 4, 5, '098/SPM/GFA/27/05/2021', '2021-05-27', '', '', '', 84, 4, 890300, '', 30, '2021-07-27 07:56:40', 'N', '', 'N', 2, 1),
(25, 4, 2, '148/SPM/UMJE/17/05/2021', '2021-05-18', '', '', '', 12, 4, 2000000, '', 30, '2021-07-27 08:01:45', 'N', '', 'N', 4, 1),
(26, 4, 2, '150/SPM/UMJE/20/05/2021', '2021-05-20', '', '', '', 12, 4, 1690000, '', 30, '2021-07-27 08:30:32', 'N', '', 'N', 2, 1),
(27, 3, 5, '003/SPM/GFA/12/01/2021', '2021-01-12', '', '', '', 11, 3, 74000, '', 30, '2021-07-28 08:26:53', 'N', '', 'N', 2, 1),
(28, 3, 5, '009/SPM/GFA/21/01/2021', '2021-01-21', '', '', '', 11, 3, 501170, '', 30, '2021-07-28 08:45:05', 'N', '', 'N', 2, 1),
(29, 3, 5, '010/SPM/GFA/16/01/2021', '2021-01-16', '', '', '', 11, 3, 804300, '', 30, '2021-07-28 08:50:35', 'N', '', 'N', 2, 1),
(30, 3, 5, '011/SPM/GFA/22/01/2021', '2021-01-22', '', '', '', 11, 3, 100000, '', 30, '2021-07-28 08:54:11', 'N', '', 'N', 2, 1),
(31, 3, 5, '012/SPM/GFA/22/01/2021', '2021-01-22', '', '', '', 11, 3, 45000, '', 30, '2021-07-28 09:00:19', 'N', '', 'N', 19, 1),
(32, 3, 2, '072/SPM/UMJE/02/02/2021', '2021-02-02', '', '', '', 98, 3, 800000, '', 30, '2021-08-16 01:32:13', 'N', '', 'N', 2, 1),
(33, 3, 2, '030/SPM/UMJE/14/01/2021', '2021-01-14', '', '', '', 98, 3, 8940000, '', 30, '2021-08-16 01:38:59', 'N', '', 'N', 2, 1),
(34, 3, 2, '031/SPM/UMJE/14/01/2021', '2021-01-14', '', '', '', 98, 3, 1130000, '', 30, '2021-08-16 01:41:23', 'N', '', 'N', 2, 1),
(35, 3, 2, '052/SPM/UMJE/21/01/2021', '2021-01-21', '', '', '', 20, 3, 1200000, '', 30, '2021-08-16 01:58:12', 'N', '', 'N', 34, 1),
(36, 3, 2, '054/SPM/UMJE/21/01/2021', '2021-01-21', '', '', '', 98, 3, 2543000, '', 30, '2021-08-16 02:00:45', 'N', '', 'N', 2, 1),
(37, 3, 2, '055/SPM/UMJE/21/01/2021', '2021-01-21', '', '', '', 47, 3, 1103000, '', 30, '2021-08-16 02:03:24', 'N', '', 'N', 32, 1),
(38, 3, 2, '063/SPM/UMJE/28/01/2021', '2021-08-28', '', '', '', 98, 3, 373500, '', 30, '2021-08-16 02:16:51', 'N', '', 'N', 15, 1),
(39, 3, 2, '019/SPM/UMJE/09/01/2021', '2021-01-11', '', '', '', 98, 3, 546500, '', 30, '2021-08-16 02:35:47', 'N', '', 'N', 2, 1),
(40, 3, 2, '020/SPM/UMJE/09/01/2021', '2021-01-11', '', '', '', 98, 3, 256500, '', 30, '2021-08-16 02:38:51', 'N', '', 'N', 32, 1),
(41, 3, 2, '024/SPM/UMJE/12/01/2021', '2021-01-12', '', '', '', 98, 3, 471000, '', 30, '2021-08-16 02:41:41', 'N', '', 'N', 32, 1),
(42, 3, 2, '067/SPM/UMJE/29/01/2021', '2021-08-29', '', '', '', 98, 3, 1664000, '', 30, '2021-08-16 02:46:24', 'N', '', 'N', 34, 1),
(43, 3, 2, '021/SPM/UMJE/09/01/2021', '2021-01-11', '', '', '', 98, 3, 2000000, '', 30, '2021-08-16 02:49:01', 'N', '', 'N', 33, 1),
(44, 3, 2, '028/SPM/UMJE/13/01/2021', '2021-01-13', '', '', '', 98, 3, 2525000, '', 30, '2021-08-16 03:01:22', 'N', '', 'N', 33, 1),
(45, 3, 2, '069/SPM/UMJE/01/02/2021', '2021-01-01', '', '', '', 98, 3, 493500, '', 30, '2021-09-22 01:56:37', 'N', '', 'N', 33, 1),
(46, 3, 1, '009/SPM/UMJT/01/02/2021', '2021-02-01', '', '', '', 11, 3, 121000, '', 30, '2021-09-22 02:01:05', 'N', '', 'N', 14, 1),
(47, 3, 2, '070/SPM/UMJE/01/02/2021', '2021-02-02', '', '', '', 97, 3, 1116000, '', 30, '2021-09-22 02:09:48', 'N', '', 'N', 2, 1),
(48, 3, 2, '071/SPM/UMJE/01/02/2021', '2021-02-02', '', '', '', 98, 3, 772100, '', 30, '2021-09-22 02:14:49', 'N', '', 'N', 33, 1),
(49, 3, 2, '073/SPM/UMJE/03/02/2021', '2021-02-03', '', '', '', 98, 3, 432000, '', 30, '2021-09-22 02:18:18', 'N', '', 'N', 15, 1),
(50, 3, 1, '010/SPM/UMJT/03/02/2021', '2021-02-03', '', '', '', 11, 3, 100000, '', 30, '2021-09-22 02:22:20', 'N', '', 'N', 23, 1),
(51, 3, 1, '008/SPM/UMJT/29/01/2021', '2021-02-03', '', '', '', 11, 3, 3560000, '', 30, '2021-09-22 02:27:56', 'N', '', 'N', 2, 1),
(52, 3, 2, '074/SPM/UMJE/03/02/2021', '2021-02-03', '', '', '', 98, 3, 4000000, '', 30, '2021-09-22 02:31:32', 'N', '', 'N', 2, 1),
(53, 3, 2, '075/SPM/UMJE/04/02/2021', '2021-02-04', '', '', '', 98, 3, 1404000, '', 30, '2021-09-22 02:34:44', 'N', '', 'N', 2, 1),
(54, 3, 2, '076/SPM/UMJE/04/02/2021', '2021-02-04', '', '', '', 98, 3, 1041000, '', 30, '2021-09-22 02:37:18', 'N', '', 'N', 34, 1),
(55, 3, 2, '077/SPM/UMJE/05/02/2021', '2021-02-05', '', '', '', 98, 3, 17293500, '', 30, '2021-09-22 02:40:24', 'N', '', 'N', 33, 1),
(56, 3, 2, '078/SPM/UMJE/05/02/2021', '2021-02-05', '', '', '', 98, 3, 2220000, '', 30, '2021-09-22 02:43:03', 'N', '', 'N', 2, 1),
(57, 3, 2, '079/SPM/UMJE/05/02/2021', '2021-02-05', '', '', '', 47, 3, 4620000, '', 30, '2021-09-22 02:45:52', 'N', '', 'N', 32, 1),
(58, 3, 1, '012/SPM/UMJT/10/02/2021', '2021-02-10', '', '', '', 11, 3, 56000, '', 30, '2021-09-22 02:50:14', 'N', '', 'N', 2, 1),
(59, 3, 2, '081/SPM/UMJE/10/02/2021', '2021-02-10', '', '', '', 98, 3, 163500, '', 30, '2021-09-22 02:52:24', 'N', '', 'N', 32, 1),
(60, 3, 2, '083/SPM/UMJE/11/02/2021', '2021-02-11', '', '', '', 47, 3, 3900000, '', 30, '2021-09-22 02:58:53', 'N', '', 'N', 32, 1),
(61, 3, 2, '084/SPM/UMJE/15/02/2021', '2021-02-15', '', '', '', 98, 3, 1000000, '', 30, '2021-09-22 03:01:02', 'N', '', 'N', 32, 1),
(62, 3, 2, '085/SPM/UMJE/17/02/2021', '2021-02-17', '', '', '', 98, 3, 250000, '', 30, '2021-09-22 03:05:34', 'N', '', 'N', 2, 1),
(63, 3, 1, '013/SPM/UMJT/16/02/2021', '2021-02-19', '', '', '', 11, 3, 600000, '', 30, '2021-09-22 03:08:08', 'N', '', 'N', 2, 1),
(64, 3, 2, '086/SPM/UMJE/19/02/2021', '2021-02-19', '', '', '', 98, 3, 15242300, '', 30, '2021-09-22 03:10:45', 'N', '', 'N', 32, 1),
(65, 3, 2, '087/SPM/UMJE/19/02/2021', '2021-02-19', '', '', '', 47, 3, 830000, '', 30, '2021-09-22 03:14:05', 'N', '', 'N', 32, 1),
(66, 3, 2, '088/SPM/UMJE/25/02/2021', '2021-02-25', '', '', '', 98, 3, 19622300, '', 30, '2021-09-22 03:17:18', 'N', '', 'N', 32, 1),
(67, 3, 2, '089/SPM/UMJE/26/02/2021', '2021-02-26', '', '', '', 47, 3, 3450000, '', 30, '2021-09-22 03:21:01', 'N', '', 'N', 32, 1),
(68, 3, 2, '002/SPM/UMJE/05/01/2021', '2021-01-05', '', '', '', 98, 3, 2345000, '', 30, '2021-09-24 04:25:44', 'N', '', 'N', 2, 1),
(69, 3, 2, '003/SPM/UMJE/05/01/2021', '2021-01-05', '', '', '', 98, 3, 1365000, '', 30, '2021-09-24 04:28:03', 'N', '', 'N', 2, 1),
(70, 3, 2, '001/SPM/UMJE/05/01/2021', '2021-01-06', '', '', '', 98, 3, 2265000, '', 30, '2021-09-24 04:31:14', 'N', '', 'N', 2, 1),
(71, 3, 2, '004/SPM/UMJE/06/01/2021', '2021-01-06', '', '', '', 98, 3, 14300000, '', 30, '2021-09-24 05:49:27', 'N', '', 'N', 2, 1),
(72, 3, 2, '008/SPM/UMJE/06/01/2021', '2021-01-06', '', '', '', 98, 3, 1860000, '', 30, '2021-09-24 05:55:37', 'N', '', 'N', 2, 1),
(73, 3, 2, '006/SPM/UMJE/06/01/2021', '2021-01-06', '', '', '', 98, 3, 4800000, '', 30, '2021-09-24 05:58:01', 'N', '', 'N', 2, 1),
(74, 3, 2, '009/SPM/UMJE/06/01/2021', '2021-01-06', '', '', '', 98, 3, 1710000, '', 30, '2021-09-24 06:06:39', 'N', '', 'N', 2, 1),
(75, 3, 2, '007/SPM/UMJE/06/01/2021', '2021-01-06', '', '', '', 98, 3, 6000000, '', 30, '2021-09-24 06:29:15', 'N', '', 'N', 2, 1),
(76, 3, 2, '013/SPM/UMJE/08/01/2021', '2021-01-08', '', '', '', 98, 3, 4330000, '', 30, '2021-09-24 06:40:11', 'N', '', 'N', 2, 1),
(77, 3, 2, '014/SPM/UMJE/08/01/2021', '2021-01-08', '', '', '', 48, 3, 3000000, '', 30, '2021-09-24 06:42:26', 'N', '', 'N', 2, 1),
(78, 3, 2, '017/SPM/UMJE/09/01/2021', '2021-01-09', '', '', '', 47, 3, 1500000, '', 30, '2021-09-24 07:26:02', 'N', '', 'N', 33, 1),
(79, 3, 2, '016/SPM/UMJE/09/01/2021', '2021-01-09', '', '', '', 98, 3, 6645000, '', 30, '2021-09-24 07:27:27', 'N', '', 'N', 2, 1),
(80, 3, 2, '025/SPM/UMJE/12/01/2021', '2021-01-12', '', '', '', 98, 3, 1350000, '', 30, '2021-09-24 07:29:41', 'N', '', 'N', 2, 1),
(81, 3, 2, '022/SPM/UMJE/12/01/2021', '2021-01-12', '', '', '', 98, 3, 4470000, '', 30, '2021-09-24 07:31:35', 'N', '', 'N', 2, 1),
(82, 3, 2, '023/SPM/UMJE/12/01/2021', '2021-01-12', '', '', '', 98, 3, 1727500, '', 30, '2021-09-24 08:06:01', 'N', '', 'N', 32, 1),
(83, 3, 2, '029/SPM/UMJE/13/01/2021', '2021-01-13', '', '', '', 98, 3, 4200000, '', 30, '2021-09-24 08:08:05', 'N', '', 'N', 2, 1),
(84, 3, 2, '027/SPM/UMJE/14/01/2021', '2021-01-14', '', '', '', 98, 3, 1425000, '', 30, '2021-09-24 08:09:59', 'N', '', 'N', 2, 1),
(85, 3, 2, '033/SPM/UMJE/15/01/2021', '2021-01-15', '', '', '', 47, 3, 1408000, '', 30, '2021-09-24 08:12:18', 'N', '', 'N', 32, 1),
(86, 3, 2, '032/SPM/UMJE/15/01/2021', '2021-01-15', '', '', '', 47, 3, 1169000, '', 30, '2021-09-24 08:14:50', 'N', '', 'N', 32, 1),
(87, 3, 2, '037/SPM/UMJE/16/01/2021', '2021-01-16', '', '', '', 47, 3, 239000, '', 30, '2021-09-24 08:20:15', 'N', '', 'N', 32, 1),
(88, 3, 2, '036/SPM/UMJE/16/01/2021', '2021-01-16', '', '', '', 47, 3, 1400000, '', 30, '2021-09-24 08:22:15', 'N', '', 'N', 33, 1),
(89, 3, 2, '035/SPM/UMJE/16/01/2021', '2021-01-16', '', '', '', 98, 3, 1758627, '', 30, '2021-09-24 08:24:02', 'N', '', 'N', 32, 1),
(90, 3, 2, '038/SPM/UMJE/18/01/2021', '2021-01-18', '', '', '', 98, 3, 1450000, '', 30, '2021-09-24 08:27:21', 'N', '', 'N', 2, 1),
(91, 3, 2, '040/SPM/UMJE/18/01/2021', '2021-01-18', '', '', '', 47, 3, 600000, '', 30, '2021-09-24 08:29:11', 'N', '', 'N', 33, 1),
(92, 3, 2, '041/SPM/UMJE/18/01/2021', '2021-01-18', '', '', '', 98, 3, 1146000, '', 30, '2021-09-24 08:30:55', 'N', '', 'N', 2, 1),
(93, 3, 2, '044/SPM/UMJE/18/01/2021', '2021-01-19', '', '', '', 98, 3, 138000, '', 30, '2021-09-24 08:32:20', 'N', '', 'N', 2, 1),
(94, 3, 2, '043/SPM/UMJE/18/01/2021', '2021-01-19', '', '', '', 98, 3, 214500, '', 30, '2021-09-24 08:34:07', 'N', '', 'N', 32, 1),
(95, 3, 2, '046/SPM/UMJE/19/01/2021', '2021-01-19', '', '', '', 98, 3, 328000, '', 30, '2021-09-24 08:35:24', 'N', '', 'N', 2, 1),
(96, 3, 2, '045/SPM/UMJE/18/01/2021', '2021-01-19', '', '', '', 98, 3, 630000, '', 30, '2021-09-24 08:50:10', 'N', '', 'N', 2, 1),
(97, 3, 2, '042/SPM/UMJE/18/01/2021', '2021-01-19', '', '', '', 53, 3, 1946373, '', 30, '2021-09-24 08:52:24', 'N', '', 'N', 49, 1),
(98, 3, 2, '048/SPM/UMJE/19/01/2021', '2021-01-19', '', '', '', 98, 3, 842500, '', 30, '2021-09-24 08:54:14', 'N', '', 'N', 34, 1),
(99, 3, 2, '050/SPM/UMJE/20/01/2021', '2021-01-20', '', '', '', 98, 3, 168000, '', 30, '2021-09-24 08:55:45', 'N', '', 'N', 2, 1),
(100, 3, 2, '056/SPM/UMJE/22/01/2021', '2021-01-22', '', '', '', 47, 3, 2071000, '', 30, '2021-09-24 09:02:34', 'N', '', 'N', 32, 1),
(101, 3, 2, '057/SPM/UMJE/23/01/2021', '2021-09-23', '', '', '', 98, 3, 750000, '', 30, '2021-09-24 09:05:08', 'N', '', 'N', 33, 1),
(102, 3, 2, '051/SPM/UMJE/20/01/2021', '2021-01-20', '', '', '', 98, 3, 2129868, '', 30, '2021-09-24 09:13:50', 'N', '', 'N', 33, 1),
(103, 3, 2, '064/SPM/UMJE/28/01/2021', '2021-01-28', '', '', '', 98, 3, 6383500, '', 30, '2021-09-24 09:16:10', 'N', '', 'N', 15, 1),
(104, 3, 2, '066/SPM/UMJE/29/01/2021', '2021-01-29', '', '', '', 47, 3, 2780000, '', 30, '2021-09-24 09:17:58', 'N', '', 'N', 32, 1),
(105, 3, 2, '068/SPM/UMJE/29/01/2021', '2021-01-30', '', '', '', 98, 3, 9500000, '', 30, '2021-09-24 09:19:24', 'N', '', 'N', 2, 1),
(106, 3, 2, '127/SPM/UMJE/19/04/2021', '2021-04-19', '', '', '', 12, 3, 5311500, '', 30, '2021-11-11 00:56:36', 'N', '', 'N', 2, 1),
(107, 3, 2, '128/SPM/UMJE/19/04/2021', '2021-04-19', '', '', '', 12, 3, 1315000, '', 30, '2021-11-11 00:58:20', 'N', '', 'N', 34, 1),
(108, 3, 2, '129/SPM/UMJE/20/04/2021', '2021-04-20', '', '', '', 12, 3, 4575000, '', 30, '2021-11-11 00:59:41', 'N', '', 'N', 2, 1),
(109, 3, 2, '130/SPM/UMJE/21/04/2021', '2021-04-21', '', '', '', 12, 3, 8442500, '', 30, '2021-11-11 01:01:59', 'N', '', 'N', 2, 1),
(110, 3, 2, '131/SPM/UMJE/22/04/2021', '2021-04-22', '', '', '', 12, 3, 1688000, '', 30, '2021-11-11 01:03:34', 'N', '', 'N', 34, 1),
(111, 3, 2, '132/SPM/UMJE/23/04/2021', '2021-04-23', '', '', '', 12, 3, 8297000, '', 30, '2021-11-11 01:11:13', 'N', '', 'N', 51, 1),
(112, 3, 2, '133/SPM/UMJE/23/04/2021', '2021-04-23', '', '', '', 12, 3, 6764500, '', 30, '2021-11-11 01:12:55', 'N', '', 'N', 2, 1),
(113, 3, 2, '134/SPM/UMJE/23/04/2021', '2021-04-26', '', '', '', 12, 3, 1108000, '', 30, '2021-11-11 01:15:29', 'N', '', 'N', 34, 1),
(114, 3, 2, '135/SPM/UMJE/26/04/2021', '2021-04-26', '', '', '', 12, 3, 3245000, '', 30, '2021-11-11 01:16:59', 'N', '', 'N', 2, 1),
(115, 3, 2, '136/SPM/UMJE/26/04/2021', '2021-04-29', '', '', '', 12, 3, 19749000, '', 30, '2021-11-11 01:22:58', 'N', '', 'N', 52, 1),
(116, 3, 2, '137/SPM/UMJE/27/04/2021', '2021-04-27', '', '', '', 12, 3, 2832000, '', 30, '2021-11-11 01:27:32', 'N', '', 'N', 34, 1),
(117, 3, 2, '138/SPM/UMJE/27/04/2021', '2021-04-27', '', '', '', 12, 3, 2700000, '', 30, '2021-11-11 01:29:16', 'N', '', 'N', 2, 1),
(118, 3, 2, '139/SPM/UMJE/30/04/2021', '2021-04-30', '', '', '', 12, 3, 9300000, '', 30, '2021-11-11 01:34:47', 'N', '', 'N', 51, 1),
(119, 3, 2, '140/SPM/UMJE/30/04/2021', '2021-04-30', '', '', '', 12, 3, 9914000, '', 30, '2021-11-11 01:37:02', 'N', '', 'N', 2, 1),
(120, 3, 2, '143/SPM/UMJE/07/05/2021', '2021-05-07', '', '', '', 12, 3, 5167500, '', 30, '2021-11-11 01:38:48', 'N', '', 'N', 2, 1),
(121, 3, 2, '144/SPM/UMJE/07/05/2021', '2021-05-07', '', '', '', 12, 3, 9240000, '', 30, '2021-11-11 01:40:40', 'N', '', 'N', 51, 1),
(122, 3, 2, '145/SPM/UMJE/10/05/2021', '2021-05-10', '', '', '', 12, 3, 275000, '', 30, '2021-11-11 01:44:04', 'N', '', 'N', 51, 1),
(123, 3, 2, '146/SPM/UMJE/11/05/2021', '2021-05-11', '', '', '', 12, 3, 2066000, '', 30, '2021-11-11 01:45:25', 'N', '', 'N', 2, 1),
(124, 3, 2, '147/SPM/UMJE/11/05/2021', '2021-05-11', '', '', '', 12, 3, 2590000, '', 30, '2021-11-11 01:46:53', 'N', '', 'N', 51, 1),
(125, 4, 2, '159/SPM/UMJE/04/06/2021', '2021-06-04', '', '', '', 12, 4, 2525000, '', 30, '2021-11-11 01:49:07', 'N', '', 'N', 2, 1),
(126, 2, 2, '012/SPM/UMJE/08/01/2021', '2021-01-08', '', '', '', 48, 2, 800000, '', 30, '2021-11-11 01:51:50', 'N', '', 'N', 32, 1),
(127, 3, 2, '015/SPM/UMJE/09/01/2021', '2021-01-09', '', '', '', 47, 3, 3839000, '', 30, '2021-11-11 01:54:44', 'N', '', 'N', 51, 1),
(128, 3, 2, '034/SPM/UMJE/15/01/2021', '2021-01-15', '', '', '', 47, 3, 325000, '', 30, '2021-11-11 02:03:40', 'N', '', 'N', 53, 1),
(129, 3, 2, '053/SPM/UMJE/21/01/2021', '2021-01-21', '', '', '', 98, 3, 1490000, '', 30, '2021-11-11 02:06:57', 'N', '', 'N', 51, 1),
(130, 3, 2, '060/SPM/UMJE/26/01/2021', '2021-01-26', '', '', '', 98, 3, 2500000, '', 30, '2021-11-11 02:09:16', 'N', '', 'N', 33, 1),
(131, 3, 2, '082/SPM/UMJE/08/02/2021', '2021-02-08', '', '', '', 57, 3, 100000, '', 30, '2021-11-11 02:13:58', 'N', '', 'N', 51, 1),
(132, 5, 2, '090/SPM/UMJE/03/03/2021', '2021-03-03', '', '', '', 98, 5, 17190000, '', 30, '2021-11-11 02:16:02', 'N', '', 'N', 2, 1),
(133, 3, 2, '091/SPM/UMJE/04/03/2021', '2021-03-05', '', '', '', 98, 3, 4865000, '', 30, '2021-11-11 02:18:48', 'N', '', 'N', 32, 1),
(134, 3, 2, '092/SPM/UMJE/05/03/2021', '2021-03-05', '', '', '', 47, 3, 5510000, '', 30, '2021-11-11 02:20:58', 'N', '', 'N', 32, 1),
(135, 3, 2, '093/SPM/UMJE/05/03/2021', '2021-03-05', '', '', '', 98, 3, 3000000, '', 30, '2021-11-11 02:28:45', 'N', '', 'N', 54, 1),
(136, 3, 2, '094/SPM/UMJE/15/03/2021', '2021-03-15', '', '', '', 98, 3, 2153500, '', 30, '2021-11-11 02:31:04', 'N', '', 'N', 34, 1),
(137, 3, 2, '095/SPM/UMJE/12/03/2021', '2021-03-12', '', '', '', 47, 3, 4428000, '', 30, '2021-11-11 02:38:33', 'N', '', 'N', 51, 1),
(138, 3, 2, '096/SPM/UMJE/16/03/2021', '2021-03-16', '', '', '', 95, 3, 278000, '', 30, '2021-11-12 01:31:46', 'N', '', 'N', 32, 1),
(139, 3, 2, '097/SPM/UMJE/16/03/2021', '2021-03-16', '', '', '', 98, 3, 1345000, '', 30, '2021-11-12 01:37:18', 'N', '', 'N', 2, 1),
(140, 3, 2, '098/SPM/UMJE/16/03/2021', '2021-03-16', '', '', '', 98, 3, 1770000, '', 30, '2021-11-12 01:39:04', 'N', '', 'N', 2, 1),
(141, 3, 2, '099/SPM/UMJE/17/03/2021', '2021-03-17', '', '', '', 98, 3, 1150000, '', 30, '2021-11-12 01:41:43', 'N', '', 'N', 51, 1),
(142, 3, 2, '100/SPM/UMJE/17/03/2021', '2021-03-17', '', '', '', 2, 3, 20000000, '', 30, '2021-11-12 01:43:29', 'N', '', 'N', 52, 1),
(143, 3, 2, '101/SPM/UMJE/18/03/2021', '2021-03-18', '', '', '', 98, 3, 2150500, '', 30, '2021-11-12 01:46:14', 'N', '', 'N', 15, 1),
(144, 3, 2, '102/SPM/UMJE/19/03/2021', '2021-03-19', '', '', '', 47, 3, 4482000, '', 30, '2021-11-12 01:48:10', 'N', '', 'N', 51, 1),
(145, 3, 2, '103/SPM/UMJE/19/03/2021', '2021-03-19', '', '', '', 98, 3, 1875500, '', 30, '2021-11-12 01:50:54', 'N', '', 'N', 51, 1),
(146, 3, 2, '104/SPM/UMJE/26/03/2021', '2021-03-26', '', '', '', 47, 3, 1550000, '', 30, '2021-11-12 01:53:53', 'N', '', 'N', 51, 1),
(147, 3, 2, '105/SPM/UMJE/26/03/2021', '2021-03-26', '', '', '', 47, 3, 4375000, '', 30, '2021-11-12 01:55:29', 'N', '', 'N', 51, 1),
(148, 3, 2, '106/SPM/UMJE/26/03/2021', '2021-03-26', '', '', '', 98, 3, 5289000, '', 30, '2021-11-12 01:56:59', 'N', '', 'N', 2, 1),
(149, 3, 2, '107/SPM/UMJE/26/03/2021', '2021-03-26', '', '', '', 98, 3, 1300000, '', 30, '2021-11-12 01:59:45', 'N', '', 'N', 2, 1),
(150, 3, 2, '108/SPM/UMJE/29/03/2021', '2021-03-29', '', '', '', 98, 3, 3000000, '', 30, '2021-11-12 02:01:27', 'N', '', 'N', 51, 1),
(151, 3, 2, '109/SPM/UMJE/30/03/2021', '2021-03-30', '', '', '', 98, 3, 3272500, '', 30, '2021-11-12 02:03:39', 'N', '', 'N', 2, 1),
(152, 3, 2, '110/SPM/UMJE/20/05/2021', '2021-03-30', '', '', '', 98, 3, 462000, '', 30, '2021-11-12 02:06:31', 'N', '', 'N', 2, 1),
(153, 3, 2, '111/SPM/UMJE/30/03/2021', '2021-03-30', '', '', '', 2, 3, 20000000, '', 30, '2021-11-12 02:09:33', 'N', '', 'N', 52, 1),
(154, 3, 2, '112/SPM/UMJE/31/03/2021', '2021-04-01', '', '', '', 84, 3, 498000, '', 30, '2021-11-12 02:11:27', 'N', '', 'N', 2, 1),
(155, 3, 2, '113/SPM/UMJE/31/03/2021', '2021-03-31', '', '', '', 98, 3, 17267000, '', 30, '2021-11-12 02:13:10', 'N', '', 'N', 2, 1),
(156, 3, 2, '114/SPM/UMJE/01/04/2021', '2021-04-01', '', '', '', 98, 3, 2200000, '', 30, '2021-11-12 02:15:22', 'N', '', 'N', 51, 1),
(157, 3, 2, '115/SPM/UMJE/02/04/2021', '2021-04-02', '', '', '', 47, 3, 4626000, '', 30, '2021-11-12 02:17:18', 'N', '', 'N', 51, 1),
(158, 3, 2, '116/SPM/UMJE/01/04/2021', '2021-04-01', '', '', '', 98, 3, 2250000, '', 30, '2021-11-12 02:18:52', 'N', '', 'N', 2, 1),
(159, 3, 2, '117/SPM/UMJE/02/04/2021', '2021-04-06', '', '', '', 12, 3, 3715500, '', 30, '2021-11-12 02:23:25', 'N', '', 'N', 2, 1),
(160, 3, 2, '118/SPM/UMJE/07/04/2021', '2021-04-07', '', '', '', 12, 3, 8080000, '', 30, '2021-11-12 02:25:00', 'N', '', 'N', 51, 1),
(161, 3, 2, '119/SPM/UMJE/09/04/2021', '2021-04-09', '', '', '', 12, 3, 4190000, '', 30, '2021-11-12 02:26:31', 'N', '', 'N', 51, 1),
(162, 3, 2, '120/SPM/UMJE/07/04/2021', '2021-04-07', '', '', '', 12, 3, 210000, '', 30, '2021-11-12 02:28:20', 'N', '', 'N', 2, 1),
(163, 3, 2, '121/SPM/UMJE/09/04/2021', '2021-04-08', '', '', '', 12, 3, 10860000, '', 30, '2021-11-12 02:29:47', 'N', '', 'N', 2, 1),
(164, 3, 2, '122/SPM/UMJE/09/04/2021', '2021-04-09', '', '', '', 12, 3, 7901000, '', 30, '2021-11-12 02:31:26', 'N', '', 'N', 2, 1),
(165, 3, 2, '123/SPM/UMJE/12/04/2021', '2021-04-12', '', '', '', 12, 3, 2030000, '', 30, '2021-11-12 02:33:26', 'N', '', 'N', 2, 1),
(166, 3, 2, '124/SPM/UMJE/16/04/2021', '2021-04-16', '', '', '', 12, 3, 250000, '', 30, '2021-11-12 02:35:29', 'N', '', 'N', 51, 1),
(167, 3, 2, '125/SPM/UMJE/16/04/2021', '2021-04-16', '', '', '', 12, 3, 4780000, '', 30, '2021-11-12 02:38:05', 'N', '', 'N', 51, 1),
(168, 3, 2, '126/SPM/UMJE/16/04/2021', '2021-04-16', '', '', '', 12, 3, 5977500, '', 30, '2021-11-12 02:40:00', 'N', '', 'N', 2, 1),
(169, 3, 2, '141/SPM/UMJE/04/05/2021', '2021-05-04', '', '', '', 12, 3, 5266500, '', 30, '2021-11-12 02:41:31', 'N', '', 'N', 2, 1),
(170, 3, 2, '142/SPM/UMJE/04/05/2021', '2021-05-04', '', '', '', 12, 3, 1850000, '', 30, '2021-11-12 02:43:28', 'N', '', 'N', 51, 1),
(171, 4, 2, '160/SPM/UMJE/04/06/2021', '2021-06-04', '', '', '', 12, 4, 5145000, '', 30, '2021-11-12 02:45:40', 'N', '', 'N', 51, 1),
(172, 4, 2, '161/SPM/UMJE/04/06/2021', '2021-06-04', '', '', '', 12, 4, 4756000, '', 30, '2021-11-12 02:47:07', 'N', '', 'N', 2, 1),
(173, 4, 2, '162/SPM/UMJE/07/06/2021', '2021-06-07', '', '', '', 12, 4, 20058000, '', 30, '2021-11-12 02:48:41', 'N', '', 'N', 52, 1),
(174, 4, 2, '163/SPM/UMJE/11/06/2021', '2021-06-11', '', '', '', 12, 4, 2070000, '', 30, '2021-11-12 02:50:21', 'N', '', 'N', 51, 1),
(175, 3, 5, '002/SPM/GFA/06/01/2021', '2021-01-06', '', '', '', 11, 3, 184500, '', 30, '2021-11-22 00:49:22', 'N', '', 'N', 19, 1),
(176, 3, 5, '002/SPM/GFA/06/01/2021', '2021-01-06', '', '', '', 11, 3, 716500, '', 30, '2021-11-22 00:50:35', 'N', '', 'N', 19, 1),
(177, 5, 5, '004/SPM/GFA/16/01/2021', '2021-01-16', '', '', '', 11, 5, 18360000, '', 30, '2021-11-22 00:52:38', 'N', '', 'N', 2, 1),
(178, 5, 5, '008/SPM/GFA/15/01/2021', '2021-01-19', '', '', '', 11, 5, 5358423, '', 30, '2021-11-22 01:36:54', 'N', '', 'N', 55, 1),
(179, 3, 5, '013/SPM/GFA/26/01/2021', '2021-01-26', '', '', '', 11, 3, 4197700, '', 30, '2021-11-22 01:42:50', 'N', '', 'N', 19, 1),
(180, 2, 5, '014/SPM/GFA/26/01/2021', '2021-01-26', '', '', '', 11, 2, 300000, '', 30, '2021-11-22 02:14:22', 'N', '', 'N', 19, 1),
(181, 3, 5, '015/SPM/GFA/27/01/2021', '2021-01-27', '', '', '', 11, 3, 3000000, '', 30, '2021-11-22 02:25:32', 'N', '', 'N', 17, 1),
(182, 3, 5, '016/SPM/GFA/27/01/2021', '2021-01-27', '', '', '', 11, 3, 1414260, '', 30, '2021-11-22 02:36:53', 'N', '', 'N', 2, 1),
(183, 3, 5, '018/SPM/GFA/27/01/2021', '2021-01-27', '', '', '', 11, 3, 100000, '', 30, '2021-11-22 02:50:52', 'N', '', 'N', 21, 1),
(184, 3, 5, '019/SPM/GFA/29/01/2021', '2021-01-27', '', '', '', 11, 3, 16100, '', 30, '2021-11-22 02:53:23', 'N', '', 'N', 21, 1),
(185, 2, 5, '020/SPM/GFA/29/01/2021', '2021-01-29', '', '', '', 11, 2, 495000, '', 30, '2021-11-22 02:55:16', 'N', '', 'N', 21, 1),
(186, 3, 5, '042/SPM/GFA/01/03/2021', '2021-03-01', '', '', '', 50, 3, 41487740, '', 30, '2021-11-22 02:57:05', 'N', '', 'N', 58, 1),
(187, 3, 5, '143/SPM/UMJE/01/03/2021', '2021-03-01', '', '', '', 47, 3, 700000, '', 30, '2021-11-22 02:59:09', 'N', '', 'N', 57, 1),
(188, 2, 5, '044/SPM/UMJE/01/03/2021', '2021-03-01', '', '', '', 88, 2, 184200, '', 30, '2021-11-22 03:00:52', 'N', '', 'N', 21, 1),
(189, 5, 5, '046/SPM/GFA/05/03/2021', '2021-03-05', '', '', '', 53, 5, 8884140, '', 30, '2021-11-22 03:11:20', 'N', '', 'N', 55, 1),
(190, 3, 5, '047/SPM/GFA/01/03/2021', '2021-03-01', '', '', '', 50, 3, 150000, '', 30, '2021-11-22 03:20:32', 'N', '', 'N', 21, 1),
(191, 3, 5, '047/SPM/GFA/01/03/2021', '2021-03-01', '', '', '', 82, 3, 1000000, '', 30, '2021-11-22 03:22:06', 'N', '', 'N', 56, 1),
(192, 2, 5, '048/SPM/GFA/09/03/2021', '2021-03-09', '', '', '', 85, 2, 46600, '', 30, '2021-11-22 03:24:11', 'N', '', 'N', 21, 1),
(193, 2, 5, '049/SPM/GFA/12/03/2021', '2021-03-12', '', '', '', 85, 2, 100000, '', 30, '2021-11-22 03:57:27', 'N', '', 'N', 23, 1),
(194, 2, 5, '050/SPM/GFA/16/03/2021', '2021-03-16', '', '', '', 84, 2, 31000, '', 30, '2021-11-22 04:00:40', 'N', '', 'N', 20, 1),
(195, 2, 5, '053/SPM/GFA/25/03/2021', '2021-03-25', '', '', '', 85, 2, 23200, '', 30, '2021-11-22 07:22:14', 'N', '', 'N', 21, 1),
(196, 3, 5, '045/SPM/GFA/01/03/2021', '2021-03-01', '', '', '', 50, 3, 350000, '', 30, '2021-11-22 07:25:41', 'N', '', 'N', 60, 1),
(197, 2, 5, '050/SPM/GFA/15/03/2021', '2021-03-15', '', '', '', 96, 2, 412506, '', 30, '2021-11-22 07:27:43', 'N', '', 'N', 20, 1),
(198, 3, 5, '052/SPM/GFA/16/03/2021', '2021-03-16', '', '', '', 65, 3, 613000, '', 30, '2021-11-22 07:29:40', 'N', '', 'N', 61, 1),
(199, 3, 5, '054/SPM/GFA/25/03/2021', '2021-03-25', '', '', '', 85, 3, 285000, '', 30, '2021-11-22 07:31:44', 'N', '', 'N', 2, 1),
(200, 3, 5, '055/SPM/GFA/25/03/2021', '2021-03-25', '', '', '', 64, 3, 1200000, '', 30, '2021-11-22 07:34:50', 'N', '', 'N', 56, 1),
(201, 5, 5, '056/SPM/GFA/01/03/2021', '2021-03-27', '', '', '', 53, 5, 440980, '', 30, '2021-11-22 07:47:08', 'N', '', 'N', 55, 1),
(202, 3, 5, '057/SPM/GFA/05/04/2021', '2021-04-05', '', '', '', 83, 3, 250000, '', 30, '2021-11-22 07:50:46', 'N', '', 'N', 41, 1),
(203, 3, 5, '058/SPM/GFA/01/04/2021', '2021-04-01', '', '', '', 50, 3, 1000000, '', 30, '2021-11-22 07:53:27', 'N', '', 'N', 56, 1),
(204, 3, 5, '058/SPM/GFA/01/04/2021', '2021-04-01', '', '', '', 50, 3, 250000, '', 30, '2021-11-22 07:54:57', 'N', '', 'N', 21, 1),
(205, 2, 5, '060/SPM/GFA/05/04/2021', '2021-04-05', '', '', '', 85, 2, 187500, '', 30, '2021-11-22 08:05:08', 'N', '', 'N', 21, 1),
(206, 2, 5, '061/SPM/GFA/05/04/2021', '2021-04-05', '', '', '', 83, 2, 750000, '', 30, '2021-11-22 08:10:11', 'N', '', 'N', 17, 1),
(207, 2, 5, '063/SPM/GFA/08/04/2021', '2021-04-08', '', '', '', 87, 2, 90000, '', 30, '2021-11-22 08:21:38', 'N', '', 'N', 21, 1),
(208, 5, 5, '064/SPM/GFA/08/04/2021', '2021-04-12', '', '', '', 104, 5, 16800, '', 30, '2021-11-22 08:24:35', 'N', '', 'N', 55, 1),
(209, 3, 5, '068/SPM/GFA/15/04/2021', '2021-04-15', '', '', '', 65, 3, 350000, '', 30, '2021-11-22 08:26:17', 'N', '', 'N', 2, 1),
(210, 3, 5, '069/SPM/GFA/15/04/2021', '2021-04-15', '', '', '', 83, 3, 100000, '', 30, '2021-11-22 08:28:47', 'N', '', 'N', 59, 1),
(211, 3, 5, '069/SPM/GFA/15/04/2021', '2021-04-15', '', '', '', 104, 3, 6265351, '', 30, '2021-11-22 08:37:27', 'N', '', 'N', 55, 1),
(212, 5, 5, '059/SPM/GFA/01904/2021', '2021-04-10', '', '', '', 53, 5, 2944575, '', 30, '2021-11-22 08:40:58', 'N', '', 'N', 55, 1),
(213, 3, 5, '071/SPM/GFA/19/04/2021', '2021-04-19', '', '', '', 64, 3, 1750000, '', 30, '2021-11-22 08:45:17', 'N', '', 'N', 57, 1),
(214, 5, 5, '071/SPM/GFA/19/04/2021', '2021-04-19', '', '', '', 64, 5, 1000000, '', 30, '2021-11-22 08:46:13', 'N', '', 'N', 57, 1),
(215, 3, 5, '073/SPM/GFA/19/04/2021', '2021-04-19', '', '', '', 95, 3, 51500, '', 30, '2021-11-22 08:47:53', 'N', '', 'N', 19, 1),
(216, 3, 5, '077/SPM/GFA/20/04/2021', '2021-04-20', '', '', '', 64, 3, 1200000, '', 30, '2021-11-22 08:49:18', 'N', '', 'N', 56, 1),
(217, 3, 5, '079/SPM/GFA/26/04/2021', '2021-04-26', '', '', '', 83, 3, 1500000, '', 30, '2021-11-22 08:51:02', 'N', '', 'N', 15, 1),
(218, 3, 5, '081/SPM/GFA/21/04/2021', '2021-04-21', '', '', '', 85, 3, 293000, '', 30, '2021-11-22 08:52:30', 'N', '', 'N', 19, 1),
(219, 5, 5, '085/SPM/GFA/22/04/2021', '2021-04-21', '', '', '', 64, 5, 1300000, '', 30, '2021-11-22 08:54:52', 'N', '', 'N', 7, 1),
(220, 3, 5, '085/SPM/GFA/22/04/2021', '2021-04-22', '', '', '', 21, 3, 5999000, '', 30, '2021-11-22 08:56:11', 'N', '', 'N', 17, 1),
(221, 4, 5, '087/SPM/GFA/29/04/2021', '2021-04-29', '', '', '', 62, 4, 60000, '', 30, '2021-11-22 08:57:58', 'N', '', 'N', 2, 1),
(222, 3, 5, '088/SPM/GFA/30/04/2021', '2021-04-30', '', '', '', 50, 3, 39766850, '', 30, '2021-11-22 08:59:23', 'N', '', 'N', 58, 1),
(223, 3, 5, '089/SPM/GFA/30/04/2021', '2021-04-30', '', '', '', 50, 3, 250000, '', 30, '2021-11-22 09:00:25', 'N', '', 'N', 21, 1),
(224, 3, 5, '090/SPM/GFA/04/05/2021', '2021-05-04', '', '', '', 105, 3, 27134803, '', 30, '2021-11-22 09:02:14', 'N', '', 'N', 58, 1),
(225, 2, 5, '095/SPM/GFA/11/05/2021', '2021-05-11', '', '', '', 53, 2, 270000, '', 30, '2021-11-22 09:03:42', 'N', '', 'N', 58, 1),
(226, 5, 5, '090/SPM/GFA/31/05/2021', '2021-05-31', '', '', '', 50, 5, 39861076, '', 30, '2021-11-22 09:05:45', 'N', '', 'N', 58, 1),
(227, 3, 1, '001/SPM/UMJT/20/01/2021', '2021-01-20', '', '', '', 11, 3, 2416633, '', 30, '2021-11-23 05:40:33', 'N', '', 'N', 49, 1),
(228, 3, 1, '002/SPM/UMJT/12/01/2021', '2021-01-07', '', '', '', 11, 3, 235000, '', 30, '2021-11-23 05:42:25', 'N', '', 'N', 22, 1),
(229, 3, 1, '003/SPM/UMJT/12/01/2021', '2021-01-08', '', '', '', 11, 3, 900000, '', 30, '2021-11-23 05:54:48', 'N', '', 'N', 28, 1),
(230, 3, 1, '003/SPM/UMJT/12/01/2021', '2021-01-08', '', '', '', 11, 3, 2278329, '', 30, '2021-11-23 06:00:52', 'N', '', 'N', 26, 1),
(231, 3, 1, '003/SPM/UMJT/12/01/2021', '2021-01-08', '', '', '', 11, 3, 450000, '', 30, '2021-11-23 06:02:04', 'N', '', 'N', 26, 1),
(232, 3, 1, '004/SPM/UMJT/12/01/2021', '2021-01-09', '', '', '', 11, 3, 520000, '', 30, '2021-11-23 06:12:16', 'N', '', 'N', 30, 1),
(233, 3, 1, '004/SPM/UMJT/12/01/2021', '2021-01-09', '', '', '', 11, 3, 50000, '', 30, '2021-11-23 06:13:19', 'N', '', 'N', 27, 1),
(234, 3, 1, '004/SPM/UMJT/12/01/2021', '2021-01-09', '', '', '', 11, 3, 260000, '', 30, '2021-11-23 06:15:32', 'N', '', 'N', 27, 1),
(235, 3, 1, '004/SPM/UMJT/12/01/2021', '2021-01-09', '', '', '', 11, 3, 912758, '', 30, '2021-11-23 06:18:41', 'N', '', 'N', 27, 1),
(236, 3, 1, '005/SPM/UMJT/26/01/2021', '2021-01-26', '', '', '', 11, 3, 126730, '', 30, '2021-11-23 06:33:20', 'N', '', 'N', 22, 1),
(237, 3, 1, '005/SPM/UMJT/26/01/2021', '2021-01-26', '', '', '', 11, 3, 523000, '', 30, '2021-11-23 06:34:43', 'N', '', 'N', 22, 1),
(238, 3, 1, '006/SPM/UMJT/26/01/2021', '2021-01-26', '', '', '', 11, 3, 1199000, '', 30, '2021-11-23 06:36:47', 'N', '', 'N', 2, 1),
(239, 3, 1, '011/SPM/UMJT/09/02/2021', '2021-02-09', '', '', '', 11, 3, 16000, '', 30, '2021-11-23 06:38:53', 'N', '', 'N', 23, 1),
(240, 3, 1, '017/SPM/UMJT/01/03/2021', '2021-03-02', '', '', '', 85, 3, 300000, '', 30, '2021-11-23 06:40:33', 'N', '', 'N', 22, 1),
(241, 3, 1, '018/SPM/UMJT/08/03/2021', '2021-03-08', '', '', '', 95, 3, 101500, '', 30, '2021-11-23 06:42:42', 'N', '', 'N', 23, 1),
(242, 5, 1, '019/SPM/UMJT/08/03/2021', '2021-03-07', '', '', '', 46, 5, 260000, '', 30, '2021-11-23 06:50:51', 'N', '', 'N', 57, 1),
(243, 5, 1, '019/SPM/UMJT/08/03/2021', '2021-03-07', '', '', '', 46, 5, 50000, '', 30, '2021-11-23 06:52:16', 'N', '', 'N', 22, 1),
(244, 5, 1, '019/SPM/UMJT/08/03/2021', '2021-03-07', '', '', '', 46, 5, 200000, '', 30, '2021-11-23 06:53:15', 'N', '', 'N', 57, 1),
(245, 5, 1, '019/SPM/UMJT/08/03/2021', '2021-03-07', '', '', '', 46, 5, 130000, '', 30, '2021-11-23 06:54:13', 'N', '', 'N', 57, 1),
(246, 5, 1, '019/SPM/UMJT/08/03/2021', '2021-03-07', '', '', '', 57, 5, 120000, '', 30, '2021-11-23 06:55:33', 'N', '', 'N', 57, 1),
(247, 5, 1, '019/SPM/UMJT/08/03/2021', '2021-03-07', '', '', '', 88, 5, 700000, '', 30, '2021-11-23 06:56:35', 'N', '', 'N', 57, 1),
(248, 5, 1, '020/SPM/UMJT/09/03/2021', '2021-03-08', '', '', '', 46, 5, 260000, '', 30, '2021-11-23 07:01:42', 'N', '', 'N', 22, 1),
(249, 5, 1, '020/SPM/UMJT/09/03/2021', '2021-03-08', '', '', '', 46, 5, 200000, '', 30, '2021-11-23 07:04:52', 'N', '', 'N', 22, 1),
(250, 5, 1, '020/SPM/UMJT/09/03/2021', '2021-03-08', '', '', '', 46, 5, 130000, '', 30, '2021-11-23 07:06:21', 'N', '', 'N', 22, 1),
(251, 5, 1, '020/SPM/UMJT/09/03/2021', '2021-03-08', '', '', '', 57, 5, 120000, '', 30, '2021-11-23 07:10:26', 'N', '', 'N', 22, 1),
(252, 5, 1, '020/SPM/UMJT/09/03/2021', '2021-03-08', '', '', '', 88, 5, 721518, '', 30, '2021-11-23 07:12:46', 'N', '', 'N', 22, 1),
(253, 3, 1, '021/SPM/UMJT/10/03/2021', '2021-03-10', '', '', '', 46, 3, 151200, '', 30, '2021-11-23 07:16:31', 'N', '', 'N', 23, 1),
(254, 3, 1, '021/SPM/UMJT/10/03/2021', '2021-03-10', '', '', '', 95, 3, 60000, '', 30, '2021-11-23 07:17:50', 'N', '', 'N', 23, 1),
(255, 3, 1, '022/SPM/UMJT/10/03/2021', '2021-03-12', '', '', '', 21, 3, 150000, '', 30, '2021-11-23 07:19:31', 'N', '', 'N', 22, 1),
(256, 3, 1, '024/SPM/UMJT/15/03/2021', '2021-03-15', '', '', '', 95, 3, 75000, '', 30, '2021-11-23 07:22:51', 'N', '', 'N', 23, 1),
(257, 3, 1, '024/SPM/UMJT/15/03/2021', '2021-03-15', '', '', '', 97, 3, 145000, '', 30, '2021-11-23 07:23:43', 'N', '', 'N', 23, 1),
(258, 5, 1, '025/SPM/UMJT/15/03/2021', '2021-03-13', '', '', '', 46, 5, 1419306, '', 30, '2021-11-23 07:26:16', 'N', '', 'N', 22, 1),
(259, 5, 1, '026/SPM/UMJT/15/03/2021', '2021-03-14', '', '', '', 46, 5, 1435805, '', 30, '2021-11-23 07:27:37', 'N', '', 'N', 22, 1),
(260, 3, 1, '027/SPM/UMJT/15/03/2021', '2021-03-15', '', '', '', 46, 3, 1085483, '', 30, '2021-11-23 07:29:16', 'N', '', 'N', 57, 1),
(261, 2, 1, '028/SPM/UMJT/15/03/2021', '2021-03-16', '', '', '', 46, 2, 1484701, '', 30, '2021-11-23 07:30:40', 'N', '', 'N', 23, 1),
(262, 2, 1, '029/SPM/UMJT/15/03/2021', '2021-03-16', '', '', '', 46, 2, 1432000, '', 30, '2021-11-23 07:31:49', 'N', '', 'N', 23, 1),
(263, 2, 1, '030/SPM/UMJT/15/03/2021', '2021-03-16', '', '', '', 46, 2, 1473234, '', 30, '2021-11-23 07:33:05', 'N', '', 'N', 23, 1),
(264, 5, 1, '031/SPM/UMJT/15/03/2021', '2021-03-14', '', '', '', 46, 5, 1458266, '', 30, '2021-11-23 07:34:14', 'N', '', 'N', 22, 1),
(265, 2, 1, '032/SPM/UMJT/19/03/2021', '2021-03-19', '', '', '', 46, 2, 1467093, '', 30, '2021-11-23 07:35:23', 'N', '', 'N', 23, 1),
(266, 5, 1, '033/SPM/UMJT/19/03/2021', '2021-03-15', '', '', '', 46, 5, 1472843, '', 30, '2021-11-23 07:36:25', 'N', '', 'N', 22, 1),
(267, 3, 1, '034/SPM/UMJT/23/03/2021', '2021-03-23', '', '', '', 97, 3, 200000, '', 30, '2021-11-23 07:37:43', 'N', '', 'N', 23, 1),
(268, 3, 1, '035/SPM/UMJT/25/03/2021', '2021-03-25', '', '', '', 46, 3, 780000, '', 30, '2021-11-23 07:43:19', 'N', '', 'N', 30, 1),
(269, 3, 1, '035/SPM/UMJT/25/03/2021', '2021-03-25', '', '', '', 46, 3, 390000, '', 30, '2021-11-23 07:45:40', 'N', '', 'N', 27, 1),
(270, 3, 1, '035/SPM/UMJT/25/03/2021', '2021-03-25', '', '', '', 88, 3, 1299135, '', 30, '2021-11-23 07:46:55', 'N', '', 'N', 27, 1),
(271, 3, 1, '035/SPM/UMJT/25/03/2021', '2021-03-25', '', '', '', 95, 3, 100000, '', 30, '2021-11-23 07:47:59', 'N', '', 'N', 27, 1),
(272, 3, 1, '036/SPM/UMJT/26/03/2021', '2021-03-26', '', '', '', 46, 3, 180000, '', 30, '2021-11-23 08:20:57', 'N', '', 'N', 22, 1),
(273, 3, 1, '036/SPM/UMJT/26/03/2021', '2021-03-26', '', '', '', 46, 3, 90000, '', 30, '2021-11-23 08:21:47', 'N', '', 'N', 22, 1),
(274, 3, 1, '036/SPM/UMJT/26/03/2021', '2021-03-26', '', '', '', 88, 3, 443526, '', 30, '2021-11-23 08:22:53', 'N', '', 'N', 22, 1),
(275, 3, 1, '036/SPM/UMJT/26/03/2021', '2021-03-26', '', '', '', 95, 3, 30000, '', 30, '2021-11-23 08:23:58', 'N', '', 'N', 22, 1),
(276, 3, 1, '037/SPM/UMJT/26/03/2021', '2021-03-26', '', '', '', 46, 3, 300000, '', 30, '2021-11-23 08:26:16', 'N', '', 'N', 22, 1),
(277, 3, 1, '037/SPM/UMJT/26/03/2021', '2021-03-26', '', '', '', 46, 3, 150000, '', 30, '2021-11-23 08:26:57', 'N', '', 'N', 22, 1),
(278, 3, 1, '037/SPM/UMJT/26/03/2021', '2021-03-26', '', '', '', 88, 3, 850000, '', 30, '2021-11-23 08:27:46', 'N', '', 'N', 22, 1),
(279, 3, 1, '037/SPM/UMJT/26/03/2021', '2021-03-26', '', '', '', 95, 3, 50000, '', 30, '2021-11-23 08:28:42', 'N', '', 'N', 22, 1),
(280, 2, 1, '038/SPM/UMJT/26/03/2021', '2021-03-26', '', '', '', 46, 2, 1427625, '', 30, '2021-11-23 08:29:53', 'N', '', 'N', 23, 1),
(281, 2, 1, '039/SPM/UMJT/26/03/2021', '2021-03-26', '', '', '', 46, 2, 1448703, '', 30, '2021-11-23 08:31:12', 'N', '', 'N', 23, 1),
(282, 2, 1, '040/SPM/UMJT/26/03/2021', '2021-03-26', '', '', '', 88, 2, 300000, '', 30, '2021-11-23 08:32:21', 'N', '', 'N', 23, 1),
(283, 3, 3, '001/SPM/UMJOP/12/01/2021', '2021-01-12', '', '', '', 11, 3, 3505700, '', 30, '2021-11-23 08:37:13', 'N', '', 'N', 23, 1),
(284, 3, 3, '002/SPM/UMJOP/25/01/2021', '2021-01-25', '', '', '', 11, 3, 2250000, '', 30, '2021-11-23 08:39:00', 'N', '', 'N', 2, 1),
(285, 5, 5, '101/GFA/UMJT/07/06/2021', '2021-06-08', '', '', '', 104, 5, 11464603, '', 30, '2021-11-23 08:55:03', 'N', '', 'N', 55, 1),
(286, 2, 5, '101/SPM/GFA/07/06/2021', '2021-06-08', '', '', '', 96, 2, 1040500, '', 30, '2021-11-23 08:56:31', 'N', '', 'N', 21, 1),
(287, 5, 5, '059/SPM/GFA/09/04/2021', '2021-04-10', '', '', '', 96, 5, 568923, '', 30, '2021-11-23 09:07:38', 'N', '', 'N', 62, 1),
(288, 5, 5, '103/SPM/GFA/10/06/2021', '2021-06-17', '', '', '', 104, 5, 6282151, '', 30, '2021-11-23 09:22:05', 'N', '', 'N', 55, 1),
(290, 0, 3, '164/SPM/UMJE/17/06/2021', '2021-06-17', '', '', '', 4, 44, 1496000, 'Pembelian Bahan Bangunan', 34, '0000-00-00 00:00:00', '', '', '', 2, 1),
(291, 0, 3, '165/SPM/UMJE/18/06/2021', '2021-06-18', '', '', '', 4, 47, 2445000, 'Honorium Tukang', 34, '0000-00-00 00:00:00', '', '', '', 51, 1),
(292, 0, 3, '166/SPM/UMJE/25/06/2021', '2021-06-19', '', '', '', 4, 47, 1230000, 'Honorium Tukang', 34, '0000-00-00 00:00:00', '', '', '', 51, 1),
(293, 0, 3, '166/SPM/UMJE/25/06/2021', '2021-06-20', '', '', '', 2, 47, 425000, 'Honorium Tukang', 34, '0000-00-00 00:00:00', '', '', '', 51, 1),
(294, 0, 3, '167/SPM/UMJE/30/06/2021', '2021-06-21', '', '', '', 4, 85, 274500, 'Pembelian Konsumsi', 34, '0000-00-00 00:00:00', '', '', '', 2, 1),
(295, 0, 3, '167/SPM/UMJE/30/06/2021', '2021-06-22', '', '', '', 2, 84, 635500, 'Pembelian ATK', 34, '0000-00-00 00:00:00', '', '', '', 2, 1),
(296, 0, 3, '168/SPM/UMJE/30/06/2021', '2021-06-23', '', '', '', 2, 12, 1438500, 'Cash On Hand Pembelian Bahan Bangunan', 34, '0000-00-00 00:00:00', '', '', '', 34, 1),
(297, 0, 3, '169/SPM/UMJE/01/07/2021', '2021-06-24', '', '', '', 4, 12, 19859596, 'Isi Kas Pembelian Bahan Proyek', 34, '0000-00-00 00:00:00', '', '', '', 34, 1),
(298, 0, 3, '170/SPM/UMJE/02/07/2021', '2021-06-25', '', '', '', 4, 47, 4515000, 'Honorium Tukang', 34, '0000-00-00 00:00:00', '', '', '', 51, 1),
(299, 0, 3, '170/SPM/UMJE/02/07/2021', '2021-06-26', '', '', '', 2, 47, 660000, 'Honorium Tukang', 34, '0000-00-00 00:00:00', '', '', '', 51, 1),
(300, 0, 3, '171/SPM/UMJE/09/07/2021', '2021-06-27', '', '', '', 4, 47, 685000, 'Honorium Tukang', 34, '0000-00-00 00:00:00', '', '', '', 51, 1),
(301, 0, 3, '171/SPM/UMJE/09/07/2021', '2021-06-28', '', '', '', 2, 47, 340000, 'Honorium Tukang', 34, '0000-00-00 00:00:00', '', '', '', 51, 1),
(302, 0, 3, '173/SPM/UMJE/13/07/2021', '2021-06-29', '', '', '', 4, 4, 3750000, 'Pembayaran Pembelian Triplek 8 mm Sengon', 34, '0000-00-00 00:00:00', '', '', '', 2, 1),
(303, 0, 3, '174/SPM/UMJE/13/07/2021', '2021-06-30', '', '', '', 4, 4, 32800000, ' Pembelian Besi HIJ dan Besi Ulir HIJ', 34, '0000-00-00 00:00:00', '', '', '', 2, 1),
(304, 0, 3, '175/SPM/UMJE/16/07/2021', '2021-07-01', '', '', '', 4, 47, 855000, 'Honorium Tukang', 34, '0000-00-00 00:00:00', '', '', '', 51, 1),
(305, 0, 3, '176/SPM/UMJE/19/07/2021', '2021-07-02', '', '', '', 4, 4, 32895000, 'Pembelian Besi 22 Ulir HIJ', 34, '0000-00-00 00:00:00', '', '', '', 2, 1),
(306, 0, 3, '177/SPM/UMJE/23/07/2021', '2021-07-03', '', '', '', 4, 47, 1625000, 'Honorium Tukang', 34, '0000-00-00 00:00:00', '', '', '', 51, 1),
(307, 0, 3, '178/SPM/UMJE/30/07/2021', '2021-07-04', '', '', '', 4, 47, 3945000, 'Honorium Tukang', 34, '0000-00-00 00:00:00', '', '', '', 51, 1),
(308, 0, 3, '179/SPM/UMJE/06/08/2021', '2021-07-05', '', '', '', 4, 47, 1482500, 'Honorium Tukang', 34, '0000-00-00 00:00:00', '', '', '', 51, 1),
(309, 0, 3, '181/SPM/UMJE/09/08/2021', '2021-07-06', '', '', '', 4, 0, 14340000, 'Pengembalian Dana Salah Kirim ke Bendahara 4', 34, '0000-00-00 00:00:00', '', '', '', 49, 1),
(310, 0, 3, '182/SPM/UMJE/13/08/2021', '2021-07-07', '', '', '', 4, 47, 1405000, 'Honorium Tukang', 34, '0000-00-00 00:00:00', '', '', '', 51, 1),
(311, 0, 3, '183/SPM/UMJE/16/08/2021', '2021-07-08', '', '', '', 2, 96, 137500, 'Pembayaran PLN', 34, '0000-00-00 00:00:00', '', '', '', 62, 1),
(312, 0, 3, '184/SPM/UMJE/20/08/2021', '2021-07-09', '', '', '', 2, 47, 490000, 'Honorium Tukang', 34, '0000-00-00 00:00:00', '', '', '', 51, 1),
(313, 0, 3, '185/SPM/UMJE/27/08/2021', '2021-07-10', '', '', '', 4, 47, 395000, 'Honorium Tukang', 34, '0000-00-00 00:00:00', '', '', '', 51, 1),
(314, 0, 3, '186/SPM/UMJE/03/09/2021', '2021-07-11', '', '', '', 4, 47, 600000, 'Honorium Tukang', 34, '0000-00-00 00:00:00', '', '', '', 51, 1),
(315, 0, 3, '187/SPM/UMJE/10/09/2021', '2021-07-12', '', '', '', 4, 47, 1702500, 'Honorium Tukang', 34, '0000-00-00 00:00:00', '', '', '', 51, 1),
(316, 0, 3, '188/SPM/UMJE/17/09/2021', '2021-07-13', '', '', '', 4, 47, 1155000, 'Honorium Tukang', 34, '0000-00-00 00:00:00', '', '', '', 51, 1),
(317, 0, 3, '189/SPM/UMJE/17/09/2021', '2021-07-14', '', '', '', 2, 96, 183200, 'Pembayaran PLN', 34, '0000-00-00 00:00:00', '', '', '', 62, 1),
(318, 0, 3, '190/SPM/UMJE/24/09/2021', '2021-07-15', '', '', '', 4, 47, 1410000, 'Honorium Tukang', 34, '0000-00-00 00:00:00', '', '', '', 51, 1),
(319, 0, 3, '191/SPM/UMJE/27/09/2021', '2021-07-16', '', '', '', 4, 12, 34384500, 'Pembelajaan Kebutuhan Elektrikal Gedung B', 34, '0000-00-00 00:00:00', '', '', '', 52, 1),
(320, 0, 3, '192/SPM/UMJE/01/10/2021', '2021-07-17', '', '', '', 4, 47, 1175000, 'Honorium Tukang', 34, '0000-00-00 00:00:00', '', '', '', 51, 1),
(321, 0, 3, '193/SPM/UMJE/01/10/2021', '2021-07-18', '', '', '', 4, 48, 850000, 'Pembayaran Uji Hammer Test untuk 17 titik', 34, '0000-00-00 00:00:00', '', '', '', 49, 1),
(322, 0, 3, '194/SPM/UMJE/05/10/2021', '2021-07-19', '', '', '', 4, 48, 1500000, 'Pembayaran Tenaga Ahli Tanah dan Ahli Struktur', 34, '0000-00-00 00:00:00', '', '', '', 54, 1),
(323, 0, 3, '195/SPM/UMJE/06/10/2021', '2021-07-20', '', '', '', 4, 44, 1102500, 'Pembayaran Bahan Bangunan ', 34, '0000-00-00 00:00:00', '', '', '', 2, 1),
(324, 0, 3, '197/SPM/UMJE/08/10/2021', '2021-07-21', '', '', '', 2, 47, 2485000, 'Honorium Tukang', 34, '0000-00-00 00:00:00', '', '', '', 51, 1),
(325, 0, 3, '198/SPM/UMJE/08/10/2021', '2021-07-22', '', '', '', 4, 48, 1000000, 'Pembayaran Jasa Uji Sondir', 34, '0000-00-00 00:00:00', '', '', '', 49, 1),
(326, 0, 3, '199/SPM/UMJE/05/12/2021', '2021-07-23', '', '', '', 4, 48, 2500000, 'Pembayaran Tenaga Ahli Tanah dan Ahli Struktur termin 2', 34, '0000-00-00 00:00:00', '', '', '', 54, 1),
(327, 0, 3, '200/SPM/UMJE/15/10/2021', '2021-07-24', '', '', '', 2, 47, 7030500, 'Honorium Tukang', 34, '0000-00-00 00:00:00', '', '', '', 51, 1),
(328, 0, 3, '201/SPM/UMJE/15/10/2021', '2021-07-25', '', '', '', 4, 44, 6875000, 'Pembayaran Pembelian Bahan Bangunan', 34, '0000-00-00 00:00:00', '', '', '', 2, 1),
(329, 0, 3, '202/SPM/UMJE/18/10/2021', '2021-07-26', '', '', '', 4, 48, 1000000, 'Pembayaran Tenaga Ahli Tanah dan Ahli Struktur termin 3', 34, '0000-00-00 00:00:00', '', '', '', 54, 1),
(330, 0, 3, '203/SPM/UMJE/22/10/2021', '2021-07-27', '', '', '', 2, 47, 3185000, 'Honorium Tukang', 34, '0000-00-00 00:00:00', '', '', '', 51, 1),
(331, 0, 3, '204/SPM/UMJE/22/10/2021', '2021-07-28', '', '', '', 4, 44, 4068000, 'Pembayaran Pembelian Bahan Bangunan', 34, '0000-00-00 00:00:00', '', '', '', 2, 1),
(332, 0, 3, '205/SPM/UMJE/25/10/2021', '2021-07-29', '', '', '', 2, 47, 4502000, 'Honorium Tukang', 34, '0000-00-00 00:00:00', '', '', '', 51, 1),
(333, 0, 3, '206/SPM/UMJE/29/10/2021', '2021-07-30', '', '', '', 2, 47, 5156500, 'Honorium Tukang', 34, '0000-00-00 00:00:00', '', '', '', 51, 1),
(334, 0, 3, '207/SPM/UMJE/29/10/2021', '2021-07-31', '', '', '', 4, 44, 1842000, 'Pembayaran Pembelian Bahan Bangunan', 34, '0000-00-00 00:00:00', '', '', '', 2, 1),
(335, 0, 3, '208/SPM/UMJE/29/10/2021', '2021-08-01', '', '', '', 2, 47, 2813000, 'Honorium Tukang', 34, '0000-00-00 00:00:00', '', '', '', 51, 1),
(336, 0, 3, '209/SPM/UMJE/01/11/2021', '2021-08-02', '', '', '', 4, 44, 8649000, 'Pembelian Bahan Bangunan', 34, '0000-00-00 00:00:00', '', '', '', 2, 1),
(337, 0, 3, '210/SPM/UMJE/03/11/2021', '2021-08-03', '', '', '', 4, 44, 3636500, 'Pembelian Bahan Perbaikan AC PAUD YASMIN', 34, '0000-00-00 00:00:00', '', '', '', 2, 1),
(338, 0, 3, '211/SPM/UMJE/04/11/2021', '2021-08-04', '', '', '', 4, 44, 7772000, 'Pembelian Bahan Bangunan', 34, '0000-00-00 00:00:00', '', '', '', 2, 1),
(339, 0, 3, '212/SPM/UMJE/05/11/2021', '2021-08-05', '', '', '', 2, 47, 4833000, 'Honorium Tukang', 34, '0000-00-00 00:00:00', '', '', '', 34, 1),
(340, 0, 3, '213/SPM/UMJE/05/11/2021', '2021-08-06', '', '', '', 4, 44, 1938500, 'Pembelian Bahan Bangunan', 34, '0000-00-00 00:00:00', '', '', '', 2, 1),
(341, 0, 3, '214/SPM/UMJE/08/11/2021', '2021-08-07', '', '', '', 4, 44, 2504000, 'Pembelian Bahan Renov Lab. Ilmu Komunikasi FISIPOL', 34, '0000-00-00 00:00:00', '', '', '', 2, 1),
(342, 0, 3, '215/SPM/UMJE/09/11/2021', '2021-08-08', '', '', '', 4, 44, 4104000, 'Pembelian Bahan Pembuatan Lab.Tanah Pertanian', 34, '0000-00-00 00:00:00', '', '', '', 2, 1),
(343, 0, 3, '216/SPM/UMJE/10/11/2021', '2021-08-09', '', '', '', 4, 49, 2960000, 'Pembayaran Tenaga Borongan Renov Lab. Ilmu Komunikasi FISIPOL', 34, '0000-00-00 00:00:00', '', '', '', 54, 1),
(344, 0, 3, '218/SPM/UMJE/12/11/2021', '2021-08-10', '', '', '', 2, 47, 4787000, 'Honorium Tukang', 34, '0000-00-00 00:00:00', '', '', '', 51, 1),
(345, 0, 3, '218/SPM/UMJE/12/11/2021', '2021-08-11', '', '', '', 4, 47, 500000, 'Honorium Tukang', 34, '0000-00-00 00:00:00', '', '', '', 33, 1),
(346, 0, 3, '219/SPM/UMJE/12/11/2021', '2021-08-12', '', '', '', 4, 44, 2080000, 'Pembelian Bahan Bangunan', 34, '0000-00-00 00:00:00', '', '', '', 2, 1),
(347, 0, 3, '220/SPM/UMJE/13/11/2021', '2021-08-13', '', '', '', 4, 44, 12445000, 'Pembelian Bahan Pertisi Lab Pertanian', 34, '0000-00-00 00:00:00', '', '', '', 2, 1),
(348, 0, 3, '221/SPM/UMJE/16/11/2021', '2021-08-14', '', '', '', 4, 49, 18000000, 'Pembayaran Tenaga Furniture Termin 1 Ruang Dekan FKIP', 34, '0000-00-00 00:00:00', '', '', '', 54, 1),
(349, 0, 3, '222/SPM/UMJE/16/11/2021', '2021-08-15', '', '', '', 4, 48, 1100000, 'Pembayaran Tenaga Las Termin 1 Pembuatan Papan Nama PT.BCA II', 34, '0000-00-00 00:00:00', '', '', '', 54, 1),
(350, 0, 3, '223/SPM/UMJE/17/11/2021', '2021-08-16', '', '', '', 4, 44, 9191500, 'Pembelian Bahan Renov Lab. Ilmu Komunikasi FISIPOL', 34, '0000-00-00 00:00:00', '', '', '', 2, 1),
(351, 0, 3, '225/SPM/UMJE/20/11/2021', '2021-08-17', '', '', '', 4, 44, 1428000, 'Pembelian Bahan Bangunan', 34, '0000-00-00 00:00:00', '', '', '', 2, 1),
(352, 0, 3, '227/SPM/UMJE/23/11/2021', '2021-08-18', '', '', '', 4, 49, 750000, 'Pembayaran Tenaga Borongan Pembuatan Kanopi Bu Berlian', 34, '0000-00-00 00:00:00', '', '', '', 35, 1),
(353, 0, 3, '228/SPM/UMJE/23/11/2021', '2021-08-19', '', '', '', 2, 49, 3366000, 'HR Tenaga Borongan Pemasangan Plafon FAPERTA', 34, '0000-00-00 00:00:00', '', '', '', 32, 1),
(354, 0, 3, '229/SPM/UMJE/24/11/2021', '2021-08-20', '', '', '', 4, 44, 6457500, 'Pembelian Keramik', 34, '0000-00-00 00:00:00', '', '', '', 2, 1),
(355, 0, 3, '230/SPM/UMJE/25/11/2021', '2021-08-21', '', '', '', 4, 48, 5300000, 'Pembayaran Tenaga Las Termin 1 Pembuatan Papan Nama PT.BCA III', 34, '0000-00-00 00:00:00', '', '', '', 54, 1),
(356, 0, 10, '021/SPM/GFA/01/02/2021', '2021-08-22', '', '', '', 3, 63, 6984500, 'Biaya Sambung kWh baru Ruko Pasca Bayar (Ahmad Fauzan)', 34, '0000-00-00 00:00:00', '', '', '', 62, 1),
(357, 0, 10, '022/SPM/GFA/01/02/2021', '2021-08-23', '', '', '', 3, 50, 26176891, 'Gaji Karyawan Bulan Januari', 34, '0000-00-00 00:00:00', '', '', '', 58, 1),
(358, 0, 10, '022/SPM/GFA/01/02/2021', '2021-08-24', '', '', '', 5, 50, 11645282, 'Gaji Karyawan Bulan Januari', 34, '0000-00-00 00:00:00', '', '', '', 58, 1),
(359, 0, 10, '023/SPM/GFA/03/02/2021', '2021-08-25', '', '', '', 2, 57, 170000, 'Biaya Perdin', 34, '0000-00-00 00:00:00', '', '', '', 57, 1),
(360, 0, 10, '024/SPM/GFA/04/02/2021', '2021-08-26', '', '', '', 3, 85, 932000, 'Pembelian Kebutuhan Rumah Tangga Kantor', 34, '0000-00-00 00:00:00', '', '', '', 21, 1),
(361, 0, 10, '025/SPM/GFA/04/02/2021', '2021-08-27', '', '', '', 3, 87, 159000, 'Biaya Jamuan Tamu', 34, '0000-00-00 00:00:00', '', '', '', 17, 1),
(362, 0, 10, '026/SPM/GFA/08/02/2021', '2021-08-28', '', '', '', 2, 57, 90000, 'Biaya Driver Perdin', 34, '0000-00-00 00:00:00', '', '', '', 28, 1),
(363, 0, 10, '027/SPM/GFA/08/02/2021', '2021-08-29', '', '', '', 3, 87, 61000, 'Biaya Jamuan Tamu', 34, '0000-00-00 00:00:00', '', '', '', 17, 1),
(364, 0, 10, '027/SPM/GFA/08/02/2021', '2021-08-30', '', '', '', 3, 87, 545050, 'Biaya Jamuan Tamu', 34, '0000-00-00 00:00:00', '', '', '', 13, 1),
(365, 0, 10, '028/SPM/GFA/10/02/2021', '2021-08-31', '', '', '', 3, 57, 1639600, 'PERDIN LUMAJANG - SURABAYA', 34, '0000-00-00 00:00:00', '', '', '', 57, 1),
(366, 0, 10, '029/SPM/GFA/10/02/2021', '2021-09-01', '', '', '', 2, 85, 32000, 'Konsumsi Pasang Gorden', 34, '0000-00-00 00:00:00', '', '', '', 19, 1),
(367, 0, 10, '030/SPM/GFA/10/02/2021', '2021-09-02', '', '', '', 2, 86, 66000, 'Biaya Kurir', 34, '0000-00-00 00:00:00', '', '', '', 19, 1),
(368, 0, 10, '031/SPM/GFA/16/02/2021', '2021-09-03', '', '', '', 5, 104, 2355660, 'Pembayaran BPJSKes', 34, '0000-00-00 00:00:00', '', '', '', 55, 1),
(369, 0, 10, '032/SPM/GFA/16/02/2021', '2021-09-04', '', '', '', 3, 83, 500000, 'Santunan Kematian Ibu/Bapak', 34, '0000-00-00 00:00:00', '', '', '', 31, 1),
(370, 0, 10, '033/SPM/GFA/16/02/2021', '2021-09-05', '', '', '', 2, 87, 60000, 'Konsumsi Rapat', 34, '0000-00-00 00:00:00', '', '', '', 21, 1);
INSERT INTO `t_jurnalumum` (`IdJurnal`, `idkascoa`, `IdUnit`, `NoTransaksi`, `Tanggal`, `NoRekening`, `Rekening`, `Uraian`, `IdDebet`, `IdKredit`, `Nilai`, `Keterangan`, `IdUser`, `TglSave`, `NU`, `MTransaksi`, `NA`, `IdSup`, `jumlah`) VALUES
(371, 0, 10, '034/SPM/GFA/17/02/2021', '2021-09-06', '', '', '', 5, 104, 6057348, 'Pembayaran BPJSTK', 34, '0000-00-00 00:00:00', '', '', '', 55, 1),
(372, 0, 10, '035/SPM/GFA/17/02/2021', '2021-09-07', '', '', '', 3, 100, 20000000, 'Saldo Awal Pembukaan Rekening PT BSI', 34, '0000-00-00 00:00:00', '', '', '', 50, 1),
(373, 0, 10, '036/SPM/GFA/17/02/2021', '2021-09-08', '', '', '', 2, 103, 90000, 'Pembelian Flasdisk Kantor 16 GB', 34, '0000-00-00 00:00:00', '', '', '', 19, 1),
(374, 0, 10, '037/SPM/GFA/18/02/2021', '2021-09-09', '', '', '', 2, 85, 46300, 'Pembelian Kebutuhan Rumah Tangga Kantor', 34, '0000-00-00 00:00:00', '', '', '', 21, 1),
(375, 0, 10, '038/SPM/GFA/22/02/2021', '2021-09-10', '', '', '', 3, 64, 450000, 'Biaya Marketing Trans (UM dan BBM)', 34, '0000-00-00 00:00:00', '', '', '', 28, 1),
(376, 0, 10, '039/SPM/GFA/22/02/2021', '2021-09-11', '', '', '', 2, 95, 153000, 'Pembelian Pulsa Kantor dan Biaya Parkir', 34, '0000-00-00 00:00:00', '', '', '', 21, 1),
(377, 0, 10, '040/SPM/GFA/22/02/2021', '2021-09-12', '', '', '', 2, 84, 123000, 'Pembelian Materai 10000', 34, '0000-00-00 00:00:00', '', '', '', 21, 1),
(378, 0, 10, '041/SPM/GFA/23/02/2021', '2021-09-13', '', '', '', 3, 64, 1200000, 'Biaya Shooting Podcast', 34, '0000-00-00 00:00:00', '', '', '', 63, 1),
(379, 0, 10, '104/SPM/GFA/14/06/2021', '2021-09-14', '', '', '', 2, 85, 39300, 'Pembelian Kebutuhan Rumah Tangga Kantor', 34, '0000-00-00 00:00:00', '', '', '', 21, 1),
(380, 0, 10, '105/SPM/GFA/25/06/2021', '2021-09-15', '', '', '', 4, 51, 250000, 'Dana Suka Cita Kelahiran Anak', 34, '0000-00-00 00:00:00', '', '', '', 34, 1),
(381, 0, 10, '106/SPM/GFA/25/06/2021', '2021-09-16', '', '', '', 2, 85, 20000, 'Isi Gas LPG', 34, '0000-00-00 00:00:00', '', '', '', 21, 1),
(382, 0, 10, '107/SPM/GFA/29/06/2021', '2021-09-17', '', '', '', 2, 95, 52000, 'Pembelian Pulsa CS', 34, '0000-00-00 00:00:00', '', '', '', 19, 1),
(383, 0, 10, '108/SPM/GFA/30/06/2021', '2021-09-18', '', '', '', 2, 56, 160000, 'Perjalanan Dinas Divisi Engineering Bondowoso ', 34, '0000-00-00 00:00:00', '', '', '', 15, 1),
(384, 0, 10, '110/SPM/GFA/01/07/2021', '2021-09-19', '', '', '', 4, 96, 465627, 'Pembayaran PLN PT MKG Bulan Juli', 34, '0000-00-00 00:00:00', '', '', '', 19, 1),
(385, 0, 10, '110/SPM/GFA/01/07/2021', '2021-09-20', '', '', '', 2, 96, 3000, 'Kekurangan Pembayaran Biaya Administrasi PLN PT MKG Bulan Juli', 34, '0000-00-00 00:00:00', '', '', '', 19, 1),
(386, 0, 10, '111/SPM/GFA/02/07/2021', '2021-09-21', '', '', '', 4, 83, 500000, 'Uang Duka Cita Ibunda Robi Andri Oktafianto ', 34, '0000-00-00 00:00:00', '', '', '', 18, 1),
(387, 0, 10, '112/SPM/GFA/05/07/2021', '2021-09-22', '', '', '', 5, 104, 8432400, 'Pembayaran BPJS Kesehatan dan BPJS TK Bulan Juli 2021 ', 34, '0000-00-00 00:00:00', '', '', '', 55, 1),
(388, 0, 10, '115/SPM/GFA/07/07/2021', '2021-09-23', '', '', '', 4, 95, 52000, 'Pembelian Pulsa CS Ke Toko Koperasi', 34, '0000-00-00 00:00:00', '', '', '', 2, 1),
(389, 0, 10, '116/SPM/GFA/12/07/2021', '2021-09-24', '', '', '', 2, 53, 100000, 'Biaya Rapid Tes Robi', 34, '0000-00-00 00:00:00', '', '', '', 18, 1),
(390, 0, 10, '117/SPM/GFA/14/07/2021', '2021-09-25', '', '', '', 2, 84, 199500, 'Pembelian Kebutuhan Rumah Tangga Kantor', 34, '0000-00-00 00:00:00', '', '', '', 19, 1),
(391, 0, 10, '117/SPM/GFA/14/07/2021', '2021-09-26', '', '', '', 2, 85, 438050, 'Pembelian Alat Tulis Kantor', 34, '0000-00-00 00:00:00', '', '', '', 19, 1),
(392, 0, 10, '118/SPM/GFA/16/07/2021', '2021-09-27', '', '', '', 2, 53, 205500, 'Pembelian Vitamin Karyawan', 34, '0000-00-00 00:00:00', '', '', '', 19, 1),
(393, 0, 10, '119/SPM/GFA/22/07/2021', '2021-09-28', '', '', '', 4, 53, 1000000, 'Pengadaan Vitamin Karyawan PT MKG', 34, '0000-00-00 00:00:00', '', '', '', 17, 1),
(394, 0, 10, '120/SPM/GFA/23/07/2021', '2021-09-29', '', '', '', 2, 53, 252500, ' Pembelian Multivitamin', 34, '0000-00-00 00:00:00', '', '', '', 19, 1),
(395, 0, 10, '121/SPM/GFA/26/07/2021', '2021-09-30', '', '', '', 2, 53, 249000, ' Pembelian Multivitamin 3 Botol', 34, '0000-00-00 00:00:00', '', '', '', 19, 1),
(396, 0, 10, '126/SPM/GFA/28/07/2021', '2021-10-01', '', '', '', 2, 53, 385000, ' Pembelian Multivitamin 5 Botol', 34, '0000-00-00 00:00:00', '', '', '', 19, 1),
(397, 0, 10, '122/SPM/GFA/26/07/2021', '2021-10-02', '', '', '', 4, 53, 250000, 'Biaya Swab Antigen', 34, '0000-00-00 00:00:00', '', '', '', 27, 1),
(398, 0, 10, '123/SPM/GFA/27/07/2021', '2021-10-03', '', '', '', 4, 53, 250000, 'Biaya Swab Antigen', 34, '0000-00-00 00:00:00', '', '', '', 32, 1),
(399, 0, 10, '124/SPM/GFA/27/07/2021', '2021-10-04', '', '', '', 4, 83, 500000, 'Uang Duka Cita Ayahanda Samsul Hadi', 34, '0000-00-00 00:00:00', '', '', '', 39, 1),
(400, 0, 10, '128/SPM/GFA/02/08/2021', '2021-10-05', '', '', '', 4, 53, 135000, 'Biaya Swab Antigen', 34, '0000-00-00 00:00:00', '', '', '', 18, 1),
(401, 0, 10, '128/SPM/GFA/02/08/2021', '2021-10-06', '', '', '', 4, 53, 135000, 'Biaya Swab Antigen', 34, '0000-00-00 00:00:00', '', '', '', 19, 1),
(402, 0, 10, '130/SPM/GFA/05/08/2021', '2021-10-07', '', '', '', 5, 104, 8199425, 'Pembayaran BPJS Kesehatan dan BPJS TK Bulan Agustus 2021 ', 34, '0000-00-00 00:00:00', '', '', '', 55, 1),
(403, 0, 10, '131/SPM/GFA/05/08/2021', '2021-10-08', '', '', '', 4, 96, 358974, 'Pembayaran PLN Bulan Agustus 2021', 34, '0000-00-00 00:00:00', '', '', '', 19, 1),
(404, 0, 10, '133/SPM/GFA/05/08/2021', '2021-10-09', '', '', '', 4, 0, 1500000, 'Biaya Parkir Shuttle Bus SBY UMJ Trans ', 34, '0000-00-00 00:00:00', '', '', '', 17, 1),
(405, 0, 10, '134/SPM/GFA/09/08/2021', '2021-10-10', '', '', '', 4, 95, 76500, 'Pembelian Pulsa CS', 34, '0000-00-00 00:00:00', '', '', '', 19, 1),
(406, 0, 10, '135/SPM/GFA/25/08/2021', '2021-10-11', '', '', '', 4, 83, 500000, 'Uang Duka Cita Ibunda Yasin Mustakim', 34, '0000-00-00 00:00:00', '', '', '', 36, 1),
(407, 0, 10, '138/SPM/GFA/01/09/2021', '2021-10-12', '', '', '', 5, 104, 11694049, 'Pembayaran BPJS Kesehatan dan BPJS TK Bulan September 2021 ', 34, '0000-00-00 00:00:00', '', '', '', 55, 1),
(408, 0, 10, '139/SPM/GFA/02/09/2021', '2021-10-13', '', '', '', 2, 96, 352200, 'Pembayaran PLN Bulan September 2021 dan Biaya Admin ', 34, '0000-00-00 00:00:00', '', '', '', 21, 1),
(409, 0, 10, '142/SPM/GFA/08/09/2021', '2021-10-14', '', '', '', 4, 85, 240000, 'Pembayaran Moyamu Juni - Agustus', 34, '0000-00-00 00:00:00', '', '', '', 2, 1),
(410, 0, 10, '142/SPM/GFA/08/09/2021', '2021-10-15', '', '', '', 2, 95, 51500, 'Pembelian Pulsa Kantor', 34, '0000-00-00 00:00:00', '', '', '', 19, 1),
(411, 0, 10, '143/SPM/GFA/17/09/2021', '2021-10-16', '', '', '', 2, 85, 374400, 'Pembelian Kebutuhan Rumah Tangga Kantor', 34, '0000-00-00 00:00:00', '', '', '', 19, 1),
(412, 0, 10, '143/SPM/GFA/17/09/2021', '2021-10-17', '', '', '', 2, 85, 104000, 'Pembelian Alat Tulis Kantor', 34, '0000-00-00 00:00:00', '', '', '', 19, 1),
(413, 0, 10, '146/SPM/GFA/27/09/2021', '2021-10-18', '', '', '', 2, 85, 20000, 'Pengisian LPG 3Kg', 34, '0000-00-00 00:00:00', '', '', '', 21, 1),
(414, 0, 10, '147/SPM/GFA/01/10/2021', '2021-10-19', '', '', '', 4, 0, 250000, 'HR Bulan September Overtime Umum', 34, '0000-00-00 00:00:00', '', '', '', 21, 1),
(415, 0, 10, '148/SPM/GFA/04/10/2021', '2021-10-20', '', '', '', 2, 96, 461800, 'Pembayaran PLN Bulan Oktober 2021 dan Biaya Admin', 34, '0000-00-00 00:00:00', '', '', '', 21, 1),
(416, 0, 10, '149/SPM/GFA/04/10/2021', '2021-10-21', '', '', '', 5, 104, 13137761, 'Pembayaran BPJS Kesehatan dan BPJS TK Bulan Oktokber 2021 ', 34, '0000-00-00 00:00:00', '', '', '', 55, 1),
(417, 4, 5, 'FT2200412WZ3', '2022-01-04', '', '', '', 4, 4, 1000000, '', 30, '2022-01-07 03:04:45', 'N', '', 'N', 58, 1),
(418, 4, 5, 'FT22004489PG ', '2022-01-04', '', '', '', 4, 4, 1000000, '', 30, '2022-01-07 03:07:14', 'N', '', 'N', 58, 1),
(419, 4, 5, 'FT220043Y5KN ', '2022-01-04', '', '', '', 4, 4, 1000000, '', 30, '2022-01-07 03:08:52', 'N', '', 'N', 58, 1),
(420, 4, 3, 'FIRDAUS ZULKARNAIN', '2022-01-04', '', '', '', 4, 4, 1300000, '', 30, '2022-01-07 03:10:28', 'N', '', 'N', 2, 1),
(421, 2, 5, '003/SPM/GFA/04/01/2022', '2022-01-05', '', '', '', 2, 2, 528600, '', 30, '2022-01-07 03:13:25', 'N', '', 'N', 62, 1),
(422, 4, 2, 'FT22005M0TMZ ', '2022-01-05', '', '', '', 4, 4, 6200000, '', 30, '2022-01-07 06:10:20', 'N', '', 'N', 51, 1),
(423, 2, 2, '003/SPM/UMJE/07/01/2022', '2022-01-07', '', '', '', 2, 2, 145000, '', 30, '2022-01-07 07:22:39', 'N', '', 'N', 51, 1),
(424, 2, 2, '003/SPM/UMJE/07/01/2022', '2022-01-07', '', '', '', 2, 2, 169000, '', 30, '2022-01-07 07:24:10', 'N', '', 'N', 51, 1),
(425, 2, 2, '003/SPM/UMJE/07/01/2022', '2022-01-07', '', '', '', 2, 2, 640000, '', 30, '2022-01-07 07:25:29', 'N', '', 'N', 51, 1),
(426, 2, 2, '003/SPM/UMJE/07/01/2022', '2022-01-07', '', '', '', 2, 2, 2000000, '', 30, '2022-01-07 07:27:10', 'N', '', 'N', 51, 1),
(427, 2, 2, '003/SPM/UMJE/07/01/2022', '2022-01-07', '', '', '', 2, 2, 2300000, '', 30, '2022-01-07 07:28:33', 'N', '', 'N', 51, 1),
(428, 2, 2, '003/SPM/UMJE/07/01/2022', '2022-01-07', '', '', '', 2, 2, 965000, '', 30, '2022-01-07 07:29:46', 'N', '', 'N', 51, 1),
(429, 4, 1, 'FT22007QDTDK ', '2022-01-07', '', '', '', 4, 4, 1575000, '', 30, '2022-01-07 07:37:21', 'N', '', 'N', 14, 1),
(430, 2, 6, '001/PGJ/SEKLAB YASMIN/I/2022', '2022-01-03', '', '', '', 2, 2, 1800000, '', 30, '2022-01-10 08:56:35', 'N', '', 'N', 2, 1),
(431, 4, 6, ' FT22004LD31J ', '2022-01-04', '', '', '', 4, 4, 490000, '', 30, '2022-01-10 08:58:50', 'N', '', 'N', 2, 1),
(432, 2, 5, '005/SPM/GFA/10/01/2022', '2022-01-10', '', '', '', 2, 2, 20000, '', 30, '2022-01-10 09:00:07', 'N', '', 'N', 21, 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_kasir`
--

CREATE TABLE `t_kasir` (
  `id_kasir` int(11) NOT NULL,
  `id_pengajuan` int(11) NOT NULL,
  `tgl_acc` date NOT NULL DEFAULT current_timestamp(),
  `status` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_kasir`
--

INSERT INTO `t_kasir` (`id_kasir`, `id_pengajuan`, `tgl_acc`, `status`) VALUES
(1, 84, '2021-05-24', NULL),
(2, 116, '2021-05-24', NULL),
(3, 85, '2021-05-24', NULL),
(4, 87, '2021-05-24', NULL),
(5, 117, '2021-05-24', NULL),
(6, 86, '2021-05-24', NULL),
(7, 88, '2021-05-24', NULL),
(8, 89, '2021-05-24', NULL),
(9, 90, '2021-05-24', NULL),
(10, 94, '2021-05-24', NULL),
(11, 91, '2021-05-24', NULL),
(12, 99, '2021-05-24', NULL),
(13, 92, '2021-05-24', NULL),
(14, 102, '2021-05-24', NULL),
(15, 93, '2021-05-24', NULL),
(17, 95, '2021-05-24', NULL),
(18, 105, '2021-05-24', NULL),
(19, 96, '2021-05-24', NULL),
(20, 106, '2021-05-24', NULL),
(21, 97, '2021-05-24', NULL),
(22, 110, '2021-05-24', NULL),
(23, 98, '2021-05-24', NULL),
(25, 100, '2021-05-24', NULL),
(26, 114, '2021-05-24', NULL),
(27, 101, '2021-05-24', NULL),
(28, 104, '2021-05-24', NULL),
(29, 107, '2021-05-24', NULL),
(30, 108, '2021-05-24', NULL),
(31, 111, '2021-05-24', NULL),
(32, 112, '2021-05-24', NULL),
(33, 83, '2021-05-24', NULL),
(34, 115, '2021-05-24', NULL),
(36, 164, '2021-06-08', NULL),
(37, 129, '2021-06-08', NULL),
(38, 130, '2021-06-08', NULL),
(39, 131, '2021-06-08', NULL),
(40, 132, '2021-06-08', NULL),
(41, 133, '2021-06-08', NULL),
(42, 134, '2021-06-08', NULL),
(43, 135, '2021-06-08', NULL),
(44, 136, '2021-06-08', NULL),
(45, 138, '2021-06-08', NULL),
(46, 139, '2021-06-08', NULL),
(47, 141, '2021-06-08', NULL),
(48, 142, '2021-06-08', NULL),
(49, 152, '2021-06-08', NULL),
(50, 153, '2021-06-08', NULL),
(51, 156, '2021-06-08', NULL),
(52, 157, '2021-06-08', NULL),
(53, 158, '2021-06-08', NULL),
(54, 159, '2021-06-08', NULL),
(55, 160, '2021-06-08', NULL),
(56, 161, '2021-06-08', NULL),
(57, 162, '2021-06-08', NULL),
(58, 163, '2021-06-08', NULL),
(59, 168, '2021-06-08', NULL),
(60, 169, '2021-06-08', NULL),
(61, 170, '2021-06-08', NULL),
(62, 171, '2021-06-08', NULL),
(63, 172, '2021-06-08', NULL),
(64, 173, '2021-06-08', NULL),
(65, 174, '2021-06-08', NULL),
(66, 181, '2021-06-08', NULL),
(67, 183, '2021-06-08', NULL),
(68, 184, '2021-06-08', NULL),
(69, 186, '2021-06-08', NULL),
(70, 187, '2021-06-08', NULL),
(71, 192, '2021-06-08', NULL),
(72, 193, '2021-06-08', NULL),
(73, 194, '2021-06-08', NULL),
(74, 195, '2021-06-08', NULL),
(75, 196, '2021-06-08', NULL),
(76, 197, '2021-06-08', NULL),
(77, 198, '2021-06-08', NULL),
(78, 199, '2021-06-08', NULL),
(79, 200, '2021-06-08', NULL),
(80, 201, '2021-06-08', NULL),
(81, 202, '2021-06-08', NULL),
(82, 203, '2021-06-08', NULL),
(83, 204, '2021-06-08', NULL),
(84, 205, '2021-06-08', NULL),
(85, 206, '2021-06-08', NULL),
(86, 207, '2021-06-08', NULL),
(87, 208, '2021-06-08', NULL),
(88, 209, '2021-06-08', NULL),
(89, 210, '2021-06-08', NULL),
(90, 211, '2021-06-08', NULL),
(91, 165, '2021-06-08', NULL),
(92, 166, '2021-06-08', NULL),
(93, 167, '2021-06-08', NULL),
(94, 175, '2021-06-08', NULL),
(95, 176, '2021-06-08', NULL),
(96, 177, '2021-06-08', NULL),
(97, 178, '2021-06-08', NULL),
(98, 179, '2021-06-08', NULL),
(99, 180, '2021-06-08', NULL),
(100, 182, '2021-06-08', NULL),
(101, 185, '2021-06-08', NULL),
(102, 188, '2021-06-08', NULL),
(103, 189, '2021-06-08', NULL),
(104, 190, '2021-06-08', NULL),
(105, 191, '2021-06-08', NULL),
(106, 212, '2021-06-10', NULL),
(107, 213, '2021-06-10', NULL),
(108, 214, '2021-06-10', NULL),
(109, 216, '2021-06-10', NULL),
(110, 217, '2021-06-10', NULL),
(111, 218, '2021-06-10', NULL),
(112, 219, '2021-06-10', NULL),
(113, 220, '2021-06-10', NULL),
(114, 221, '2021-06-10', NULL),
(115, 222, '2021-06-10', NULL),
(116, 223, '2021-06-10', NULL),
(117, 224, '2021-06-10', NULL),
(118, 228, '2021-06-10', NULL),
(119, 229, '2021-06-10', NULL),
(120, 230, '2021-06-10', NULL),
(121, 231, '2021-06-10', NULL),
(122, 233, '2021-06-10', NULL),
(123, 235, '2021-06-10', NULL),
(124, 236, '2021-06-10', NULL),
(125, 238, '2021-06-10', NULL),
(126, 239, '2021-06-10', NULL),
(127, 240, '2021-06-10', NULL),
(128, 241, '2021-06-10', NULL),
(129, 242, '2021-06-10', NULL),
(130, 243, '2021-06-10', NULL),
(131, 248, '2021-06-10', NULL),
(132, 249, '2021-06-10', NULL),
(133, 250, '2021-06-10', NULL),
(134, 253, '2021-06-10', NULL),
(135, 255, '2021-06-10', NULL),
(136, 256, '2021-06-10', NULL),
(137, 257, '2021-06-10', NULL),
(138, 259, '2021-06-10', NULL),
(139, 261, '2021-06-10', NULL),
(140, 262, '2021-06-10', NULL),
(141, 264, '2021-06-10', NULL),
(142, 265, '2021-06-10', NULL),
(143, 266, '2021-06-10', NULL),
(144, 267, '2021-06-10', NULL),
(145, 268, '2021-06-10', NULL),
(146, 269, '2021-06-10', NULL),
(147, 284, '2021-06-10', NULL),
(148, 285, '2021-06-10', NULL),
(149, 286, '2021-06-10', NULL),
(150, 290, '2021-06-10', NULL),
(151, 215, '2021-06-10', NULL),
(152, 225, '2021-06-10', NULL),
(153, 232, '2021-06-10', NULL),
(154, 234, '2021-06-10', NULL),
(155, 237, '2021-06-10', NULL),
(156, 245, '2021-06-10', NULL),
(157, 246, '2021-06-10', NULL),
(158, 254, '2021-06-10', NULL),
(159, 258, '2021-06-10', NULL),
(160, 263, '2021-06-10', NULL),
(161, 270, '2021-06-10', NULL),
(162, 271, '2021-06-10', NULL),
(163, 272, '2021-06-10', NULL),
(164, 273, '2021-06-10', NULL),
(165, 274, '2021-06-10', NULL),
(166, 275, '2021-06-10', NULL),
(167, 276, '2021-06-10', NULL),
(168, 277, '2021-06-10', NULL),
(169, 278, '2021-06-10', NULL),
(170, 279, '2021-06-10', NULL),
(171, 280, '2021-06-10', NULL),
(172, 282, '2021-06-10', NULL),
(173, 283, '2021-06-10', NULL),
(174, 287, '2021-06-10', NULL),
(177, 281, '2021-06-10', NULL),
(178, 291, '2021-06-10', NULL),
(179, 292, '2021-06-10', NULL),
(180, 293, '2021-06-14', NULL),
(181, 294, '2021-06-14', NULL),
(182, 295, '2021-06-14', NULL),
(183, 296, '2021-06-14', NULL),
(184, 297, '2021-06-14', NULL),
(185, 298, '2021-06-14', NULL),
(186, 299, '2021-06-14', NULL),
(187, 300, '2021-06-14', NULL),
(188, 301, '2021-06-14', NULL),
(189, 302, '2021-06-14', NULL),
(190, 303, '2021-06-14', NULL),
(191, 304, '2021-06-14', NULL),
(192, 305, '2021-06-14', NULL),
(193, 306, '2021-06-14', NULL),
(194, 307, '2021-06-14', NULL),
(195, 308, '2021-06-14', NULL),
(196, 309, '2021-06-14', NULL),
(197, 310, '2021-06-14', NULL),
(198, 311, '2021-06-14', NULL),
(199, 312, '2021-06-14', NULL),
(200, 313, '2021-06-14', NULL),
(201, 314, '2021-06-14', NULL),
(202, 315, '2021-06-14', NULL),
(203, 316, '2021-06-14', NULL),
(204, 317, '2021-06-14', NULL),
(205, 318, '2021-06-14', NULL),
(206, 319, '2021-06-14', NULL),
(207, 320, '2021-06-14', NULL),
(208, 321, '2021-06-14', NULL),
(209, 322, '2021-06-14', NULL),
(210, 323, '2021-06-14', NULL),
(211, 324, '2021-06-14', NULL),
(212, 325, '2021-06-14', NULL),
(213, 326, '2021-06-14', NULL),
(214, 327, '2021-06-14', NULL),
(215, 328, '2021-06-14', NULL),
(216, 329, '2021-06-14', NULL),
(217, 330, '2021-06-14', NULL),
(218, 331, '2021-06-14', NULL),
(219, 332, '2021-06-14', NULL),
(220, 333, '2021-06-14', NULL),
(221, 334, '2021-06-14', NULL),
(222, 118, '2021-06-18', NULL),
(226, 122, '2021-06-18', NULL),
(227, 123, '2021-06-18', NULL),
(228, 124, '2021-06-18', NULL),
(229, 125, '2021-06-18', NULL),
(230, 126, '2021-06-18', NULL),
(231, 127, '2021-06-18', NULL),
(233, 335, '2021-06-18', NULL),
(234, 336, '2021-06-18', NULL),
(235, 337, '2021-06-18', NULL),
(236, 338, '2021-06-18', NULL),
(237, 339, '2021-06-18', NULL),
(238, 340, '2021-06-18', NULL),
(239, 341, '2021-06-18', NULL),
(240, 342, '2021-06-18', NULL),
(241, 343, '2021-06-18', NULL),
(242, 344, '2021-06-18', NULL),
(243, 345, '2021-06-18', NULL),
(244, 346, '2021-06-18', NULL),
(245, 347, '2021-06-18', NULL),
(246, 348, '2021-06-18', NULL),
(247, 349, '2021-06-18', NULL),
(248, 350, '2021-06-18', NULL),
(249, 351, '2021-06-18', NULL),
(250, 352, '2021-09-27', NULL),
(251, 353, '2021-09-27', NULL),
(252, 354, '2021-09-27', NULL),
(253, 355, '2021-09-27', NULL),
(254, 356, '2021-09-27', NULL),
(270, 113, '2021-11-23', NULL),
(271, 288, '2021-11-23', NULL),
(272, 289, '2021-11-23', NULL),
(273, 414, '2022-01-07', NULL),
(274, 409, '2022-01-07', NULL),
(275, 412, '2022-01-07', NULL),
(276, 410, '2022-01-07', NULL),
(277, 413, '2022-01-07', NULL),
(278, 415, '2022-01-07', NULL),
(279, 416, '2022-01-07', NULL),
(280, 411, '2022-01-07', NULL),
(281, 418, '2022-01-07', NULL),
(282, 417, '2022-01-07', NULL),
(283, 424, '2022-01-07', NULL),
(284, 420, '2022-01-07', NULL),
(285, 421, '2022-01-07', NULL),
(286, 422, '2022-01-07', NULL),
(287, 423, '2022-01-07', NULL),
(288, 419, '2022-01-10', NULL),
(289, 425, '2022-01-10', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `t_pengajuan`
--

CREATE TABLE `t_pengajuan` (
  `id` int(11) NOT NULL,
  `id_mak_4` int(11) NOT NULL,
  `no_pengajuan` varchar(100) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `pengajuan` text NOT NULL,
  `tgl_pengajuan` date NOT NULL DEFAULT current_timestamp(),
  `status` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status_hapus` enum('hapus','ada') NOT NULL DEFAULT 'ada',
  `user_manager` varchar(100) NOT NULL,
  `id_divisi` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_pengajuan`
--

INSERT INTO `t_pengajuan` (`id`, `id_mak_4`, `no_pengajuan`, `judul`, `pengajuan`, `tgl_pengajuan`, `status`, `user_id`, `status_hapus`, `user_manager`, `id_divisi`) VALUES
(1, 6, '3/SPM/UMJOP/02/02/2021', 'Belanja kertas HVS 70 A4 PPO', '<p>Belanja kertas HVS 70 A4 PPO</p>', '2021-02-03', 'PO03', 36, 'ada', 'UMJOP', 3),
(3, 10, '9/SPM/UMJT/01/02/2021', 'Konsumsi', '<p>Konsumsi</p>', '2021-02-03', 'PO03', 35, 'ada', 'UMJT', 1),
(4, 10, '10/SPM/UMJT/03/02/2021', 'Pulsa UMJ Trans', '<p>Pulsa UMJ Trans</p>', '2021-02-03', 'PO03', 35, 'ada', 'UMJT', 1),
(5, 1, '22/SPM/GFA/01/02/2021', 'Gaji Karyawan', '<p>Gaji Karyawan</p>', '2021-02-03', 'PO03', 46, 'ada', 'GFA', 5),
(6, 8, '21/SPM/GFA/01/02/2021', 'Biaya kWh Pasca Bayar', '<p>Biaya kWh Pasca Bayar</p>', '2021-02-03', 'PO03', 46, 'ada', 'GFA', 5),
(7, 9, '23/SPM/GFA/03/02/2021', 'Perjalanan Dinas', '<p>Pak Muhaimin 40.000</p><p>Pak Nur 40.000</p><p>Pak Edy 90.000</p>', '2021-02-03', 'PO03', 46, 'ada', 'GFA', 5),
(8, 4, '69/SPM/UMJE/01/02/2021', 'Cash On Hand Persiapan Pekerjaan Pembangunan SD Muhammadiyah', '<p>Cash On Hand Persiapan Pekerjaan Pembangunan SD Muhammadiyah</p>', '2021-02-03', 'PO03', 33, 'ada', 'UMJE', 2),
(9, 4, '71/SPM/UMJE/01/02/2021', 'Cash On Hand Persiapan Pekerjaan Pembangunan SD Muhammadiyah', '<p>Cash On Hand Persiapan Pekerjaan Pembangunan SD Muhammadiyah</p>', '2021-02-03', 'PO03', 33, 'ada', 'UMJE', 2),
(10, 11, '70/SPM/UMJE/01/02/2021', 'Pengajuan Pembayaran RTK Div. Engineering Januari', '<p>Pengajuan Pembayaran RTK Div. Engineering Januari&nbsp;</p>', '2021-02-03', 'PO03', 33, 'ada', 'UMJE', 2),
(11, 12, '72/SPM/UMJE/02/02/2021', 'Pembelian Dispenser Miyako WDP 300', '<p>Pembelian Dispenser Miyako WDP 300 Toko Thomas Jember</p>', '2021-02-03', 'PO03', 33, 'ada', 'UMJE', 2),
(12, 4, '73/SPM/UMJE/03/02/2021', 'Pengajuan Dana Pengukuran Pemasangan Patok Titik Bangunan SD Muhammadiyah', '<p>Pengajuan Dana Pengukuran Pemasangan Patok Titik Bangunan SD Muhammadiyah</p>', '2021-02-03', 'PO03', 33, 'ada', 'UMJE', 2),
(13, 4, '74/SPM/UMJE/03/02/2021', 'Pengajuan Tambahan Pembayaran Sewa Alat Berat Excavator Proyek Persiapan Pembangunan SD Muhammadiyah', '<p>Pengajuan Tambahan Pembayaran Sewa Alat Berat Excavator Proyek Persiapan Pembangunan SD Muhammadiyah</p>', '2021-02-03', 'PO03', 33, 'ada', 'UMJE', 2),
(14, 5, '75/SPM/UMJE/04/02/2021', 'Pembelian Bahan Bangunan Renovasi Rumah Bu Veny', '<p>Pembelian Bahan Bangunan Renovasi Rumah Bu Veny</p>', '2021-02-03', 'PO03', 33, 'ada', 'UMJE', 2),
(15, 9, '24/SPM/GFA/04/02/2021', 'Biaya RTK', '<p>Biaya RTK</p>', '2021-02-03', 'PO03', 46, 'ada', 'GFA', 5),
(77, 9, '25/SPM/GFA/04/02/2021', 'Biaya Jamuan Tamu', '<p>Biaya Jamuan Tamu</p>', '2021-02-04', 'PO03', 46, 'ada', 'GFA', 5),
(78, 9, '26/SPM/GFA/08/02/2021', 'PERDIN', '<p>PERDIN</p>', '2021-02-08', 'PO03', 46, 'ada', 'GFA', 5),
(79, 9, '27/SPM/GFA/08/02/2021', 'Konsumsi dan Hadiah Tamu', '<p>Konsumsi dan Hadiah Tamu</p>', '2021-02-08', 'PO03', 46, 'ada', 'GFA', 5),
(80, 13, '76/SPM/UMJE/04/02/2021', 'Pengajuan Cash On Hand', '<p>Pengajuan Cash On Hand</p>', '2021-02-09', 'PO03', 33, 'ada', 'UMJE', 2),
(83, 8, '019/SPM/GFA/27/01/2021\r\n', 'Pembelian Remote AC', '<p>Pembelian Remote AC mas eka</p>', '2021-01-27', 'PO03', 46, 'ada', 'GFA', 5),
(84, 7, '001/SPM/GFA/05/01/2021', 'Pembelian Handphone CS', '<p>&nbsp;</p>', '2021-01-05', 'PO03', 46, 'ada', 'GFA', 5),
(85, 10, '001/SPM/UMJT/20/01/2021', 'BPJS', '<p>Tagihan BPJS Kesehatan &amp; Ketenagakerjaan</p>', '2021-01-20', 'PO03', 35, 'ada', 'UMJT', 1),
(86, 10, '002/SPM/UMJT/12/01/2021', 'PEMBELIAN OLI', '<p>PEMBELIAN OLI</p>', '2021-01-12', 'PO03', 35, 'ada', 'UMJT', 1),
(87, 9, '002/SPM/GFA/06/01/2021\r\n', 'Pembelian Operasional Kantor', '<p>&nbsp;</p>', '2021-01-06', 'PO03', 46, 'ada', 'GFA', 5),
(88, 9, '003/SPM/GFA/12/01/2021\r\n', 'Pembelian Operasional Kantor', '<p>&nbsp;</p>', '2021-01-12', 'PO03', 46, 'ada', 'GFA', 5),
(89, 2, '003/SPM/UMJT/12/01/2021', 'PREMI TRIP Yogyakarta ', '<p>Premi Trip Yogyakarta&nbsp;</p>', '2021-01-12', 'PO03', 35, 'ada', 'UMJT', 1),
(90, 7, '004/SPM/GFA/-/01/2021', 'Pembelian Mebelair', '<p>&nbsp;</p>', '2021-01-05', 'PO03', 46, 'ada', 'GFA', 5),
(91, 9, '005/SPM/GFA/-/01/2021\r\n', 'Pembelian Alat Kebersihan', '<p>&nbsp;</p>', '2021-01-05', 'PO03', 46, 'ada', 'GFA', 5),
(92, 7, '006/SPM/GFA/-/01/2021\r\n', 'Pembelian Laptop Programmer dan Manager', '<p>&nbsp;</p>', '2021-01-06', 'PO03', 46, 'ada', 'GFA', 5),
(93, 9, '007/SPM/GFA/-/01/2021\r\n', ' Pengurusan Ijin Pariwisata', '<p>&nbsp;</p>', '2021-01-05', 'PO03', 46, 'ada', 'GFA', 5),
(94, 2, '004/SPM/UMJT/12/01/2021', 'Premi Trip Surabaya', '<p>&nbsp;</p>', '2021-01-12', 'PO03', 35, 'ada', 'UMJT', 1),
(95, 1, '008/SPM/GFA/15/01/2021\r\n', 'Pembayaran BPJS TK Januari', '<p>&nbsp;</p>', '2021-01-15', 'PO03', 46, 'ada', 'GFA', 5),
(96, 9, '009/SPM/GFA/21/01/2021\r\n', 'Biaya RTK', '<p>&nbsp;</p>', '2021-01-21', 'PO03', 46, 'ada', 'GFA', 5),
(97, 9, '010/SPM/GFA/16/01/2021\r\n', 'Biaya ATK', '<p>&nbsp;</p>', '2021-01-16', 'PO03', 46, 'ada', 'GFA', 5),
(98, 9, '011/SPM/GFA/22/01/2021\r\n', ' Biaya Pulsa HP Kantor', '<p>&nbsp;</p>', '2021-01-22', 'PO03', 46, 'ada', 'GFA', 5),
(99, 10, '005/SPM/UMJT/26/01/2021', 'Belanja Operasional', '<p>Belanja Rumah Tanggal &amp; ATK</p>', '2021-01-26', 'PO03', 35, 'ada', 'UMJT', 1),
(100, 9, '012/SPM/GFA/22/01/2021\r\n', 'Biaya Pengiriman Finger Print', '<p>&nbsp;</p>', '2021-01-22', 'PO03', 46, 'ada', 'GFA', 5),
(101, 9, '013/SPM/GFA/26/01/2021\r\n', 'Pembayaran PLN', '<p>&nbsp;</p>', '2021-01-26', 'PO03', 46, 'ada', 'GFA', 5),
(102, 10, '006/SPM/UMJT/26/01/2021', 'Tagihan Belanja Sticker', '<p>&nbsp;</p>', '2021-01-26', 'PO03', 35, 'ada', 'UMJT', 1),
(104, 9, '014/SPM/GFA/26/01/2021\r\n', 'Biaya RTK', '<p>&nbsp;</p>', '2021-01-26', 'PO03', 46, 'ada', 'GFA', 5),
(105, 10, '008/SPM/UMJT/29/01/2021', 'Belanja Stiker Grade B', '<p>&nbsp;</p>', '2021-01-29', 'PO03', 35, 'ada', 'UMJT', 1),
(106, 10, '009/SPM/UMJT/29/01/2021', 'Konsumsi', '<p>Konsumsi untuk tenaga pemasanga sticker</p>', '2021-01-29', 'PO03', 35, 'ada', 'UMJT', 1),
(107, 1, '015/SPM/GFA/27/01/2021\r\n', 'Tunjangan Manajer', '<p>&nbsp;</p>', '2021-01-27', 'PO03', 46, 'ada', 'GFA', 5),
(108, 7, '016/SPM/GFA/27/01/2021\r\n', ' Pembayaran Korden Kantor', '<p>&nbsp;</p>', '2021-01-27', 'PO03', 46, 'ada', 'GFA', 5),
(109, 10, '109/SPM/UMJT/24/05/2021', 'Cetak Banner', '<p>Cetak Banner Bengkel Garasi</p>', '2021-05-24', 'PO01', 35, 'hapus', 'UMJT', 1),
(110, 10, '010/SPM/UMJT/29/01/2021', 'Pulsa Manajer UMJ Trans', '<p>pulsa manajer umj trans</p>', '2021-01-29', 'PO03', 35, 'ada', 'UMJT', 1),
(111, 8, '017/SPM/GFA/27/01/2021\r\n', 'Tambah Daya Listrik Kantor', '<p>&nbsp;</p>', '2021-01-27', 'PO03', 46, 'ada', 'GFA', 5),
(112, 9, '018/SPM/GFA/27/01/2021\r\n', 'Pembelian Token Listrik', '<p>&nbsp;</p>', '2021-01-27', 'PO03', 46, 'ada', 'GFA', 5),
(113, 10, '011/SPM/UMJT/09/02/2021', 'Cetak Banner', '<p>Cetak Banner Bengkel Garasi</p>', '2021-01-09', 'PO03', 35, 'ada', 'UMJT', 1),
(114, 10, '012/SPM/UMJT/10/02/2021', 'Cetak Banner', '<p>&nbsp;</p>', '2021-02-10', 'PO03', 35, 'ada', 'UMJT', 1),
(115, 9, '020/SPM/GFA/29/01/2021\r\n', 'Biaya RTK', '<p>&nbsp;</p>', '2021-01-29', 'PO03', 46, 'ada', 'GFA', 5),
(116, 6, '001/SPM/UMJOP/12/01/2021\r\n', 'Belanja ATK & Habis Pakai', '<p>&nbsp;</p>', '2021-01-12', 'PO03', 36, 'ada', 'UMJOP', 3),
(117, 6, '002/SPM/UMJOP/25/01/2021\r\n', 'Tagihan Toko Jaya', '<p>&nbsp;</p>', '2021-01-25', 'PO03', 36, 'ada', 'UMJOP', 3),
(118, 10, '013/SPM/UMJT/16/02/2021\r\n', 'Tagihan Moyamu', '<p>&nbsp;</p>', '2021-02-16', 'PO03', 35, 'ada', 'UMJT', 1),
(122, 10, '017/SPM/UMJT/01/03/2021\r\n', 'Belanja Kebutuhan Kantor dan Garasi UMJ Trans', '<ul><li>isi ulang tabung gas 12kg</li><li>Bubuk 2 kg</li><li>Gula</li></ul>', '2021-03-01', 'PO03', 35, 'ada', 'UMJT', 1),
(123, 10, '018/SPM/UMJT/08/03/2021\r\n', 'Pulsa Manajer UMJ Trans', '<p>Pulsa bulan Februari</p>', '2021-03-08', 'PO03', 35, 'ada', 'UMJT', 1),
(124, 2, '019/SPM/UMJT/08/03/2021\r\n', 'Shuttle Bus 8 Maret 2021', '<p>&nbsp;</p><p>&nbsp;</p>', '2021-03-08', 'PO03', 35, 'ada', 'UMJT', 1),
(125, 2, '020/SPM/UMJT/09/03/2021\r\n', 'Shuttle Bus 9 Maret 2021', '<p>&nbsp;</p>', '2021-03-09', 'PO03', 35, 'ada', 'UMJT', 1),
(126, 10, '021/SPM/UMJT/10/03/2021\r\n', 'Belanja Operasional Shuttle Bus', '<p>Konsumsi untuk penumpang shuttle</p><p>&nbsp;</p>', '2021-03-10', 'PO03', 35, 'ada', 'UMJT', 1),
(127, 10, '022/SPM/UMJT/10/03/2021\r\n', 'Belanja Flasdisk', '<p>&nbsp;</p>', '2021-03-10', 'PO03', 35, 'ada', 'UMJT', 1),
(129, 4, '001/SPM/UMJE/05/01/2021\r\n', 'Pengajuan Dana Pembuatan Rak & Kolam Hidroponik Pertanian', '<ol><li>PVC 2 1/2 putih power&nbsp;</li><li>Tutup pipa&nbsp;</li><li>Vplas&nbsp;</li></ol>', '2021-01-05', 'PO03', 33, 'ada', 'UMJE', 2),
(130, 4, '002/SPM/UMJE/05/01/2021\r\n', 'Pengajuan Dana Pembuatan Rak & Kolam Hidroponik Pertanian', '<p>1. Kanal C 80 x 0,75&nbsp;</p><p>2. Reng 0,40 R&nbsp;</p><p>3. Baut Trus&nbsp;</p><p>4. Ongkos kirim&nbsp;</p>', '2021-01-05', 'PO03', 33, 'ada', 'UMJE', 2),
(131, 4, '003/SPM/UMJE/05/01/2021\r\n', 'Pengajuan Dana Pembuatan Rak dan Kolam Hidroponik Pertanian', '<p>1. Pembelian 1500 Bata pembuatan Kolam Hidroponik&nbsp;</p><p>2. Pembelian 15 sak Semen pembuatan Kolam Hidroponik&nbsp;</p>', '2021-01-05', 'PO03', 33, 'ada', 'UMJE', 2),
(132, 4, '004/SPM/UMJE/06/01/2021\r\n', 'Pengajuan Dana Renovasi Ruko PT. Mentari Karya Gemilang', '<p>Pembayaran Jasa Pekerjaan Plafond dan Elektrikal PT. MKG</p>', '2021-01-06', 'PO03', 33, 'ada', 'UMJE', 2),
(133, 4, '006/SPM/UMJE/06/01/2021\r\n', 'Pengajuan Dana Renovasi Ruko PT. Mentari Karya Gemilang', '<p>Pembayaran 30 Dus Granit Renovasi Ruko PT. MKG&nbsp;</p>', '2021-01-06', 'PO03', 33, 'ada', 'UMJE', 2),
(134, 4, '007/SPM/UMJE/06/01/2021\r\n', 'Pengajuan Dana Renovasi Ruko PT. Mentari Karya Gemilang', '<p>Pembelian 1 Unit AC Sharp 2 PK PT. MKG Lt. 1 Toko Thomas</p>', '2021-01-06', 'PO03', 33, 'ada', 'UMJE', 2),
(135, 4, '008/SPM/UMJE/06/01/2021\r\n', 'Pengajuan Dana Renovasi Ruko PT. Mentari Karya Gemilang', '<p>Pembelian 9 dos Keramik Granito Infiniti D\'brown 60 x 60 &nbsp;Toko Intisari</p>', '2021-01-06', 'PO03', 33, 'ada', 'UMJE', 2),
(136, 4, '009/SPM/UMJE/06/01/2021\r\n', 'Pengajuan Dana Renovasi Ruko PT. Mentari Karya Gemilang', '<p>Pembelian Kaca Proyek Renovasi Ruko PT. MKG&nbsp;</p>', '2021-01-06', 'PO03', 33, 'ada', 'UMJE', 2),
(137, 9, '137/SPM/GFA/06/06/2021', 'Biaya kWh Pasca Bayar', '<p>&nbsp;</p>', '2021-06-06', 'PO01', 46, 'hapus', 'GFA', 5),
(138, 4, '012/SPM/UMJE/08/01/2021\r\n', 'Pengajuan Dana HR Tukang Alumunium Renovasi Ruko PT. MKG', '<p>HR Tukang Alumunium Renovasi PT. MKG Pak Nasir</p>', '2021-01-08', 'PO03', 33, 'ada', 'UMJE', 2),
(139, 4, '013/SPM/UMJE/08/01/2021\r\n', 'Pengajuan Dana Pembuatan Rak & Kolam Hidroponik Pertanian', '<p>Pembelian 2 lembar Policarbonat Hidroponik Toko Mulia</p>', '2021-01-08', 'PO03', 33, 'ada', 'UMJE', 2),
(140, 1, '140/SPM/GFA/06/06/2021', 'Gaji Karyawan', '<p>Gaji pokok karyawan bulan januari</p>', '2021-06-06', 'PO01', 46, 'hapus', 'GFA', 5),
(141, 4, '014/SPM/UMJE/08/01/2021\r\n', 'Pengajuan Dana Proyek Renovasi SPBU Situbondo', '<p>Pembayaran Termin 3 Jasa Konstruksi Pak Sujono&nbsp;</p>', '2021-01-08', 'PO03', 33, 'ada', 'UMJE', 2),
(142, 4, '015/SPM/UMJE/09/01/2021\r\n', 'Pengajuan Honorium Tukang', '<ol><li>HR Tukang Bangunan an. Febri Imam Santoso&nbsp;</li><li>HR Tukang Las an. Arsin&nbsp;</li></ol>', '2021-01-09', 'PO03', 33, 'ada', 'UMJE', 2),
(143, 9, '143/SPM/GFA/06/06/2021', 'PERDIN', '<p>Uang Saku Pak Muhaimin&nbsp;</p><p>Uang Saku Pak Nur&nbsp;</p><p>Uang saku Pak Edy &nbsp;</p>', '2021-06-06', 'PO01', 46, 'hapus', 'GFA', 5),
(144, 9, '144/SPM/GFA/06/06/2021', 'PERDIN', '<p>Uang Saku Pak Muhaimin&nbsp;</p><p>Uang Saku Pak Nur&nbsp;</p><p>Uang saku Pak Edy &nbsp;</p>', '2021-06-06', 'PO01', 46, 'hapus', 'GFA', 5),
(145, 9, '145/SPM/GFA/06/06/2021', 'PERDIN', '<p>Uang Saku Pak Muhaimin&nbsp;</p><p>Uang Saku Pak Nur&nbsp;</p><p>Uang saku Pak Edy &nbsp;</p>', '2021-06-06', 'PO01', 46, 'hapus', 'GFA', 5),
(146, 9, '146/SPM/GFA/06/06/2021', 'PERDIN', '<p>Uang Saku Pak Muhaimin&nbsp;</p><p>Uang Saku Pak Nur&nbsp;</p><p>Uang saku Pak Edy &nbsp;</p>', '2021-06-06', 'PO01', 46, 'hapus', 'GFA', 5),
(147, 9, '147/SPM/GFA/06/06/2021', 'PERDIN', '<p>Uang Saku Pak Muhaimin&nbsp;</p><p>Uang Saku Pak Nur&nbsp;</p><p>Uang saku Pak Edy &nbsp;</p>', '2021-06-06', 'PO01', 46, 'hapus', 'GFA', 5),
(148, 9, '148/SPM/GFA/06/06/2021', 'PERDIN', '<p>Uang Saku Pak Muhaimin&nbsp;</p><p>Uang Saku Pak Nur&nbsp;</p><p>Uang saku Pak Edy &nbsp;</p>', '2021-06-06', 'PO01', 46, 'hapus', 'GFA', 5),
(149, 9, '149/SPM/GFA/06/06/2021', 'PERDIN', '<p>Uang Saku Pak Muhaimin&nbsp;</p><p>Uang Saku Pak Nur&nbsp;</p><p>Uang saku Pak Edy &nbsp;</p>', '2021-06-06', 'PO01', 46, 'hapus', 'GFA', 5),
(150, 9, '150/SPM/GFA/06/06/2021', 'PERDIN', '<p>Uang Saku Pak Muhaimin&nbsp;</p><p>Uang Saku Pak Nur&nbsp;</p><p>Uang saku Pak Edy &nbsp;</p>', '2021-06-06', 'PO01', 46, 'hapus', 'GFA', 5),
(151, 9, '151/SPM/GFA/06/06/2021', 'PERDIN', '<p>Uang Saku Pak Muhaimin&nbsp;</p><p>Uang Saku Pak Nur&nbsp;</p><p>Uang saku Pak Edy &nbsp;</p>', '2021-06-06', 'PO01', 46, 'hapus', 'GFA', 5),
(152, 4, '016/SPM/UMJE/09/01/2021\r\n', 'Pengajuan Dana Proyek SPBU Situbondo', '<p>Pembelian Baja Berat Tk. Rahmad Bondowoso&nbsp;</p>', '2021-01-09', 'PO03', 33, 'ada', 'UMJE', 2),
(153, 4, '017/SPM/UMJE/09/01/2021\r\n', 'Pengajuan Dana Proyek Renovasi PT. MKG', '<p>HR Tukang Granit Proyek Renovasi PT. MKG&nbsp;</p>', '2021-01-09', 'PO03', 33, 'ada', 'UMJE', 2),
(154, 9, '154/SPM/GFA/07/06/2021', 'Biaya RTK', '<p>Pembelian RTK Transfer ke Mas Eka</p>', '2021-06-07', 'PO01', 46, 'hapus', 'GFA', 5),
(155, 9, '155/SPM/GFA/07/06/2021', 'Biaya Jamuan Tamu', '<p>&nbsp;</p>', '2021-06-07', 'PO01', 46, 'hapus', 'GFA', 5),
(156, 4, '019/SPM/UMJE/09/01/2021\r\n', 'Pengajuan Dana Proyek Pembuatan Rak dan Kolam Hidroponik', '<ol><li>Tk. Sampurna Truss (6 btg Reng) &nbsp;</li><li>Kebutuhan kelistrikan Hidroponik &nbsp;</li><li>Pembelian Selang Hidroponik 5 mm&nbsp;</li></ol>', '2021-01-09', 'PO03', 33, 'ada', 'UMJE', 2),
(157, 4, '020/SPM/UMJE/09/01/2021\r\n', 'Pengajuan Dana Proyek Renovasi PT. Mentari Karya Gemilang', '<p>Kebutuhan pembelian bahan bangunan</p>', '2021-01-09', 'PO03', 33, 'ada', 'UMJE', 2),
(158, 4, '021/SPM/UMJE/09/01/2021\r\n', 'Pengajuan Dana Proyek Renovasi SPBU Situbondo', '<p>Kebutuhan pembelian bahan bangunan&nbsp;</p>', '2021-01-09', 'PO03', 33, 'ada', 'UMJE', 2),
(159, 4, '022/SPM/UMJE/12/01/2021\r\n', 'Pengajuan Dana Renovasi Ruko PT. Mentari Karya Gemilang', '<p>Pembayaran Jasa Pekerjaan Pantry Lt. 1 dan Electrical Lt.2 Renovasi &nbsp;Ruko Kantor PT. MKG (Termin&nbsp; 1 = 30%)&nbsp;</p>', '2021-01-12', 'PO03', 33, 'ada', 'UMJE', 2),
(160, 4, '023/SPM/UMJE/12/01/2021\r\n', 'Pengajuan Dana Renovasi Ruko PT. Mentari Karya Gemilang', '<ol><li>Pembelian Cat Renovasi Ruko PT. MKG &nbsp;</li><li>Pembelian bahan bangunan kebutuhan PT. MKG&nbsp;</li></ol>', '2021-01-12', 'PO03', 33, 'ada', 'UMJE', 2),
(161, 4, '024/SPM/UMJE/12/01/2021\r\n', 'Pengajuan Dana Renovasi SPBU Situbondo', '<p>Pembelian bahan bangunan kebutuhan SPBU Situbondo&nbsp;</p>', '2021-01-12', 'PO03', 33, 'ada', 'UMJE', 2),
(162, 4, '025/SPM/UMJE/12/01/2021\r\n', 'Pengajuan Dana Renovasi PT. MKG', '<p>Pembayaran DP 50% Pemasangan Wallpaper PT. MKG&nbsp;</p>', '2021-01-12', 'PO03', 33, 'ada', 'UMJE', 2),
(163, 4, '027/SPM/UMJE/14/01/2021\r\n', 'Pengajuan Dana Renovasi PT. MKG', '<ol><li>Pembelian Sunblast 15 m x @ Rp. 45.000,-&nbsp;</li><li>Pembayaran Jasa Pemasangan dan Kebutuhan AC 1,5 PK Sharp Lt. 2&nbsp;</li></ol>', '2021-01-14', 'PO03', 33, 'ada', 'UMJE', 2),
(164, 4, '028/SPM/UMJE/13/01/2021\r\n', 'Pengajuan Dana Renovasi SPBU Situbondo', '<ol><li>Pembelian bahan bangunan dan operasional SPBU Situbondo&nbsp;</li><li>Pembayaran driver pick-up dan BBM&nbsp;</li></ol>', '2021-01-13', 'PO03', 33, 'ada', 'UMJE', 2),
(165, 1, '042/SPM/GFA/01/03/2021\r\n', 'Gaji Karyawan', '<p>Gaji Karyawan Februari&nbsp;</p>', '2021-03-01', 'PO03', 46, 'ada', 'GFA', 5),
(166, 9, '043/SPM/GFA/01/03/2021\r\n', 'HR Supervisor Tenaga Umum dan Bartender', '<p>Sunarto &nbsp;350.000&nbsp;</p><p>Beni Auri 350.000&nbsp;</p>', '2021-03-03', 'PO03', 46, 'ada', 'GFA', 5),
(167, 9, '044/SPM/GFA/01/03/2021\r\n', 'Biaya Jamuan Tamu', '<p>di berikan tunai kepada mas eka</p>', '2021-03-03', 'PO03', 46, 'ada', 'GFA', 5),
(168, 4, '029/SPM/UMJE/13/01/2021\r\n', 'Pengajuan Dana Renovasi Ruko PT. MKG', '<p>Pembelian AC Sharp 1,5 PK untuk Lantai 2 PT. MKG&nbsp;</p><p>Toko Thomas Jember&nbsp;</p>', '2021-01-13', 'PO03', 33, 'ada', 'UMJE', 2),
(169, 4, '030/SPM/UMJE/14/01/2021\r\n', 'Pengajuan Dana Renovasi Ruko PT. MKG', '<p>Pembayaran Jasa Pekerjaan Pantry Lt. 1 dan Electrical Lt.2 Renovasi&nbsp;</p><p>Ruko Kantor PT. MKG (Termin&nbsp; 2 = 60%)&nbsp;</p>', '2021-01-14', 'PO03', 33, 'ada', 'UMJE', 2),
(170, 4, '031/SPM/UMJE/14/01/2021\r\n', 'Pengajuan Dana Renovasi Ruko PT. MKG', '<p>Pembelian keperluan pemasangan AC Lt. 1 Ruko PT. MKG&nbsp;</p><p>Toko Dunia Perkakas&nbsp;</p>', '2021-01-14', 'PO03', 33, 'ada', 'UMJE', 2),
(171, 4, '032/SPM/UMJE/15/01/2021\r\n', 'Pengajuan Honorium (Hidroponik Pertanian)', '<ol><li>HR Tukang Bangunan Febri Imam Santoso&nbsp;</li><li>HR Tukang Las Arsin&nbsp;</li></ol>', '2021-01-15', 'PO03', 33, 'ada', 'UMJE', 2),
(172, 4, '033/SPM/UMJE/15/01/2021\r\n', 'Pengajuan Honorarium (PT. MKG)', '<ol><li>HR Tukang Bangunan Febri Imam Santoso&nbsp;</li><li>HR Tukang Las Arsin&nbsp;</li></ol>', '2021-01-15', 'PO03', 33, 'ada', 'UMJE', 2),
(173, 4, '034/SPM/UMJE/15/01/2021\r\n', 'Pengajuan Honorium Driver SPBU Situbondo', '<p>HR Penjemputan tim Pak Arsin SPBU Situbondo&nbsp;</p>', '2021-01-15', 'PO03', 33, 'ada', 'UMJE', 2),
(174, 4, '035/SPM/UMJE/16/01/2021\r\n', 'Pengajuan Dana Pembuatan Rak dan Kolam Hidroponik Pertanian', '<p>Pembelian 3 buah Pompa Air Sunsun JTP 5800&nbsp;</p><p>Via Shopee&nbsp;</p>', '2021-01-16', 'PO03', 33, 'ada', 'UMJE', 2),
(175, 1, '045/SPM/GFA/01/03/2021\r\n', 'HR Tenaga Magang P3LM', '<p>&nbsp;</p>', '2021-03-01', 'PO03', 46, 'ada', 'GFA', 5),
(176, 1, '046/SPM/GFA/05/03/2021\r\n', 'BPJS TK dan BPJS Kesehatan', '<p>Pembayaran BPJS TK dan BPJS Kesehatan</p>', '2021-03-05', 'PO03', 46, 'ada', 'GFA', 5),
(177, 1, '047/SPM/GFA/01/03/2021\r\n', 'HR  Overtime Umum dan Programer', '<p>&nbsp;</p>', '2021-03-01', 'PO03', 46, 'ada', 'GFA', 5),
(178, 9, '048/SPM/GFA/09/03/2021\r\n', 'Belanja RTK', '<p>Belanja ATK oleh mas Eka diberikan Tunai</p>', '2021-03-09', 'PO03', 46, 'ada', 'GFA', 5),
(179, 9, '049/SPM/GFA/12/03/2021\r\n', 'Belanja RTK', '<p>di terima mas Eka Tunai</p>', '2021-03-12', 'PO03', 46, 'ada', 'GFA', 5),
(180, 9, '050/SPM/GFA/15/03/2021\r\n', 'Pembayaran Tagihan PLN', '<p>Bayar Listrik Oleh Mas Eka Tunai</p>', '2021-03-15', 'PO03', 46, 'ada', 'GFA', 5),
(181, 4, '036/SPM/UMJE/16/01/2021\r\n', 'Pengajuan Dana Renovasi SPBU Situbondo', '<p>HR Tukang Las SPBU Situbondo 5 Hari&nbsp;</p>', '2021-01-16', 'PO03', 33, 'ada', 'UMJE', 2),
(182, 9, '051/SPM/GFA/16/03/2021\r\n', 'Biaya ATK', '<p>Tunai</p>', '2021-03-16', 'PO03', 46, 'ada', 'GFA', 5),
(183, 4, '037/SPM/UMJE/16/01/2021\r\n', 'Pengajuan Dana HR Tukang Alumunium Renovasi Sekat Musholla Ged. CC', '<p>HR Tukang Alumunium Renovasi Sekat Musholla Ged. CC &nbsp;Pak Nasir&nbsp;</p>', '2021-01-16', 'PO03', 33, 'ada', 'UMJE', 2),
(184, 4, '038/SPM/UMJE/18/01/2021\r\n', 'Pengajuan Dana Pembayaran Pelunasan Pemasangan Wallpaper PT. MKG', '<ol><li>Pembayaran Pelunasan Pemasangan Wallpaper &nbsp;</li><li>Pembayaran Plamir wallpaper&nbsp;</li></ol>', '2021-01-18', 'PO03', 33, 'ada', 'UMJE', 2),
(185, 9, '052/SPM/GFA/16/03/2021\r\n', 'Biaya Iklan Promo IG', '<p>&nbsp;</p>', '2021-03-16', 'PO03', 46, 'ada', 'GFA', 5),
(186, 4, '040/SPM/UMJE/18/01/2021\r\n', 'Pengajuan Dana Proyek Renovasi PT. MKG', '<p>HR Tukang Granit Proyek Renovasi PT. MKG&nbsp;</p>', '2021-01-18', 'PO03', 33, 'ada', 'UMJE', 2),
(187, 4, '041/SPM/UMJE/18/01/2021\r\n', 'Pengajuan Dana Proyek Renovasi PT. MKG', '<ol><li>Pembayaran biaya bongkar pasang AC Lt. 1 PT. MKG&nbsp;</li><li>Bongkar AC&nbsp;</li><li>Ongkos Pasang AC&nbsp;</li><li>Pasang Kabel Duckting&nbsp;</li><li>Tambahan Bahan&nbsp;</li></ol>', '2021-01-18', 'PO03', 33, 'ada', 'UMJE', 2),
(188, 9, '053/SPM/GFA/25/03/2021\r\n', 'Biaya RTK', '<p>2 IDM Plastik Sampah oleh mas eka Tunai</p>', '2021-03-25', 'PO03', 46, 'ada', 'GFA', 5),
(189, 9, '054/SPM/GFA/25/03/2021\r\n', 'Tagihan Air Mineral MoyaMU', '<p>&nbsp;Pembayaran MOYAMU</p>', '2021-03-25', 'PO03', 46, 'ada', 'GFA', 5),
(190, 9, '055/SPM/GFA/25/03/2021\r\n', 'HR Shooting Podcast', '<p>&nbsp;</p>', '2021-03-25', 'PO03', 46, 'ada', 'GFA', 5),
(191, 1, '056/SPM/GFA/26/03/2021\r\n', 'BPJS TK', '<p>Penetapan Iuran Pertama BPJS Ketenagakerjaan</p>', '2021-03-26', 'PO03', 46, 'ada', 'GFA', 5),
(192, 11, '042/SPM/UMJE/18/01/2021\r\n', 'Pengajuan Dana Pembayaran BPJS Ketenagakerjaan', '<p>Pembayaran BPJS Ketenagakerjaan Div. Engineering&nbsp;</p>', '2021-01-18', 'PO03', 33, 'ada', 'UMJE', 2),
(193, 4, '043/SPM/UMJE/18/01/2021\r\n', 'Pengajuan Dana Proyek Hidroponik Pertanian', '<p>Pembelian Selang Hidroponik 7 mm dan Connector&nbsp;</p><p>Via Shopee&nbsp;</p>', '2021-01-18', 'PO03', 33, 'ada', 'UMJE', 2),
(194, 4, '044/SPM/UMJE/18/01/2021\r\n', 'Pengajuan Dana Pembuatan Rak Alat Lab. Pertanian', '<p>Pembelian Bahan Rak Alat Lab. Pertanian&nbsp;</p><p>Toko Mulia&nbsp;</p>', '2021-01-18', 'PO03', 33, 'ada', 'UMJE', 2),
(195, 4, '045/SPM/UMJE/18/01/2021\r\n', 'Pengajuan Dana Pembuatan Kursi Lab. Teknik Elektro', '<p>Pembelian Bahan Kursi Lab. Elektro + ongkir&nbsp;</p><p>Toko Mulia&nbsp;</p>', '2021-01-18', 'PO03', 33, 'ada', 'UMJE', 2),
(196, 4, '046/SPM/UMJE/19/01/2021\r\n', 'Pengajuan Dana Renovasi Ruko PT. MKG', '<p>Pembelian Kaca 5 mm Putih Polos&nbsp;</p><p>Toko Mulia Kaca&nbsp;</p>', '2021-01-19', 'PO03', 33, 'ada', 'UMJE', 2),
(197, 4, '048/SPM/UMJE/19/01/2021\r\n', 'Pengajuan Dana Rehabilitasi Atap Ged. A', '<p>Permohonan Cash On Hand&nbsp;</p>', '2021-04-19', 'PO03', 33, 'ada', 'UMJE', 2),
(198, 4, '050/SPM/UMJE/20/01/2021\r\n', 'Pengajuan Dana Proyek Kursi Lab. Teknik Elektronik', '<p>Pembelian HPL Kursi Lab. Teknik Elektronik&nbsp;</p>', '2021-01-20', 'PO03', 33, 'ada', 'UMJE', 2),
(199, 4, '051/SPM/UMJE/20/01/2021\r\n', 'Pengajuan Dana Proyek Renovasi SPBU Situbondo', '<p>Pembelian bahan bangunan untuk Renovasi SPBU Situbondo&nbsp;</p>', '2021-01-20', 'PO03', 33, 'ada', 'UMJE', 2),
(200, 11, '052/SPM/UMJE/21/01/2021\r\n', 'Pengajuan Dana Pembelian Safety Harness Inventaris Kantor', '<p>Pengajuan Dana Pembelian 4 buah Safety Harness Inventaris Kantor&nbsp;</p>', '2021-01-21', 'PO03', 33, 'ada', 'UMJE', 2),
(201, 4, '053/SPM/UMJE/21/01/2021\r\n', 'Pengajuan Dana Renovasi Ruko PT. MKG', '<p>Pembayaran Jasa Pekerjaan Pantry Lt. 1 dan Electrical Lt.2 Renovasi&nbsp;</p><p>Ruko Kantor PT. MKG (Termin&nbsp; 3 = 10%)&nbsp;</p>', '2021-01-21', 'PO03', 33, 'ada', 'UMJE', 2),
(202, 4, '054/SPM/UMJE/21/01/2021\r\n', 'Pengajuan Dana Renovasi Ruko PT. MKG', '<p>Pembayaran Jasa Pemasangan AC Lt. 2 PT. MKG&nbsp;</p>', '2021-01-21', 'PO03', 33, 'ada', 'UMJE', 2),
(203, 4, '055/SPM/UMJE/21/01/2021\r\n', 'Pengajuan Dana HR Tukang Alumunium Renovasi Ruko PT. MKG', '<p>HR Tukang Alumunium Renovasi PT. MKG&nbsp;</p><p>Pak Nasir&nbsp;</p>', '2021-01-21', 'PO03', 33, 'ada', 'UMJE', 2),
(204, 4, '056/SPM/UMJE/22/01/2021\r\n', 'Pengajuan Honorium Tukang', '<ol><li>HR Tukang Bangunan Febri Imam Santoso&nbsp;</li><li>HR Tukang Las Arsin&nbsp;</li></ol>', '2021-01-22', 'PO03', 33, 'ada', 'UMJE', 2),
(205, 4, '057/SPM/UMJE/23/01/2021\r\n', 'Pengajuan Dana Proyek Renovasi SPBU Situbondo', '<p>Pembelian bahan bangunan untuk Renovasi SPBU Situbondo&nbsp;</p>', '2021-01-23', 'PO03', 33, 'ada', 'UMJE', 2),
(206, 4, '060/SPM/UMJE/26/01/2021\r\n', 'Pengajuan Dana Proyek Renovasi SPBU Situbondo', '<p>Pembayaran Termin Terakhir Jasa Konstruksi Pak Sujono&nbsp;</p>', '2021-01-26', 'PO03', 33, 'ada', 'UMJE', 2),
(207, 4, '063/SPM/UMJE/28/01/2021\r\n', 'Pengajuan Dana Proyek Pembangunan SD Muhammadiyah', '<p>Dana Operasional Survey Lokasi SD Muhammadiyah&nbsp;</p>', '2021-01-28', 'PO03', 33, 'ada', 'UMJE', 2),
(208, 4, '064/SPM/UMJE/28/01/2021\r\n', 'Pengajuan Dana Proyek Renovasi Rumah Bu Veny', '<p>Pengajuan Dana Pembelian Bahan Renovasi Rumah Bu Veny&nbsp;</p>', '2021-01-28', 'PO03', 33, 'ada', 'UMJE', 2),
(209, 4, '066/SPM/UMJE/29/01/2021\r\n', 'Pengajuan Honorium Tukang', '<ol><li>HR Tukang Bangunan Febri Imam Santoso&nbsp;</li><li>HR Tukang Las Arsin&nbsp;</li></ol>', '2021-01-09', 'PO03', 33, 'ada', 'UMJE', 2),
(210, 4, '067/SPM/UMJE/29/01/2021\r\n', 'Pengajuan Dana Cash On Hand', '<p>Pengajuan Dana Cash on Hand&nbsp;</p>', '2021-01-29', 'PO03', 33, 'ada', 'UMJE', 2),
(211, 4, '068/SPM/UMJE/29/01/2021', 'Pengajuan Dana Proyek Persiapan Pembangunan SD Muhammadiyah', '<ol><li>Pengajuan Dana Pembayaran Sewa Alat Berat Excavator&nbsp;</li><li>Proyek Persiapan Pembangunan SD Muhammadiyah&nbsp;</li></ol>', '2021-01-29', 'PO03', 33, 'ada', 'UMJE', 2),
(212, 4, '069/SPM/UMJE/01/02/2021\r\n', 'Pengajuan Dana Proyek Persiapan Pekerjaan Pembangunan SD Muhammadiyah', '<p>Cash On Hand Persiapan Pekerjaan Pembangunan SD Muhammadiyah&nbsp;</p>', '2021-02-01', 'PO03', 33, 'ada', 'UMJE', 2),
(213, 11, '070/SPM/UMJE/01/02/2021\r\n', 'Pengajuan Pembayaran RTK Div. Engineering Januari', '<ol><li>Pembelian Pulsa Prabayar No. Layanan Kantor Div. Engineering &nbsp;</li><li>Pembelian Air Mineral Dalam Kemasan bulan Januari &nbsp;</li><li>Pembelian konsumsi RTK bulan Januari &nbsp;</li><li>Pembelian kebutuhan ATK bulan Januari&nbsp;</li></ol>', '2021-02-01', 'PO03', 33, 'ada', 'UMJE', 2),
(214, 4, '071/SPM/UMJE/01/02/2021\r\n', 'Pengajuan Dana Proyek Persiapan Pekerjaan Pembangunan SD Muhammadiyah', '<p>Cash On Hand Persiapan Pekerjaan Pembangunan SD Muhammadiyah&nbsp;</p>', '2021-02-01', 'PO03', 33, 'ada', 'UMJE', 2),
(215, 1, '057/SPM/GFA/05/04/2021\r\n', 'Dana Suka Cita', '<p>Dana Suka Cita Kelahiran Anak Muhammad Ariefin</p>', '2021-05-04', 'PO03', 46, 'ada', 'GFA', 5),
(216, 4, '072/SPM/UMJE/02/02/2021\r\n', 'Pengajuan Dana Renovasi Ruko PT. MKG', '<p>Pembelian Dispenser Miyako WDP 300&nbsp;</p><p>Toko Thomas Jember&nbsp;</p>', '2021-02-02', 'PO03', 33, 'ada', 'UMJE', 2),
(217, 4, '073/SPM/UMJE/03/02/2021\r\n', 'Pengajuan Dana Proyek SD Muhammadiyah', '<p>Pengajuan Dana Pengukuran Pemasangan Patok Titik Bangunan&nbsp;</p><p>SD Muhammadiyah&nbsp;</p>', '2021-02-03', 'PO03', 33, 'ada', 'UMJE', 2),
(218, 4, '074/SPM/UMJE/03/02/2021\r\n', 'Pengajuan Dana Proyek Persiapan Pembangunan SD Muhammadiyah', '<p>Pengajuan Tambahan Pembayaran Sewa Alat Berat Excavator&nbsp;</p><p>Proyek Persiapan Pembangunan SD Muhammadiyah&nbsp;</p>', '2021-02-03', 'PO03', 33, 'ada', 'UMJE', 2),
(219, 4, '075/SPM/UMJE/04/02/2021\r\n', 'Pengajuan Dana Proyek Rumah Bu Veny', '<p>Pembelian Bahan Bangunan Renovasi Rumah Bu Veny&nbsp;</p>', '2021-02-04', 'PO03', 33, 'ada', 'UMJE', 2),
(220, 4, '076/SPM/UMJE/04/02/2021\r\n', 'Pengajuan Dana Cash On Hand', '<p>Pengajuan Cash On Hand&nbsp;</p>', '2021-02-04', 'PO03', 33, 'ada', 'UMJE', 2),
(221, 4, '077/SPM/UMJE/05/02/2021\r\n', 'Pengajuan Dana Proyek Persiapan Pekerjaan Pembangunan SD Muhammadiyah', '<p>Pembelian Kayu Bowplank &amp; Bekisting SD Muhammadiyah&nbsp;</p>', '2021-02-05', 'PO03', 33, 'ada', 'UMJE', 2),
(222, 4, '078/SPM/UMJE/05/02/2021\r\n', 'Pengajuan Dana Proyek Persiapan Pekerjaan Pembangunan SD Muhammadiyah', '<ol><li>Pembelian Coral 2 rit &nbsp;</li><li>Semen Gresik&nbsp;</li></ol>', '2021-02-05', 'PO03', 33, 'ada', 'UMJE', 2),
(223, 4, '079/SPM/UMJE/05/02/2021\r\n', 'Pengajuan Honorium Tukang', '<ol><li>HR Tukang Bangunan Febri Imam Santoso&nbsp;</li><li>HR Tukang Las Arsin&nbsp;</li><li>HR Pak Mullah&nbsp;</li></ol>', '2021-02-05', 'PO03', 33, 'ada', 'UMJE', 2),
(224, 4, '081/SPM/UMJE/10/02/2021\r\n', 'Pengajuan Dana Pembelian Konsumsi Rapat SD Muhammadiyah', '<p>Pembelian Konsumsi Rapat bersama Ketua PC Muhammadiyah&nbsp;</p><p>beserta Tim dari PC Muhammadiyah Kaliwates&nbsp;</p>', '2021-02-10', 'PO03', 33, 'ada', 'UMJE', 2),
(225, 1, '058/SPM/GFA/01/04/2021\r\n', 'HR  Overtime Umum dan Programer', '<p>HR&nbsp; Overtime Umum 250.000&nbsp;</p><p>HR Programer 1.000.000</p>', '2021-04-01', 'PO03', 46, 'ada', 'GFA', 5),
(227, 4, '227/SPM/UMJE/08/06/2021', 'Pengajuan Dana Mobilisasi Survey Pavingisasi Lumajang', '<p>Uang Saku Survey Lapangan Pavingisasi Lumajang&nbsp;</p>', '2021-06-08', 'PO01', 33, 'hapus', 'UMJE', 2),
(228, 4, '083/SPM/UMJE/11/02/2021\r\n', 'Pengajuan Honorium Tukang', '<ol><li>HR Tukang Bangunan Febri Imam Santoso&nbsp;</li><li>HR Tukang Las Arsin&nbsp;</li><li>HR Iran&nbsp;</li></ol>', '2021-02-11', 'PO03', 33, 'ada', 'UMJE', 2),
(229, 4, '084/SPM/UMJE/15/02/2021\r\n', 'Pengajuan Dana Cash On Hand', '<p>Cash On Hand Syechul Hardiansyah Permana&nbsp;</p><p>Kekurangan HR Tim Tukang Las&nbsp;</p>', '2021-02-15', 'PO03', 33, 'ada', 'UMJE', 2),
(230, 4, '085/SPM/UMJE/17/02/2021\r\n', 'Pengajuan Dana Proyek Renovasi Rumah Bu Veni', '<p>Semen Gresik&nbsp;</p><p>Toko Karimata Abadi&nbsp;</p>', '2021-02-17', 'PO03', 33, 'ada', 'UMJE', 2),
(231, 4, '086/SPM/UMJE/19/02/2021\r\n', 'Pengajuan Dana Pembelian Alat dan Bahan', '<p>Pembelian Alat dan Bahan&nbsp;</p>', '2021-02-19', 'PO03', 33, 'ada', 'UMJE', 2),
(232, 1, '059/SPM/GFA/09/04/2021\r\n', 'Tagihan BPJS Kes, BPJS TK, dan PLN Bulan April 2021', '<p>Pembayaran Tagihan BPJS Kesehatan 2.944.575</p><p>Pembayaran Tagihan BPJS Ketenagakerjaan PT MKG 5.824.374</p><p>Pembayaran Tagihan PLN 568.923</p><p>Pembayaran Tagihan BPJS Ketenagakerjaan PT THS 440.977</p>', '2021-04-09', 'PO03', 46, 'ada', 'GFA', 5),
(233, 4, '087/SPM/UMJE/19/02/2021\r\n', 'Pengajuan Honorium Tukang', '<ol><li>HR Tukang Bangunan Febri Imam Santoso&nbsp;</li><li>HR Tukang Las Arsin&nbsp;</li><li>HR Iran&nbsp;</li><li>HR Supir Mobilisasi SD Muhammadiyah&nbsp;</li></ol>', '2021-02-19', 'PO03', 33, 'ada', 'UMJE', 2),
(234, 9, '060/SPM/GFA/05/04/2021\r\n', 'RTK', '<p>Gula,Sariwangi,Galade,Stella,Kamper,Spr Pell&nbsp;</p>', '2021-04-05', 'PO03', 46, 'ada', 'GFA', 5),
(235, 4, '088/SPM/UMJE/25/02/2021\r\n', 'Pengajuan Dana Cash On Hand', '<p>Pengajuan Dana Pembelian Bahan&nbsp;<br>&nbsp;</p>', '2021-02-25', 'PO03', 33, 'ada', 'UMJE', 2),
(236, 4, '089/SPM/UMJE/26/02/2021\r\n', 'Pengajuan Honorium Tukang', '<ol><li>HR Tukang Bangunan Febri Imam Santoso&nbsp;</li><li>HR Tukang Las Arsin&nbsp;</li></ol>', '2021-02-26', 'PO03', 33, 'ada', 'UMJE', 2),
(237, 1, '061/SPM/GFA/05/04/2021\r\n', 'Dana Suka Cita', '<p>Uang Duka Ibu Akhmad Suharto Tunai</p>', '2021-04-05', 'PO03', 46, 'ada', 'GFA', 5),
(238, 4, '090/SPM/UMJE/03/03/2021\r\n', 'Pengajuan Dana Pembelian Bahan Proyek Gudang Belakang Gedung G', '<p>Pembelian Bahan Bangunan&nbsp;</p><p>Toko Sampurna Truss&nbsp;</p>', '2021-03-03', 'PO03', 33, 'ada', 'UMJE', 2),
(239, 4, '091/SPM/UMJE/04/03/2021\r\n', 'Pengajuan Dana Cash On Hand', '<p>Pengajuan Dana Pembelian Bahan&nbsp;</p>', '2021-03-04', 'PO03', 33, 'ada', 'UMJE', 2),
(240, 4, '092/SPM/UMJE/05/03/2021\r\n', 'Pengajuan Honorium Tukang', '<ol><li>HR Tukang Bangunan Febri Imam Santoso&nbsp;</li><li>HR Tukang Las Arsin&nbsp;</li><li>HR Pak Andis&nbsp;</li><li>HR Pak Nasir&nbsp;</li></ol>', '2021-03-05', 'PO03', 33, 'ada', 'UMJE', 2),
(241, 4, '093/SPM/UMJE/05/03/2021\r\n', 'Pengajuan Dana Proyek SPBU Situbondo', '<ol><li>Pembayaran Tenaga Ahli Tanah Pak Arief Alihudien&nbsp;</li><li>Pembayaran Tenaga Ahli Struktur Pak Pujo Priyono&nbsp;</li></ol>', '2021-03-05', 'PO03', 33, 'ada', 'UMJE', 2),
(242, 4, '094/SPM/UMJE/15/03/2021\r\n', 'Pengajuan Dana Cash On Hand', '<p>Pengajuan Dana Pembelian Bahan&nbsp;</p>', '2021-03-15', 'PO03', 33, 'ada', 'UMJE', 2),
(243, 4, '095/SPM/UMJE/12/03/2021\r\n', 'Pengajuan Honorium Tukang', '<ol><li>HR Tukang Bangunan Febri Imam Santoso&nbsp;</li><li>HR Tukang Bangunan Samsul Hadi&nbsp;</li><li>HR Tukang Las Arsin&nbsp;</li><li>HR Tukang Las Yasin&nbsp;</li></ol>', '2021-03-12', 'PO03', 33, 'ada', 'UMJE', 2),
(244, 9, '244/SPM/GFA/08/06/2021', 'Dana Pembersihan Lahan Tanam Jagung', '<p>Dana Pembersihan Lahan Tanam Jagung Termin 1 Tunai Fachmi</p>', '2021-06-08', 'PO01', 46, 'hapus', 'GFA', 5),
(245, 9, '063/SPM/GFA/08/04/2021\r\n', 'Dana Konsumsi Rapat', '<p>6 Bungkus Nasi Rendang Telor Tunai Eka</p>', '2021-04-08', 'PO03', 46, 'ada', 'GFA', 5),
(246, 1, '064/SPM/GFA/08/04/2021\r\n', 'Pembayaran Tagihan BPJS Ketenagakerjaan', '<p>BPJS TK Bapak Aswari Bulan April 2021</p>', '2021-04-08', 'PO03', 46, 'ada', 'GFA', 5),
(247, 9, '247/SPM/GFA/08/06/2021', 'Dana Pembersihan Lahan Tanam Jagung', '<p>Pembersihan Lahan Tanam Jagung Termin 2 Tunai Fachmi</p>', '2021-06-08', 'PO01', 46, 'hapus', 'GFA', 5),
(248, 4, '096/SPM/UMJE/16/03/2021\r\n', 'Pengajuan Dana Perpanjangan Nomor Layanan Engineering', '<p>Pengajuan Dana Perepanjangan Nomor Layanan Prabayar</p><p>Divisi Engineering</p>', '2021-03-16', 'PO03', 33, 'ada', 'UMJE', 2),
(249, 4, '097/SPM/UMJE/16/03/2021\r\n', 'Pengajuan Dana Pembelian Bahan Proyek Gudang Belakang Gedung G', '<p>Pembelian Bahan Bangunan&nbsp;</p><p>Toko Sampurna Truss&nbsp;</p>', '2021-03-16', 'PO03', 33, 'ada', 'UMJE', 2),
(250, 4, '098/SPM/UMJE/16/03/2021\r\n', 'Pengajuan Dana Pembelian Bahan Proyek Workshop Engineering', '<p>Pembelian Bahan Bangunan&nbsp;</p><p>Karimata Abadi&nbsp;</p>', '2021-03-16', 'PO03', 33, 'ada', 'UMJE', 2),
(252, 4, '252/SPM/UMJE/08/06/2021', 'Pengajuan Dana Pembayaran List Ruang Dekan Gedung B', '<p>Pembayaran Pemasangan List Ruang Dekan Gedung B&nbsp;</p>', '2021-06-08', 'PO01', 33, 'hapus', 'UMJE', 2),
(253, 4, '100/SPM/UMJE/17/03/2021\r\n', 'Pengajuan Dana Kas ATM BCA', '<p>Kas untuk ATM BCA&nbsp;<br>&nbsp;</p>', '2021-03-17', 'PO03', 33, 'ada', 'UMJE', 2),
(254, 9, '068/SPM/GFA/15/04/2021\r\n', 'HR Design Flyer Shuttle', '<p>&nbsp;</p>', '2021-04-15', 'PO03', 46, 'ada', 'GFA', 5),
(255, 4, '101/SPM/UMJE/18/03/2021\r\n', 'Pengajuan Dana Pembelian Bahan Proyek Penutupan Lantai Rumah Bu Veny', '<p>Pembelian Bahan Bangunan&nbsp;</p>', '2021-03-18', 'PO03', 33, 'ada', 'UMJE', 2),
(256, 4, '102/SPM/UMJE/19/03/2021\r\n', 'Pengajuan Honorium Tukang', '<ol><li>HR Tukang Bangunan Febri Imam Santoso&nbsp;</li><li>HR Tukang Bangunan Samsul Hadi&nbsp;</li><li>HR Tukang Las Arsin&nbsp;</li><li>HR Tukang Las Yasin&nbsp;</li></ol>', '2021-03-19', 'PO03', 33, 'ada', 'UMJE', 2),
(257, 4, '103/SPM/UMJE/19/03/2021\r\n', 'Pengajuan Dana Pembayaran Pemasangan Sekat Ruang Dekan Gedung B', '<p>Pembayaran Pemasangan Sekat Ruang Dekan Gedung B&nbsp;</p>', '2021-03-19', 'PO03', 33, 'ada', 'UMJE', 2),
(258, 9, '069/SPM/GFA/15/04/2021\r\n', 'Sumbangan Ke MUI Jember', '<p>&nbsp;</p>', '2021-04-15', 'PO03', 46, 'ada', 'GFA', 5),
(259, 4, '104/SPM/UMJE/26/03/2021\r\n', 'Pengajuan Dana Pembayaran HR Pak Nasir Ruang Dekan Gedung B', '<p>Pembayaran HR Pak Nasir Ruang Dekan Gedung B&nbsp;</p>', '2021-03-26', 'PO03', 33, 'ada', 'UMJE', 2),
(261, 4, '106/SPM/UMJE/26/03/2021\r\n', 'Pengajuan Dana Pembelian Bahan Proyek Workshop Engineering', '<p>Pembelian Bahan Bangunan&nbsp;</p><p>Toko Karimata Abadi&nbsp;</p>', '2021-03-26', 'PO03', 33, 'ada', 'UMJE', 2),
(262, 4, '107/SPM/UMJE/26/03/2021\r\n', 'Pengajuan Dana Pemasangan Wallpaper Ruang Dekan Gedung B', '<p>Pembayaran Pemasangan Wallpaper Ruang Dekan Gedung B&nbsp;</p>', '2021-03-26', 'PO03', 33, 'ada', 'UMJE', 2),
(263, 9, '071/SPM/GFA/19/04/2021\r\n', 'Biaya Marketing UM Trans', '<p>Rek.Mandiri 1.000.000&nbsp;</p><p>Rek.BSI 1.750.000&nbsp;</p>', '2021-04-19', 'PO03', 46, 'ada', 'GFA', 5),
(264, 4, '108/SPM/UMJE/29/03/2021\r\n', 'Pengajuan Dana Renovasi Rumah Bu Veny', '<p>Pembayaran Borongan Pak Nasir Proyek Renovasi Rumah Bu Veny&nbsp;</p>', '2021-03-29', 'PO03', 33, 'ada', 'UMJE', 2),
(265, 4, '109/SPM/UMJE/30/03/2021\r\n', 'Pengajuan Dana Pembelian Bahan', '<p>Pembelian Bahan Proyek Workshop Engineering&nbsp;</p>', '2021-03-30', 'PO03', 33, 'ada', 'UMJE', 2),
(266, 4, '110/SPM/UMJE/30/03/2021\r\n', 'Pengajuan Dana Pembelian Bahan', '<p>Pembelian Bahan Proyek Workshop Engineering Toko Mulia&nbsp;</p>', '2021-03-30', 'PO03', 33, 'ada', 'UMJE', 2),
(267, 4, '111/SPM/UMJE/30/03/2021\r\n', 'Pengajuan Dana Kas ATM BCA', '<p>Kas untuk ATM BCA&nbsp;<br>&nbsp;</p>', '2021-03-30', 'PO03', 33, 'ada', 'UMJE', 2),
(268, 11, '112/SPM/UMJE/31/03/2021\r\n', 'Pengajuan Pembayaran RTK Div. Engineering Maret', '<ol><li>Pembelian Air Mineral Dalam Kemasan bulan Maret &nbsp;</li><li>Pembelian konsumsi RTK bulan Maret &nbsp;</li><li>Pembelian kebutuhan ATK bulan Maret&nbsp;</li></ol>', '2021-03-31', 'PO03', 33, 'ada', 'UMJE', 2),
(269, 4, '113/SPM/UMJE/31/03/2021\r\n', 'Pengajuan Dana Pembelian Bahan', '<p>Pembelian Bahan Proyek Workshop Engineering&nbsp;</p><p>Toko SAE&nbsp;</p>', '2021-03-31', 'PO03', 33, 'ada', 'UMJE', 2),
(270, 9, '073/SPM/GFA/19/04/2021\r\n', 'Pembelian Pulsa HP Kantor', '<p>&nbsp;</p>', '2021-04-19', 'PO03', 46, 'ada', 'GFA', 5),
(271, 9, '077/SPM/GFA/20/04/2021\r\n', 'Honorium Shooting Podcast', '<p>&nbsp;Dana Honorium (Shooting) Bulan April 2021</p>', '2021-04-20', 'PO03', 46, 'ada', 'GFA', 5),
(272, 1, '079/SPM/GFA/26/04/2021\r\n', 'Dana Suka Cita', '<p>Dana Suka Cita Adhitya Surya</p>', '2021-04-26', 'PO03', 46, 'ada', 'GFA', 5),
(273, 9, '081/SPM/GFA/21/04/2021\r\n', 'Pembelian Pigura dan Cetak Foto', '<p>&nbsp;</p>', '2021-04-21', 'PO03', 46, 'ada', 'GFA', 5),
(274, 9, '082/SPM/GFA/22/04/2021\r\n', 'Biaya Akomodasi Marketing UM Trans', '<p>Akomodasi Marketing dan Uang Saku 3 hari TF dari Mandiri</p>', '2021-04-22', 'PO03', 46, 'ada', 'GFA', 5),
(275, 7, '085/SPM/GFA/22/04/2021\r\n', 'Pembelian Aset HP', '<p>&nbsp;</p>', '2021-04-22', 'PO03', 46, 'ada', 'GFA', 5),
(276, 8, '087/SPM/GFA/29/04/2021\r\n', 'Pembayaran Tagihan Tagihan Service AC', '<p>Service AC Kantor PT. MKG</p>', '2021-04-29', 'PO03', 46, 'ada', 'GFA', 5),
(277, 1, '088/SPM/GFA/30/04/2021\r\n', 'Gaji Karyawan', '<p>Gaji Karyawan April 2021 dari Mandiri</p>', '2021-04-30', 'PO03', 46, 'ada', 'GFA', 5),
(278, 1, '089/SPM/GFA/30/04/2021\r\n', 'HR  Overtime Umum', '<p>HR&nbsp; Overtime Umum april 2021</p>', '2021-04-30', 'PO03', 46, 'ada', 'GFA', 5),
(279, 1, '090/SPM/GFA/04/05/2021\r\n', 'THR Karyawan 2021', '<p>&nbsp;</p>', '2021-04-05', 'PO03', 46, 'ada', 'GFA', 5),
(280, 1, '095/SPM/GFA/11/05/2021\r\n', 'Biaya Rapid Karyawan', '<p>tunai</p>', '2021-05-11', 'PO03', 46, 'ada', 'GFA', 5),
(281, 9, '281/SPM/GFA/09/06/2021', 'Isi Gas LPG dan Pulsa Nomer Layanan Kantor', '<p>TUNAI eka</p>', '2021-06-09', 'PO03', 46, 'ada', 'GFA', 5),
(282, 9, '098/SPM/GFA/27/05/2021\r\n', 'Biaya ATK dan RTK', '<p>&nbsp;</p>', '2021-05-27', 'PO03', 46, 'ada', 'GFA', 5),
(283, 1, '099/SPM/GFA/31/05/2021\r\n', 'Gaji Karyawan Mei', '<p>&nbsp;</p>', '2021-05-31', 'PO03', 46, 'ada', 'GFA', 5),
(284, 4, '114/SPM/UMJE/01/04/2021\r\n', 'Pengajuan Dana Renovasi Rumah Bu Veny', '<p>Pembayaran Borongan Pak Nasir Proyek Renovasi Rumah Bu Veny&nbsp;</p>', '2021-04-01', 'PO03', 33, 'ada', 'UMJE', 2),
(285, 4, '115/SPM/UMJE/02/04/2021\r\n', 'Pengajuan Honorium Tukang', '<ol><li>HR Tukang Bangunan Febri Imam Santoso&nbsp;</li><li>HR Tukang Bangunan Samsul Hadi&nbsp;</li><li>HR Tukang Las Arsin&nbsp;</li><li>HR Tukang Las Yasin&nbsp;</li></ol>', '2021-04-02', 'PO03', 33, 'ada', 'UMJE', 2),
(286, 4, '116/SPM/UMJE/01/04/2021\r\n', 'Pengajuan Dana Workshop Engineering', '<p>Pembayaran Pekerjaan Rangka Kuda-kuda Termin 1</p>', '2021-04-01', 'PO03', 33, 'ada', 'UMJE', 2),
(287, 1, '100/SPM/GFA/02/06/2021\r\n', 'HR Mei Overtime Umum ', '<p>HR Mei Overtime Umum MEI</p>', '2021-06-02', 'PO03', 46, 'ada', 'GFA', 5),
(288, 1, '101/SPM/GFA/07/06/2021\r\n', 'Tagihan BPJS Kes, PLN, dan BPJS TK', '<ul><li>Pembayaran Tagihan BPJS Kesehatan PT MKG 5.182.452</li><li>Pembayaran Tagihan BPJS Ketenagakerjaan PT MKG Bulan Mei 5.824.374</li><li>Pembayaran Tagihan PLN PT MKG 1.034.432 TUNAI</li><li>Pembayaran Tagihan BPJS Ketenagakerjaan PT THS Bulan Mei 440.977</li><li>Pembayaran Tagihan BPJS Ketenagakerjaan Bapak Aswari Bulan Mei 16.800</li></ul>', '2021-06-07', 'PO03', 46, 'ada', 'GFA', 5),
(289, 1, '103/SPM/GFA/10/06/2021\r\n', ' Tagihan BPJS TK (PT MKG, PT THS, Bapak Aswari) Bulan Juni 2021 ', '<ul><li>Pembayaran Tagihan BPJS Ketenagakerjaan PT MKG</li><li>Pembayaran Tagihan BPJS Ketenagakerjaan PT THS</li><li>Pembayaran Tagihan BPJS Ketenagakerjaan Bapak Aswari</li></ul>', '2021-06-10', 'PO03', 46, 'ada', 'GFA', 5),
(290, 4, '082/SPM/UMJE/08/02/2021', 'Pengajuan Dana Mobilisasi Survey Pavingisasi Lumajang', '<p>Uang Saku Survey Lapangan Pavingisasi Lumajang&nbsp;</p>', '2021-02-02', 'PO03', 33, 'ada', 'UMJE', 2),
(291, 4, '099/SPM/UMJE/17/03/2021', 'Pengajuan Dana Pembayaran List Ruang Dekan Gedung B', '<p>Pembayaran Pemasangan List Ruang Dekan Gedung B&nbsp;</p>', '2021-03-17', 'PO03', 33, 'ada', 'UMJE', 2),
(292, 4, '105/SPM/UMJE/26/03/2021', 'Pengajuan Honorium Tukang', '<ol><li>HR Tukang Bangunan Febri Imam Santoso&nbsp;</li><li>HR Tukang Bangunan Samsul Hadi&nbsp;</li><li>HR Tukang Las Arsin&nbsp;</li><li>HR Tukang Las Yasin&nbsp;</li></ol>', '2021-03-26', 'PO03', 33, 'ada', 'UMJE', 2),
(293, 4, '117/SPM/UMJE/02/04/2021\r\n', 'Pengajuan Dana Workshop Engineering', '<p>Pembayaran Pembelian Bahan Bangunan di Toko Karimata Abadi&nbsp;</p>', '2021-04-02', 'PO03', 33, 'ada', 'UMJE', 2),
(294, 4, '118/SPM/UMJE/07/04/2021\r\n', 'Pengajuan Dana Workshop Engineering', '<p>Pembayaran Term. 3 dan Pembelian Bahan Bangunan</p>', '2021-04-07', 'PO03', 33, 'ada', 'UMJE', 2),
(295, 4, '119/SPM/UMJE/09/04/2021\r\n', 'Pengajuan Honorium Tukang', '<ol><li>HR Tukang Bangunan Febri Imam Santoso&nbsp;</li><li>HR Tukang Las Arsin&nbsp;</li></ol>', '2021-04-09', 'PO03', 33, 'ada', 'UMJE', 2),
(296, 4, '120/SPM/UMJE/07/04/2021\r\n', 'Pengajuan Pembayaran Moyamu Workshop Engineering', '<p>Pembelian Air Mineral Dalam Kemasan Galon bulan Maret&nbsp;</p>', '2021-04-07', 'PO03', 33, 'ada', 'UMJE', 2),
(297, 4, '121/SPM/UMJE/09/04/2021\r\n', 'Pengajuan Dana Workshop Engineering', '<p>Pembelian Bahan Bangunan Workshop di Toko Sampurna Truss&nbsp;</p>', '2021-04-09', 'PO03', 33, 'ada', 'UMJE', 2),
(298, 4, '122/SPM/UMJE/09/04/2021\r\n', 'Pengajuan Dana Workshop Engineering', '<ol><li>Pembayaran Pembelian Bahan Bangunan di Toko Karimata Abadi&nbsp;</li><li>Pembayaran Pembelian Pasir di CV. Maju SS&nbsp;</li></ol>', '2021-04-09', 'PO03', 33, 'ada', 'UMJE', 2),
(299, 4, '123/SPM/UMJE/12/04/2021\r\n', 'Pengajuan Dana Pembelian Bahan', '<p>Pembelian Bahan Proyek Workshop Engineering di Toko Mulia</p>', '2021-04-12', 'PO03', 33, 'ada', 'UMJE', 2),
(300, 4, '124/SPM/UMJE/16/04/2021\r\n', 'Pengajuan Dana Renovasi Ruang Dekan FT Gedung B', '<p>Pembayaran Borongan Pak Nasir Pemasangan Sticker Kaca&nbsp;</p>', '2021-04-16', 'PO03', 33, 'ada', 'UMJE', 2),
(301, 4, '125/SPM/UMJE/16/04/2021\r\n', 'Pengajuan Honorium Tukang', '<ol><li>HR Tukang Bangunan Febri Imam Santoso&nbsp;</li><li>HR Tukang Las Arsin&nbsp;</li></ol>', '2021-04-16', 'PO03', 33, 'ada', 'UMJE', 2),
(302, 4, '126/SPM/UMJE/16/04/2021\r\n', 'Pengajuan Dana Workshop Engineering', '<ol><li>Pembayaran Pembeliah Bahan Bangunan di Toko Karimata Abadi&nbsp;</li><li>Pembayaran Pembelian Pasir di CV. Maju SS&nbsp;</li></ol>', '2021-04-16', 'PO03', 33, 'ada', 'UMJE', 2),
(303, 4, ' 127/SPM/UMJE/19/04/2021 \r\n', 'Pengajuan Dana Proyek Atap Hidroponik', '<p>Pembelian Bahan Proyek Atap Hidroponik Toko Sampurna Truss</p>', '2021-04-19', 'PO03', 33, 'ada', 'UMJE', 2),
(304, 4, ' 128/SPM/UMJE/19/04/2021 \r\n', 'Pengajuan Dana Cash On Hand', '<p>Pengajuan Cash On Hand Syechul Hardiansyah Permana</p>', '2021-04-19', 'PO03', 33, 'ada', 'UMJE', 2),
(305, 4, ' 129/SPM/UMJE/20/04/2021 \r\n', 'Pengajuan Pembelian Bahan Bangunan Toko TB Mulia Jaya', '<ol><li>Pembelian Bahan Proyek Workshop Engineering&nbsp;</li><li>Pembelian Bahan Proyek Service Keramik Ged. A&nbsp;</li></ol>', '2021-05-20', 'PO03', 33, 'ada', 'UMJE', 2),
(306, 4, ' 130/SPM/UMJE/21/04/2021 \r\n', 'Pengajuan Pembelian Bahan Bangunan', '<ol><li>Pembelian Bahan Proyek Workshop Engineering&nbsp;</li><li>Pembuatan Pintu Harmonika&nbsp;</li></ol>', '2021-04-21', 'PO03', 33, 'ada', 'UMJE', 2),
(307, 4, ' 131/SPM/UMJE/22/04/2021 \r\n', 'Pengajuan Dana Cash On Hand', '<p>Pengajuan Cash On Hand Syechul Hardiansyah Permana &nbsp;</p>', '2021-04-22', 'PO03', 33, 'ada', 'UMJE', 2),
(308, 4, ' 132/SPM/UMJE/23/04/2021 \r\n', 'Pengajuan Honorium Tukang', '<ol><li>HR Tukang Bangunan Febri Imam Santoso&nbsp;</li><li>HR Tukang Las Arsin&nbsp;</li></ol>', '2021-04-23', 'PO03', 33, 'ada', 'UMJE', 2),
(309, 4, ' 133/SPM/UMJE/23/04/2021 \r\n', 'Pengajuan Dana Workshop Engineering', '<ol><li>Pembayaran Pembeliah Bahan Bangunan di Toko Karimata Abadi&nbsp;</li><li>Pembayaran Pembelian Pasir di CV. Maju SS&nbsp;</li></ol>', '2021-04-23', 'PO03', 33, 'ada', 'UMJE', 2),
(310, 4, ' 134/SPM/UMJE/23/04/2021 \r\n', 'Pengajuan Dana Cash On Hand', '<p>Pengajuan Cash On Hand Syechul Hardiansyah Permana &nbsp;</p>', '2021-04-23', 'PO03', 33, 'ada', 'UMJE', 2),
(311, 4, ' 135/SPM/UMJE/26/04/2021 \r\n', 'Pengajuan Dana Pembelian Bahan', '<p>Pembelian Bahan Proyek Workshop Engineering di Toko SAE&nbsp;</p>', '2021-04-26', 'PO03', 33, 'ada', 'UMJE', 2),
(312, 4, ' 136/SPM/UMJE/26/04/2021 \r\n', 'Pengajuan Dana Kas ATM BCA', '<p>Dana Kas ATM BCA</p>', '2021-04-26', 'PO03', 33, 'ada', 'UMJE', 2),
(313, 4, ' 137/SPM/UMJE/27/04/2021 \r\n', 'Pengajuan Dana Cash On Hand', '<p>Pengajuan Cash On Hand Syechul Hardiansyah Permana &nbsp;</p>', '2021-04-27', 'PO03', 33, 'ada', 'UMJE', 2),
(314, 4, ' 138/SPM/UMJE/27/04/2021 \r\n', 'Pengajuan Pembelian Bahan Bangunan', '<p>Pembelian Wermes MG Biasa di Toko Dunia Perkakas&nbsp;</p>', '2021-04-27', 'PO03', 33, 'ada', 'UMJE', 2),
(315, 4, ' 139/SPM/UMJE/30/04/2021 \r\n', 'Pengajuan Honorium Tukang', '<ol><li>HR Tukang Bangunan Febri Imam Santoso&nbsp;</li><li>HR Tukang Las Arsin&nbsp;</li><li>HR Tukang Keramik Saiful&nbsp;</li></ol>', '2021-04-30', 'PO03', 33, 'ada', 'UMJE', 2),
(316, 4, ' 140/SPM/UMJE/30/04/2021 \r\n', 'Pengajuan Dana Pembelanjaan Bahan', '<p>Pembayaran Pembeliah Bahan Bangunan di Toko Karimata Abadi&nbsp;</p><p>Pembayaran Pembelian Pasir di CV. Maju SS&nbsp;</p>', '2021-04-30', 'PO03', 33, 'ada', 'UMJE', 2),
(317, 4, '141/SPM/UMJE/04/05/2021\r\n', 'Pengajuan Dana Pembelanjaan Bahan', '<p>Pengurusan dan Pemasangan KWH Meter 3.500&nbsp;</p><p>Workshop Engineering&nbsp;</p>', '2021-05-04', 'PO03', 33, 'ada', 'UMJE', 2),
(318, 4, '142/SPM/UMJE/04/05/2021\r\n', 'Pengajuan Dana Renovasi Fakultas Hukum', '<p>Pembayaran Borongan Pak Nasir Pemasangan Sekat&nbsp;</p>', '2021-05-04', 'PO03', 33, 'ada', 'UMJE', 2),
(319, 4, ' 143/SPM/UMJE/07/05/2021 \r\n', 'Pengajuan Dana Workshop Engineering', '<ol><li>Pembayaran Pembeliah Bahan Bangunan di Toko Karimata Abadi&nbsp;</li><li>Pembayaran Pembelian Pasir di CV. Maju SS&nbsp;</li></ol>', '2021-05-07', 'PO03', 33, 'ada', 'UMJE', 2),
(320, 4, ' 144/SPM/UMJE/07/05/2021 \r\n', 'Pengajuan Honorium Tukang', '<ol><li>HR Tukang Bangunan Febri Imam Santoso&nbsp;</li><li>HR Tukang Las Arsin&nbsp;</li></ol>', '2021-05-07', 'PO03', 33, 'ada', 'UMJE', 2),
(321, 4, ' 145/SPM/UMJE/10/05/2021 \r\n', 'Pengajuan Dana Pembuatan Etalase Mama', '<p>Pembayaran Borongan Pak Nasir Pembuatan Etalase Mama&nbsp;</p>', '2021-05-10', 'PO03', 33, 'ada', 'UMJE', 2),
(322, 4, ' 146/SPM/UMJE/11/05/2021 \r\n', 'Pengajuan Dana Workshop Engineering', '<ol><li>Pembayaran Pembeliah Bahan Bangunan Toko Karimata Abadi&nbsp;</li><li>Pembayaran Pembelian Pasir CV. Maju SS&nbsp;</li></ol>', '2021-05-11', 'PO03', 33, 'ada', 'UMJE', 2),
(323, 4, ' 147/SPM/UMJE/11/05/2021 \r\n', 'Pengajuan Honorium Tukang', '<ol><li>HR Tukang Bangunan Febri Imam Santoso&nbsp;</li><li>HR Tukang Las Arsin&nbsp;</li></ol>', '2021-05-11', 'PO03', 33, 'ada', 'UMJE', 2),
(324, 4, ' 148/SPM/UMJE/17/05/2021 \r\n', 'Pengajuan DP Pembuatan KOP Kampus', '<p>Pembayaran DP Pembuatan KOP Kampus Universitas Muhammadiyah Jember&nbsp;</p>', '2021-05-17', 'PO03', 33, 'ada', 'UMJE', 2),
(325, 4, ' 149/SPM/UMJE/20/05/2021 \r\n', 'Pengajuan Dana Workshop Engineering', '<p>Pembelian Bahan Proyek Workshop Engineering di Toko Mitra Sejati&nbsp;</p>', '2021-05-20', 'PO03', 33, 'ada', 'UMJE', 2),
(326, 4, ' 150/SPM/UMJE/20/05/2021 \r\n', 'Pengajuan Dana Workshop Engineering', '<p>Pembelian Bahan Proyek Workshop Engineering&nbsp;</p>', '2021-05-20', 'PO03', 33, 'ada', 'UMJE', 2),
(327, 4, ' 151/SPM/UMJE/21/05/2021 \r\n', 'Pengajuan Honorium Tukang', '<ol><li>HR Tukang Bangunan Febri Imam Santoso&nbsp;</li><li>HR Tukang Las Arsin&nbsp;</li><li>HR Tukang Las Yasin Mustakim&nbsp;</li><li>HR Tukang Alumunium Pak Nasir&nbsp;</li></ol>', '2021-05-21', 'PO03', 33, 'ada', 'UMJE', 2),
(328, 4, ' 152/SPM/UMJE/21/05/2021 \r\n', 'Pengajuan Dana Workshop Engineering', '<ol><li>Pembayaran Pembeliah Bahan Bangunan di Toko Karimata Abadi&nbsp;</li><li>Pembayaran Pembelian Pasir di CV. Maju SS&nbsp;</li><li>Pembayaran Pembelian Bahan Bangunan di Toko Mitra Sejati&nbsp;</li></ol>', '2021-05-21', 'PO03', 33, 'ada', 'UMJE', 2),
(329, 4, ' 153/SPM/UMJE/22/05/2021 \r\n', 'Pengajuan Dana Workshop Engineering', '<p>Pembelian Bahan Proyek Workshop Engineering&nbsp;</p>', '2021-05-22', 'PO03', 33, 'ada', 'UMJE', 2),
(330, 4, ' 154/SPM/UMJE/22/05/2021 \r\n', 'Pengajuan Pembayaran Pembuatan KOP Kampus term. 2', '<p>Pembayaran Pembuatan KOP Kampus Term.2 Universitas Muhammadiyah Jember&nbsp;</p>', '2021-05-22', 'PO03', 33, 'ada', 'UMJE', 2),
(331, 4, ' 155/SPM/UMJE/24/05/2021 \r\n', 'Pengajuan Pembayaran Pembuatan KOP Kampus term. 3 Pelunasan', '<p>Pembayaran Pembuatan KOP Kampus Term.3 Pelunasan Universitas Muhammadiyah Jember&nbsp;</p>', '2021-05-24', 'PO03', 33, 'ada', 'UMJE', 2),
(332, 4, ' 156/SPM/UMJE/24/05/2021 \r\n', 'Pengajuan Dana Kas ATM BCA', '<p>Kas untuk ATM BCA&nbsp;</p>', '2021-05-24', 'PO03', 33, 'ada', 'UMJE', 2),
(333, 4, ' 157/SPM/UMJE/28/05/2021 \r\n', 'Pengajuan Honorium Tukang', '<ol><li>HR Tukang Bangunan Febri Imam Santoso&nbsp;</li><li>HR Tukang Las Arsin&nbsp;</li><li>HR Pembantu Tukang Las Yasin Mustakim&nbsp;</li></ol>', '2021-05-28', 'PO03', 33, 'ada', 'UMJE', 2),
(334, 4, ' 158/SPM/UMJE/28/05/2021 \r\n', 'Pengajuan Dana Workshop Engineering', '<ol><li>Pembayaran Pembeliah Bahan Bangunan di Toko Karimata Abadi&nbsp;</li><li>Pembayaran Pembelian Pasir di CV. Maju SS&nbsp;</li></ol>', '2021-05-28', 'PO03', 33, 'ada', 'UMJE', 2),
(335, 2, '024/SPM/UMJT/15/03/2021\r\n', 'Operasional Shuttle Bus', '<p>Pulsa CS Shuttle Bus</p><p>Mamiri&nbsp;</p>', '2021-03-15', 'PO03', 35, 'ada', 'UMJT', 1);
INSERT INTO `t_pengajuan` (`id`, `id_mak_4`, `no_pengajuan`, `judul`, `pengajuan`, `tgl_pengajuan`, `status`, `user_id`, `status_hapus`, `user_manager`, `id_divisi`) VALUES
(336, 2, '025/SPM/UMJT/15/03/2021\r\n', 'Shuttle bus 13 Maret 2021', '<p>&nbsp;</p>', '2021-03-15', 'PO03', 35, 'ada', 'UMJT', 1),
(337, 2, '026/SPM/UMJT/15/03/2021\r\n', 'Shuttle Bus 14 Maret 2021', '<p>&nbsp;</p>', '2021-03-15', 'PO03', 35, 'ada', 'UMJT', 1),
(338, 2, '027/SPM/UMJT/15/03/2021\r\n', 'Trip Malang 16 Maret 2021', '<p>&nbsp;</p>', '2021-03-15', 'PO03', 35, 'ada', 'UMJT', 1),
(339, 2, '028/SPM/UMJT/15/03/2021\r\n', 'Shuttle Bus 18 Maret 2021', '<p>&nbsp;</p>', '2021-03-15', 'PO03', 35, 'ada', 'UMJT', 1),
(340, 2, '029/SPM/UMJT/15/03/2021\r\n', 'Shuttle bus 19 Maret 2021', '<p>&nbsp;</p>', '2021-03-15', 'PO03', 35, 'ada', 'UMJT', 1),
(341, 2, '030/SPM/UMJT/15/03/2021\r\n', 'Shuttle Bus 20 Maret 2021', '<p>&nbsp;</p>', '2021-03-15', 'PO03', 35, 'ada', 'UMJT', 1),
(342, 2, '031/SPM/UMJT/15/03/2021\r\n', 'Shuttle Bus 16 Maret 2021', '<p>&nbsp;</p>', '2021-04-15', 'PO03', 35, 'ada', 'UMJT', 1),
(343, 2, '032/SPM/UMJT/19/03/2021\r\n', 'Shuttle BUs 23 Maret 2021', '<p>&nbsp;</p>', '2021-02-19', 'PO03', 35, 'ada', 'UMJT', 1),
(344, 2, '033/SPM/UMJT/19/03/2021\r\n', 'Shuttle Bus 15 Maret 2021', '<p>&nbsp;</p>', '2021-04-19', 'PO03', 35, 'ada', 'UMJT', 1),
(345, 10, '034/SPM/UMJT/23/03/2021\r\n', 'Konsumsi Shooting Iklan', '<p>&nbsp;</p>', '2021-03-23', 'PO03', 35, 'ada', 'UMJT', 1),
(346, 2, '035/SPM/UMJT/25/03/2021\r\n', 'Premi Trip Bali (26-28 Maret 2021)', '<p>&nbsp;</p>', '2021-03-25', 'PO03', 35, 'ada', 'UMJT', 1),
(347, 2, '036/SPM/UMJT/26/03/2021\r\n', 'Premi Trip Situbondo 28 Maret 2021', '<p>US Supir 180.000</p><p>US Kernet 90.000</p>', '2021-03-25', 'PO03', 35, 'ada', 'UMJT', 1),
(348, 2, '037/SPM/UMJT/26/03/2021\r\n', 'Premi Trip Malang 28 Maret 2021', '<p>&nbsp;</p>', '2021-03-26', 'PO03', 35, 'ada', 'UMJT', 1),
(349, 2, '038/SPM/UMJT/26/03/2021\r\n', 'Shuttle Bus 29 Maret 2021', '<p>&nbsp;</p>', '2021-03-26', 'PO03', 35, 'ada', 'UMJT', 1),
(350, 2, '039/SPM/UMJT/26/03/2021\r\n', 'Shuttle Bus 5 April 2021', '<p>&nbsp;</p>', '2021-03-26', 'PO03', 35, 'ada', 'UMJT', 1),
(351, 10, '040/SPM/UMJT/26/03/2021\r\n', 'Belanja BBM Armada HDD', '<p>&nbsp;</p>', '2021-03-26', 'PO03', 35, 'ada', 'UMJT', 1),
(352, 4, ' 159/SPM/UMJE/04/06/2021 \r\n', 'Pengajuan Dana', '<p>Pembayaran Pembeliah Bahan Bangunan di Toko Karimata Abadi&nbsp;</p>', '2021-06-04', 'PO03', 33, 'ada', 'UMJE', 2),
(353, 4, '160/SPM/UMJE/04/06/2021\r\n', 'Pengajuan Honorium Tukang', '<ol><li>HR Tukang Bangunan Febri Imam Santoso&nbsp;</li><li>HR Arsin&nbsp;</li><li>HR Yasin Mustakim&nbsp;</li><li>HR Ahmad Nasir&nbsp;</li></ol>', '2021-06-04', 'PO03', 33, 'ada', 'UMJE', 2),
(354, 4, '161/SPM/UMJE/04/06/2021\r\n', 'Pengajuan Dana', '<p>Pembayaran Pembeliah Bahan Bangunan di Toko Mulia Kaca&nbsp;</p>', '2021-06-04', 'PO03', 33, 'ada', 'UMJE', 2),
(355, 4, '162/SPM/UMJE/07/06/2021\r\n', 'Pengajuan Dana Kas ATM BCA', '<p>Dana Kas ATM BCA&nbsp;</p>', '2021-06-07', 'PO03', 33, 'ada', 'UMJE', 2),
(356, 4, '163/SPM/UMJE/11/06/2021\r\n', 'Pengajuan Honorium Tukang', '<ol><li>HR Tukang Bangunan Febri Imam Santoso&nbsp;</li><li>HR Arsin&nbsp;</li><li>HR Yasin Mustakim&nbsp;</li></ol>', '2021-06-11', 'PO03', 33, 'ada', 'UMJE', 2),
(372, 10, '372/SPM/UMJT/21/06/2021', 'Belanja Operasional', '<p>&nbsp;</p>', '2021-06-21', 'PO02', 35, 'ada', 'UMJT', 1),
(373, 2, '373/SPM/UMJT/21/06/2021', 'Premi Trip Yogyakarta (8 April 2021)', '<p>&nbsp;</p>', '2021-06-21', 'PO02', 35, 'ada', 'UMJT', 1),
(374, 2, '374/SPM/UMJT/21/06/2021', 'Premi Trip Subang (10 April 2021)', '<p>&nbsp;</p>', '2021-06-21', 'PO01', 35, 'hapus', 'UMJT', 1),
(375, 2, '375/SPM/UMJT/01/07/2021', 'Premi Trip Subang (10 April 2021)', '<p>&nbsp;</p>', '2021-07-01', 'PO01', 35, 'hapus', 'UMJT', 1),
(376, 2, '376/SPM/UMJT/01/07/2021', 'Premi Trip Banyuwangi (8 April 2021)', '<p>&nbsp;</p>', '2021-07-01', 'PO02', 35, 'ada', 'UMJT', 1),
(377, 2, '377/SPM/UMJT/01/07/2021', 'Premi Trip Subang (10 April 2021)', '<p>&nbsp;</p>', '2021-07-01', 'PO02', 35, 'ada', 'UMJT', 1),
(378, 2, '378/SPM/UMJT/01/07/2021', 'Premi Trip Surabaya (11 April 2021)', '<p>&nbsp;</p>', '2021-07-01', 'PO01', 35, 'ada', 'UMJT', 1),
(379, 10, '379/SPM/UMJT/01/07/2021', 'Ongkos Ganti Kursi', '<p>&nbsp;</p>', '2021-07-01', 'PO01', 35, 'ada', 'UMJT', 1),
(380, 10, '380/SPM/UMJT/01/07/2021', 'Tagihan Service Hino FC9 NOPOL P 7176 UG', '<p>&nbsp;</p>', '2021-07-01', 'PO01', 35, 'ada', 'UMJT', 1),
(381, 6, '381/SPM/UMJOP/08/08/2021', 'Pembayaran tagihan BPJS', '<p>BPJS Ketenaga Kerjaan Dan BPJS Kesehatan</p>', '2021-08-08', 'PO01', 36, 'ada', 'UMJOP', 3),
(382, 6, '382/SPM/UMJOP/08/08/2021', 'Pengajuan Belanja ATK dan Bahan Habis Pakai', '<p>Belanja ATK :</p><p>MATERAI 6000 @100 PCS&nbsp;</p><p>HVS A4 10 BOX&nbsp;</p><p>HVS F4 2 BOX&nbsp;</p><p>ESTIMASI BELANJA LAIN-LAIN&nbsp;</p>', '2021-08-08', 'PO01', 36, 'ada', 'UMJOP', 3),
(383, 6, '383/SPM/UMJOP/08/08/2021', 'Pengajuan Belanja ATK dan Bahan Habis Pakai', '<p>BELANJA ATK:</p><p>Materai 10000&nbsp;</p><p>Kertas HVS F4 BMO&nbsp;</p><p>Kertas HVS A4 BMO&nbsp;</p><p>Glue Stick&nbsp;</p><p>Lek kekomi&nbsp;</p><p>Sticky Note&nbsp;</p>', '2021-08-08', 'PO01', 36, 'ada', 'UMJOP', 3),
(384, 6, '384/SPM/UMJOP/08/08/2021', 'Pengajuan Belanja ATK dan Bahan Habis Pakai', '<p>Pembelian Kertas</p>', '2021-08-08', 'PO01', 36, 'ada', 'UMJOP', 3),
(385, 6, '385/SPM/UMJOP/08/08/2021', 'Pengajuan Belanja ATK dan Bahan Habis Pakai', '<p>Cetak Brosur PMB</p>', '2021-08-08', 'PO01', 36, 'ada', 'UMJOP', 3),
(386, 6, '386/SPM/UMJOP/08/08/2021', 'Pengajuan Belanja ATK dan Bahan Habis Pakai', '<p>Cetak Laser AP 150 gr&nbsp;</p><p>Cetak Laser AP 260 gr&nbsp;</p><p>Banding&nbsp;</p><p>Jasa Design&nbsp;</p>', '2021-08-08', 'PO01', 36, 'ada', 'UMJOP', 3),
(387, 6, '387/SPM/UMJOP/08/08/2021', 'Pengajuan Belanja ATK dan Bahan Habis Pakai', '<p>Beli Materai</p>', '2021-08-08', 'PO01', 36, 'ada', 'UMJOP', 3),
(388, 6, '388/SPM/UMJOP/08/08/2021', 'Cetak Katalog', '<p>&nbsp;</p>', '2021-08-08', 'PO01', 36, 'ada', 'UMJOP', 3),
(389, 6, '389/SPM/UMJOP/08/08/2021', 'Pelunasan Tagihan Umbul umbul', '<p>&nbsp;</p>', '2021-08-08', 'PO01', 36, 'ada', 'UMJOP', 3),
(390, 6, '390/SPM/UMJOP/08/08/2021', 'Pelunasan Tagihan Belanja ', '<p>&nbsp;</p>', '2021-08-08', 'PO01', 36, 'ada', 'UMJOP', 3),
(391, 6, '391/SPM/UMJOP/08/08/2021', 'Pengajuan Belanja ATK dan Bahan Habis Pakai', '<p>Pembelian TINTA</p>', '2021-08-08', 'PO01', 36, 'ada', 'UMJOP', 3),
(392, 6, '392/SPM/UMJOP/08/08/2021', 'Pengajuan Belanja ATK dan Bahan Habis Pakai', '<p>Pembelian Kertas:</p><p>Kertas HVS A4 BMO&nbsp;</p><p>Kertas Foto Doff&nbsp;</p>', '2021-08-08', 'PO01', 36, 'ada', 'UMJOP', 3),
(393, 6, '393/SPM/UMJOP/08/08/2021', 'Pengajuan Belanja ATK dan Bahan Habis Pakai', '<p>Cetak Brosur&nbsp;</p>', '2021-08-08', 'PO01', 36, 'ada', 'UMJOP', 3),
(394, 6, '394/SPM/UMJOP/08/08/2021', 'Pengajuan Belanja ATK dan Bahan Habis Pakai', '<p>Belanja Tinta Printer</p>', '2021-08-08', 'PO01', 36, 'ada', 'UMJOP', 3),
(395, 6, '395/SPM/UMJOP/08/08/2021', 'Pengajuan Belanja ATK dan Bahan Habis Pakai', '<p>Belanja HVS BMO</p>', '2021-08-08', 'PO01', 36, 'ada', 'UMJOP', 3),
(396, 6, '396/SPM/UMJOP/08/08/2021', 'Pengajuan Belanja ATK dan Bahan Habis Pakai', '<p>Cetak Laser AP&nbsp;</p>', '2021-08-08', 'PO01', 36, 'ada', 'UMJOP', 3),
(397, 6, '397/SPM/UMJOP/08/08/2021', 'Pengajuan Belanja ATK dan Bahan Habis Pakai', '<p>Cetak Laser AP&nbsp;</p>', '2021-08-08', 'PO01', 36, 'hapus', 'UMJOP', 3),
(398, 6, '398/SPM/UMJOP/08/08/2021', 'Pengajuan Belanja ATK dan Bahan Habis Pakai', '<p>Tagihan belanja ATK</p>', '2021-08-08', 'PO01', 36, 'ada', 'UMJOP', 3),
(399, 6, '399/SPM/UMJOP/09/08/2021', 'Pengajuan Belanja ATK dan Bahan Habis Pakai', '<p>Tagihan Belanja ATK</p>', '2021-08-09', 'PO01', 36, 'ada', 'UMJOP', 3),
(400, 6, '400/SPM/UMJOP/09/08/2021', 'Pengajuan Belanja ATK dan Bahan Habis Pakai', '<p>Tagihan Belanja ATK</p>', '2021-08-09', 'PO01', 36, 'hapus', 'UMJOP', 3),
(401, 6, '401/SPM/UMJOP/09/08/2021', 'Pengajuan Belanja ATK dan Bahan Habis Pakai', '<p>Kertas HVS PPO A4&nbsp;</p><p>Kertas HVS PPO F4&nbsp;</p>', '2021-08-09', 'PO01', 36, 'ada', 'UMJOP', 3),
(402, 6, '402/SPM/UMJOP/09/08/2021', 'Pengajuan Belanja ATK dan Bahan Habis Pakai', '<p>Belanja Tinta Printer &nbsp;</p><p>Tagihan Belanja ATK&nbsp;</p><p>Cetak Brosur A3 750 lbr&nbsp;</p>', '2021-08-09', 'PO01', 36, 'ada', 'UMJOP', 3),
(403, 6, '403/SPM/UMJOP/09/08/2021', 'Pengajuan Belanja ATK dan Bahan Habis Pakai', '<p>Tagihan Belanja ATK&nbsp;</p>', '2021-08-09', 'PO01', 36, 'ada', 'UMJOP', 3),
(404, 6, '404/SPM/UMJOP/09/08/2021', 'Pengajuan Belanja ATK dan Bahan Habis Pakai', '<p>Cetak Brosur A3</p>', '2021-08-09', 'PO01', 36, 'ada', 'UMJOP', 3),
(405, 6, '405/SPM/UMJOP/09/08/2021', 'Pengajuan Belanja ATK dan Bahan Habis Pakai', '<p>Kertas HVS70 BMO&nbsp;</p>', '2021-08-09', 'PO01', 36, 'ada', 'UMJOP', 3),
(406, 6, '406/SPM/UMJOP/09/08/2021', 'Pengajuan Belanja ATK dan Bahan Habis Pakai', '<p>Belanja Amplop&nbsp;</p>', '2021-08-09', 'PO01', 36, 'ada', 'UMJOP', 3),
(407, 6, '407/SPM/UMJOP/09/08/2021', 'Pengajuan Belanja ATK dan Bahan Habis Pakai', '<p>Cetak Banner 9 x 3.5&nbsp;</p>', '2021-08-09', 'PO01', 36, 'ada', 'UMJOP', 3),
(409, 4, '1/SPM/UMJE/03/01/2022', 'ISI KAS ATM ENGINEERING', '<p>KAS ENGINEERING PENANGGUNG JAWAB SYECHUL</p>', '2022-01-03', 'PO03', 33, 'ada', 'UMJE', 2),
(410, 2, '1/SPM/UMJT/03/01/2022', 'TRIP PEMALANG 2 JANUARI 2022', '<p>-MUHAMMAD NUR FAUZI- US SUPIR 650.000</p><p>-BENNY AURI- &nbsp;US KERNET 325.000&nbsp;</p><p>-BENNY AURI-&nbsp;BBM 1.800.000&nbsp;</p><p>-BENNY AURI- Internet 100.000</p>', '2022-01-03', 'PO03', 35, 'ada', 'UMJT', 1),
(411, 2, '2/SPM/UMJT/03/01/2022', 'TRIP BONDOWOSO 2 JANUARI 2022', '<p>-AKHMAD MUHAIMIN- US DAN UM SUPIR 175.000&nbsp;</p><p>-Lukman Hakim- US DAN UM KERNET 100.000</p><p>-Lukman Hakim-&nbsp;BBM 250.000</p><p>-Lukman Hakim-&nbsp;Internet 50.000</p>', '2022-01-03', 'PO03', 35, 'ada', 'UMJT', 1),
(412, 6, '1/SPM/UMJOP/04/01/2022', 'SERVICE MESIN FOTOCOPY', '<p>SERVICE MESIN FOTOCOPY</p>', '2022-01-04', 'PO03', 36, 'ada', 'UMJOP', 3),
(413, 14, '1/SPM/Yasmin/04/01/2022', 'Dana Konsumsi Rapotan dan Dana Kaos Olahraga', '<p>Penerima Asri Estiningtyas<br>Dana Konsumsi 1.800.000 dan Dana Kaos Olahraga 490.000</p>', '2022-01-04', 'PO03', 53, 'ada', 'Yasmin', 6),
(414, 15, '1/SPM/GFA/04/01/2022', 'HR Programmer', '<p>-Firdaus Zulkarnain-HR Ke 1 Sistem Informasi Manajemen Koperasi 1.000.000</p><p>-Baharuddin Malik-HR Ke 2 Programmer Pengembangan KSP 1.000.000</p><p>-Dwiki Prayogo-HR Aplikasi Bridge Gaming 1.000.000</p>', '2022-01-04', 'PO03', 46, 'ada', 'GFA', 5),
(415, 1, '2/SPM/GFA/04/01/2022', 'Pembayaran BPJS Januari', '<p>Pembayaran Tagihan BPJS Kesehatan PT MKG 3.651.273&nbsp;</p><p>Pembayaran Tagihan BPJS Kesehatan Tambahan PT MKG 259.127&nbsp;</p><p>Pembayaran Tagihan BPJS Ketenagakerjaan PT MKG 9.086.023&nbsp;</p>', '2022-01-04', 'PO03', 46, 'ada', 'GFA', 5),
(416, 4, '2/SPM/UMJE/04/01/2022', 'Pembayaran Tenaga Las', '<p>-Edi Susanto- Proyek Pembuatan Kanopi Geser Rumah Bu Dewi Term 2 (Pelunasan)</p>', '2022-01-04', 'PO03', 33, 'ada', 'UMJE', 2),
(417, 2, '3/SPM/UMJT/04/01/2022', 'TRIP BONDOWOSO 5 JANUARI 2022', '<p>-Akhmad Muhaimin- US dan UM supir 175.000</p><p>-Lukman Hakim- US dan UM Kernet 100.000</p><p>-Lukman Hakim- BBM 250.000</p><p>-Lukman Hakim- Internet 50.000</p><p>&nbsp;</p>', '2022-01-04', 'PO03', 35, 'ada', 'UMJT', 1),
(418, 16, '3/SPM/GFA/04/01/2022', 'Tagihan PLN Bulan Januari 2022 ', '<p>Diterima dan Dibayarkan Oleh Mas Eka</p>', '2022-01-04', 'PO03', 46, 'ada', 'GFA', 5),
(419, 14, '2/SPM/Yasmin/07/01/2022', ' Dana Bahan Dan Alat KBM Tema \"Kesiapan Dini Menghadapi Bencana\"', '<p>Penerima - Asri Estiningtyas -</p>', '2022-01-07', 'PO03', 53, 'ada', 'Yasmin', 6),
(420, 17, '4/SPM/UMJT/07/01/2022', 'Ganti Talangan Pembayaran Perbaikan Bus', '<p>Penerima -Bagus Indra T-</p>', '2022-01-07', 'PO03', 35, 'ada', 'UMJT', 1),
(421, 2, '5/SPM/UMJT/07/01/2022', 'Trip Jogja 7 Januari', '<p>-Akhmad Muhaimin- US Supir 780.000</p><p>-Didik Hariyadi- US Kernet 390.000</p><p>-Didik Hariyadi- BBM 2.000.000</p><p>-Didik Hariyadi- Internet 101.500</p>', '2022-01-07', 'PO03', 35, 'ada', 'UMJT', 1),
(422, 2, '6/SPM/UMJT/07/01/2022', 'Trip Jogja 7 Januari', '<p>-Wahyu Hidayat- US Supir 780.000</p><p>-Lukman Hakim- US Kernet 390.000</p><p>-Lukman Hakim- BBM 1.800.000</p><p>-Lukman Hakim- Internet 101.500</p><p>-Lukman Hakim- Bongkar Bangku 150.000</p>', '2022-01-07', 'PO03', 35, 'ada', 'UMJT', 1),
(423, 2, '7/SPM/UMJT/07/01/2022', 'Trip Wali 5 Tgl 8 Januari', '<p>-Muhammad Nur F- US Supir 450.000</p><p>-Beny Auri- US Kernet 225.000</p><p>-Beny Auri- BBM 1.200.000</p><p>-Beny Auri-&nbsp;Internet 76.500</p>', '2022-01-07', 'PO03', 35, 'ada', 'UMJT', 1),
(424, 4, '3/SPM/UMJE/07/01/2022', 'HR Tukang Pekerjaan Proyek', '<p>HR Tukang Renovasi Lab. Tanah Faperta 145.000&nbsp;</p><p>HR Tukang Perbaikan Atap Alfanani 965.000</p><p>HR Tukang Perbaikan Atap Alfanani (Plafond) 2.300.000</p><p>HR Tukang Renovasi Ruang Dekan FKIP (Meja Resepsionis) 2.000.000</p><p>HR Tukang Pagar Ruko Teknik Mesin 640.000</p><p>HR Tukang Instalasi Kelistrikan PAUD 169.000</p>', '2022-01-07', 'PO03', 33, 'ada', 'UMJE', 2),
(425, 19, '5/SPM/GFA/10/01/2022', 'Pengisian Gas LPG 3 Kg', '<p>Penerima Eka</p>', '2022-01-10', 'PO03', 46, 'ada', 'GFA', 5),
(426, 14, '3/SPM/Yasmin/10/01/2022', 'DANA MAKAN TPA YASMIN MINGGU 1 DAN 2 JANUARI', '<p>Penerima Asri</p>', '2022-01-10', 'PO01', 53, 'ada', 'Yasmin', 6),
(427, 4, '4/SPM/UMJE/10/01/2022', 'Pembayaran Tenaga Cutting Sticker Proyek Papan Nama PT.BCA', '<p>Penerima Moch.Umar Faruk (Tenaga Ahli)</p>', '2022-01-10', 'PO01', 33, 'ada', 'UMJE', 2),
(428, 17, '8/SPM/UMJT/10/01/2022', 'Biaya Sewa dan Laundry Selimut,Bongkar Bangku dan Uang Harian Kru', '<p>Uang Harian Kru 120.000</p><p>Biaya Perawatan 750.000</p>', '2022-01-10', 'PO01', 35, 'ada', 'UMJT', 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_realisasi`
--

CREATE TABLE `t_realisasi` (
  `id_realisasi` int(11) NOT NULL,
  `id_rinci` int(11) NOT NULL,
  `nilai_realisasi` double NOT NULL,
  `tgl_realisasi` date NOT NULL,
  `no_trans` varchar(100) NOT NULL,
  `tgl_aksi` datetime NOT NULL DEFAULT current_timestamp(),
  `detail` text NOT NULL,
  `supplier` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `t_realisasi`
--

INSERT INTO `t_realisasi` (`id_realisasi`, `id_rinci`, `nilai_realisasi`, `tgl_realisasi`, `no_trans`, `tgl_aksi`, `detail`, `supplier`) VALUES
(3, 33, 2500000, '2021-01-05', '001/SPM/GFA', '2021-05-28 15:52:12', 'Pembelian HP CS', '19'),
(4, 346, 3022000, '2021-05-20', '149/SPM/UMJE/20/05/2021', '2021-07-27 08:20:30', 'Pembelian Bahan Proyek Workshop Engineering', '2'),
(5, 348, 4040000, '2021-05-21', '151/SPM/UMJE/21/05/2021', '2021-07-27 08:34:44', 'HR Tukang Proyek Workshop Engineering', '2'),
(6, 349, 2899000, '2021-05-21', '152/SPM/UMJE/21/05/2021', '2021-07-27 08:41:44', 'Pembelian Bahan Proyek Workshop Engineering', '2'),
(7, 350, 1306000, '2021-05-24', '153/SPM/UMJE/22/05/2021', '2021-07-27 08:45:52', 'Pembelian Bahan Proyek Workshop Engineering', '34'),
(8, 351, 2000000, '2021-05-24', '154/SPM/UMJE/22/05/2021', '2021-07-27 08:55:24', 'Pembayaran Pembuatan KOP Kampus Term.2', '2'),
(9, 352, 1000000, '2021-05-24', '155/SPM/UMJE/24/05/2021', '2021-07-27 08:56:16', 'Pembayaran Pembuatan KOP Kampus Term.3 Pelunasan', '2'),
(10, 353, 19366000, '2021-05-25', '156/SPM/UMJE/24/05/2021', '2021-07-27 09:09:41', 'Biaya Pembelian Bahan Proyek', '2'),
(11, 354, 4975000, '2021-05-28', '157/SPM/UMJE/28/05/2021', '2021-07-27 09:49:49', 'HR Tukang Proyek Workshop Engineering', '50'),
(12, 355, 1956500, '2021-05-28', '158/SPM/UMJE/28/05/2021', '2021-07-27 09:53:51', 'Pembelian Bahan Proyek Workshop Engineering', '2'),
(13, 296, 79000, '2021-05-21', '096/SPM/GFA/21/05/2021', '2021-07-27 14:43:41', ' Isi Gas dan Pulsa Nomer Layanan Kantor', '2'),
(14, 304, 250000, '2021-06-04', '100/SPM/GFA/02/06/2021', '2021-07-27 14:47:41', 'HR Bulan Mei Overtime Umum', '50'),
(15, 298, 890300, '2021-05-27', '098/SPM/GFA/27/05/2021', '2021-07-27 14:56:40', 'Pembelian ATK dan RTK', '2'),
(16, 345, 2000000, '2021-05-18', '148/SPM/UMJE/17/05/2021', '2021-07-27 15:01:45', 'DP Pembuatan KOP Kampus UMJ', '4'),
(17, 347, 1690000, '2021-05-20', '150/SPM/UMJE/20/05/2021', '2021-07-27 15:30:32', 'Pembelian Bahan Proyek Workshop Engineering', '2'),
(18, 39, 74000, '2021-01-12', '003/SPM/GFA/12/01/2021', '2021-07-28 15:26:53', 'Pembelian Operasional Kantor (ATK dan Kartu Perdana)', '2'),
(19, 52, 501170, '2021-01-21', '009/SPM/GFA/21/01/2021', '2021-07-28 15:45:05', 'Pembelian RTK', '2'),
(20, 53, 804300, '2021-01-16', '010/SPM/GFA/16/01/2021', '2021-07-28 15:50:35', 'Biaya ATK', '2'),
(21, 54, 100000, '2021-01-22', '011/SPM/GFA/22/01/2021', '2021-07-28 15:54:11', 'Biaya Pulsa HP Kantor', '2'),
(22, 57, 45000, '2021-01-22', '012/SPM/GFA/22/01/2021', '2021-07-28 16:00:19', 'Biaya Pengiriman Finger Print dan Tambah Proteksi', '19'),
(23, 228, 800000, '2021-02-02', '072/SPM/UMJE/02/02/2021', '2021-08-16 08:32:13', '1 Miyako WDP 300', '2'),
(24, 172, 8940000, '2021-01-14', '030/SPM/UMJE/14/01/2021', '2021-08-16 08:38:59', 'Pembayaran Jasa Pekerjaan Pantry Lt. 1 dan Electrical Lt.2', '2'),
(25, 173, 1130000, '2021-01-14', '031/SPM/UMJE/14/01/2021', '2021-08-16 08:41:23', 'Pembelian Bahan Proyek PT MKG', '2'),
(26, 211, 1200000, '2021-01-21', '052/SPM/UMJE/21/01/2021', '2021-08-16 08:58:12', 'Pembelian 4 buah NANKAI Safety Harness Full Body', '34'),
(27, 214, 2543000, '2021-01-21', '054/SPM/UMJE/21/01/2021', '2021-08-16 09:00:45', 'Pembayaran Jasa Pemasangan AC Lt. 2 PT. MKG', '2'),
(28, 215, 1103000, '2021-01-21', '055/SPM/UMJE/21/01/2021', '2021-08-16 09:03:24', 'HR Tukang Alumunium', '32'),
(29, 219, 373500, '2021-08-28', '063/SPM/UMJE/28/01/2021', '2021-08-16 09:16:51', 'Survei Lokasi SD Muhammadiyah', '15'),
(30, 157, 546500, '2021-01-11', '019/SPM/UMJE/09/01/2021', '2021-08-16 09:35:47', 'Tk. Sampurna Truss (6 btg Reng), Kebutuhan kelistrikan Hidroponik, Pembelian Selang Hidroponik 5 mm ', '2'),
(31, 160, 256500, '2021-01-11', '020/SPM/UMJE/09/01/2021', '2021-08-16 09:38:51', 'Pembelian Bahan Proyek PT MKG', '32'),
(32, 164, 471000, '2021-01-12', '024/SPM/UMJE/12/01/2021', '2021-08-16 09:41:41', 'Pembelian Bahan Proyek SPBU Situbondo', '32'),
(33, 222, 1664000, '2021-08-29', '067/SPM/UMJE/29/01/2021', '2021-08-16 09:46:24', 'Proyek Renovasi Rumah Pak Taufiq, Proyek Pembuatan Rak Buku Pak Delvin, Proyek SD Muhammadiyah', '34'),
(34, 161, 2000000, '2021-01-11', '021/SPM/UMJE/09/01/2021', '2021-08-16 09:49:01', 'Pembelian Bahan Proyek SPBU Situbondo', '33'),
(35, 167, 2525000, '2021-01-13', '028/SPM/UMJE/13/01/2021', '2021-08-16 10:01:22', 'Pembayaran driver pick-up dan BBM, Pembelian Bahan Proyek SPBU Situbondo', '33'),
(36, 224, 493500, '2021-01-01', '069/SPM/UMJE/01/02/2021', '2021-09-22 08:56:37', 'Persiapan Pekerjaan Pembangunan SD Muhammadiyah', '33'),
(37, 63, 121000, '2021-02-01', '009/SPM/UMJT/01/02/2021', '2021-09-22 09:01:05', 'Konsumsi Pasang GPS Bus', '14'),
(38, 225, 1116000, '2021-02-02', '070/SPM/UMJE/01/02/2021', '2021-09-22 09:09:48', 'Pembayaran RTK dan ATK', '2'),
(39, 226, 772100, '2021-02-02', '071/SPM/UMJE/01/02/2021', '2021-09-22 09:14:49', 'Proyek SD Muhammadiyah', '33'),
(40, 229, 432000, '2021-02-03', '073/SPM/UMJE/03/02/2021', '2021-09-22 09:18:18', 'Kebutuhan Proyek SD Muhammadiyah', '15'),
(41, 67, 100000, '2021-02-03', '010/SPM/UMJT/03/02/2021', '2021-09-22 09:22:20', 'Pulsa Manajer Trans', '23'),
(42, 62, 3560000, '2021-02-03', '008/SPM/UMJT/29/01/2021', '2021-09-22 09:27:56', 'Pelunasan Sticker Grade B', '2'),
(43, 230, 4000000, '2021-02-03', '074/SPM/UMJE/03/02/2021', '2021-09-22 09:31:32', 'Tambahan Pembayaran Sewa Alat Berat Excavator', '2'),
(44, 231, 1404000, '2021-02-04', '075/SPM/UMJE/04/02/2021', '2021-09-22 09:34:44', 'Pembelian Bahan Bangunan Proyek Rumah Bu Veny', '2'),
(45, 232, 1041000, '2021-02-04', '076/SPM/UMJE/04/02/2021', '2021-09-22 09:37:18', 'Pembelian Keperluan Proyek', '34'),
(46, 233, 17293500, '2021-02-05', '077/SPM/UMJE/05/02/2021', '2021-09-22 09:40:24', 'Pembelian Bahan Proyek', '33'),
(47, 234, 2220000, '2021-02-05', '078/SPM/UMJE/05/02/2021', '2021-09-22 09:43:03', 'Pembelian Bahan Bangunan Proyek SD Muhammadiyah', '2'),
(48, 235, 4620000, '2021-02-05', '079/SPM/UMJE/05/02/2021', '2021-09-22 09:45:52', 'Pembayaran HR Tukang Pekerjaan Proyek', '32'),
(49, 71, 56000, '2021-02-10', '012/SPM/UMJT/10/02/2021', '2021-09-22 09:50:14', 'Pembayaran Cetak Benner', '2'),
(50, 236, 163500, '2021-02-10', '081/SPM/UMJE/10/02/2021', '2021-09-22 09:52:24', 'Jamuan Tamu', '32'),
(51, 239, 3900000, '2021-02-11', '083/SPM/UMJE/11/02/2021', '2021-09-22 09:58:53', 'HR Tukang Pekerjaan Proyek', '32'),
(52, 240, 1000000, '2021-02-15', '084/SPM/UMJE/15/02/2021', '2021-09-22 10:01:02', 'Kebutuhan Proyek', '32'),
(53, 241, 250000, '2021-02-17', '085/SPM/UMJE/17/02/2021', '2021-09-22 10:05:34', '5 Sak Semen Gresik', '2'),
(54, 75, 600000, '2021-02-19', '013/SPM/UMJT/16/02/2021', '2021-09-22 10:08:08', 'Pembayaran MoyaMU Januari-Februari', '2'),
(55, 242, 15242300, '2021-02-19', '086/SPM/UMJE/19/02/2021', '2021-09-22 10:10:45', 'Pembelian Alat dan Bahan Proyek', '32'),
(56, 246, 830000, '2021-02-19', '087/SPM/UMJE/19/02/2021', '2021-09-22 10:14:05', 'HR Tukang Pekerjaan Proyek', '32'),
(57, 248, 19622300, '2021-02-25', '088/SPM/UMJE/25/02/2021', '2021-09-22 10:17:18', 'Pembelian Kebutuhan Proyek dan Kantor', '32'),
(58, 249, 3450000, '2021-02-26', '089/SPM/UMJE/26/02/2021', '2021-09-22 10:21:01', 'HR Tukang Pekerjaan Proyek', '32'),
(59, 138, 2345000, '2021-01-05', '002/SPM/UMJE/05/01/2021', '2021-09-24 11:25:44', 'Pembelian Bahan Proyek Kolam Hidroponik FAPERTA', '2'),
(60, 139, 1365000, '2021-01-05', '003/SPM/UMJE/05/01/2021', '2021-09-24 11:28:03', 'Pembelian 1.500 Bata dan 15 Sak Semen', '2'),
(61, 159, 2265000, '2021-01-06', '001/SPM/UMJE/05/01/2021', '2021-09-24 11:31:14', 'Pembelian Bahan Proyek', '2'),
(62, 140, 14300000, '2021-01-06', '004/SPM/UMJE/06/01/2021', '2021-09-24 12:49:27', 'Jasa Pekerjaan Plafon dan Elektrikal', '2'),
(63, 143, 1860000, '2021-01-06', '008/SPM/UMJE/06/01/2021', '2021-09-24 12:55:37', 'Pembelian 9 Dus Keramik', '2'),
(64, 141, 4800000, '2021-01-06', '006/SPM/UMJE/06/01/2021', '2021-09-24 12:58:01', '30 Dus Granit', '2'),
(65, 144, 1710000, '2021-01-06', '009/SPM/UMJE/06/01/2021', '2021-09-24 13:06:39', 'Pembelian Kaca', '2'),
(66, 142, 6000000, '2021-01-06', '007/SPM/UMJE/06/01/2021', '2021-09-24 13:29:15', 'Pembelian 1 Unit AC Sharp 2 PK', '2'),
(67, 148, 4330000, '2021-01-08', '013/SPM/UMJE/08/01/2021', '2021-09-24 13:40:11', 'Pembelian Policarbonat', '2'),
(68, 150, 3000000, '2021-01-08', '014/SPM/UMJE/08/01/2021', '2021-09-24 13:42:26', 'Termin 3 Jasa Konstruksi Bpk Sujono', '2'),
(69, 154, 1500000, '2021-01-09', '017/SPM/UMJE/09/01/2021', '2021-09-24 14:26:02', 'HR Tukang Granit', '33'),
(70, 152, 6645000, '2021-01-09', '016/SPM/UMJE/09/01/2021', '2021-09-24 14:27:27', 'Pembelian Baja', '2'),
(71, 165, 1350000, '2021-01-12', '025/SPM/UMJE/12/01/2021', '2021-09-24 14:29:41', 'Pembayaran DP 50% Pasang Wallpaper', '2'),
(72, 162, 4470000, '2021-01-12', '022/SPM/UMJE/12/01/2021', '2021-09-24 14:31:35', 'Jasa Pantry dan Elektrikal Termin 1', '2'),
(73, 163, 1727500, '2021-01-12', '023/SPM/UMJE/12/01/2021', '2021-09-24 15:06:01', 'Pembelian Bahan', '32'),
(74, 171, 4200000, '2021-01-13', '029/SPM/UMJE/13/01/2021', '2021-09-24 15:08:05', 'Pembelian AC Sharp 1,5 PK', '2'),
(75, 166, 1425000, '2021-01-14', '027/SPM/UMJE/14/01/2021', '2021-09-24 15:09:59', 'Pembelian Sunblast dan Jasa Pasang AC', '2'),
(76, 175, 1408000, '2021-01-15', '033/SPM/UMJE/15/01/2021', '2021-09-24 15:12:18', 'HR  Tukang ', '32'),
(77, 174, 1169000, '2021-01-15', '032/SPM/UMJE/15/01/2021', '2021-09-24 15:14:50', 'HR Tukang', '32'),
(78, 193, 239000, '2021-01-16', '037/SPM/UMJE/16/01/2021', '2021-09-24 15:20:15', 'HR Tukang Alumunium', '32'),
(79, 191, 1400000, '2021-01-16', '036/SPM/UMJE/16/01/2021', '2021-09-24 15:22:15', 'HR Tukang Las', '33'),
(80, 177, 1758627, '2021-01-16', '035/SPM/UMJE/16/01/2021', '2021-09-24 15:24:02', 'Pembelian Pompa Air 3 Buah', '32'),
(81, 194, 1450000, '2021-01-18', '038/SPM/UMJE/18/01/2021', '2021-09-24 15:27:21', 'Pelunasan Pasang Wallpaper dan Pembayaran Plamir Wallpaper', '2'),
(82, 196, 600000, '2021-01-18', '040/SPM/UMJE/18/01/2021', '2021-09-24 15:29:11', 'HR Tukang Granit', '33'),
(83, 197, 1146000, '2021-01-18', '041/SPM/UMJE/18/01/2021', '2021-09-24 15:30:55', 'Biaya Bongkar Pasang AC Lt 1', '2'),
(84, 205, 138000, '2021-01-19', '044/SPM/UMJE/18/01/2021', '2021-09-24 15:32:20', 'Pembelian Bahan', '2'),
(85, 204, 214500, '2021-01-19', '043/SPM/UMJE/18/01/2021', '2021-09-24 15:34:07', 'Pembelian Selang Hidroponik 7mm', '32'),
(86, 207, 328000, '2021-01-19', '046/SPM/UMJE/19/01/2021', '2021-09-24 15:35:24', 'Pembelian Kaca', '2'),
(87, 206, 630000, '2021-01-19', '045/SPM/UMJE/18/01/2021', '2021-09-24 15:50:10', 'Pembelian Bahan', '2'),
(88, 203, 1946373, '2021-01-19', '042/SPM/UMJE/18/01/2021', '2021-09-24 15:52:24', 'Pembayaran BPJSTK', '49'),
(89, 208, 842500, '2021-01-19', '048/SPM/UMJE/19/01/2021', '2021-09-24 15:54:14', 'Pembelian Bahan Proyek', '34'),
(90, 209, 168000, '2021-01-20', '050/SPM/UMJE/20/01/2021', '2021-09-24 15:55:45', 'Pembelian HPL', '2'),
(91, 216, 2071000, '2021-01-22', '056/SPM/UMJE/22/01/2021', '2021-09-24 16:02:34', 'HR Tukang', '32'),
(92, 217, 750000, '2021-09-23', '057/SPM/UMJE/23/01/2021', '2021-09-24 16:05:08', 'Pembelian Bahan Bangunan', '33'),
(93, 210, 2129868, '2021-01-20', '051/SPM/UMJE/20/01/2021', '2021-09-24 16:13:50', 'Pembelian Bahan Bangunan', '33'),
(94, 220, 6383500, '2021-01-28', '064/SPM/UMJE/28/01/2021', '2021-09-24 16:16:10', 'Pembelian Bahan Bangunan', '15'),
(95, 221, 2780000, '2021-01-29', '066/SPM/UMJE/29/01/2021', '2021-09-24 16:17:58', 'HR Tukang', '32'),
(96, 223, 9500000, '2021-01-30', '068/SPM/UMJE/29/01/2021', '2021-09-24 16:19:24', 'Sewa Alat Berat', '2'),
(97, 323, 5311500, '2021-04-19', '127/SPM/UMJE/19/04/2021', '2021-11-11 07:56:36', 'Pembelian Bahan Bangunan', '2'),
(98, 324, 1315000, '2021-04-19', '128/SPM/UMJE/19/04/2021', '2021-11-11 07:58:20', 'Cash On Hand', '34'),
(99, 325, 4575000, '2021-04-20', '129/SPM/UMJE/20/04/2021', '2021-11-11 07:59:41', 'Pembelian Bahan Bangunan', '2'),
(100, 326, 8442500, '2021-04-21', '130/SPM/UMJE/21/04/2021', '2021-11-11 08:01:59', 'Pembelian Bahan Bangunan', '2'),
(101, 327, 1688000, '2021-04-22', '131/SPM/UMJE/22/04/2021', '2021-11-11 08:03:34', 'Cash On Hand', '34'),
(102, 328, 8297000, '2021-04-23', '132/SPM/UMJE/23/04/2021', '2021-11-11 08:11:13', 'HR Tukang Pekerjaan Proyek', '51'),
(103, 329, 6764500, '2021-04-23', '133/SPM/UMJE/23/04/2021', '2021-11-11 08:12:55', 'Pembelian Bahan Proyek Workshop Engineering', '2'),
(104, 330, 1108000, '2021-04-26', '134/SPM/UMJE/23/04/2021', '2021-11-11 08:15:29', 'Cash On Hand', '34'),
(105, 331, 3245000, '2021-04-26', '135/SPM/UMJE/26/04/2021', '2021-11-11 08:16:59', 'Pembelian Bahan Bangunan', '2'),
(106, 332, 19749000, '2021-04-29', '136/SPM/UMJE/26/04/2021', '2021-11-11 08:22:58', 'Dana Kas Engineering', '52'),
(107, 333, 2832000, '2021-04-27', '137/SPM/UMJE/27/04/2021', '2021-11-11 08:27:32', 'Cash On Hand', '34'),
(108, 334, 2700000, '2021-04-27', '138/SPM/UMJE/27/04/2021', '2021-11-11 08:29:16', 'Pembelian Bahan Bangunan', '2'),
(109, 335, 9300000, '2021-04-30', '139/SPM/UMJE/30/04/2021', '2021-11-11 08:34:47', 'HR Tukang Pekerjaan Proyek', '51'),
(110, 336, 9914000, '2021-04-30', '140/SPM/UMJE/30/04/2021', '2021-11-11 08:37:02', 'Pembelian Bahan Bangunan', '2'),
(111, 339, 5167500, '2021-05-07', '143/SPM/UMJE/07/05/2021', '2021-11-11 08:38:48', 'Pembelian Bahan Proyek Workshop Engineering', '2'),
(112, 340, 9240000, '2021-05-07', '144/SPM/UMJE/07/05/2021', '2021-11-11 08:40:40', 'HR Tukang Pekerjaan Proyek', '51'),
(113, 341, 275000, '2021-05-10', '145/SPM/UMJE/10/05/2021', '2021-11-11 08:44:04', 'HR  Tukang ', '51'),
(114, 342, 2066000, '2021-05-11', '146/SPM/UMJE/11/05/2021', '2021-11-11 08:45:25', 'Pembelian Bahan Bangunan', '2'),
(115, 344, 2590000, '2021-05-11', '147/SPM/UMJE/11/05/2021', '2021-11-11 08:46:53', 'HR Tukang Pekerjaan Proyek', '51'),
(116, 383, 2525000, '2021-06-04', '159/SPM/UMJE/04/06/2021', '2021-11-11 08:49:07', 'Pembelian Bahan Bangunan', '2'),
(117, 147, 800000, '2021-01-08', '012/SPM/UMJE/08/01/2021', '2021-11-11 08:51:50', 'HR Tukang Pekerjaan Proyek', '32'),
(118, 158, 3839000, '2021-01-09', '015/SPM/UMJE/09/01/2021', '2021-11-11 08:54:44', 'HR Tukang Pekerjaan Proyek', '51'),
(119, 176, 325000, '2021-01-15', '034/SPM/UMJE/15/01/2021', '2021-11-11 09:03:40', 'HR Driver', '53'),
(120, 212, 1490000, '2021-01-21', '053/SPM/UMJE/21/01/2021', '2021-11-11 09:06:57', 'Pembelian Bahan Bangunan', '51'),
(121, 218, 2500000, '2021-01-26', '060/SPM/UMJE/26/01/2021', '2021-11-11 09:09:16', 'Pembelian Bahan Bangunan', '33'),
(122, 308, 100000, '2021-02-08', '082/SPM/UMJE/08/02/2021', '2021-11-11 09:13:58', 'Dana Mobilisasi Survey Lumajang', '51'),
(123, 251, 17190000, '2021-03-03', '090/SPM/UMJE/03/03/2021', '2021-11-11 09:16:02', 'Pembelian Bahan Bangunan', '2'),
(124, 252, 4865000, '2021-03-05', '091/SPM/UMJE/04/03/2021', '2021-11-11 09:18:48', 'Cash On Hand', '32'),
(125, 253, 5510000, '2021-03-05', '092/SPM/UMJE/05/03/2021', '2021-11-11 09:20:58', 'HR Tukang Pekerjaan Proyek', '32'),
(126, 254, 3000000, '2021-03-05', '093/SPM/UMJE/05/03/2021', '2021-11-11 09:28:45', 'Dana Proyek SPBU Situbondo', '54'),
(127, 255, 2153500, '2021-03-15', '094/SPM/UMJE/15/03/2021', '2021-11-11 09:31:04', 'Cash On Hand', '34'),
(128, 256, 4428000, '2021-03-12', '095/SPM/UMJE/12/03/2021', '2021-11-11 09:38:33', 'HR Tukang Pekerjaan Proyek', '51'),
(129, 261, 278000, '2021-03-16', '096/SPM/UMJE/16/03/2021', '2021-11-12 08:31:46', 'Perpanjangan Nomer Layanan', '32'),
(130, 262, 1345000, '2021-03-16', '097/SPM/UMJE/16/03/2021', '2021-11-12 08:37:18', 'Pembelian Bahan Bangunan', '2'),
(131, 263, 1770000, '2021-03-16', '098/SPM/UMJE/16/03/2021', '2021-11-12 08:39:04', 'Pembelian Bahan Proyek Workshop Engineering', '2'),
(132, 309, 1150000, '2021-03-17', '099/SPM/UMJE/17/03/2021', '2021-11-12 08:41:43', 'Pembayaran Tenaga Kerja', '51'),
(133, 264, 20000000, '2021-03-17', '100/SPM/UMJE/17/03/2021', '2021-11-12 08:43:29', 'Isi Kas ATM', '52'),
(134, 266, 2150500, '2021-03-18', '101/SPM/UMJE/18/03/2021', '2021-11-12 08:46:14', 'Pembelian Bahan Bangunan', '15'),
(135, 267, 4482000, '2021-03-19', '102/SPM/UMJE/19/03/2021', '2021-11-12 08:48:10', 'HR Tukang Pekerjaan Proyek', '51'),
(136, 268, 1875500, '2021-03-19', '103/SPM/UMJE/19/03/2021', '2021-11-12 08:50:54', 'Pembayaran Jasa Pasang Sekat', '51'),
(137, 270, 1550000, '2021-03-26', '104/SPM/UMJE/26/03/2021', '2021-11-12 08:53:53', 'HR Tukang Alumunium', '51'),
(138, 310, 4375000, '2021-03-26', '105/SPM/UMJE/26/03/2021', '2021-11-12 08:55:29', 'HR  Tukang ', '51'),
(139, 271, 5289000, '2021-03-26', '106/SPM/UMJE/26/03/2021', '2021-11-12 08:56:59', 'Pembelian Bahan Proyek Workshop Engineering', '2'),
(140, 272, 1300000, '2021-03-26', '107/SPM/UMJE/26/03/2021', '2021-11-12 08:59:45', 'Pembayaran Wallpaper dan Jasa Pemasangan', '2'),
(141, 274, 3000000, '2021-03-29', '108/SPM/UMJE/29/03/2021', '2021-11-12 09:01:27', 'Pembelian Bahan Bangunan', '51'),
(142, 275, 3272500, '2021-03-30', '109/SPM/UMJE/30/03/2021', '2021-11-12 09:03:39', 'Pembelian Bahan Bangunan', '2'),
(143, 276, 462000, '2021-03-30', '110/SPM/UMJE/20/05/2021', '2021-11-12 09:06:31', 'Pembelian Bahan Bangunan', '2'),
(144, 277, 20000000, '2021-03-30', '111/SPM/UMJE/30/03/2021', '2021-11-12 09:09:33', 'Isi Kas ATM', '52'),
(145, 278, 498000, '2021-04-01', '112/SPM/UMJE/31/03/2021', '2021-11-12 09:11:27', 'Pembayaran RTK', '2'),
(146, 279, 17267000, '2021-03-31', '113/SPM/UMJE/31/03/2021', '2021-11-12 09:13:10', 'Pembelian Bahan Bangunan', '2'),
(147, 300, 2200000, '2021-04-01', '114/SPM/UMJE/01/04/2021', '2021-11-12 09:15:22', 'Pembayaran Tenaga Kerja', '51'),
(148, 301, 4626000, '2021-04-02', '115/SPM/UMJE/02/04/2021', '2021-11-12 09:17:18', 'HR Tukang Pekerjaan Proyek', '51'),
(149, 302, 2250000, '2021-04-01', '116/SPM/UMJE/01/04/2021', '2021-11-12 09:18:52', 'Pembelian Bahan Bangunan', '2'),
(150, 311, 3715500, '2021-04-06', '117/SPM/UMJE/02/04/2021', '2021-11-12 09:23:25', 'Pembelian Bahan Proyek Workshop Engineering', '2'),
(151, 312, 8080000, '2021-04-07', '118/SPM/UMJE/07/04/2021', '2021-11-12 09:25:00', 'Pembayaran Tenaga Kerja', '51'),
(152, 313, 4190000, '2021-04-09', '119/SPM/UMJE/09/04/2021', '2021-11-12 09:26:31', 'HR Tukang Pekerjaan Proyek', '51'),
(153, 315, 210000, '2021-04-07', '120/SPM/UMJE/07/04/2021', '2021-11-12 09:28:20', 'Pembayaran MoyaMU', '2'),
(154, 316, 10860000, '2021-04-08', '121/SPM/UMJE/09/04/2021', '2021-11-12 09:29:47', 'Pembelian Bahan Bangunan', '2'),
(155, 317, 7901000, '2021-04-09', '122/SPM/UMJE/09/04/2021', '2021-11-12 09:31:26', 'Pembelian Bahan Bangunan', '2'),
(156, 318, 2030000, '2021-04-12', '123/SPM/UMJE/12/04/2021', '2021-11-12 09:33:26', 'Pembelian Bahan Bangunan', '2'),
(157, 319, 250000, '2021-04-16', '124/SPM/UMJE/16/04/2021', '2021-11-12 09:35:29', 'Pembayaran Tenaga Kerja', '51'),
(158, 320, 4780000, '2021-04-16', '125/SPM/UMJE/16/04/2021', '2021-11-12 09:38:05', 'HR Tukang Pekerjaan Proyek', '51'),
(159, 322, 5977500, '2021-04-16', '126/SPM/UMJE/16/04/2021', '2021-11-12 09:40:00', 'Pembelian Bahan Bangunan', '2'),
(160, 337, 5266500, '2021-05-04', '141/SPM/UMJE/04/05/2021', '2021-11-12 09:41:31', 'Pembelian Bahan Bangunan', '2'),
(161, 338, 1850000, '2021-05-04', '142/SPM/UMJE/04/05/2021', '2021-11-12 09:43:28', 'Pembayaran Tenaga Kerja', '51'),
(162, 384, 5145000, '2021-06-04', '160/SPM/UMJE/04/06/2021', '2021-11-12 09:45:40', 'HR Tukang Pekerjaan Proyek', '51'),
(163, 385, 4756000, '2021-06-04', '161/SPM/UMJE/04/06/2021', '2021-11-12 09:47:07', 'Pembelian Bahan Bangunan', '2'),
(164, 386, 20058000, '2021-06-07', '162/SPM/UMJE/07/06/2021', '2021-11-12 09:48:41', 'Isi Kas ATM', '52'),
(165, 387, 2070000, '2021-06-11', '163/SPM/UMJE/11/06/2021', '2021-11-12 09:50:21', 'HR Tukang Pekerjaan Proyek', '51'),
(166, 102, 184500, '2021-01-06', '002/SPM/GFA/06/01/2021', '2021-11-22 07:49:22', 'Pembelian ATK', '19'),
(167, 103, 716500, '2021-01-06', '002/SPM/GFA/06/01/2021', '2021-11-22 07:50:35', 'Pembelian RTK', '19'),
(168, 43, 18360000, '2021-01-16', '004/SPM/GFA/16/01/2021', '2021-11-22 07:52:38', 'Pembelian Mebelair', '2'),
(169, 51, 5358423, '2021-01-19', '008/SPM/GFA/15/01/2021', '2021-11-22 08:36:54', 'Pembayaran BPJSTK', '55'),
(170, 58, 4197700, '2021-01-26', '013/SPM/GFA/26/01/2021', '2021-11-22 08:42:50', 'Pembayaran PLN', '19'),
(171, 61, 300000, '2021-01-26', '014/SPM/GFA/26/01/2021', '2021-11-22 09:14:22', 'Pembelian RTK', '19'),
(172, 64, 3000000, '2021-01-27', '015/SPM/GFA/27/01/2021', '2021-11-22 09:25:32', 'Tunjangan Manager GFA', '17'),
(173, 65, 1414260, '2021-01-27', '016/SPM/GFA/27/01/2021', '2021-11-22 09:36:53', 'Pembelian Gorden Kantor', '2'),
(174, 69, 100000, '2021-01-27', '018/SPM/GFA/27/01/2021', '2021-11-22 09:50:52', 'Pembelian Token Listrik', '21'),
(175, 30, 16100, '2021-01-27', '019/SPM/GFA/29/01/2021', '2021-11-22 09:53:23', 'Pembelian Baterai Remote AC', '21'),
(176, 72, 495000, '2021-01-29', '020/SPM/GFA/29/01/2021', '2021-11-22 09:55:16', 'Pembelian RTK', '21'),
(177, 183, 41487740, '2021-03-01', '042/SPM/GFA/01/03/2021', '2021-11-22 09:57:05', 'Gaji Karyawan', '58'),
(178, 181, 700000, '2021-03-01', '143/SPM/UMJE/01/03/2021', '2021-11-22 09:59:09', 'HR Supervisor Tenaga Umum dan Bartender', '57'),
(179, 180, 184200, '2021-03-01', '044/SPM/UMJE/01/03/2021', '2021-11-22 10:00:52', 'Pembelian Jamuan Tamu', '21'),
(180, 185, 8884140, '2021-03-05', '046/SPM/GFA/05/03/2021', '2021-11-22 10:11:20', 'Pembayaran BPJS TK dan BPJS Kesehatan', '55'),
(181, 187, 150000, '2021-03-01', '047/SPM/GFA/01/03/2021', '2021-11-22 10:20:32', 'HR Over Time Umum', '21'),
(182, 186, 1000000, '2021-03-01', '047/SPM/GFA/01/03/2021', '2021-11-22 10:22:06', 'HR Programer Dwiki Prayoga', '56'),
(183, 188, 46600, '2021-03-09', '048/SPM/GFA/09/03/2021', '2021-11-22 10:24:11', 'Pembelian RTK', '21'),
(184, 189, 100000, '2021-03-12', '049/SPM/GFA/12/03/2021', '2021-11-22 10:57:27', 'Biaya Operasional Perdin Trans', '23'),
(185, 192, 31000, '2021-03-16', '050/SPM/GFA/16/03/2021', '2021-11-22 11:00:40', 'Biaya ATK', '20'),
(186, 198, 23200, '2021-03-25', '053/SPM/GFA/25/03/2021', '2021-11-22 14:22:14', 'Pembelian RTK', '21'),
(187, 179, 350000, '2021-03-01', '045/SPM/GFA/01/03/2021', '2021-11-22 14:25:41', 'HR Tenaga Magang P3LM', '60'),
(188, 190, 412506, '2021-03-15', '050/SPM/GFA/15/03/2021', '2021-11-22 14:27:43', 'Pembayaran PLN ', '20'),
(189, 201, 613000, '2021-03-16', '052/SPM/GFA/16/03/2021', '2021-11-22 14:29:40', 'Biaya Iklan Promo IG', '61'),
(190, 199, 285000, '2021-03-25', '054/SPM/GFA/25/03/2021', '2021-11-22 14:31:44', 'Pembayaran MoyaMU', '2'),
(191, 200, 1200000, '2021-03-25', '055/SPM/GFA/25/03/2021', '2021-11-22 14:34:50', 'HR Shooting Podcast ke Fajar Nurrochmansyah', '56'),
(192, 202, 440980, '2021-03-27', '056/SPM/GFA/01/03/2021', '2021-11-22 14:47:08', 'Pembayaran BPJSTK PT THS', '55'),
(193, 227, 250000, '2021-04-05', '057/SPM/GFA/05/04/2021', '2021-11-22 14:50:46', 'Dana Suka Cita kelahiran anak', '41'),
(194, 237, 1000000, '2021-04-01', '058/SPM/GFA/01/04/2021', '2021-11-22 14:53:27', 'HR Programer Dwiki Prayogo', '56'),
(195, 238, 250000, '2021-04-01', '058/SPM/GFA/01/04/2021', '2021-11-22 14:54:57', 'HR Over Time Umum', '21'),
(196, 247, 187500, '2021-04-05', '060/SPM/GFA/05/04/2021', '2021-11-22 15:05:08', 'Pembelian RTK', '21'),
(197, 250, 750000, '2021-04-05', '061/SPM/GFA/05/04/2021', '2021-11-22 15:10:11', 'Dana Duka Cita Ibunda Akhmad Suharto', '17'),
(198, 258, 90000, '2021-04-08', '063/SPM/GFA/08/04/2021', '2021-11-22 15:21:38', 'Pembelian Konsumsi Rapat', '21'),
(199, 280, 16800, '2021-04-12', '064/SPM/GFA/08/04/2021', '2021-11-22 15:24:35', 'Pembayaran BPJSTK Bapak Aswari', '55'),
(200, 265, 350000, '2021-04-15', '068/SPM/GFA/15/04/2021', '2021-11-22 15:26:17', 'HR Disign Flyer Shuttle', '2'),
(201, 269, 100000, '2021-04-15', '069/SPM/GFA/15/04/2021', '2021-11-22 15:28:47', 'Sumbangan Ke MUI Jember', '59'),
(202, 283, 6265351, '2021-04-15', '069/SPM/GFA/15/04/2021', '2021-11-22 15:37:27', 'Pembayaran BPJSTK', '55'),
(203, 281, 2944575, '2021-04-10', '059/SPM/GFA/01904/2021', '2021-11-22 15:40:58', 'Pembayaran BPJS Kesehatan', '55'),
(204, 273, 1750000, '2021-04-19', '071/SPM/GFA/19/04/2021', '2021-11-22 15:45:17', 'Biaya Marketing Trans', '57'),
(205, 273, 1000000, '2021-04-19', '071/SPM/GFA/19/04/2021', '2021-11-22 15:46:13', 'Biaya Marketing Trans', '57'),
(206, 284, 51500, '2021-04-19', '073/SPM/GFA/19/04/2021', '2021-11-22 15:47:53', 'Pembelian pulsa CS', '19'),
(207, 285, 1200000, '2021-04-20', '077/SPM/GFA/20/04/2021', '2021-11-22 15:49:18', 'HR Shooting Podcast ke Fajar Nurrochmansyah', '56'),
(208, 286, 1500000, '2021-04-26', '079/SPM/GFA/26/04/2021', '2021-11-22 15:51:02', 'Tunjangan Kelahiran Anak', '15'),
(209, 287, 293000, '2021-04-21', '081/SPM/GFA/21/04/2021', '2021-11-22 15:52:30', 'Pembelian Pigura dan Cetak Foto', '19'),
(210, 288, 1300000, '2021-04-21', '085/SPM/GFA/22/04/2021', '2021-11-22 15:54:52', 'Biaya Akomodasi Marketing UM Trans', '7'),
(211, 289, 5999000, '2021-04-22', '085/SPM/GFA/22/04/2021', '2021-11-22 15:56:11', 'Pembelian Aset HP', '17'),
(212, 290, 60000, '2021-04-29', '087/SPM/GFA/29/04/2021', '2021-11-22 15:57:58', 'Pembayaran Service AC Kantor lantai 2', '2'),
(213, 291, 39766850, '2021-04-30', '088/SPM/GFA/30/04/2021', '2021-11-22 15:59:23', 'Gaji Karyawan MKG', '58'),
(214, 292, 250000, '2021-04-30', '089/SPM/GFA/30/04/2021', '2021-11-22 16:00:25', 'HR Over Time Umum', '21'),
(215, 293, 27134803, '2021-05-04', '090/SPM/GFA/04/05/2021', '2021-11-22 16:02:14', 'THR Karyawan ', '58'),
(216, 295, 270000, '2021-05-11', '095/SPM/GFA/11/05/2021', '2021-11-22 16:03:42', 'Biaya Rapid Test Elok dan Robi', '58'),
(217, 299, 39861076, '2021-05-31', '090/SPM/GFA/31/05/2021', '2021-11-22 16:05:45', 'Gaji Karyawan MKG', '58'),
(218, 34, 2416633, '2021-01-20', '001/SPM/UMJT/20/01/2021', '2021-11-23 12:40:33', 'Pembayaran BPJS', '49'),
(219, 36, 235000, '2021-01-07', '002/SPM/UMJT/12/01/2021', '2021-11-23 12:42:25', 'Pembelian Oli ', '22'),
(220, 40, 900000, '2021-01-08', '003/SPM/UMJT/12/01/2021', '2021-11-23 12:54:48', 'US Supir', '28'),
(221, 41, 2278329, '2021-01-08', '003/SPM/UMJT/12/01/2021', '2021-11-23 13:00:52', 'Pembelian BBM,Pulsa Internet,dan Pewangi Toilet', '26'),
(222, 42, 450000, '2021-01-08', '003/SPM/UMJT/12/01/2021', '2021-11-23 13:02:04', 'US Kernet', '26'),
(223, 47, 520000, '2021-01-09', '004/SPM/UMJT/12/01/2021', '2021-11-23 13:12:16', 'US Supir', '30'),
(224, 48, 50000, '2021-01-09', '004/SPM/UMJT/12/01/2021', '2021-11-23 13:13:19', 'Pembelian Pulsa Internet', '27'),
(225, 50, 260000, '2021-01-09', '004/SPM/UMJT/12/01/2021', '2021-11-23 13:15:32', 'US Kernet', '27'),
(226, 49, 912758, '2021-01-09', '004/SPM/UMJT/12/01/2021', '2021-11-23 13:18:41', 'Pembelian BBM,Switch Klakson,dan Lem G', '27'),
(227, 55, 126730, '2021-01-26', '005/SPM/UMJT/26/01/2021', '2021-11-23 13:33:20', 'Pembelian Kebutuhan Kantor dan Garasi', '22'),
(228, 56, 523000, '2021-01-26', '005/SPM/UMJT/26/01/2021', '2021-11-23 13:34:43', 'Pembelian Kebutuhan Kantor dan Garasi', '22'),
(229, 59, 1199000, '2021-01-26', '006/SPM/UMJT/26/01/2021', '2021-11-23 13:36:47', 'Pembayaran Pasang Sticker Bus', '2'),
(230, 495, 16000, '2021-02-09', '011/SPM/UMJT/09/02/2021', '2021-11-23 13:38:53', 'Cetak Banner', '23'),
(231, 104, 300000, '2021-03-02', '017/SPM/UMJT/01/03/2021', '2021-11-23 13:40:33', 'Belanja Kebutuhan Kantor dan Garasi', '22'),
(232, 105, 101500, '2021-03-08', '018/SPM/UMJT/08/03/2021', '2021-11-23 13:42:42', 'Pulsa Manajer Trans', '23'),
(233, 106, 260000, '2021-03-07', '019/SPM/UMJT/08/03/2021', '2021-11-23 13:50:51', 'US Supir', '57'),
(234, 107, 50000, '2021-03-07', '019/SPM/UMJT/08/03/2021', '2021-11-23 13:52:16', 'US Fafan', '22'),
(235, 108, 200000, '2021-03-07', '019/SPM/UMJT/08/03/2021', '2021-11-23 13:53:15', 'E-Toll', '57'),
(236, 111, 130000, '2021-03-07', '019/SPM/UMJT/08/03/2021', '2021-11-23 13:54:13', 'US Kernet', '57'),
(237, 109, 120000, '2021-03-07', '019/SPM/UMJT/08/03/2021', '2021-11-23 13:55:33', 'Uang Makan Kru', '57'),
(238, 110, 700000, '2021-03-07', '019/SPM/UMJT/08/03/2021', '2021-11-23 13:56:35', 'BBM', '57'),
(239, 112, 260000, '2021-03-08', '020/SPM/UMJT/09/03/2021', '2021-11-23 14:01:42', 'US Supir', '22'),
(240, 114, 200000, '2021-03-08', '020/SPM/UMJT/09/03/2021', '2021-11-23 14:04:52', 'E-Toll', '22'),
(241, 116, 130000, '2021-03-08', '020/SPM/UMJT/09/03/2021', '2021-11-23 14:06:21', 'US Kernet', '22'),
(242, 115, 120000, '2021-03-08', '020/SPM/UMJT/09/03/2021', '2021-11-23 14:10:26', 'Uang Makan Kru', '22'),
(243, 113, 721518, '2021-03-08', '020/SPM/UMJT/09/03/2021', '2021-11-23 14:12:46', 'BBM', '22'),
(244, 118, 151200, '2021-03-10', '021/SPM/UMJT/10/03/2021', '2021-11-23 14:16:31', 'Pembelian Mamiri', '23'),
(245, 117, 60000, '2021-03-10', '021/SPM/UMJT/10/03/2021', '2021-11-23 14:17:50', 'Pembelian Paket Data', '23'),
(246, 119, 150000, '2021-03-12', '022/SPM/UMJT/10/03/2021', '2021-11-23 14:19:31', 'Pembelian Flasdisk', '22'),
(247, 356, 75000, '2021-03-15', '024/SPM/UMJT/15/03/2021', '2021-11-23 14:22:51', 'Pembelian pulsa CS', '23'),
(248, 357, 145000, '2021-03-15', '024/SPM/UMJT/15/03/2021', '2021-11-23 14:23:43', 'Belanja Mamiri dan Map Klip', '23'),
(249, 358, 1419306, '2021-03-13', '025/SPM/UMJT/15/03/2021', '2021-11-23 14:26:16', 'HR Supir,Kernet,dan Kebutuhan Shuttle', '22'),
(250, 359, 1435805, '2021-03-14', '026/SPM/UMJT/15/03/2021', '2021-11-23 14:27:37', 'HR Supir,Kernet,dan Kebutuhan Shuttle', '22'),
(251, 360, 1085483, '2021-03-15', '027/SPM/UMJT/15/03/2021', '2021-11-23 14:29:16', 'US Supir,Kernet,dan Kebutuhan Trip', '57'),
(252, 361, 1484701, '2021-03-16', '028/SPM/UMJT/15/03/2021', '2021-11-23 14:30:40', 'HR Supir,Kernet,dan Kebutuhan Shuttle', '23'),
(253, 362, 1432000, '2021-03-16', '029/SPM/UMJT/15/03/2021', '2021-11-23 14:31:49', 'HR Supir,Kernet,dan Kebutuhan Shuttle', '23'),
(254, 363, 1473234, '2021-03-16', '030/SPM/UMJT/15/03/2021', '2021-11-23 14:33:05', 'HR Supir,Kernet,dan Kebutuhan Shuttle', '23'),
(255, 364, 1458266, '2021-03-14', '031/SPM/UMJT/15/03/2021', '2021-11-23 14:34:14', 'HR Supir,Kernet,dan Kebutuhan Shuttle', '22'),
(256, 365, 1467093, '2021-03-19', '032/SPM/UMJT/19/03/2021', '2021-11-23 14:35:23', 'HR Supir,Kernet,dan Kebutuhan Shuttle', '23'),
(257, 366, 1472843, '2021-03-15', '033/SPM/UMJT/19/03/2021', '2021-11-23 14:36:25', 'HR Supir,Kernet,dan Kebutuhan Shuttle', '22'),
(258, 367, 200000, '2021-03-23', '034/SPM/UMJT/23/03/2021', '2021-11-23 14:37:43', 'Pembelian Konsumsi Shooting Iklan', '23'),
(259, 368, 780000, '2021-03-25', '035/SPM/UMJT/25/03/2021', '2021-11-23 14:43:19', 'US Supir', '30'),
(260, 371, 390000, '2021-03-25', '035/SPM/UMJT/25/03/2021', '2021-11-23 14:45:40', 'US Kernet', '27'),
(261, 370, 1299135, '2021-03-25', '035/SPM/UMJT/25/03/2021', '2021-11-23 14:46:55', 'BBM, dan Parkir', '27'),
(262, 369, 100000, '2021-03-25', '035/SPM/UMJT/25/03/2021', '2021-11-23 14:47:59', 'Pembelian Pulsa Internet', '27'),
(263, 372, 180000, '2021-03-26', '036/SPM/UMJT/26/03/2021', '2021-11-23 15:20:57', 'US Supir', '22'),
(264, 375, 90000, '2021-03-26', '036/SPM/UMJT/26/03/2021', '2021-11-23 15:21:47', 'US Kernet', '22'),
(265, 374, 443526, '2021-03-26', '036/SPM/UMJT/26/03/2021', '2021-11-23 15:22:53', 'BBM', '22'),
(266, 373, 30000, '2021-03-26', '036/SPM/UMJT/26/03/2021', '2021-11-23 15:23:58', 'Pembelian Pulsa Internet', '22'),
(267, 376, 300000, '2021-03-26', '037/SPM/UMJT/26/03/2021', '2021-11-23 15:26:16', 'US Supir', '22'),
(268, 379, 150000, '2021-03-26', '037/SPM/UMJT/26/03/2021', '2021-11-23 15:26:57', 'US Kernet', '22'),
(269, 378, 850000, '2021-03-26', '037/SPM/UMJT/26/03/2021', '2021-11-23 15:27:46', 'BBM', '22'),
(270, 377, 50000, '2021-03-26', '037/SPM/UMJT/26/03/2021', '2021-11-23 15:28:42', 'Pembelian Pulsa Internet', '22'),
(271, 380, 1427625, '2021-03-26', '038/SPM/UMJT/26/03/2021', '2021-11-23 15:29:53', 'HR Supir,Kernet,dan Kebutuhan Shuttle', '23'),
(272, 381, 1448703, '2021-03-26', '039/SPM/UMJT/26/03/2021', '2021-11-23 15:31:12', 'HR Supir,Kernet,dan Kebutuhan Shuttle', '23'),
(273, 382, 300000, '2021-03-26', '040/SPM/UMJT/26/03/2021', '2021-11-23 15:32:21', 'Belanja BBM Armada HDD', '23'),
(274, 73, 3505700, '2021-01-12', '001/SPM/UMJOP/12/01/2021', '2021-11-23 15:37:13', 'Pembelanjaan ATK', '23'),
(275, 74, 2250000, '2021-01-25', '002/SPM/UMJOP/25/01/2021', '2021-11-23 15:39:00', 'Pembayaran Belanja ATK', '2'),
(276, 496, 11464603, '2021-06-08', '101/GFA/UMJT/07/06/2021', '2021-11-23 15:55:03', 'Pembayaran bpjs', '55'),
(277, 497, 1040500, '2021-06-08', '101/SPM/GFA/07/06/2021', '2021-11-23 15:56:31', 'Pembayaran PLN', '21'),
(278, 282, 568923, '2021-04-10', '059/SPM/GFA/09/04/2021', '2021-11-23 16:07:38', 'Pembayaran PLN', '62'),
(279, 498, 6282151, '2021-06-17', '103/SPM/GFA/10/06/2021', '2021-11-23 16:22:05', 'Pembayaran BPJS dan PLN', '55'),
(280, 509, 1000000, '2022-01-04', 'FT2200412WZ3', '2022-01-07 10:04:45', 'HR KE 2 PROGRAMMER PENGEMBANGAN KSP ', '58'),
(281, 510, 1000000, '2022-01-04', 'FT22004489PG ', '2022-01-07 10:07:14', 'HR APLIKASI BRIDGE GAMING -DWIKI PRAYOGO-', '58'),
(282, 511, 1000000, '2022-01-04', 'FT220043Y5KN ', '2022-01-07 10:08:52', 'HR KE 1 SISTEM INFORMASI MANAJEMEN KOPERASI -FIRDAUS ZULKARNAIN-', '58'),
(283, 503, 1300000, '2022-01-04', 'FIRDAUS ZULKARNAIN', '2022-01-07 10:10:28', 'PEMBAYARAN SERVICE MESIN FOTOCOPY ', '2'),
(284, 528, 528600, '2022-01-05', '003/SPM/GFA/04/01/2022', '2022-01-07 10:13:25', 'Pembayaran PLN Bulan Januari', '62'),
(285, 515, 6200000, '2022-01-05', 'FT22005M0TMZ ', '2022-01-07 13:10:20', 'PEMBAYARAN TENAGA LAS PROYEK KANOPI GESER BU DEWI TERM 2 PELUNASAN ', '51'),
(286, 544, 145000, '2022-01-07', '003/SPM/UMJE/07/01/2022', '2022-01-07 14:22:39', 'HR Tukang Renovasi Lab. Tanah Faperta', '51'),
(287, 545, 169000, '2022-01-07', '003/SPM/UMJE/07/01/2022', '2022-01-07 14:24:10', 'HR Tukang Instalasi Kelistrikan PAUD', '51'),
(288, 546, 640000, '2022-01-07', '003/SPM/UMJE/07/01/2022', '2022-01-07 14:25:29', 'HR Tukang Pagar Ruko Teknik Mesin', '51'),
(289, 547, 2000000, '2022-01-07', '003/SPM/UMJE/07/01/2022', '2022-01-07 14:27:10', 'HR Tukang Renovasi Ruang Dekan FKIP (Meja Resepsionis)', '51'),
(290, 548, 2300000, '2022-01-07', '003/SPM/UMJE/07/01/2022', '2022-01-07 14:28:33', 'HR Tukang Perbaikan Atap Alfanani (Plafond)', '51'),
(291, 549, 965000, '2022-01-07', '003/SPM/UMJE/07/01/2022', '2022-01-07 14:29:46', 'HR Tukang Perbaikan Atap Alfanani', '51'),
(292, 530, 1575000, '2022-01-07', 'FT22007QDTDK ', '2022-01-07 14:37:21', 'GANTI TALANGAN PEMBAYARAN PERBAIKAN BUS', '14'),
(293, 507, 1800000, '2022-01-03', '001/PGJ/SEKLAB YASMIN/I/2022', '2022-01-10 15:56:35', 'PEMBELIAN MAMIRAT RAPOTAN', '2'),
(294, 508, 490000, '2022-01-04', ' FT22004LD31J ', '2022-01-10 15:58:50', 'Pembelian Kaos Olahraga', '2'),
(295, 550, 20000, '2022-01-10', '005/SPM/GFA/10/01/2022', '2022-01-10 16:00:07', 'Pengisian Gas LPG 3 Kg', '21');

-- --------------------------------------------------------

--
-- Table structure for table `t_rinci`
--

CREATE TABLE `t_rinci` (
  `id_rinci` int(11) NOT NULL,
  `id_pengajuan` int(11) NOT NULL,
  `coa_4_id` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `harga` int(20) NOT NULL,
  `total_harga` int(21) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `t_rinci`
--

INSERT INTO `t_rinci` (`id_rinci`, `id_pengajuan`, `coa_4_id`, `jumlah`, `harga`, `total_harga`) VALUES
(1, 1, 11, 1, 4250000, 4250000),
(4, 3, 11, 1, 121000, 121000),
(5, 4, 11, 1, 100000, 100000),
(6, 5, 11, 1, 37822181, 37822181),
(7, 6, 11, 1, 6219500, 6219500),
(8, 6, 11, 1, 600000, 600000),
(9, 6, 11, 1, 165000, 165000),
(10, 7, 11, 1, 170000, 170000),
(11, 8, 11, 1, 500000, 500000),
(12, 9, 11, 1, 1000000, 1000000),
(13, 10, 11, 1, 300000, 300000),
(14, 10, 11, 1, 85000, 85000),
(15, 10, 11, 1, 77500, 77500),
(16, 10, 11, 1, 953500, 953500),
(17, 11, 11, 1, 800000, 800000),
(18, 12, 11, 1, 500000, 500000),
(19, 13, 11, 1, 4000000, 4000000),
(20, 14, 11, 1, 1404000, 1404000),
(21, 15, 11, 1, 1300000, 1300000),
(24, 77, 11, 1, 159000, 159000),
(25, 78, 11, 1, 90000, 90000),
(26, 79, 11, 1, 606050, 606050),
(27, 80, 11, 1, 1000000, 1000000),
(30, 83, 11, 1, 200000, 200000),
(33, 84, 11, 1, 2500000, 2500000),
(34, 85, 11, 1, 2416633, 2416633),
(36, 86, 11, 1, 250000, 250000),
(39, 88, 11, 1, 300000, 300000),
(40, 89, 11, 1, 900000, 900000),
(41, 89, 11, 1, 2300000, 2300000),
(42, 89, 11, 1, 450000, 450000),
(43, 90, 11, 1, 18360000, 18360000),
(44, 91, 11, 1, 2000000, 2000000),
(45, 92, 11, 1, 25000000, 25000000),
(46, 93, 11, 1, 35000000, 35000000),
(47, 94, 11, 1, 520000, 520000),
(48, 94, 11, 1, 50000, 50000),
(49, 94, 11, 1, 900000, 900000),
(50, 94, 11, 1, 260000, 260000),
(51, 95, 11, 1, 5358423, 5358423),
(52, 96, 11, 1, 750000, 750000),
(53, 97, 11, 1, 750000, 750000),
(54, 98, 11, 1, 100000, 100000),
(55, 99, 11, 1, 127000, 127000),
(56, 99, 11, 1, 523000, 523000),
(57, 100, 11, 1, 50000, 50000),
(58, 101, 11, 1, 4197700, 4197700),
(59, 102, 11, 1, 1119000, 1119000),
(61, 104, 11, 1, 300000, 300000),
(62, 105, 11, 1, 3560000, 3560000),
(63, 106, 11, 1, 121000, 121000),
(64, 107, 11, 1, 3000000, 3000000),
(65, 108, 11, 1, 1500000, 1500000),
(66, 109, 11, 1, 16000, 16000),
(67, 110, 11, 1, 100000, 100000),
(68, 111, 11, 1, 4200000, 4200000),
(69, 112, 11, 1, 150000, 150000),
(71, 114, 11, 1, 56000, 56000),
(72, 115, 11, 1, 500000, 500000),
(73, 116, 11, 1, 3550000, 3550000),
(74, 117, 11, 1, 2250000, 2250000),
(75, 118, 11, 1, 600000, 600000),
(102, 87, 11, 1, 200000, 200000),
(103, 87, 11, 1, 800000, 800000),
(104, 122, 85, 1, 300000, 300000),
(105, 123, 95, 1, 101500, 101500),
(106, 124, 46, 1, 260000, 260000),
(107, 124, 46, 1, 50000, 50000),
(108, 124, 46, 1, 200000, 200000),
(109, 124, 57, 1, 120000, 120000),
(110, 124, 88, 1, 800000, 800000),
(111, 124, 46, 1, 130000, 130000),
(112, 125, 46, 1, 260000, 260000),
(113, 125, 88, 1, 800000, 800000),
(114, 125, 46, 1, 200000, 200000),
(115, 125, 57, 1, 120000, 120000),
(116, 125, 46, 1, 130000, 130000),
(117, 126, 95, 1, 60000, 60000),
(118, 126, 46, 1, 151200, 151200),
(119, 127, 21, 1, 150000, 150000),
(138, 130, 98, 1, 2345000, 2345000),
(139, 131, 98, 1, 1365000, 1365000),
(140, 132, 98, 1, 14300000, 14300000),
(141, 133, 98, 1, 4800000, 4800000),
(142, 134, 98, 1, 6000000, 6000000),
(143, 135, 98, 1, 1860000, 1860000),
(144, 136, 98, 1, 1710000, 1710000),
(145, 137, 96, 6984500, 1, 6984500),
(147, 138, 48, 1, 800000, 800000),
(148, 139, 98, 1, 4330000, 4330000),
(149, 140, 50, 37822181, 1, 37822181),
(150, 141, 48, 1, 3000000, 3000000),
(152, 152, 98, 1, 6645000, 6645000),
(154, 153, 47, 1, 1500000, 1500000),
(155, 154, 85, 1300000, 1, 1300000),
(156, 155, 87, 159, 1, 159),
(157, 156, 98, 1, 799500, 799500),
(158, 142, 47, 1, 3900000, 3900000),
(159, 129, 98, 1, 2265000, 2265000),
(160, 157, 98, 1, 500000, 500000),
(161, 158, 98, 1, 2000000, 2000000),
(162, 159, 98, 1, 4470000, 4470000),
(163, 160, 98, 1, 1715000, 1715000),
(164, 161, 98, 1, 500000, 500000),
(165, 162, 98, 1, 1350000, 1350000),
(166, 163, 98, 1, 1425000, 1425000),
(167, 164, 98, 1, 2525000, 2525000),
(171, 168, 98, 1, 4200000, 4200000),
(172, 169, 98, 1, 8940000, 8940000),
(173, 170, 98, 1, 1130000, 1130000),
(174, 171, 47, 1, 1000000, 1000000),
(175, 172, 47, 1, 1900000, 1900000),
(176, 173, 47, 1, 325000, 325000),
(177, 174, 98, 1, 1774450, 1774450),
(179, 175, 50, 1, 350000, 350000),
(180, 167, 88, 1, 220000, 220000),
(181, 166, 47, 1, 700000, 700000),
(183, 165, 50, 1, 41487740, 41487740),
(185, 176, 53, 1, 8884140, 8884140),
(186, 177, 82, 1, 1000000, 1000000),
(187, 177, 50, 1, 150000, 150000),
(188, 178, 85, 1, 50000, 50000),
(189, 179, 85, 1, 100000, 100000),
(190, 180, 96, 1, 415000, 415000),
(191, 181, 47, 1, 1400000, 1400000),
(192, 182, 84, 1, 50000, 50000),
(193, 183, 47, 1, 250000, 250000),
(194, 184, 98, 1, 1450000, 1450000),
(196, 186, 47, 1, 600000, 600000),
(197, 187, 98, 1, 1146000, 1146000),
(198, 188, 85, 1, 100000, 100000),
(199, 189, 85, 1, 285000, 285000),
(200, 190, 64, 1, 1200000, 1200000),
(201, 185, 65, 1, 613000, 613000),
(202, 191, 53, 1, 440980, 440980),
(203, 192, 53, 1, 1946373, 1946373),
(204, 193, 98, 1, 214500, 214500),
(205, 194, 98, 1, 138000, 138000),
(206, 195, 98, 1, 630000, 630000),
(207, 196, 98, 1, 328000, 328000),
(208, 197, 98, 1, 1000000, 1000000),
(209, 198, 98, 1, 168000, 168000),
(210, 199, 98, 1, 2000000, 2000000),
(211, 200, 20, 1, 1200000, 1200000),
(212, 201, 98, 1, 1490000, 1490000),
(214, 202, 98, 1, 2543000, 2543000),
(215, 203, 47, 1, 1150000, 1150000),
(216, 204, 47, 1, 2850000, 2850000),
(217, 205, 98, 1, 750000, 750000),
(218, 206, 98, 1, 2500000, 2500000),
(219, 207, 98, 1, 500000, 500000),
(220, 208, 98, 1, 8000000, 8000000),
(221, 209, 47, 1, 3200000, 3200000),
(222, 210, 98, 1, 2000000, 2000000),
(223, 211, 98, 1, 9500000, 9500000),
(224, 212, 98, 1, 500000, 500000),
(225, 213, 97, 1, 1416000, 1416000),
(226, 214, 98, 1, 1000000, 1000000),
(227, 215, 83, 1, 250000, 250000),
(228, 216, 98, 1, 800000, 800000),
(229, 217, 98, 1, 500000, 500000),
(230, 218, 98, 1, 4000000, 4000000),
(231, 219, 98, 1, 1404000, 1404000),
(232, 220, 98, 1, 1000000, 1000000),
(233, 221, 98, 1, 20050000, 20050000),
(234, 222, 98, 1, 2220000, 2220000),
(235, 223, 47, 1, 5200000, 5200000),
(236, 224, 98, 1, 200000, 200000),
(237, 225, 50, 1, 1000000, 1000000),
(238, 225, 50, 1, 250000, 250000),
(239, 228, 47, 1, 4700000, 4700000),
(240, 229, 98, 1, 1100000, 1100000),
(241, 230, 98, 1, 250000, 250000),
(242, 231, 98, 1, 16000000, 16000000),
(246, 233, 47, 1, 1250000, 1250000),
(247, 234, 85, 1, 200000, 200000),
(248, 235, 98, 1, 20000000, 20000000),
(249, 236, 47, 1, 3700000, 3700000),
(250, 237, 83, 1, 750000, 750000),
(251, 238, 98, 1, 17190000, 17190000),
(252, 239, 98, 1, 5000000, 5000000),
(253, 240, 47, 1, 8000000, 8000000),
(254, 241, 98, 1, 3000000, 3000000),
(255, 242, 98, 1, 2000000, 2000000),
(256, 243, 47, 1, 4428000, 4428000),
(257, 244, 47, 1, 350000, 350000),
(258, 245, 87, 1, 100000, 100000),
(260, 247, 47, 1, 750000, 750000),
(261, 248, 95, 1, 350000, 350000),
(262, 249, 98, 1, 1345000, 1345000),
(263, 250, 98, 1, 1770000, 1770000),
(264, 253, 2, 1, 20000000, 20000000),
(265, 254, 65, 1, 350000, 350000),
(266, 255, 98, 1, 2150500, 2150500),
(267, 256, 47, 1, 4482000, 4482000),
(268, 257, 98, 1, 1875500, 1875500),
(269, 258, 83, 1, 100000, 100000),
(270, 259, 47, 1, 1550000, 1550000),
(271, 261, 98, 1, 5289000, 5289000),
(272, 262, 98, 1, 1300000, 1300000),
(273, 263, 64, 1, 2750000, 2750000),
(274, 264, 98, 1, 3000000, 3000000),
(275, 265, 98, 1, 3272500, 3272500),
(276, 266, 98, 1, 462000, 462000),
(277, 267, 2, 1, 20000000, 20000000),
(278, 268, 84, 1, 498000, 498000),
(279, 269, 98, 1, 17267000, 17267000),
(280, 246, 104, 1, 16800, 16800),
(281, 232, 53, 1, 2944575, 2944575),
(282, 232, 96, 1, 568923, 568923),
(283, 232, 104, 1, 6265351, 6265351),
(284, 270, 95, 1, 51500, 51500),
(285, 271, 64, 1, 1200000, 1200000),
(286, 272, 83, 1, 1500000, 1500000),
(287, 273, 85, 1, 500000, 500000),
(288, 274, 64, 1, 1300000, 1300000),
(289, 275, 21, 1, 5999000, 5999000),
(290, 276, 62, 1, 60000, 60000),
(291, 277, 50, 1, 39766850, 39766850),
(292, 278, 50, 1, 250000, 250000),
(293, 279, 105, 1, 27134803, 27134803),
(295, 280, 53, 1, 250000, 250000),
(296, 281, 85, 1, 79000, 79000),
(298, 282, 84, 1, 1100000, 1100000),
(299, 283, 50, 1, 39861076, 39861076),
(300, 284, 98, 1, 2200000, 2200000),
(301, 285, 47, 1, 4626000, 4626000),
(302, 286, 98, 1, 2250000, 2250000),
(304, 287, 50, 1, 250000, 250000),
(308, 290, 57, 1, 100000, 100000),
(309, 291, 98, 1, 1150000, 1150000),
(310, 292, 47, 1, 4375000, 4375000),
(311, 293, 12, 1, 3715500, 3715500),
(312, 294, 12, 1, 8080000, 8080000),
(313, 295, 12, 1, 4190000, 4190000),
(315, 296, 12, 1, 210000, 210000),
(316, 297, 12, 1, 10860000, 10860000),
(317, 298, 12, 1, 7901000, 7901000),
(318, 299, 12, 1, 2030000, 2030000),
(319, 300, 12, 1, 250000, 250000),
(320, 301, 12, 1, 4780000, 4780000),
(322, 302, 12, 1, 5977500, 5977500),
(323, 303, 12, 1, 5311500, 5311500),
(324, 304, 12, 1, 2000000, 2000000),
(325, 305, 12, 1, 4575000, 4575000),
(326, 306, 12, 1, 8442500, 8442500),
(327, 307, 12, 1, 1500000, 1500000),
(328, 308, 12, 1, 8310000, 8310000),
(329, 309, 12, 1, 6764500, 6764500),
(330, 310, 12, 1, 2000000, 2000000),
(331, 311, 12, 1, 3245000, 3245000),
(332, 312, 12, 1, 20000000, 20000000),
(333, 313, 12, 1, 2000000, 2000000),
(334, 314, 12, 1, 2700000, 2700000),
(335, 315, 12, 1, 9300000, 9300000),
(336, 316, 12, 1, 9914000, 9914000),
(337, 317, 12, 1, 5266500, 5266500),
(338, 318, 12, 1, 1850000, 1850000),
(339, 319, 12, 1, 5167500, 5167500),
(340, 320, 12, 1, 9244000, 9244000),
(341, 321, 12, 1, 275000, 275000),
(342, 322, 12, 1, 2066000, 2066000),
(344, 323, 12, 1, 2590000, 2590000),
(345, 324, 12, 1, 2000000, 2000000),
(346, 325, 12, 1, 3022000, 3022000),
(347, 326, 12, 1, 1690000, 1690000),
(348, 327, 12, 1, 4040000, 4040000),
(349, 328, 12, 1, 2899000, 2899000),
(350, 329, 12, 1, 1306000, 1306000),
(351, 330, 12, 1, 2000000, 2000000),
(352, 331, 12, 1, 1000000, 1000000),
(353, 332, 12, 1, 20000000, 20000000),
(354, 333, 12, 1, 4975000, 4975000),
(355, 334, 12, 1, 1956500, 1956500),
(356, 335, 95, 1, 75000, 75000),
(357, 335, 97, 1, 150000, 150000),
(358, 336, 46, 1, 1500000, 1500000),
(359, 337, 46, 1, 1500000, 1500000),
(360, 338, 46, 1, 1120000, 1120000),
(361, 339, 46, 1, 1500000, 1500000),
(362, 340, 46, 1, 1500000, 1500000),
(363, 341, 46, 1, 1500000, 1500000),
(364, 342, 46, 1, 1500000, 1500000),
(365, 343, 46, 1, 1500000, 1500000),
(366, 344, 46, 1, 1500000, 1500000),
(367, 345, 97, 1, 200000, 200000),
(368, 346, 46, 1, 780000, 780000),
(369, 346, 95, 1, 100000, 100000),
(370, 346, 88, 1, 1300000, 1300000),
(371, 346, 46, 1, 390000, 390000),
(372, 347, 46, 1, 180000, 180000),
(373, 347, 95, 1, 30000, 30000),
(374, 347, 88, 1, 450000, 450000),
(375, 347, 46, 1, 90000, 90000),
(376, 348, 46, 1, 300000, 300000),
(377, 348, 95, 1, 50000, 50000),
(378, 348, 88, 1, 850000, 850000),
(379, 348, 46, 1, 150000, 150000),
(380, 349, 46, 1, 1500000, 1500000),
(381, 350, 46, 1, 1500000, 1500000),
(382, 351, 88, 1, 300000, 300000),
(383, 352, 12, 1, 2525000, 2525000),
(384, 353, 12, 1, 5145000, 5145000),
(385, 354, 12, 1, 4756000, 4756000),
(386, 355, 12, 1, 20000000, 20000000),
(387, 356, 12, 1, 2085000, 2085000),
(425, 372, 85, 1, 201000, 201000),
(431, 374, 46, 1, 780000, 780000),
(432, 374, 95, 1, 100000, 100000),
(433, 374, 46, 1, 1600000, 1600000),
(434, 374, 88, 1, 2300000, 2300000),
(435, 374, 46, 1, 390000, 390000),
(436, 374, 46, 1, 780000, 780000),
(437, 375, 46, 1, 780000, 780000),
(438, 375, 95, 1, 100000, 100000),
(439, 375, 46, 1, 1600000, 1600000),
(440, 375, 88, 1, 2300000, 2300000),
(441, 375, 46, 1, 390000, 390000),
(442, 375, 46, 1, 780000, 780000),
(443, 373, 46, 1, 780000, 780000),
(444, 373, 46, 1, 250000, 250000),
(445, 373, 95, 1, 100000, 100000),
(446, 373, 88, 1, 1800000, 1800000),
(447, 373, 46, 1, 390000, 390000),
(448, 376, 46, 1, 210000, 210000),
(449, 376, 95, 1, 30000, 30000),
(450, 376, 88, 1, 600000, 600000),
(451, 376, 46, 1, 105000, 105000),
(452, 377, 46, 1, 780000, 780000),
(453, 377, 95, 1, 100000, 100000),
(454, 377, 46, 1, 1600000, 1600000),
(455, 377, 88, 1, 2300000, 2300000),
(456, 377, 46, 1, 390000, 390000),
(457, 377, 46, 1, 780000, 780000),
(458, 378, 46, 1, 300000, 300000),
(459, 378, 95, 1, 50000, 50000),
(460, 378, 88, 1, 1300000, 1300000),
(461, 378, 46, 1, 150000, 150000),
(462, 379, 61, 1, 150000, 150000),
(463, 380, 61, 1, 3182901, 3182901),
(464, 381, 104, 1, 713111, 713111),
(465, 382, 43, 1, 1000000, 1000000),
(466, 383, 43, 1, 2100000, 2100000),
(468, 384, 43, 1, 7214000, 7214000),
(469, 385, 43, 1, 14145000, 14145000),
(470, 386, 43, 1, 398250, 398250),
(471, 387, 43, 1, 500000, 500000),
(472, 388, 43, 1, 917500, 917500),
(473, 389, 43, 1, 600000, 600000),
(474, 390, 43, 1, 1014200, 1014200),
(475, 391, 43, 1, 170000, 170000),
(476, 392, 43, 1, 1690000, 1690000),
(477, 393, 43, 1, 5470000, 5470000),
(478, 394, 43, 1, 725000, 725000),
(479, 395, 43, 1, 1655000, 1655000),
(481, 397, 43, 1, 1660000, 1660000),
(482, 396, 43, 1, 1660000, 1660000),
(483, 398, 43, 1, 113800, 113800),
(484, 399, 43, 1, 313000, 313000),
(485, 400, 43, 1, 313000, 313000),
(486, 401, 43, 1, 5975000, 5975000),
(487, 402, 43, 1, 4092000, 4092000),
(488, 403, 43, 1, 289500, 289500),
(489, 404, 43, 1, 3657500, 3657500),
(490, 405, 43, 1, 1800000, 1800000),
(491, 406, 43, 1, 560000, 560000),
(492, 407, 43, 1, 472500, 472500),
(495, 113, 11, 1, 50000, 50000),
(496, 288, 104, 1, 11464603, 11464603),
(497, 288, 96, 1, 1040500, 1040500),
(498, 289, 104, 1, 6282151, 6282151),
(500, 409, 4, 1, 20000000, 20000000),
(503, 412, 4, 1, 1300000, 1300000),
(507, 413, 2, 1, 1800000, 1800000),
(508, 413, 4, 1, 490000, 490000),
(509, 414, 4, 1, 1000000, 1000000),
(510, 414, 4, 1, 1000000, 1000000),
(511, 414, 4, 1, 1000000, 1000000),
(512, 415, 5, 1, 3651273, 3651273),
(513, 415, 5, 1, 9086023, 9086023),
(514, 415, 5, 1, 259127, 259127),
(515, 416, 4, 1, 6200000, 6200000),
(516, 417, 4, 1, 175000, 175000),
(517, 417, 4, 1, 50000, 50000),
(518, 417, 4, 1, 250000, 250000),
(519, 417, 4, 1, 100000, 100000),
(520, 410, 4, 1, 650000, 650000),
(521, 410, 4, 1, 100000, 100000),
(522, 410, 4, 1, 1800000, 1800000),
(523, 410, 4, 1, 325000, 325000),
(524, 411, 4, 1, 175000, 175000),
(525, 411, 4, 1, 50000, 50000),
(526, 411, 4, 1, 250000, 250000),
(527, 411, 4, 1, 100000, 100000),
(528, 418, 2, 1, 528600, 528600),
(529, 419, 4, 1, 1378000, 1378000),
(530, 420, 4, 1, 1575000, 1575000),
(531, 421, 4, 1, 780000, 780000),
(532, 421, 4, 1, 101500, 101500),
(533, 421, 4, 1, 2000000, 2000000),
(534, 421, 4, 1, 390000, 390000),
(535, 422, 4, 1, 780000, 780000),
(536, 422, 4, 1, 150000, 150000),
(537, 422, 4, 1, 101500, 101500),
(538, 422, 4, 1, 1800000, 1800000),
(539, 422, 4, 1, 390000, 390000),
(540, 423, 4, 1, 450000, 450000),
(541, 423, 4, 1, 76500, 76500),
(542, 423, 4, 1, 1200000, 1200000),
(543, 423, 4, 1, 225000, 225000),
(544, 424, 2, 1, 145000, 145000),
(545, 424, 2, 1, 169000, 169000),
(546, 424, 2, 1, 640000, 640000),
(547, 424, 2, 1, 2000000, 2000000),
(548, 424, 2, 1, 2300000, 2300000),
(549, 424, 2, 1, 965000, 965000),
(550, 425, 2, 1, 20000, 20000),
(551, 426, 2, 1, 882000, 882000),
(552, 427, 4, 1, 4200000, 4200000),
(553, 428, 4, 1, 120000, 120000),
(554, 428, 4, 1, 750000, 750000);

-- --------------------------------------------------------

--
-- Table structure for table `t_status`
--

CREATE TABLE `t_status` (
  `id_status` int(11) NOT NULL,
  `status_code` varchar(25) NOT NULL,
  `status_name` varchar(50) NOT NULL,
  `status_type` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_status`
--

INSERT INTO `t_status` (`id_status`, `status_code`, `status_name`, `status_type`) VALUES
(1, 'PO01', 'Pengajuan Manager', 'pengajuan'),
(2, 'PO02', 'Pengajuan Manager Keuangan', 'pengajuan'),
(3, 'PO03', 'Disetujui', 'pengajuan'),
(4, 'PO04', 'Ditolak', 'pengajuan'),
(5, 'KO01', 'Tercairkan', 'kasir');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `email` varchar(255) NOT NULL,
  `username` varchar(30) DEFAULT NULL,
  `fullname` varchar(255) DEFAULT NULL,
  `user_img` varchar(255) NOT NULL DEFAULT 'default.svg',
  `password_hash` varchar(255) NOT NULL,
  `reset_hash` varchar(255) DEFAULT NULL,
  `reset_at` datetime DEFAULT NULL,
  `reset_expires` datetime DEFAULT NULL,
  `activate_hash` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `status_message` varchar(255) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 0,
  `force_pass_reset` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `id_divisi` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `username`, `fullname`, `user_img`, `password_hash`, `reset_hash`, `reset_at`, `reset_expires`, `activate_hash`, `status`, `status_message`, `active`, `force_pass_reset`, `created_at`, `updated_at`, `deleted_at`, `id_divisi`) VALUES
(30, 'kasir@gmail.com', 'kasir', 'kasir', 'default.svg', '$2y$10$tbml3PEFVMGbaSeKhmGYB.0sS/bkmtYEFZ7/RFygvKC6UxHZ6iAYO', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-12-20 19:38:08', '2020-12-20 19:38:08', NULL, 0),
(33, 'enginering@mail.com', 'enginering', 'Engineering', 'default.svg', '$2y$10$MrWV6iCc43Fp1/CUts9SROZ8otPLfN2XRZsNhkmgtAAd4TfRiGKbK', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2021-01-20 19:55:23', '2021-01-20 19:55:23', NULL, 2),
(34, 'robi.andri.oktafianto@gmail.com', 'robiokta', 'Robi Andri ', 'default.svg', '$2y$10$wbJjEI.8En4ZJW2AerAzhedxPG5/AJ/DzIvMcK5d2v17nddppAAei', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2021-01-24 21:32:07', '2021-01-24 21:32:07', NULL, 0),
(35, 'trans@gmail.com', 'devtrans', 'UMJ Trans', 'default.svg', '$2y$10$FH.xv.NFP7gqLgARLtdyze/ixrqOQYRWZ0KQsP7XjEg0bISjJRB/S', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2021-01-24 21:33:37', '2021-01-24 21:33:37', NULL, 1),
(36, 'offset@gmail.com', 'offset', 'Divisi Offset', 'default.svg', '$2y$10$4BwzpJWEFDs6DrZKO3aR9ehMhhiqafD6UI1MuBav/z4jGmukCaGeO', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2021-01-24 21:35:43', '2021-01-24 21:35:43', NULL, 3),
(37, 'manager@gmail.com', 'manager', 'Manager', 'default.svg', '$2y$10$xjmtvQnzK.VTKMo7UqA/uOQWWxXLevnQ.jSiceHHZZ6TmvniH7x..', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2021-01-24 21:36:37', '2021-01-24 21:36:37', NULL, 0),
(38, 'menku@gmail.com', 'menku', NULL, 'default.svg', '$2y$10$4p83U70OR/Uom6KJuqjpr.Mjpqhqf6f6aJ6f154piqJSY4Ag0q1fi', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2021-01-24 21:37:15', '2021-01-24 21:37:15', NULL, 0),
(43, 'managertrans@gmail.com', 'managertrans', 'Manager Trans UMJ', 'default.svg', '$2y$10$Hlp2AzwcBNdJjoYdC23Lbuhk5qt3gaKCaDS6BdkFXZCpItuYWhOhO', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2021-02-02 19:43:56', '2021-02-02 19:43:56', NULL, 1),
(44, 'manageroffset@gmail.com', 'manageroffset', 'Manager Offset', 'default.svg', '$2y$10$0t9pb.nbM6uayGdmELSAnOSZ85WGG/xxrH.gUK22QRg/61TUBofE6', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2021-02-02 19:45:25', '2021-02-02 19:45:25', NULL, 3),
(45, 'dir@gmail.com', 'admdir', 'admin direktur', 'default.svg', '$2y$10$pSPaXuhMxULn/QWKdH2.buy6mzZ2whuHDoMD4U3ZIYbzZT0Z8GrVy', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2021-02-03 01:25:36', '2021-02-03 01:25:36', NULL, 0),
(46, 'admgfa@gmail.com', 'admgfa', 'Admin GFA', 'default.svg', '$2y$10$Fef.cRfZdHqQzVV20U166usFdXgZtWSKhIiztGjFr1ECNVFj8rjKq', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2021-02-03 02:27:51', '2021-02-03 02:27:51', NULL, 5),
(47, 'managerfga@gmail.com', 'managerfga', 'Manager FGA', 'default.svg', '$2y$10$V.9VwvChj7U/bfIUVxMbAOXvuAI6tDbXVMQ2xNTDDg1hLMZLlVlB6', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2021-02-04 01:16:33', '2021-02-04 01:16:33', NULL, 5),
(48, 'managerengineering@gmail.com', 'managerengineering', 'Manager Engineering', 'default.svg', '$2y$10$W03eFHfVivBbghX2PEK1re4jmd2oS8DJ/yvwcGglKvth6s1gG9Lba', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2021-02-04 01:45:30', '2021-02-04 01:45:30', NULL, 2),
(49, 'p3lm@gmail.com', 'p3lm', 'P3LM', 'default.svg', '$2y$10$aN496kPRD5CNoMv.Xxbbg.sAwmmfgFqxzt9zY6WPpzmJKZvCUqO1a', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2021-04-07 20:39:32', '2021-04-07 20:39:32', NULL, 4),
(51, 'managerp3lm@gmail.com', 'manp3lm', 'Manager P3LM', 'default.svg', '$2y$10$2.APknDmIpVPX8afj3MmfewktffFGWcgQkhjQ7egaS6d7a9pY3liy', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2021-04-07 20:40:56', '2021-04-07 20:40:56', NULL, 4),
(52, 'diyan.diana60@gmail.com', 'diyan_diana', 'Diah Diana Putri', 'default.svg', '$2y$10$m3Vj1YAR6N4FSdvBP.ZJ1.l3KsgxXm35A6jRKd4OoCVaHOaLzI2YC', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2021-07-23 01:08:29', '2021-07-23 01:08:29', NULL, 0),
(53, 'adminyasmin@gmial.com', 'admyasmin', 'Admin Paud Yasmine', 'default.svg', '$2y$10$CExWWV29vrMQLu7o309xIe/YPs1C6fqZQKkXUjLZQ3Fa.teXyTV72', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2022-01-03 02:13:35', '2022-01-03 02:13:35', NULL, 6),
(54, 'adminmkg@gmail.com', 'adminmkg', 'ADMIN MKG', 'default.svg', '$2y$10$xZ7DqkBRnanInR0MxGrEvuTGGpPiQGD3.G31iIE1BjwsP/mjngxAi', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2022-01-04 22:21:02', '2022-01-04 22:21:02', NULL, 0);

-- --------------------------------------------------------

--
-- Structure for view `coa`
--
DROP TABLE IF EXISTS `coa`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `coa`  AS SELECT `m_coa`.`kode_coa` AS `kode_coa_1`, `m_coa`.`m_coa_id` AS `m_coa_id`, `m_coa_2`.`m_coa_2_id` AS `coa_2_id`, `m_coa_2`.`kode_coa_2` AS `kode_coa_2`, `m_coa_3`.`m_coa_3_id` AS `coa_3_id`, `m_coa_3`.`kode_coa_3` AS `kode_coa_3`, `m_coa_4`.`m_coa_4_id` AS `m_coa_4_id`, `m_coa_4`.`kode_coa_4` AS `kode_coa_4`, `m_coa_4`.`nama_coa` AS `namacoa` FROM (((`m_coa_4` join `m_coa_2` on(`m_coa_2`.`m_coa_2_id` = `m_coa_4`.`m_coa_2_id`)) join `m_coa` on(`m_coa`.`m_coa_id` = `m_coa_4`.`m_coa_1_id`)) join `m_coa_3` on(`m_coa_3`.`m_coa_3_id` = `m_coa_4`.`m_coa_3_id`)) WHERE `m_coa_4`.`status` = 'active' AND `m_coa_3`.`status` = 'active' AND `m_coa_2`.`status` = 'active' AND `m_coa`.`status` = 'active' ;

-- --------------------------------------------------------

--
-- Structure for view `coa2`
--
DROP TABLE IF EXISTS `coa2`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `coa2`  AS SELECT `m_coa_2`.`m_coa_2_id` AS `m_coa_2_id`, `m_coa_2`.`kode_coa_2` AS `kode_coa_2`, `m_coa`.`kode_coa` AS `kode_coa_1`, `m_coa_2`.`nama_coa` AS `namacoa` FROM (`m_coa_2` join `m_coa` on(`m_coa`.`m_coa_id` = `m_coa_2`.`m_coa_id`)) WHERE `m_coa_2`.`status` = 'active' AND `m_coa`.`status` = 'active' ;

-- --------------------------------------------------------

--
-- Structure for view `coa3`
--
DROP TABLE IF EXISTS `coa3`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `coa3`  AS SELECT `m_coa_3`.`m_coa_3_id` AS `coa_3_id`, `m_coa`.`kode_coa` AS `kode_coa_1`, `m_coa_2`.`kode_coa_2` AS `kode_coa_2`, `m_coa_3`.`kode_coa_3` AS `kode_coa_3`, `m_coa_3`.`nama_coa` AS `namacoa` FROM ((`m_coa_3` join `m_coa_2` on(`m_coa_2`.`m_coa_2_id` = `m_coa_3`.`m_coa_2_id`)) join `m_coa` on(`m_coa`.`m_coa_id` = `m_coa_3`.`id_coa_1`)) WHERE `m_coa_3`.`status` = 'active' AND `m_coa_2`.`status` = 'active' AND `m_coa`.`status` = 'active' ORDER BY `m_coa_3`.`m_coa_3_id` ASC ;

-- --------------------------------------------------------

--
-- Structure for view `detail_bukubesar`
--
DROP TABLE IF EXISTS `detail_bukubesar`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `detail_bukubesar`  AS SELECT `coa`.`kode_coa_1` AS `kode_coa_1`, `coa`.`kode_coa_2` AS `kode_coa_2`, `coa`.`kode_coa_3` AS `kode_coa_3`, `coa`.`kode_coa_4` AS `kode_coa_4`, `coa`.`namacoa` AS `namacoa`, `t_jurnalumum`.`Tanggal` AS `Tanggal`, `t_jurnalumum`.`Nilai` AS `Nilai`, `t_jurnalumum`.`IdUser` AS `IdUser`, 'Debit' AS `type`, `b`.`description` AS `description`, `t_jurnalumum`.`IdUnit` AS `IdUnit` FROM (((`t_jurnalumum` join `coa` on(`coa`.`m_coa_4_id` = `t_jurnalumum`.`IdDebet`)) join `auth_groups_users` `a` on(`a`.`user_id` = `t_jurnalumum`.`IdUser`)) join `auth_groups` `b` on(`b`.`id` = `t_jurnalumum`.`IdUnit`)) ;

-- --------------------------------------------------------

--
-- Structure for view `neraca`
--
DROP TABLE IF EXISTS `neraca`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `neraca`  AS SELECT `test1`.`IdDebet` AS `ID`, date_format(`test1`.`Tanggal`,'%Y-%m') AS `Tanggal`, ifnull(`test1`.`Nilai_debet`,0) AS `Nilai_debet`, ifnull(`testb`.`Nilai_Kredit`,0) AS `Nilai_kredit`, ifnull(`test1`.`Nilai_debet`,0) - ifnull(`testb`.`Nilai_Kredit`,0) AS `saldo` FROM (((select `t_jurnalumum`.`Tanggal` AS `Tanggal`,sum(`t_jurnalumum`.`Nilai`) AS `Nilai_debet`,`t_jurnalumum`.`IdDebet` AS `IdDebet`,`t_jurnalumum`.`IdKredit` AS `IdKredit` from `t_jurnalumum` group by `t_jurnalumum`.`IdDebet`,date_format(`t_jurnalumum`.`Tanggal`,'%Y-%m'))) `test1` left join (select `t_jurnalumum`.`Tanggal` AS `Tanggal`,sum(`t_jurnalumum`.`Nilai`) AS `Nilai_Kredit`,`t_jurnalumum`.`IdDebet` AS `IdDebet`,`t_jurnalumum`.`IdKredit` AS `IdKredit` from `t_jurnalumum` group by `t_jurnalumum`.`IdKredit`,date_format(`t_jurnalumum`.`Tanggal`,'%Y-%m')) `testb` on(`test1`.`IdDebet` = `testb`.`IdKredit`)) ;

-- --------------------------------------------------------

--
-- Structure for view `neraca_3`
--
DROP TABLE IF EXISTS `neraca_3`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `neraca_3`  AS SELECT `coa`.`kode_coa_1` AS `kode_coa_1`, `coa`.`kode_coa_2` AS `kode_coa_2`, `coa`.`kode_coa_3` AS `kode_coa_3`, `coa`.`kode_coa_4` AS `kode_coa_4`, `test1`.`IdDebet` AS `ID`, date_format(`test1`.`Tanggal`,'%Y-%m') AS `Tanggal`, ifnull(`test1`.`Nilai_debet`,0) AS `Nilai_debet`, ifnull(`testb`.`Nilai_Kredit`,0) AS `Nilai_kredit`, ifnull(`test1`.`Nilai_debet`,0) - ifnull(`testb`.`Nilai_Kredit`,0) AS `saldo` FROM ((((select `t_jurnalumum`.`Tanggal` AS `Tanggal`,sum(`t_jurnalumum`.`Nilai`) AS `Nilai_debet`,`t_jurnalumum`.`IdDebet` AS `IdDebet`,`t_jurnalumum`.`IdKredit` AS `IdKredit` from `t_jurnalumum` group by `t_jurnalumum`.`IdDebet`,date_format(`t_jurnalumum`.`Tanggal`,'%Y-%m'))) `test1` left join (select `t_jurnalumum`.`Tanggal` AS `Tanggal`,sum(`t_jurnalumum`.`Nilai`) AS `Nilai_Kredit`,`t_jurnalumum`.`IdDebet` AS `IdDebet`,`t_jurnalumum`.`IdKredit` AS `IdKredit` from `t_jurnalumum` group by `t_jurnalumum`.`IdKredit`,date_format(`t_jurnalumum`.`Tanggal`,'%Y-%m')) `testb` on(`test1`.`IdDebet` = `testb`.`IdKredit`)) left join `coa` on(`coa`.`m_coa_4_id` = `test1`.`IdDebet`)) ;

-- --------------------------------------------------------

--
-- Structure for view `neraca_lajur_x`
--
DROP TABLE IF EXISTS `neraca_lajur_x`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `neraca_lajur_x`  AS SELECT `coa`.`kode_coa_1` AS `kode_coa_1`, `coa`.`kode_coa_2` AS `kode_coa_2`, `coa`.`kode_coa_3` AS `kode_coa_3`, `coa`.`kode_coa_4` AS `kode_coa_4`, `test1`.`IdDebet` AS `ID`, date_format(`test1`.`Tanggal`,'%Y') AS `Tanggal`, ifnull(`test1`.`Nilai_debet`,0) AS `Nilai_debet`, ifnull(`testb`.`Nilai_Kredit`,0) AS `Nilai_kredit`, ifnull(`test1`.`Nilai_debet`,0) - ifnull(`testb`.`Nilai_Kredit`,0) AS `saldo` FROM ((((select `t_jurnalumum`.`Tanggal` AS `Tanggal`,sum(`t_jurnalumum`.`Nilai`) AS `Nilai_debet`,`t_jurnalumum`.`IdDebet` AS `IdDebet`,`t_jurnalumum`.`IdKredit` AS `IdKredit` from `t_jurnalumum` group by `t_jurnalumum`.`IdDebet`,date_format(`t_jurnalumum`.`Tanggal`,'%Y'))) `test1` left join (select `t_jurnalumum`.`Tanggal` AS `Tanggal`,sum(`t_jurnalumum`.`Nilai`) AS `Nilai_Kredit`,`t_jurnalumum`.`IdDebet` AS `IdDebet`,`t_jurnalumum`.`IdKredit` AS `IdKredit` from `t_jurnalumum` group by `t_jurnalumum`.`IdKredit`,date_format(`t_jurnalumum`.`Tanggal`,'%Y')) `testb` on(`test1`.`IdDebet` = `testb`.`IdKredit`)) left join `coa` on(`coa`.`m_coa_4_id` = `test1`.`IdDebet`)) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auth_activation_attempts`
--
ALTER TABLE `auth_activation_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth_groups`
--
ALTER TABLE `auth_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth_groups_permissions`
--
ALTER TABLE `auth_groups_permissions`
  ADD KEY `auth_groups_permissions_permission_id_foreign` (`permission_id`),
  ADD KEY `group_id_permission_id` (`group_id`,`permission_id`);

--
-- Indexes for table `auth_groups_users`
--
ALTER TABLE `auth_groups_users`
  ADD PRIMARY KEY (`id_user_gruopus`),
  ADD KEY `auth_groups_users_user_id_foreign` (`user_id`),
  ADD KEY `group_id_user_id` (`group_id`,`user_id`);

--
-- Indexes for table `auth_logins`
--
ALTER TABLE `auth_logins`
  ADD PRIMARY KEY (`id`),
  ADD KEY `email` (`email`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `auth_permissions`
--
ALTER TABLE `auth_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth_reset_attempts`
--
ALTER TABLE `auth_reset_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth_tokens`
--
ALTER TABLE `auth_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `auth_tokens_user_id_foreign` (`user_id`),
  ADD KEY `selector` (`selector`);

--
-- Indexes for table `auth_users_permissions`
--
ALTER TABLE `auth_users_permissions`
  ADD KEY `auth_users_permissions_permission_id_foreign` (`permission_id`),
  ADD KEY `user_id_permission_id` (`user_id`,`permission_id`);

--
-- Indexes for table `mak_1`
--
ALTER TABLE `mak_1`
  ADD PRIMARY KEY (`mak_1_id`);

--
-- Indexes for table `mak_2`
--
ALTER TABLE `mak_2`
  ADD PRIMARY KEY (`mak_2_id`),
  ADD KEY `mak_1_id` (`mak_1_id`);

--
-- Indexes for table `mak_3`
--
ALTER TABLE `mak_3`
  ADD PRIMARY KEY (`mak_3_id`),
  ADD KEY `mak_1_id` (`mak_1_id`),
  ADD KEY `mak_2_id` (`mak_2_id`);

--
-- Indexes for table `mak_4`
--
ALTER TABLE `mak_4`
  ADD PRIMARY KEY (`mak_4_id`),
  ADD KEY `mak_1_id` (`mak_1_id`),
  ADD KEY `mak_2_id` (`mak_2_id`),
  ADD KEY `mak_3_id` (`mak_3_id`);

--
-- Indexes for table `menu_api`
--
ALTER TABLE `menu_api`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu_role`
--
ALTER TABLE `menu_role`
  ADD PRIMARY KEY (`id_menu_role`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_coa`
--
ALTER TABLE `m_coa`
  ADD PRIMARY KEY (`m_coa_id`),
  ADD UNIQUE KEY `kode_coa` (`kode_coa`);

--
-- Indexes for table `m_coa_2`
--
ALTER TABLE `m_coa_2`
  ADD PRIMARY KEY (`m_coa_2_id`),
  ADD KEY `idd_coa_2` (`m_coa_id`);

--
-- Indexes for table `m_coa_3`
--
ALTER TABLE `m_coa_3`
  ADD PRIMARY KEY (`m_coa_3_id`),
  ADD KEY `id_coa_1` (`id_coa_1`),
  ADD KEY `id_coa_2` (`m_coa_2_id`);

--
-- Indexes for table `m_coa_4`
--
ALTER TABLE `m_coa_4`
  ADD PRIMARY KEY (`m_coa_4_id`),
  ADD KEY `m_coa_1_id` (`m_coa_1_id`),
  ADD KEY `m_coa_2_id` (`m_coa_2_id`),
  ADD KEY `m_coa_3_id` (`m_coa_3_id`);

--
-- Indexes for table `m_divisi`
--
ALTER TABLE `m_divisi`
  ADD PRIMARY KEY (`id_divisi`);

--
-- Indexes for table `m_menu`
--
ALTER TABLE `m_menu`
  ADD PRIMARY KEY (`menu_id`);

--
-- Indexes for table `m_supplier`
--
ALTER TABLE `m_supplier`
  ADD PRIMARY KEY (`id_supplier`);

--
-- Indexes for table `sttk`
--
ALTER TABLE `sttk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_jurnalumum`
--
ALTER TABLE `t_jurnalumum`
  ADD PRIMARY KEY (`IdJurnal`);

--
-- Indexes for table `t_kasir`
--
ALTER TABLE `t_kasir`
  ADD PRIMARY KEY (`id_kasir`),
  ADD KEY `id_pengajuan` (`id_pengajuan`);

--
-- Indexes for table `t_pengajuan`
--
ALTER TABLE `t_pengajuan`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `no_pengajuan` (`no_pengajuan`),
  ADD KEY `id_mak_4` (`id_mak_4`);

--
-- Indexes for table `t_realisasi`
--
ALTER TABLE `t_realisasi`
  ADD PRIMARY KEY (`id_realisasi`),
  ADD KEY `id_rinci` (`id_rinci`);

--
-- Indexes for table `t_rinci`
--
ALTER TABLE `t_rinci`
  ADD PRIMARY KEY (`id_rinci`),
  ADD KEY `id_pengajuan` (`id_pengajuan`),
  ADD KEY `coa_4_id` (`coa_4_id`);

--
-- Indexes for table `t_status`
--
ALTER TABLE `t_status`
  ADD PRIMARY KEY (`id_status`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `auth_activation_attempts`
--
ALTER TABLE `auth_activation_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_groups`
--
ALTER TABLE `auth_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `auth_groups_users`
--
ALTER TABLE `auth_groups_users`
  MODIFY `id_user_gruopus` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `auth_logins`
--
ALTER TABLE `auth_logins`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1268;

--
-- AUTO_INCREMENT for table `auth_permissions`
--
ALTER TABLE `auth_permissions`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `auth_reset_attempts`
--
ALTER TABLE `auth_reset_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_tokens`
--
ALTER TABLE `auth_tokens`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mak_1`
--
ALTER TABLE `mak_1`
  MODIFY `mak_1_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `mak_2`
--
ALTER TABLE `mak_2`
  MODIFY `mak_2_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `mak_3`
--
ALTER TABLE `mak_3`
  MODIFY `mak_3_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `mak_4`
--
ALTER TABLE `mak_4`
  MODIFY `mak_4_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `menu_api`
--
ALTER TABLE `menu_api`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `menu_role`
--
ALTER TABLE `menu_role`
  MODIFY `id_menu_role` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `m_coa`
--
ALTER TABLE `m_coa`
  MODIFY `m_coa_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `m_coa_2`
--
ALTER TABLE `m_coa_2`
  MODIFY `m_coa_2_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `m_coa_3`
--
ALTER TABLE `m_coa_3`
  MODIFY `m_coa_3_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `m_coa_4`
--
ALTER TABLE `m_coa_4`
  MODIFY `m_coa_4_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;

--
-- AUTO_INCREMENT for table `m_divisi`
--
ALTER TABLE `m_divisi`
  MODIFY `id_divisi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `m_menu`
--
ALTER TABLE `m_menu`
  MODIFY `menu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `m_supplier`
--
ALTER TABLE `m_supplier`
  MODIFY `id_supplier` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `sttk`
--
ALTER TABLE `sttk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_jurnalumum`
--
ALTER TABLE `t_jurnalumum`
  MODIFY `IdJurnal` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=433;

--
-- AUTO_INCREMENT for table `t_kasir`
--
ALTER TABLE `t_kasir`
  MODIFY `id_kasir` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=290;

--
-- AUTO_INCREMENT for table `t_pengajuan`
--
ALTER TABLE `t_pengajuan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=429;

--
-- AUTO_INCREMENT for table `t_realisasi`
--
ALTER TABLE `t_realisasi`
  MODIFY `id_realisasi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=296;

--
-- AUTO_INCREMENT for table `t_rinci`
--
ALTER TABLE `t_rinci`
  MODIFY `id_rinci` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=555;

--
-- AUTO_INCREMENT for table `t_status`
--
ALTER TABLE `t_status`
  MODIFY `id_status` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `auth_groups_permissions`
--
ALTER TABLE `auth_groups_permissions`
  ADD CONSTRAINT `auth_groups_permissions_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `auth_groups` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `auth_groups_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `auth_permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `auth_groups_users`
--
ALTER TABLE `auth_groups_users`
  ADD CONSTRAINT `auth_groups_users_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `auth_groups` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `auth_groups_users_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `auth_tokens`
--
ALTER TABLE `auth_tokens`
  ADD CONSTRAINT `auth_tokens_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `auth_users_permissions`
--
ALTER TABLE `auth_users_permissions`
  ADD CONSTRAINT `auth_users_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `auth_permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `auth_users_permissions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `mak_2`
--
ALTER TABLE `mak_2`
  ADD CONSTRAINT `mak_2_ibfk_1` FOREIGN KEY (`mak_1_id`) REFERENCES `mak_1` (`mak_1_id`);

--
-- Constraints for table `mak_3`
--
ALTER TABLE `mak_3`
  ADD CONSTRAINT `mak_3_ibfk_1` FOREIGN KEY (`mak_1_id`) REFERENCES `mak_1` (`mak_1_id`),
  ADD CONSTRAINT `mak_3_ibfk_2` FOREIGN KEY (`mak_2_id`) REFERENCES `mak_2` (`mak_2_id`);

--
-- Constraints for table `mak_4`
--
ALTER TABLE `mak_4`
  ADD CONSTRAINT `mak_4_ibfk_1` FOREIGN KEY (`mak_1_id`) REFERENCES `mak_1` (`mak_1_id`),
  ADD CONSTRAINT `mak_4_ibfk_2` FOREIGN KEY (`mak_2_id`) REFERENCES `mak_2` (`mak_2_id`),
  ADD CONSTRAINT `mak_4_ibfk_3` FOREIGN KEY (`mak_3_id`) REFERENCES `mak_3` (`mak_3_id`);

--
-- Constraints for table `m_coa_2`
--
ALTER TABLE `m_coa_2`
  ADD CONSTRAINT `idd_coa_2` FOREIGN KEY (`m_coa_id`) REFERENCES `m_coa` (`m_coa_id`);

--
-- Constraints for table `m_coa_3`
--
ALTER TABLE `m_coa_3`
  ADD CONSTRAINT `m_coa_3_ibfk_1` FOREIGN KEY (`id_coa_1`) REFERENCES `m_coa` (`m_coa_id`),
  ADD CONSTRAINT `m_coa_3_ibfk_2` FOREIGN KEY (`m_coa_2_id`) REFERENCES `m_coa_2` (`m_coa_2_id`);

--
-- Constraints for table `m_coa_4`
--
ALTER TABLE `m_coa_4`
  ADD CONSTRAINT `m_coa_4_ibfk_1` FOREIGN KEY (`m_coa_1_id`) REFERENCES `m_coa` (`m_coa_id`),
  ADD CONSTRAINT `m_coa_4_ibfk_2` FOREIGN KEY (`m_coa_2_id`) REFERENCES `m_coa_2` (`m_coa_2_id`),
  ADD CONSTRAINT `m_coa_4_ibfk_3` FOREIGN KEY (`m_coa_3_id`) REFERENCES `m_coa_3` (`m_coa_3_id`);

--
-- Constraints for table `t_kasir`
--
ALTER TABLE `t_kasir`
  ADD CONSTRAINT `id_pengajuan` FOREIGN KEY (`id_pengajuan`) REFERENCES `t_pengajuan` (`id`);

--
-- Constraints for table `t_pengajuan`
--
ALTER TABLE `t_pengajuan`
  ADD CONSTRAINT `t_pengajuan_ibfk_1` FOREIGN KEY (`id_mak_4`) REFERENCES `mak_4` (`mak_4_id`);

--
-- Constraints for table `t_realisasi`
--
ALTER TABLE `t_realisasi`
  ADD CONSTRAINT `t_realisasi_ibfk_1` FOREIGN KEY (`id_rinci`) REFERENCES `t_rinci` (`id_rinci`);

--
-- Constraints for table `t_rinci`
--
ALTER TABLE `t_rinci`
  ADD CONSTRAINT `t_rinci_ibfk_1` FOREIGN KEY (`id_pengajuan`) REFERENCES `t_pengajuan` (`id`),
  ADD CONSTRAINT `t_rinci_ibfk_2` FOREIGN KEY (`coa_4_id`) REFERENCES `m_coa_4` (`m_coa_4_id`);

DELIMITER $$
--
-- Events
--
CREATE DEFINER=`root`@`localhost` EVENT `resetmvneraca_x` ON SCHEDULE EVERY 2 MINUTE STARTS '2021-05-04 09:53:45' ON COMPLETION NOT PRESERVE ENABLE DO CALL resetmvneraca_x (@rc)$$

CREATE DEFINER=`root`@`localhost` EVENT `resetmvneraca` ON SCHEDULE EVERY 2 MINUTE STARTS '2021-05-04 09:54:31' ON COMPLETION NOT PRESERVE ENABLE DO CALL resetmvneraca (@rc)$$

DELIMITER ;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
